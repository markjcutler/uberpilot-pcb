<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.3">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="14" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="16" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="10" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="ACL">
<packages>
<package name="M10-0.4MM-SMD-RECEPTACLE">
<smd name="8" x="0" y="1.325" dx="0.23" dy="0.63" layer="1"/>
<smd name="7" x="0.4" y="1.325" dx="0.23" dy="0.63" layer="1"/>
<smd name="6" x="0.8" y="1.325" dx="0.23" dy="0.63" layer="1"/>
<smd name="9" x="-0.4" y="1.325" dx="0.23" dy="0.63" layer="1"/>
<smd name="10" x="-0.8" y="1.325" dx="0.23" dy="0.63" layer="1"/>
<smd name="3" x="0" y="-1.325" dx="0.23" dy="0.63" layer="1"/>
<smd name="4" x="0.4" y="-1.325" dx="0.23" dy="0.63" layer="1"/>
<smd name="5" x="0.8" y="-1.325" dx="0.23" dy="0.63" layer="1"/>
<smd name="2" x="-0.4" y="-1.325" dx="0.23" dy="0.63" layer="1"/>
<smd name="1" x="-0.8" y="-1.325" dx="0.23" dy="0.63" layer="1"/>
<smd name="NC_2" x="2.04" y="0" dx="0.38" dy="1.1" layer="1"/>
<smd name="NC_1" x="-2.04" y="0" dx="0.38" dy="1.1" layer="1"/>
<wire x1="-2.11" y1="1.16" x2="2.11" y2="1.16" width="0.127" layer="51"/>
<wire x1="2.11" y1="1.16" x2="2.11" y2="-1.16" width="0.127" layer="51"/>
<wire x1="2.11" y1="-1.16" x2="-2.11" y2="-1.16" width="0.127" layer="51"/>
<wire x1="-2.11" y1="-1.16" x2="-2.11" y2="1.16" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0.8" x2="-2.1" y2="1.2" width="0.127" layer="21"/>
<wire x1="-2.1" y1="1.2" x2="-1.1" y2="1.2" width="0.127" layer="21"/>
<wire x1="1.1" y1="1.2" x2="2.1" y2="1.2" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.2" x2="2.1" y2="0.8" width="0.127" layer="21"/>
<wire x1="2.1" y1="-0.8" x2="2.1" y2="-1.2" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.2" x2="1.1" y2="-1.2" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-1.2" x2="-2.1" y2="-1.2" width="0.127" layer="21"/>
<wire x1="-2.1" y1="-1.2" x2="-2.1" y2="-0.8" width="0.127" layer="21"/>
</package>
<package name="M10-0.4MM-SMD-HEADER">
<smd name="3" x="0" y="0.775" dx="0.23" dy="0.73" layer="1"/>
<smd name="4" x="0.4" y="0.775" dx="0.23" dy="0.73" layer="1"/>
<smd name="5" x="0.8" y="0.775" dx="0.23" dy="0.73" layer="1"/>
<smd name="2" x="-0.4" y="0.775" dx="0.23" dy="0.73" layer="1"/>
<smd name="1" x="-0.8" y="0.775" dx="0.23" dy="0.73" layer="1"/>
<smd name="8" x="0" y="-0.775" dx="0.23" dy="0.73" layer="1"/>
<smd name="7" x="0.4" y="-0.775" dx="0.23" dy="0.73" layer="1"/>
<smd name="6" x="0.8" y="-0.775" dx="0.23" dy="0.73" layer="1"/>
<smd name="9" x="-0.4" y="-0.775" dx="0.23" dy="0.73" layer="1"/>
<smd name="10" x="-0.8" y="-0.775" dx="0.23" dy="0.73" layer="1"/>
<smd name="NC_2" x="1.5" y="0" dx="0.38" dy="0.68" layer="1"/>
<smd name="NC_1" x="-1.5" y="0" dx="0.38" dy="0.68" layer="1"/>
<wire x1="-1.57" y1="0.73" x2="1.57" y2="0.73" width="0.127" layer="51"/>
<wire x1="1.57" y1="0.73" x2="1.57" y2="-0.73" width="0.127" layer="51"/>
<wire x1="1.57" y1="-0.73" x2="-1.57" y2="-0.73" width="0.127" layer="51"/>
<wire x1="-1.57" y1="-0.73" x2="-1.57" y2="0.73" width="0.127" layer="51"/>
<wire x1="-1.6" y1="0.5" x2="-1.6" y2="0.8" width="0.127" layer="21"/>
<wire x1="-1.6" y1="0.8" x2="-1.1" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.1" y1="0.8" x2="1.6" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.6" y1="0.8" x2="1.6" y2="0.5" width="0.127" layer="21"/>
<wire x1="1.6" y1="-0.5" x2="1.6" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.6" y1="-0.8" x2="1.1" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.1" y1="-0.8" x2="-1.6" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-0.8" x2="-1.6" y2="-0.5" width="0.127" layer="21"/>
</package>
<package name="ACL-LOGO">
<rectangle x1="3.14325" y1="0.03175" x2="3.37185" y2="0.04445" layer="21"/>
<rectangle x1="3.14325" y1="0.04445" x2="3.37185" y2="0.05715" layer="21"/>
<rectangle x1="3.14325" y1="0.05715" x2="3.37185" y2="0.06985" layer="21"/>
<rectangle x1="3.14325" y1="0.06985" x2="3.37185" y2="0.08255" layer="21"/>
<rectangle x1="3.14325" y1="0.08255" x2="3.37185" y2="0.09525" layer="21"/>
<rectangle x1="3.14325" y1="0.09525" x2="3.37185" y2="0.10795" layer="21"/>
<rectangle x1="3.14325" y1="0.10795" x2="3.37185" y2="0.12065" layer="21"/>
<rectangle x1="3.14325" y1="0.12065" x2="3.37185" y2="0.13335" layer="21"/>
<rectangle x1="6.21665" y1="0.12065" x2="6.29285" y2="0.13335" layer="21"/>
<rectangle x1="0.99695" y1="0.13335" x2="1.13665" y2="0.14605" layer="21"/>
<rectangle x1="2.07645" y1="0.13335" x2="2.24155" y2="0.14605" layer="21"/>
<rectangle x1="2.72415" y1="0.13335" x2="2.87655" y2="0.14605" layer="21"/>
<rectangle x1="3.14325" y1="0.13335" x2="3.37185" y2="0.14605" layer="21"/>
<rectangle x1="3.49885" y1="0.13335" x2="3.58775" y2="0.14605" layer="21"/>
<rectangle x1="3.91795" y1="0.13335" x2="4.00685" y2="0.14605" layer="21"/>
<rectangle x1="4.62915" y1="0.13335" x2="4.78155" y2="0.14605" layer="21"/>
<rectangle x1="5.27685" y1="0.13335" x2="5.42925" y2="0.14605" layer="21"/>
<rectangle x1="6.14045" y1="0.13335" x2="6.35635" y2="0.14605" layer="21"/>
<rectangle x1="6.83895" y1="0.13335" x2="7.00405" y2="0.14605" layer="21"/>
<rectangle x1="8.99795" y1="0.13335" x2="9.16305" y2="0.14605" layer="21"/>
<rectangle x1="10.00125" y1="0.13335" x2="10.15365" y2="0.14605" layer="21"/>
<rectangle x1="11.23315" y1="0.13335" x2="11.32205" y2="0.14605" layer="21"/>
<rectangle x1="12.12215" y1="0.13335" x2="12.21105" y2="0.14605" layer="21"/>
<rectangle x1="0.09525" y1="0.14605" x2="0.33655" y2="0.15875" layer="21"/>
<rectangle x1="0.48895" y1="0.14605" x2="0.73025" y2="0.15875" layer="21"/>
<rectangle x1="0.94615" y1="0.14605" x2="1.18745" y2="0.15875" layer="21"/>
<rectangle x1="1.44145" y1="0.14605" x2="1.67005" y2="0.15875" layer="21"/>
<rectangle x1="2.02565" y1="0.14605" x2="2.29235" y2="0.15875" layer="21"/>
<rectangle x1="2.67335" y1="0.14605" x2="2.92735" y2="0.15875" layer="21"/>
<rectangle x1="3.14325" y1="0.14605" x2="3.37185" y2="0.15875" layer="21"/>
<rectangle x1="3.47345" y1="0.14605" x2="3.62585" y2="0.15875" layer="21"/>
<rectangle x1="3.87985" y1="0.14605" x2="4.03225" y2="0.15875" layer="21"/>
<rectangle x1="4.12115" y1="0.14605" x2="4.33705" y2="0.15875" layer="21"/>
<rectangle x1="4.59105" y1="0.14605" x2="4.83235" y2="0.15875" layer="21"/>
<rectangle x1="5.22605" y1="0.14605" x2="5.48005" y2="0.15875" layer="21"/>
<rectangle x1="6.10235" y1="0.14605" x2="6.39445" y2="0.15875" layer="21"/>
<rectangle x1="6.78815" y1="0.14605" x2="7.05485" y2="0.15875" layer="21"/>
<rectangle x1="7.30885" y1="0.14605" x2="7.53745" y2="0.15875" layer="21"/>
<rectangle x1="7.63905" y1="0.14605" x2="7.86765" y2="0.15875" layer="21"/>
<rectangle x1="8.09625" y1="0.14605" x2="8.29945" y2="0.15875" layer="21"/>
<rectangle x1="8.36295" y1="0.14605" x2="8.59155" y2="0.15875" layer="21"/>
<rectangle x1="8.94715" y1="0.14605" x2="9.21385" y2="0.15875" layer="21"/>
<rectangle x1="9.46785" y1="0.14605" x2="9.70915" y2="0.15875" layer="21"/>
<rectangle x1="9.95045" y1="0.14605" x2="10.20445" y2="0.15875" layer="21"/>
<rectangle x1="10.64895" y1="0.14605" x2="11.04265" y2="0.15875" layer="21"/>
<rectangle x1="11.19505" y1="0.14605" x2="11.36015" y2="0.15875" layer="21"/>
<rectangle x1="11.43635" y1="0.14605" x2="11.65225" y2="0.15875" layer="21"/>
<rectangle x1="11.76655" y1="0.14605" x2="11.98245" y2="0.15875" layer="21"/>
<rectangle x1="12.08405" y1="0.14605" x2="12.23645" y2="0.15875" layer="21"/>
<rectangle x1="0.09525" y1="0.15875" x2="0.33655" y2="0.17145" layer="21"/>
<rectangle x1="0.48895" y1="0.15875" x2="0.73025" y2="0.17145" layer="21"/>
<rectangle x1="0.90805" y1="0.15875" x2="1.21285" y2="0.17145" layer="21"/>
<rectangle x1="1.44145" y1="0.15875" x2="1.67005" y2="0.17145" layer="21"/>
<rectangle x1="2.00025" y1="0.15875" x2="2.31775" y2="0.17145" layer="21"/>
<rectangle x1="2.64795" y1="0.15875" x2="2.96545" y2="0.17145" layer="21"/>
<rectangle x1="3.14325" y1="0.15875" x2="3.37185" y2="0.17145" layer="21"/>
<rectangle x1="3.44805" y1="0.15875" x2="3.63855" y2="0.17145" layer="21"/>
<rectangle x1="3.85445" y1="0.15875" x2="4.05765" y2="0.17145" layer="21"/>
<rectangle x1="4.12115" y1="0.15875" x2="4.33705" y2="0.17145" layer="21"/>
<rectangle x1="4.55295" y1="0.15875" x2="4.85775" y2="0.17145" layer="21"/>
<rectangle x1="5.20065" y1="0.15875" x2="5.50545" y2="0.17145" layer="21"/>
<rectangle x1="6.07695" y1="0.15875" x2="6.41985" y2="0.17145" layer="21"/>
<rectangle x1="6.76275" y1="0.15875" x2="7.08025" y2="0.17145" layer="21"/>
<rectangle x1="7.30885" y1="0.15875" x2="7.53745" y2="0.17145" layer="21"/>
<rectangle x1="7.63905" y1="0.15875" x2="7.86765" y2="0.17145" layer="21"/>
<rectangle x1="8.05815" y1="0.15875" x2="8.29945" y2="0.17145" layer="21"/>
<rectangle x1="8.36295" y1="0.15875" x2="8.59155" y2="0.17145" layer="21"/>
<rectangle x1="8.92175" y1="0.15875" x2="9.23925" y2="0.17145" layer="21"/>
<rectangle x1="9.46785" y1="0.15875" x2="9.70915" y2="0.17145" layer="21"/>
<rectangle x1="9.91235" y1="0.15875" x2="10.22985" y2="0.17145" layer="21"/>
<rectangle x1="10.64895" y1="0.15875" x2="11.04265" y2="0.17145" layer="21"/>
<rectangle x1="11.16965" y1="0.15875" x2="11.37285" y2="0.17145" layer="21"/>
<rectangle x1="11.43635" y1="0.15875" x2="11.65225" y2="0.17145" layer="21"/>
<rectangle x1="11.76655" y1="0.15875" x2="11.98245" y2="0.17145" layer="21"/>
<rectangle x1="12.05865" y1="0.15875" x2="12.26185" y2="0.17145" layer="21"/>
<rectangle x1="0.09525" y1="0.17145" x2="0.33655" y2="0.18415" layer="21"/>
<rectangle x1="0.48895" y1="0.17145" x2="0.71755" y2="0.18415" layer="21"/>
<rectangle x1="0.88265" y1="0.17145" x2="1.23825" y2="0.18415" layer="21"/>
<rectangle x1="1.44145" y1="0.17145" x2="1.67005" y2="0.18415" layer="21"/>
<rectangle x1="1.97485" y1="0.17145" x2="2.34315" y2="0.18415" layer="21"/>
<rectangle x1="2.62255" y1="0.17145" x2="2.97815" y2="0.18415" layer="21"/>
<rectangle x1="3.14325" y1="0.17145" x2="3.37185" y2="0.18415" layer="21"/>
<rectangle x1="3.43535" y1="0.17145" x2="3.66395" y2="0.18415" layer="21"/>
<rectangle x1="3.84175" y1="0.17145" x2="4.07035" y2="0.18415" layer="21"/>
<rectangle x1="4.12115" y1="0.17145" x2="4.33705" y2="0.18415" layer="21"/>
<rectangle x1="4.54025" y1="0.17145" x2="4.88315" y2="0.18415" layer="21"/>
<rectangle x1="5.17525" y1="0.17145" x2="5.53085" y2="0.18415" layer="21"/>
<rectangle x1="6.05155" y1="0.17145" x2="6.44525" y2="0.18415" layer="21"/>
<rectangle x1="6.73735" y1="0.17145" x2="7.10565" y2="0.18415" layer="21"/>
<rectangle x1="7.30885" y1="0.17145" x2="7.53745" y2="0.18415" layer="21"/>
<rectangle x1="7.63905" y1="0.17145" x2="7.86765" y2="0.18415" layer="21"/>
<rectangle x1="8.03275" y1="0.17145" x2="8.29945" y2="0.18415" layer="21"/>
<rectangle x1="8.36295" y1="0.17145" x2="8.59155" y2="0.18415" layer="21"/>
<rectangle x1="8.89635" y1="0.17145" x2="9.26465" y2="0.18415" layer="21"/>
<rectangle x1="9.46785" y1="0.17145" x2="9.70915" y2="0.18415" layer="21"/>
<rectangle x1="9.88695" y1="0.17145" x2="10.25525" y2="0.18415" layer="21"/>
<rectangle x1="10.64895" y1="0.17145" x2="11.04265" y2="0.18415" layer="21"/>
<rectangle x1="11.15695" y1="0.17145" x2="11.38555" y2="0.18415" layer="21"/>
<rectangle x1="11.43635" y1="0.17145" x2="11.65225" y2="0.18415" layer="21"/>
<rectangle x1="11.76655" y1="0.17145" x2="11.98245" y2="0.18415" layer="21"/>
<rectangle x1="12.04595" y1="0.17145" x2="12.27455" y2="0.18415" layer="21"/>
<rectangle x1="0.09525" y1="0.18415" x2="0.33655" y2="0.19685" layer="21"/>
<rectangle x1="0.48895" y1="0.18415" x2="0.71755" y2="0.19685" layer="21"/>
<rectangle x1="0.86995" y1="0.18415" x2="1.25095" y2="0.19685" layer="21"/>
<rectangle x1="1.44145" y1="0.18415" x2="1.67005" y2="0.19685" layer="21"/>
<rectangle x1="1.96215" y1="0.18415" x2="2.35585" y2="0.19685" layer="21"/>
<rectangle x1="2.59715" y1="0.18415" x2="2.99085" y2="0.19685" layer="21"/>
<rectangle x1="3.14325" y1="0.18415" x2="3.37185" y2="0.19685" layer="21"/>
<rectangle x1="3.42265" y1="0.18415" x2="3.66395" y2="0.19685" layer="21"/>
<rectangle x1="3.82905" y1="0.18415" x2="4.08305" y2="0.19685" layer="21"/>
<rectangle x1="4.12115" y1="0.18415" x2="4.33705" y2="0.19685" layer="21"/>
<rectangle x1="4.51485" y1="0.18415" x2="4.89585" y2="0.19685" layer="21"/>
<rectangle x1="5.16255" y1="0.18415" x2="5.54355" y2="0.19685" layer="21"/>
<rectangle x1="6.03885" y1="0.18415" x2="6.45795" y2="0.19685" layer="21"/>
<rectangle x1="6.72465" y1="0.18415" x2="7.11835" y2="0.19685" layer="21"/>
<rectangle x1="7.30885" y1="0.18415" x2="7.53745" y2="0.19685" layer="21"/>
<rectangle x1="7.63905" y1="0.18415" x2="7.86765" y2="0.19685" layer="21"/>
<rectangle x1="8.02005" y1="0.18415" x2="8.29945" y2="0.19685" layer="21"/>
<rectangle x1="8.36295" y1="0.18415" x2="8.59155" y2="0.19685" layer="21"/>
<rectangle x1="8.88365" y1="0.18415" x2="9.27735" y2="0.19685" layer="21"/>
<rectangle x1="9.46785" y1="0.18415" x2="9.70915" y2="0.19685" layer="21"/>
<rectangle x1="9.87425" y1="0.18415" x2="10.26795" y2="0.19685" layer="21"/>
<rectangle x1="10.64895" y1="0.18415" x2="11.04265" y2="0.19685" layer="21"/>
<rectangle x1="11.14425" y1="0.18415" x2="11.39825" y2="0.19685" layer="21"/>
<rectangle x1="11.43635" y1="0.18415" x2="11.65225" y2="0.19685" layer="21"/>
<rectangle x1="11.76655" y1="0.18415" x2="11.99515" y2="0.19685" layer="21"/>
<rectangle x1="12.03325" y1="0.18415" x2="12.28725" y2="0.19685" layer="21"/>
<rectangle x1="0.10795" y1="0.19685" x2="0.34925" y2="0.20955" layer="21"/>
<rectangle x1="0.48895" y1="0.19685" x2="0.71755" y2="0.20955" layer="21"/>
<rectangle x1="0.85725" y1="0.19685" x2="1.26365" y2="0.20955" layer="21"/>
<rectangle x1="1.44145" y1="0.19685" x2="1.67005" y2="0.20955" layer="21"/>
<rectangle x1="1.94945" y1="0.19685" x2="2.36855" y2="0.20955" layer="21"/>
<rectangle x1="2.58445" y1="0.19685" x2="3.00355" y2="0.20955" layer="21"/>
<rectangle x1="3.14325" y1="0.19685" x2="3.37185" y2="0.20955" layer="21"/>
<rectangle x1="3.40995" y1="0.19685" x2="3.67665" y2="0.20955" layer="21"/>
<rectangle x1="3.81635" y1="0.19685" x2="4.08305" y2="0.20955" layer="21"/>
<rectangle x1="4.12115" y1="0.19685" x2="4.33705" y2="0.20955" layer="21"/>
<rectangle x1="4.50215" y1="0.19685" x2="4.90855" y2="0.20955" layer="21"/>
<rectangle x1="5.13715" y1="0.19685" x2="5.55625" y2="0.20955" layer="21"/>
<rectangle x1="6.02615" y1="0.19685" x2="6.47065" y2="0.20955" layer="21"/>
<rectangle x1="6.71195" y1="0.19685" x2="7.13105" y2="0.20955" layer="21"/>
<rectangle x1="7.30885" y1="0.19685" x2="7.53745" y2="0.20955" layer="21"/>
<rectangle x1="7.63905" y1="0.19685" x2="7.86765" y2="0.20955" layer="21"/>
<rectangle x1="8.00735" y1="0.19685" x2="8.29945" y2="0.20955" layer="21"/>
<rectangle x1="8.36295" y1="0.19685" x2="8.59155" y2="0.20955" layer="21"/>
<rectangle x1="8.87095" y1="0.19685" x2="9.29005" y2="0.20955" layer="21"/>
<rectangle x1="9.46785" y1="0.19685" x2="9.70915" y2="0.20955" layer="21"/>
<rectangle x1="9.86155" y1="0.19685" x2="10.28065" y2="0.20955" layer="21"/>
<rectangle x1="10.64895" y1="0.19685" x2="11.04265" y2="0.20955" layer="21"/>
<rectangle x1="11.14425" y1="0.19685" x2="11.41095" y2="0.20955" layer="21"/>
<rectangle x1="11.43635" y1="0.19685" x2="11.65225" y2="0.20955" layer="21"/>
<rectangle x1="11.76655" y1="0.19685" x2="11.99515" y2="0.20955" layer="21"/>
<rectangle x1="12.02055" y1="0.19685" x2="12.29995" y2="0.20955" layer="21"/>
<rectangle x1="0.10795" y1="0.20955" x2="0.34925" y2="0.22225" layer="21"/>
<rectangle x1="0.47625" y1="0.20955" x2="0.71755" y2="0.22225" layer="21"/>
<rectangle x1="0.84455" y1="0.20955" x2="1.27635" y2="0.22225" layer="21"/>
<rectangle x1="1.44145" y1="0.20955" x2="1.67005" y2="0.22225" layer="21"/>
<rectangle x1="1.93675" y1="0.20955" x2="2.38125" y2="0.22225" layer="21"/>
<rectangle x1="2.57175" y1="0.20955" x2="3.01625" y2="0.22225" layer="21"/>
<rectangle x1="3.14325" y1="0.20955" x2="3.37185" y2="0.22225" layer="21"/>
<rectangle x1="3.39725" y1="0.20955" x2="3.67665" y2="0.22225" layer="21"/>
<rectangle x1="3.81635" y1="0.20955" x2="4.09575" y2="0.22225" layer="21"/>
<rectangle x1="4.10845" y1="0.20955" x2="4.33705" y2="0.22225" layer="21"/>
<rectangle x1="4.50215" y1="0.20955" x2="4.92125" y2="0.22225" layer="21"/>
<rectangle x1="5.12445" y1="0.20955" x2="5.56895" y2="0.22225" layer="21"/>
<rectangle x1="6.01345" y1="0.20955" x2="6.48335" y2="0.22225" layer="21"/>
<rectangle x1="6.69925" y1="0.20955" x2="7.14375" y2="0.22225" layer="21"/>
<rectangle x1="7.30885" y1="0.20955" x2="7.53745" y2="0.22225" layer="21"/>
<rectangle x1="7.63905" y1="0.20955" x2="7.86765" y2="0.22225" layer="21"/>
<rectangle x1="8.00735" y1="0.20955" x2="8.29945" y2="0.22225" layer="21"/>
<rectangle x1="8.36295" y1="0.20955" x2="8.59155" y2="0.22225" layer="21"/>
<rectangle x1="8.85825" y1="0.20955" x2="9.30275" y2="0.22225" layer="21"/>
<rectangle x1="9.46785" y1="0.20955" x2="9.70915" y2="0.22225" layer="21"/>
<rectangle x1="9.84885" y1="0.20955" x2="10.29335" y2="0.22225" layer="21"/>
<rectangle x1="10.64895" y1="0.20955" x2="11.04265" y2="0.22225" layer="21"/>
<rectangle x1="11.13155" y1="0.20955" x2="11.65225" y2="0.22225" layer="21"/>
<rectangle x1="11.76655" y1="0.20955" x2="11.99515" y2="0.22225" layer="21"/>
<rectangle x1="12.00785" y1="0.20955" x2="12.29995" y2="0.22225" layer="21"/>
<rectangle x1="0.10795" y1="0.22225" x2="0.34925" y2="0.23495" layer="21"/>
<rectangle x1="0.47625" y1="0.22225" x2="0.71755" y2="0.23495" layer="21"/>
<rectangle x1="0.83185" y1="0.22225" x2="1.28905" y2="0.23495" layer="21"/>
<rectangle x1="1.44145" y1="0.22225" x2="1.67005" y2="0.23495" layer="21"/>
<rectangle x1="1.92405" y1="0.22225" x2="2.39395" y2="0.23495" layer="21"/>
<rectangle x1="2.57175" y1="0.22225" x2="3.02895" y2="0.23495" layer="21"/>
<rectangle x1="3.14325" y1="0.22225" x2="3.37185" y2="0.23495" layer="21"/>
<rectangle x1="3.38455" y1="0.22225" x2="3.68935" y2="0.23495" layer="21"/>
<rectangle x1="3.81635" y1="0.22225" x2="4.33705" y2="0.23495" layer="21"/>
<rectangle x1="4.48945" y1="0.22225" x2="4.93395" y2="0.23495" layer="21"/>
<rectangle x1="5.12445" y1="0.22225" x2="5.58165" y2="0.23495" layer="21"/>
<rectangle x1="6.00075" y1="0.22225" x2="6.49605" y2="0.23495" layer="21"/>
<rectangle x1="6.68655" y1="0.22225" x2="7.15645" y2="0.23495" layer="21"/>
<rectangle x1="7.30885" y1="0.22225" x2="7.53745" y2="0.23495" layer="21"/>
<rectangle x1="7.63905" y1="0.22225" x2="7.86765" y2="0.23495" layer="21"/>
<rectangle x1="7.99465" y1="0.22225" x2="8.29945" y2="0.23495" layer="21"/>
<rectangle x1="8.36295" y1="0.22225" x2="8.59155" y2="0.23495" layer="21"/>
<rectangle x1="8.84555" y1="0.22225" x2="9.31545" y2="0.23495" layer="21"/>
<rectangle x1="9.46785" y1="0.22225" x2="9.70915" y2="0.23495" layer="21"/>
<rectangle x1="9.83615" y1="0.22225" x2="10.29335" y2="0.23495" layer="21"/>
<rectangle x1="10.64895" y1="0.22225" x2="11.04265" y2="0.23495" layer="21"/>
<rectangle x1="11.13155" y1="0.22225" x2="11.65225" y2="0.23495" layer="21"/>
<rectangle x1="11.76655" y1="0.22225" x2="12.31265" y2="0.23495" layer="21"/>
<rectangle x1="0.10795" y1="0.23495" x2="0.34925" y2="0.24765" layer="21"/>
<rectangle x1="0.47625" y1="0.23495" x2="0.71755" y2="0.24765" layer="21"/>
<rectangle x1="0.81915" y1="0.23495" x2="1.30175" y2="0.24765" layer="21"/>
<rectangle x1="1.44145" y1="0.23495" x2="1.67005" y2="0.24765" layer="21"/>
<rectangle x1="1.92405" y1="0.23495" x2="2.40665" y2="0.24765" layer="21"/>
<rectangle x1="2.55905" y1="0.23495" x2="3.02895" y2="0.24765" layer="21"/>
<rectangle x1="3.14325" y1="0.23495" x2="3.68935" y2="0.24765" layer="21"/>
<rectangle x1="3.80365" y1="0.23495" x2="4.33705" y2="0.24765" layer="21"/>
<rectangle x1="4.47675" y1="0.23495" x2="4.93395" y2="0.24765" layer="21"/>
<rectangle x1="5.11175" y1="0.23495" x2="5.58165" y2="0.24765" layer="21"/>
<rectangle x1="6.00075" y1="0.23495" x2="6.50875" y2="0.24765" layer="21"/>
<rectangle x1="6.68655" y1="0.23495" x2="7.16915" y2="0.24765" layer="21"/>
<rectangle x1="7.30885" y1="0.23495" x2="7.53745" y2="0.24765" layer="21"/>
<rectangle x1="7.63905" y1="0.23495" x2="7.86765" y2="0.24765" layer="21"/>
<rectangle x1="7.99465" y1="0.23495" x2="8.29945" y2="0.24765" layer="21"/>
<rectangle x1="8.36295" y1="0.23495" x2="8.59155" y2="0.24765" layer="21"/>
<rectangle x1="8.84555" y1="0.23495" x2="9.32815" y2="0.24765" layer="21"/>
<rectangle x1="9.46785" y1="0.23495" x2="9.70915" y2="0.24765" layer="21"/>
<rectangle x1="9.83615" y1="0.23495" x2="10.30605" y2="0.24765" layer="21"/>
<rectangle x1="10.64895" y1="0.23495" x2="11.04265" y2="0.24765" layer="21"/>
<rectangle x1="11.13155" y1="0.23495" x2="11.65225" y2="0.24765" layer="21"/>
<rectangle x1="11.76655" y1="0.23495" x2="12.31265" y2="0.24765" layer="21"/>
<rectangle x1="0.10795" y1="0.24765" x2="0.34925" y2="0.26035" layer="21"/>
<rectangle x1="0.47625" y1="0.24765" x2="0.70485" y2="0.26035" layer="21"/>
<rectangle x1="0.81915" y1="0.24765" x2="1.04775" y2="0.26035" layer="21"/>
<rectangle x1="1.07315" y1="0.24765" x2="1.30175" y2="0.26035" layer="21"/>
<rectangle x1="1.44145" y1="0.24765" x2="1.67005" y2="0.26035" layer="21"/>
<rectangle x1="1.91135" y1="0.24765" x2="2.15265" y2="0.26035" layer="21"/>
<rectangle x1="2.17805" y1="0.24765" x2="2.40665" y2="0.26035" layer="21"/>
<rectangle x1="2.55905" y1="0.24765" x2="2.77495" y2="0.26035" layer="21"/>
<rectangle x1="2.80035" y1="0.24765" x2="3.04165" y2="0.26035" layer="21"/>
<rectangle x1="3.14325" y1="0.24765" x2="3.40995" y2="0.26035" layer="21"/>
<rectangle x1="3.43535" y1="0.24765" x2="3.68935" y2="0.26035" layer="21"/>
<rectangle x1="3.80365" y1="0.24765" x2="4.04495" y2="0.26035" layer="21"/>
<rectangle x1="4.07035" y1="0.24765" x2="4.33705" y2="0.26035" layer="21"/>
<rectangle x1="4.47675" y1="0.24765" x2="4.71805" y2="0.26035" layer="21"/>
<rectangle x1="4.74345" y1="0.24765" x2="4.94665" y2="0.26035" layer="21"/>
<rectangle x1="5.09905" y1="0.24765" x2="5.34035" y2="0.26035" layer="21"/>
<rectangle x1="5.36575" y1="0.24765" x2="5.59435" y2="0.26035" layer="21"/>
<rectangle x1="5.98805" y1="0.24765" x2="6.50875" y2="0.26035" layer="21"/>
<rectangle x1="6.67385" y1="0.24765" x2="6.91515" y2="0.26035" layer="21"/>
<rectangle x1="6.94055" y1="0.24765" x2="7.16915" y2="0.26035" layer="21"/>
<rectangle x1="7.30885" y1="0.24765" x2="7.53745" y2="0.26035" layer="21"/>
<rectangle x1="7.63905" y1="0.24765" x2="7.86765" y2="0.26035" layer="21"/>
<rectangle x1="7.99465" y1="0.24765" x2="8.28675" y2="0.26035" layer="21"/>
<rectangle x1="8.36295" y1="0.24765" x2="8.59155" y2="0.26035" layer="21"/>
<rectangle x1="8.83285" y1="0.24765" x2="9.07415" y2="0.26035" layer="21"/>
<rectangle x1="9.09955" y1="0.24765" x2="9.32815" y2="0.26035" layer="21"/>
<rectangle x1="9.46785" y1="0.24765" x2="9.70915" y2="0.26035" layer="21"/>
<rectangle x1="9.82345" y1="0.24765" x2="10.03935" y2="0.26035" layer="21"/>
<rectangle x1="10.07745" y1="0.24765" x2="10.30605" y2="0.26035" layer="21"/>
<rectangle x1="10.64895" y1="0.24765" x2="11.04265" y2="0.26035" layer="21"/>
<rectangle x1="11.11885" y1="0.24765" x2="11.37285" y2="0.26035" layer="21"/>
<rectangle x1="11.39825" y1="0.24765" x2="11.65225" y2="0.26035" layer="21"/>
<rectangle x1="11.76655" y1="0.24765" x2="12.03325" y2="0.26035" layer="21"/>
<rectangle x1="12.05865" y1="0.24765" x2="12.31265" y2="0.26035" layer="21"/>
<rectangle x1="0.10795" y1="0.26035" x2="0.34925" y2="0.27305" layer="21"/>
<rectangle x1="0.47625" y1="0.26035" x2="0.70485" y2="0.27305" layer="21"/>
<rectangle x1="0.80645" y1="0.26035" x2="1.02235" y2="0.27305" layer="21"/>
<rectangle x1="1.09855" y1="0.26035" x2="1.31445" y2="0.27305" layer="21"/>
<rectangle x1="1.44145" y1="0.26035" x2="1.67005" y2="0.27305" layer="21"/>
<rectangle x1="1.91135" y1="0.26035" x2="2.12725" y2="0.27305" layer="21"/>
<rectangle x1="2.20345" y1="0.26035" x2="2.41935" y2="0.27305" layer="21"/>
<rectangle x1="2.54635" y1="0.26035" x2="2.74955" y2="0.27305" layer="21"/>
<rectangle x1="2.83845" y1="0.26035" x2="3.04165" y2="0.27305" layer="21"/>
<rectangle x1="3.14325" y1="0.26035" x2="3.38455" y2="0.27305" layer="21"/>
<rectangle x1="3.46075" y1="0.26035" x2="3.68935" y2="0.27305" layer="21"/>
<rectangle x1="3.80365" y1="0.26035" x2="4.01955" y2="0.27305" layer="21"/>
<rectangle x1="4.09575" y1="0.26035" x2="4.33705" y2="0.27305" layer="21"/>
<rectangle x1="4.46405" y1="0.26035" x2="4.69265" y2="0.27305" layer="21"/>
<rectangle x1="4.75615" y1="0.26035" x2="4.94665" y2="0.27305" layer="21"/>
<rectangle x1="5.09905" y1="0.26035" x2="5.31495" y2="0.27305" layer="21"/>
<rectangle x1="5.39115" y1="0.26035" x2="5.59435" y2="0.27305" layer="21"/>
<rectangle x1="5.98805" y1="0.26035" x2="6.52145" y2="0.27305" layer="21"/>
<rectangle x1="6.67385" y1="0.26035" x2="6.88975" y2="0.27305" layer="21"/>
<rectangle x1="6.96595" y1="0.26035" x2="7.18185" y2="0.27305" layer="21"/>
<rectangle x1="7.30885" y1="0.26035" x2="7.53745" y2="0.27305" layer="21"/>
<rectangle x1="7.63905" y1="0.26035" x2="7.86765" y2="0.27305" layer="21"/>
<rectangle x1="7.99465" y1="0.26035" x2="8.22325" y2="0.27305" layer="21"/>
<rectangle x1="8.36295" y1="0.26035" x2="8.59155" y2="0.27305" layer="21"/>
<rectangle x1="8.83285" y1="0.26035" x2="9.04875" y2="0.27305" layer="21"/>
<rectangle x1="9.12495" y1="0.26035" x2="9.34085" y2="0.27305" layer="21"/>
<rectangle x1="9.46785" y1="0.26035" x2="9.70915" y2="0.27305" layer="21"/>
<rectangle x1="9.82345" y1="0.26035" x2="10.02665" y2="0.27305" layer="21"/>
<rectangle x1="10.10285" y1="0.26035" x2="10.31875" y2="0.27305" layer="21"/>
<rectangle x1="10.64895" y1="0.26035" x2="11.04265" y2="0.27305" layer="21"/>
<rectangle x1="11.11885" y1="0.26035" x2="11.34745" y2="0.27305" layer="21"/>
<rectangle x1="11.42365" y1="0.26035" x2="11.65225" y2="0.27305" layer="21"/>
<rectangle x1="11.76655" y1="0.26035" x2="12.00785" y2="0.27305" layer="21"/>
<rectangle x1="12.08405" y1="0.26035" x2="12.31265" y2="0.27305" layer="21"/>
<rectangle x1="0.10795" y1="0.27305" x2="0.34925" y2="0.28575" layer="21"/>
<rectangle x1="0.47625" y1="0.27305" x2="0.70485" y2="0.28575" layer="21"/>
<rectangle x1="0.80645" y1="0.27305" x2="1.02235" y2="0.28575" layer="21"/>
<rectangle x1="1.11125" y1="0.27305" x2="1.31445" y2="0.28575" layer="21"/>
<rectangle x1="1.44145" y1="0.27305" x2="1.67005" y2="0.28575" layer="21"/>
<rectangle x1="1.91135" y1="0.27305" x2="2.12725" y2="0.28575" layer="21"/>
<rectangle x1="2.20345" y1="0.27305" x2="2.41935" y2="0.28575" layer="21"/>
<rectangle x1="2.54635" y1="0.27305" x2="2.73685" y2="0.28575" layer="21"/>
<rectangle x1="2.83845" y1="0.27305" x2="3.04165" y2="0.28575" layer="21"/>
<rectangle x1="3.14325" y1="0.27305" x2="3.38455" y2="0.28575" layer="21"/>
<rectangle x1="3.47345" y1="0.27305" x2="3.68935" y2="0.28575" layer="21"/>
<rectangle x1="3.80365" y1="0.27305" x2="4.01955" y2="0.28575" layer="21"/>
<rectangle x1="4.10845" y1="0.27305" x2="4.33705" y2="0.28575" layer="21"/>
<rectangle x1="4.46405" y1="0.27305" x2="4.67995" y2="0.28575" layer="21"/>
<rectangle x1="4.76885" y1="0.27305" x2="4.95935" y2="0.28575" layer="21"/>
<rectangle x1="5.09905" y1="0.27305" x2="5.30225" y2="0.28575" layer="21"/>
<rectangle x1="5.40385" y1="0.27305" x2="5.60705" y2="0.28575" layer="21"/>
<rectangle x1="5.97535" y1="0.27305" x2="6.21665" y2="0.28575" layer="21"/>
<rectangle x1="6.29285" y1="0.27305" x2="6.52145" y2="0.28575" layer="21"/>
<rectangle x1="6.67385" y1="0.27305" x2="6.88975" y2="0.28575" layer="21"/>
<rectangle x1="6.96595" y1="0.27305" x2="7.18185" y2="0.28575" layer="21"/>
<rectangle x1="7.30885" y1="0.27305" x2="7.53745" y2="0.28575" layer="21"/>
<rectangle x1="7.63905" y1="0.27305" x2="7.86765" y2="0.28575" layer="21"/>
<rectangle x1="7.99465" y1="0.27305" x2="8.22325" y2="0.28575" layer="21"/>
<rectangle x1="8.36295" y1="0.27305" x2="8.59155" y2="0.28575" layer="21"/>
<rectangle x1="8.83285" y1="0.27305" x2="9.04875" y2="0.28575" layer="21"/>
<rectangle x1="9.13765" y1="0.27305" x2="9.34085" y2="0.28575" layer="21"/>
<rectangle x1="9.46785" y1="0.27305" x2="9.70915" y2="0.28575" layer="21"/>
<rectangle x1="9.82345" y1="0.27305" x2="10.01395" y2="0.28575" layer="21"/>
<rectangle x1="10.11555" y1="0.27305" x2="10.31875" y2="0.28575" layer="21"/>
<rectangle x1="10.64895" y1="0.27305" x2="11.04265" y2="0.28575" layer="21"/>
<rectangle x1="11.11885" y1="0.27305" x2="11.33475" y2="0.28575" layer="21"/>
<rectangle x1="11.42365" y1="0.27305" x2="11.65225" y2="0.28575" layer="21"/>
<rectangle x1="11.76655" y1="0.27305" x2="12.00785" y2="0.28575" layer="21"/>
<rectangle x1="12.08405" y1="0.27305" x2="12.31265" y2="0.28575" layer="21"/>
<rectangle x1="0.12065" y1="0.28575" x2="0.34925" y2="0.29845" layer="21"/>
<rectangle x1="0.47625" y1="0.28575" x2="0.70485" y2="0.29845" layer="21"/>
<rectangle x1="0.80645" y1="0.28575" x2="1.00965" y2="0.29845" layer="21"/>
<rectangle x1="1.11125" y1="0.28575" x2="1.31445" y2="0.29845" layer="21"/>
<rectangle x1="1.44145" y1="0.28575" x2="1.67005" y2="0.29845" layer="21"/>
<rectangle x1="1.89865" y1="0.28575" x2="2.11455" y2="0.29845" layer="21"/>
<rectangle x1="2.21615" y1="0.28575" x2="2.41935" y2="0.29845" layer="21"/>
<rectangle x1="2.54635" y1="0.28575" x2="2.73685" y2="0.29845" layer="21"/>
<rectangle x1="2.85115" y1="0.28575" x2="3.05435" y2="0.29845" layer="21"/>
<rectangle x1="3.14325" y1="0.28575" x2="3.38455" y2="0.29845" layer="21"/>
<rectangle x1="3.47345" y1="0.28575" x2="3.70205" y2="0.29845" layer="21"/>
<rectangle x1="3.80365" y1="0.28575" x2="4.00685" y2="0.29845" layer="21"/>
<rectangle x1="4.10845" y1="0.28575" x2="4.33705" y2="0.29845" layer="21"/>
<rectangle x1="4.46405" y1="0.28575" x2="4.67995" y2="0.29845" layer="21"/>
<rectangle x1="4.76885" y1="0.28575" x2="4.95935" y2="0.29845" layer="21"/>
<rectangle x1="5.08635" y1="0.28575" x2="5.30225" y2="0.29845" layer="21"/>
<rectangle x1="5.40385" y1="0.28575" x2="5.60705" y2="0.29845" layer="21"/>
<rectangle x1="5.97535" y1="0.28575" x2="6.20395" y2="0.29845" layer="21"/>
<rectangle x1="6.30555" y1="0.28575" x2="6.53415" y2="0.29845" layer="21"/>
<rectangle x1="6.66115" y1="0.28575" x2="6.87705" y2="0.29845" layer="21"/>
<rectangle x1="6.97865" y1="0.28575" x2="7.18185" y2="0.29845" layer="21"/>
<rectangle x1="7.30885" y1="0.28575" x2="7.53745" y2="0.29845" layer="21"/>
<rectangle x1="7.63905" y1="0.28575" x2="7.86765" y2="0.29845" layer="21"/>
<rectangle x1="7.99465" y1="0.28575" x2="8.22325" y2="0.29845" layer="21"/>
<rectangle x1="8.36295" y1="0.28575" x2="8.59155" y2="0.29845" layer="21"/>
<rectangle x1="8.83285" y1="0.28575" x2="9.04875" y2="0.29845" layer="21"/>
<rectangle x1="9.13765" y1="0.28575" x2="9.34085" y2="0.29845" layer="21"/>
<rectangle x1="9.46785" y1="0.28575" x2="9.70915" y2="0.29845" layer="21"/>
<rectangle x1="9.81075" y1="0.28575" x2="10.01395" y2="0.29845" layer="21"/>
<rectangle x1="10.11555" y1="0.28575" x2="10.31875" y2="0.29845" layer="21"/>
<rectangle x1="10.64895" y1="0.28575" x2="11.04265" y2="0.29845" layer="21"/>
<rectangle x1="11.11885" y1="0.28575" x2="11.33475" y2="0.29845" layer="21"/>
<rectangle x1="11.42365" y1="0.28575" x2="11.65225" y2="0.29845" layer="21"/>
<rectangle x1="11.76655" y1="0.28575" x2="11.99515" y2="0.29845" layer="21"/>
<rectangle x1="12.09675" y1="0.28575" x2="12.32535" y2="0.29845" layer="21"/>
<rectangle x1="0.12065" y1="0.29845" x2="0.34925" y2="0.31115" layer="21"/>
<rectangle x1="0.47625" y1="0.29845" x2="0.70485" y2="0.31115" layer="21"/>
<rectangle x1="0.79375" y1="0.29845" x2="1.00965" y2="0.31115" layer="21"/>
<rectangle x1="1.12395" y1="0.29845" x2="1.32715" y2="0.31115" layer="21"/>
<rectangle x1="1.44145" y1="0.29845" x2="1.67005" y2="0.31115" layer="21"/>
<rectangle x1="1.89865" y1="0.29845" x2="2.11455" y2="0.31115" layer="21"/>
<rectangle x1="2.21615" y1="0.29845" x2="2.43205" y2="0.31115" layer="21"/>
<rectangle x1="2.54635" y1="0.29845" x2="2.73685" y2="0.31115" layer="21"/>
<rectangle x1="2.85115" y1="0.29845" x2="3.05435" y2="0.31115" layer="21"/>
<rectangle x1="3.14325" y1="0.29845" x2="3.37185" y2="0.31115" layer="21"/>
<rectangle x1="3.47345" y1="0.29845" x2="3.70205" y2="0.31115" layer="21"/>
<rectangle x1="3.80365" y1="0.29845" x2="4.00685" y2="0.31115" layer="21"/>
<rectangle x1="4.10845" y1="0.29845" x2="4.33705" y2="0.31115" layer="21"/>
<rectangle x1="4.45135" y1="0.29845" x2="4.66725" y2="0.31115" layer="21"/>
<rectangle x1="4.78155" y1="0.29845" x2="4.95935" y2="0.31115" layer="21"/>
<rectangle x1="5.08635" y1="0.29845" x2="5.30225" y2="0.31115" layer="21"/>
<rectangle x1="5.40385" y1="0.29845" x2="5.60705" y2="0.31115" layer="21"/>
<rectangle x1="5.97535" y1="0.29845" x2="6.20395" y2="0.31115" layer="21"/>
<rectangle x1="6.30555" y1="0.29845" x2="6.53415" y2="0.31115" layer="21"/>
<rectangle x1="6.66115" y1="0.29845" x2="6.87705" y2="0.31115" layer="21"/>
<rectangle x1="6.97865" y1="0.29845" x2="7.19455" y2="0.31115" layer="21"/>
<rectangle x1="7.30885" y1="0.29845" x2="7.53745" y2="0.31115" layer="21"/>
<rectangle x1="7.63905" y1="0.29845" x2="7.86765" y2="0.31115" layer="21"/>
<rectangle x1="7.99465" y1="0.29845" x2="8.22325" y2="0.31115" layer="21"/>
<rectangle x1="8.36295" y1="0.29845" x2="8.59155" y2="0.31115" layer="21"/>
<rectangle x1="8.82015" y1="0.29845" x2="9.03605" y2="0.31115" layer="21"/>
<rectangle x1="9.13765" y1="0.29845" x2="9.35355" y2="0.31115" layer="21"/>
<rectangle x1="9.46785" y1="0.29845" x2="9.70915" y2="0.31115" layer="21"/>
<rectangle x1="9.81075" y1="0.29845" x2="10.00125" y2="0.31115" layer="21"/>
<rectangle x1="10.11555" y1="0.29845" x2="10.31875" y2="0.31115" layer="21"/>
<rectangle x1="10.64895" y1="0.29845" x2="11.04265" y2="0.31115" layer="21"/>
<rectangle x1="11.11885" y1="0.29845" x2="11.33475" y2="0.31115" layer="21"/>
<rectangle x1="11.43635" y1="0.29845" x2="11.65225" y2="0.31115" layer="21"/>
<rectangle x1="11.76655" y1="0.29845" x2="11.99515" y2="0.31115" layer="21"/>
<rectangle x1="12.09675" y1="0.29845" x2="12.32535" y2="0.31115" layer="21"/>
<rectangle x1="0.12065" y1="0.31115" x2="0.34925" y2="0.32385" layer="21"/>
<rectangle x1="0.47625" y1="0.31115" x2="0.70485" y2="0.32385" layer="21"/>
<rectangle x1="0.79375" y1="0.31115" x2="1.00965" y2="0.32385" layer="21"/>
<rectangle x1="1.12395" y1="0.31115" x2="1.32715" y2="0.32385" layer="21"/>
<rectangle x1="1.44145" y1="0.31115" x2="1.67005" y2="0.32385" layer="21"/>
<rectangle x1="1.89865" y1="0.31115" x2="2.11455" y2="0.32385" layer="21"/>
<rectangle x1="2.21615" y1="0.31115" x2="2.43205" y2="0.32385" layer="21"/>
<rectangle x1="2.54635" y1="0.31115" x2="2.73685" y2="0.32385" layer="21"/>
<rectangle x1="2.85115" y1="0.31115" x2="3.05435" y2="0.32385" layer="21"/>
<rectangle x1="3.14325" y1="0.31115" x2="3.37185" y2="0.32385" layer="21"/>
<rectangle x1="3.47345" y1="0.31115" x2="3.70205" y2="0.32385" layer="21"/>
<rectangle x1="3.80365" y1="0.31115" x2="4.00685" y2="0.32385" layer="21"/>
<rectangle x1="4.10845" y1="0.31115" x2="4.33705" y2="0.32385" layer="21"/>
<rectangle x1="4.45135" y1="0.31115" x2="4.66725" y2="0.32385" layer="21"/>
<rectangle x1="4.78155" y1="0.31115" x2="4.97205" y2="0.32385" layer="21"/>
<rectangle x1="5.08635" y1="0.31115" x2="5.30225" y2="0.32385" layer="21"/>
<rectangle x1="5.40385" y1="0.31115" x2="5.61975" y2="0.32385" layer="21"/>
<rectangle x1="5.97535" y1="0.31115" x2="6.19125" y2="0.32385" layer="21"/>
<rectangle x1="6.30555" y1="0.31115" x2="6.53415" y2="0.32385" layer="21"/>
<rectangle x1="6.66115" y1="0.31115" x2="6.87705" y2="0.32385" layer="21"/>
<rectangle x1="6.97865" y1="0.31115" x2="7.19455" y2="0.32385" layer="21"/>
<rectangle x1="7.30885" y1="0.31115" x2="7.53745" y2="0.32385" layer="21"/>
<rectangle x1="7.63905" y1="0.31115" x2="7.86765" y2="0.32385" layer="21"/>
<rectangle x1="7.99465" y1="0.31115" x2="8.21055" y2="0.32385" layer="21"/>
<rectangle x1="8.36295" y1="0.31115" x2="8.59155" y2="0.32385" layer="21"/>
<rectangle x1="8.82015" y1="0.31115" x2="9.03605" y2="0.32385" layer="21"/>
<rectangle x1="9.13765" y1="0.31115" x2="9.35355" y2="0.32385" layer="21"/>
<rectangle x1="9.46785" y1="0.31115" x2="9.70915" y2="0.32385" layer="21"/>
<rectangle x1="9.81075" y1="0.31115" x2="10.00125" y2="0.32385" layer="21"/>
<rectangle x1="10.11555" y1="0.31115" x2="10.31875" y2="0.32385" layer="21"/>
<rectangle x1="10.64895" y1="0.31115" x2="11.04265" y2="0.32385" layer="21"/>
<rectangle x1="11.11885" y1="0.31115" x2="11.32205" y2="0.32385" layer="21"/>
<rectangle x1="11.43635" y1="0.31115" x2="11.65225" y2="0.32385" layer="21"/>
<rectangle x1="11.76655" y1="0.31115" x2="11.99515" y2="0.32385" layer="21"/>
<rectangle x1="12.09675" y1="0.31115" x2="12.32535" y2="0.32385" layer="21"/>
<rectangle x1="0.12065" y1="0.32385" x2="0.70485" y2="0.33655" layer="21"/>
<rectangle x1="0.79375" y1="0.32385" x2="1.00965" y2="0.33655" layer="21"/>
<rectangle x1="1.12395" y1="0.32385" x2="1.32715" y2="0.33655" layer="21"/>
<rectangle x1="1.44145" y1="0.32385" x2="1.67005" y2="0.33655" layer="21"/>
<rectangle x1="1.89865" y1="0.32385" x2="2.11455" y2="0.33655" layer="21"/>
<rectangle x1="2.21615" y1="0.32385" x2="2.43205" y2="0.33655" layer="21"/>
<rectangle x1="2.53365" y1="0.32385" x2="2.73685" y2="0.33655" layer="21"/>
<rectangle x1="2.85115" y1="0.32385" x2="3.05435" y2="0.33655" layer="21"/>
<rectangle x1="3.14325" y1="0.32385" x2="3.37185" y2="0.33655" layer="21"/>
<rectangle x1="3.47345" y1="0.32385" x2="3.70205" y2="0.33655" layer="21"/>
<rectangle x1="3.80365" y1="0.32385" x2="4.00685" y2="0.33655" layer="21"/>
<rectangle x1="4.10845" y1="0.32385" x2="4.33705" y2="0.33655" layer="21"/>
<rectangle x1="4.45135" y1="0.32385" x2="4.66725" y2="0.33655" layer="21"/>
<rectangle x1="4.78155" y1="0.32385" x2="4.97205" y2="0.33655" layer="21"/>
<rectangle x1="5.08635" y1="0.32385" x2="5.30225" y2="0.33655" layer="21"/>
<rectangle x1="5.40385" y1="0.32385" x2="5.61975" y2="0.33655" layer="21"/>
<rectangle x1="5.96265" y1="0.32385" x2="6.19125" y2="0.33655" layer="21"/>
<rectangle x1="6.30555" y1="0.32385" x2="6.53415" y2="0.33655" layer="21"/>
<rectangle x1="6.66115" y1="0.32385" x2="6.87705" y2="0.33655" layer="21"/>
<rectangle x1="6.97865" y1="0.32385" x2="7.19455" y2="0.33655" layer="21"/>
<rectangle x1="7.30885" y1="0.32385" x2="7.53745" y2="0.33655" layer="21"/>
<rectangle x1="7.63905" y1="0.32385" x2="7.86765" y2="0.33655" layer="21"/>
<rectangle x1="7.99465" y1="0.32385" x2="8.21055" y2="0.33655" layer="21"/>
<rectangle x1="8.36295" y1="0.32385" x2="8.59155" y2="0.33655" layer="21"/>
<rectangle x1="8.82015" y1="0.32385" x2="9.03605" y2="0.33655" layer="21"/>
<rectangle x1="9.13765" y1="0.32385" x2="9.35355" y2="0.33655" layer="21"/>
<rectangle x1="9.46785" y1="0.32385" x2="9.70915" y2="0.33655" layer="21"/>
<rectangle x1="9.81075" y1="0.32385" x2="10.00125" y2="0.33655" layer="21"/>
<rectangle x1="10.11555" y1="0.32385" x2="10.31875" y2="0.33655" layer="21"/>
<rectangle x1="10.64895" y1="0.32385" x2="10.89025" y2="0.33655" layer="21"/>
<rectangle x1="11.11885" y1="0.32385" x2="11.32205" y2="0.33655" layer="21"/>
<rectangle x1="11.43635" y1="0.32385" x2="11.65225" y2="0.33655" layer="21"/>
<rectangle x1="11.76655" y1="0.32385" x2="11.99515" y2="0.33655" layer="21"/>
<rectangle x1="12.09675" y1="0.32385" x2="12.32535" y2="0.33655" layer="21"/>
<rectangle x1="0.12065" y1="0.33655" x2="0.69215" y2="0.34925" layer="21"/>
<rectangle x1="0.79375" y1="0.33655" x2="1.00965" y2="0.34925" layer="21"/>
<rectangle x1="1.12395" y1="0.33655" x2="1.32715" y2="0.34925" layer="21"/>
<rectangle x1="1.44145" y1="0.33655" x2="1.67005" y2="0.34925" layer="21"/>
<rectangle x1="1.89865" y1="0.33655" x2="2.11455" y2="0.34925" layer="21"/>
<rectangle x1="2.21615" y1="0.33655" x2="2.43205" y2="0.34925" layer="21"/>
<rectangle x1="2.53365" y1="0.33655" x2="2.73685" y2="0.34925" layer="21"/>
<rectangle x1="2.85115" y1="0.33655" x2="3.05435" y2="0.34925" layer="21"/>
<rectangle x1="3.14325" y1="0.33655" x2="3.37185" y2="0.34925" layer="21"/>
<rectangle x1="3.47345" y1="0.33655" x2="3.70205" y2="0.34925" layer="21"/>
<rectangle x1="3.80365" y1="0.33655" x2="4.00685" y2="0.34925" layer="21"/>
<rectangle x1="4.10845" y1="0.33655" x2="4.33705" y2="0.34925" layer="21"/>
<rectangle x1="4.45135" y1="0.33655" x2="4.66725" y2="0.34925" layer="21"/>
<rectangle x1="4.78155" y1="0.33655" x2="4.97205" y2="0.34925" layer="21"/>
<rectangle x1="5.07365" y1="0.33655" x2="5.30225" y2="0.34925" layer="21"/>
<rectangle x1="5.40385" y1="0.33655" x2="5.61975" y2="0.34925" layer="21"/>
<rectangle x1="5.96265" y1="0.33655" x2="6.19125" y2="0.34925" layer="21"/>
<rectangle x1="6.30555" y1="0.33655" x2="6.53415" y2="0.34925" layer="21"/>
<rectangle x1="6.66115" y1="0.33655" x2="6.87705" y2="0.34925" layer="21"/>
<rectangle x1="6.97865" y1="0.33655" x2="7.19455" y2="0.34925" layer="21"/>
<rectangle x1="7.30885" y1="0.33655" x2="7.53745" y2="0.34925" layer="21"/>
<rectangle x1="7.63905" y1="0.33655" x2="7.86765" y2="0.34925" layer="21"/>
<rectangle x1="7.99465" y1="0.33655" x2="8.21055" y2="0.34925" layer="21"/>
<rectangle x1="8.36295" y1="0.33655" x2="8.59155" y2="0.34925" layer="21"/>
<rectangle x1="8.82015" y1="0.33655" x2="9.03605" y2="0.34925" layer="21"/>
<rectangle x1="9.13765" y1="0.33655" x2="9.35355" y2="0.34925" layer="21"/>
<rectangle x1="9.46785" y1="0.33655" x2="9.70915" y2="0.34925" layer="21"/>
<rectangle x1="9.81075" y1="0.33655" x2="10.00125" y2="0.34925" layer="21"/>
<rectangle x1="10.11555" y1="0.33655" x2="10.33145" y2="0.34925" layer="21"/>
<rectangle x1="10.64895" y1="0.33655" x2="10.89025" y2="0.34925" layer="21"/>
<rectangle x1="11.11885" y1="0.33655" x2="11.32205" y2="0.34925" layer="21"/>
<rectangle x1="11.43635" y1="0.33655" x2="11.65225" y2="0.34925" layer="21"/>
<rectangle x1="11.76655" y1="0.33655" x2="11.99515" y2="0.34925" layer="21"/>
<rectangle x1="12.09675" y1="0.33655" x2="12.32535" y2="0.34925" layer="21"/>
<rectangle x1="0.12065" y1="0.34925" x2="0.69215" y2="0.36195" layer="21"/>
<rectangle x1="0.79375" y1="0.34925" x2="1.00965" y2="0.36195" layer="21"/>
<rectangle x1="1.12395" y1="0.34925" x2="1.32715" y2="0.36195" layer="21"/>
<rectangle x1="1.44145" y1="0.34925" x2="1.67005" y2="0.36195" layer="21"/>
<rectangle x1="1.89865" y1="0.34925" x2="2.11455" y2="0.36195" layer="21"/>
<rectangle x1="2.21615" y1="0.34925" x2="2.43205" y2="0.36195" layer="21"/>
<rectangle x1="2.53365" y1="0.34925" x2="2.73685" y2="0.36195" layer="21"/>
<rectangle x1="2.85115" y1="0.34925" x2="3.05435" y2="0.36195" layer="21"/>
<rectangle x1="3.14325" y1="0.34925" x2="3.37185" y2="0.36195" layer="21"/>
<rectangle x1="3.47345" y1="0.34925" x2="3.70205" y2="0.36195" layer="21"/>
<rectangle x1="3.80365" y1="0.34925" x2="4.00685" y2="0.36195" layer="21"/>
<rectangle x1="4.10845" y1="0.34925" x2="4.33705" y2="0.36195" layer="21"/>
<rectangle x1="4.45135" y1="0.34925" x2="4.66725" y2="0.36195" layer="21"/>
<rectangle x1="4.78155" y1="0.34925" x2="4.97205" y2="0.36195" layer="21"/>
<rectangle x1="5.07365" y1="0.34925" x2="5.30225" y2="0.36195" layer="21"/>
<rectangle x1="5.40385" y1="0.34925" x2="5.61975" y2="0.36195" layer="21"/>
<rectangle x1="5.96265" y1="0.34925" x2="6.19125" y2="0.36195" layer="21"/>
<rectangle x1="6.30555" y1="0.34925" x2="6.54685" y2="0.36195" layer="21"/>
<rectangle x1="6.66115" y1="0.34925" x2="6.87705" y2="0.36195" layer="21"/>
<rectangle x1="6.97865" y1="0.34925" x2="7.19455" y2="0.36195" layer="21"/>
<rectangle x1="7.30885" y1="0.34925" x2="7.53745" y2="0.36195" layer="21"/>
<rectangle x1="7.63905" y1="0.34925" x2="7.86765" y2="0.36195" layer="21"/>
<rectangle x1="7.99465" y1="0.34925" x2="8.21055" y2="0.36195" layer="21"/>
<rectangle x1="8.36295" y1="0.34925" x2="8.59155" y2="0.36195" layer="21"/>
<rectangle x1="8.82015" y1="0.34925" x2="9.03605" y2="0.36195" layer="21"/>
<rectangle x1="9.13765" y1="0.34925" x2="9.35355" y2="0.36195" layer="21"/>
<rectangle x1="9.46785" y1="0.34925" x2="9.70915" y2="0.36195" layer="21"/>
<rectangle x1="9.81075" y1="0.34925" x2="10.00125" y2="0.36195" layer="21"/>
<rectangle x1="10.11555" y1="0.34925" x2="10.33145" y2="0.36195" layer="21"/>
<rectangle x1="10.64895" y1="0.34925" x2="10.89025" y2="0.36195" layer="21"/>
<rectangle x1="11.11885" y1="0.34925" x2="11.32205" y2="0.36195" layer="21"/>
<rectangle x1="11.43635" y1="0.34925" x2="11.65225" y2="0.36195" layer="21"/>
<rectangle x1="11.76655" y1="0.34925" x2="11.99515" y2="0.36195" layer="21"/>
<rectangle x1="12.09675" y1="0.34925" x2="12.32535" y2="0.36195" layer="21"/>
<rectangle x1="0.12065" y1="0.36195" x2="0.69215" y2="0.37465" layer="21"/>
<rectangle x1="0.78105" y1="0.36195" x2="1.00965" y2="0.37465" layer="21"/>
<rectangle x1="1.12395" y1="0.36195" x2="1.32715" y2="0.37465" layer="21"/>
<rectangle x1="1.44145" y1="0.36195" x2="1.67005" y2="0.37465" layer="21"/>
<rectangle x1="1.89865" y1="0.36195" x2="2.11455" y2="0.37465" layer="21"/>
<rectangle x1="2.21615" y1="0.36195" x2="2.43205" y2="0.37465" layer="21"/>
<rectangle x1="2.53365" y1="0.36195" x2="2.73685" y2="0.37465" layer="21"/>
<rectangle x1="2.85115" y1="0.36195" x2="3.05435" y2="0.37465" layer="21"/>
<rectangle x1="3.14325" y1="0.36195" x2="3.37185" y2="0.37465" layer="21"/>
<rectangle x1="3.47345" y1="0.36195" x2="3.70205" y2="0.37465" layer="21"/>
<rectangle x1="3.80365" y1="0.36195" x2="4.00685" y2="0.37465" layer="21"/>
<rectangle x1="4.10845" y1="0.36195" x2="4.33705" y2="0.37465" layer="21"/>
<rectangle x1="4.45135" y1="0.36195" x2="4.66725" y2="0.37465" layer="21"/>
<rectangle x1="4.78155" y1="0.36195" x2="4.97205" y2="0.37465" layer="21"/>
<rectangle x1="5.07365" y1="0.36195" x2="5.30225" y2="0.37465" layer="21"/>
<rectangle x1="5.40385" y1="0.36195" x2="5.61975" y2="0.37465" layer="21"/>
<rectangle x1="5.96265" y1="0.36195" x2="6.19125" y2="0.37465" layer="21"/>
<rectangle x1="6.30555" y1="0.36195" x2="6.54685" y2="0.37465" layer="21"/>
<rectangle x1="6.66115" y1="0.36195" x2="6.87705" y2="0.37465" layer="21"/>
<rectangle x1="6.97865" y1="0.36195" x2="7.19455" y2="0.37465" layer="21"/>
<rectangle x1="7.30885" y1="0.36195" x2="7.53745" y2="0.37465" layer="21"/>
<rectangle x1="7.63905" y1="0.36195" x2="7.86765" y2="0.37465" layer="21"/>
<rectangle x1="7.99465" y1="0.36195" x2="8.21055" y2="0.37465" layer="21"/>
<rectangle x1="8.36295" y1="0.36195" x2="8.59155" y2="0.37465" layer="21"/>
<rectangle x1="8.82015" y1="0.36195" x2="9.03605" y2="0.37465" layer="21"/>
<rectangle x1="9.13765" y1="0.36195" x2="9.35355" y2="0.37465" layer="21"/>
<rectangle x1="9.46785" y1="0.36195" x2="9.70915" y2="0.37465" layer="21"/>
<rectangle x1="9.81075" y1="0.36195" x2="10.00125" y2="0.37465" layer="21"/>
<rectangle x1="10.11555" y1="0.36195" x2="10.33145" y2="0.37465" layer="21"/>
<rectangle x1="10.64895" y1="0.36195" x2="10.89025" y2="0.37465" layer="21"/>
<rectangle x1="11.11885" y1="0.36195" x2="11.32205" y2="0.37465" layer="21"/>
<rectangle x1="11.43635" y1="0.36195" x2="11.65225" y2="0.37465" layer="21"/>
<rectangle x1="11.76655" y1="0.36195" x2="11.99515" y2="0.37465" layer="21"/>
<rectangle x1="12.09675" y1="0.36195" x2="12.32535" y2="0.37465" layer="21"/>
<rectangle x1="0.12065" y1="0.37465" x2="0.69215" y2="0.38735" layer="21"/>
<rectangle x1="0.78105" y1="0.37465" x2="1.00965" y2="0.38735" layer="21"/>
<rectangle x1="1.12395" y1="0.37465" x2="1.33985" y2="0.38735" layer="21"/>
<rectangle x1="1.44145" y1="0.37465" x2="1.67005" y2="0.38735" layer="21"/>
<rectangle x1="1.89865" y1="0.37465" x2="2.11455" y2="0.38735" layer="21"/>
<rectangle x1="2.21615" y1="0.37465" x2="2.43205" y2="0.38735" layer="21"/>
<rectangle x1="2.53365" y1="0.37465" x2="2.73685" y2="0.38735" layer="21"/>
<rectangle x1="2.85115" y1="0.37465" x2="3.05435" y2="0.38735" layer="21"/>
<rectangle x1="3.14325" y1="0.37465" x2="3.37185" y2="0.38735" layer="21"/>
<rectangle x1="3.47345" y1="0.37465" x2="3.70205" y2="0.38735" layer="21"/>
<rectangle x1="3.80365" y1="0.37465" x2="4.00685" y2="0.38735" layer="21"/>
<rectangle x1="4.10845" y1="0.37465" x2="4.33705" y2="0.38735" layer="21"/>
<rectangle x1="4.45135" y1="0.37465" x2="4.66725" y2="0.38735" layer="21"/>
<rectangle x1="4.78155" y1="0.37465" x2="4.97205" y2="0.38735" layer="21"/>
<rectangle x1="5.07365" y1="0.37465" x2="5.30225" y2="0.38735" layer="21"/>
<rectangle x1="5.40385" y1="0.37465" x2="5.61975" y2="0.38735" layer="21"/>
<rectangle x1="5.96265" y1="0.37465" x2="6.19125" y2="0.38735" layer="21"/>
<rectangle x1="6.30555" y1="0.37465" x2="6.54685" y2="0.38735" layer="21"/>
<rectangle x1="6.66115" y1="0.37465" x2="6.87705" y2="0.38735" layer="21"/>
<rectangle x1="6.97865" y1="0.37465" x2="7.19455" y2="0.38735" layer="21"/>
<rectangle x1="7.30885" y1="0.37465" x2="7.53745" y2="0.38735" layer="21"/>
<rectangle x1="7.63905" y1="0.37465" x2="7.86765" y2="0.38735" layer="21"/>
<rectangle x1="7.99465" y1="0.37465" x2="8.21055" y2="0.38735" layer="21"/>
<rectangle x1="8.36295" y1="0.37465" x2="8.59155" y2="0.38735" layer="21"/>
<rectangle x1="8.82015" y1="0.37465" x2="9.03605" y2="0.38735" layer="21"/>
<rectangle x1="9.13765" y1="0.37465" x2="9.36625" y2="0.38735" layer="21"/>
<rectangle x1="9.46785" y1="0.37465" x2="9.70915" y2="0.38735" layer="21"/>
<rectangle x1="9.81075" y1="0.37465" x2="10.00125" y2="0.38735" layer="21"/>
<rectangle x1="10.11555" y1="0.37465" x2="10.33145" y2="0.38735" layer="21"/>
<rectangle x1="10.64895" y1="0.37465" x2="10.89025" y2="0.38735" layer="21"/>
<rectangle x1="11.11885" y1="0.37465" x2="11.32205" y2="0.38735" layer="21"/>
<rectangle x1="11.43635" y1="0.37465" x2="11.65225" y2="0.38735" layer="21"/>
<rectangle x1="11.76655" y1="0.37465" x2="11.99515" y2="0.38735" layer="21"/>
<rectangle x1="12.09675" y1="0.37465" x2="12.32535" y2="0.38735" layer="21"/>
<rectangle x1="0.13335" y1="0.38735" x2="0.69215" y2="0.40005" layer="21"/>
<rectangle x1="0.78105" y1="0.38735" x2="1.00965" y2="0.40005" layer="21"/>
<rectangle x1="1.12395" y1="0.38735" x2="1.33985" y2="0.40005" layer="21"/>
<rectangle x1="1.44145" y1="0.38735" x2="1.67005" y2="0.40005" layer="21"/>
<rectangle x1="1.88595" y1="0.38735" x2="2.11455" y2="0.40005" layer="21"/>
<rectangle x1="2.21615" y1="0.38735" x2="2.44475" y2="0.40005" layer="21"/>
<rectangle x1="2.53365" y1="0.38735" x2="2.73685" y2="0.40005" layer="21"/>
<rectangle x1="2.85115" y1="0.38735" x2="3.05435" y2="0.40005" layer="21"/>
<rectangle x1="3.14325" y1="0.38735" x2="3.37185" y2="0.40005" layer="21"/>
<rectangle x1="3.47345" y1="0.38735" x2="3.70205" y2="0.40005" layer="21"/>
<rectangle x1="3.80365" y1="0.38735" x2="4.00685" y2="0.40005" layer="21"/>
<rectangle x1="4.10845" y1="0.38735" x2="4.33705" y2="0.40005" layer="21"/>
<rectangle x1="4.45135" y1="0.38735" x2="4.66725" y2="0.40005" layer="21"/>
<rectangle x1="4.78155" y1="0.38735" x2="4.97205" y2="0.40005" layer="21"/>
<rectangle x1="5.07365" y1="0.38735" x2="5.30225" y2="0.40005" layer="21"/>
<rectangle x1="5.40385" y1="0.38735" x2="5.61975" y2="0.40005" layer="21"/>
<rectangle x1="5.96265" y1="0.38735" x2="6.19125" y2="0.40005" layer="21"/>
<rectangle x1="6.31825" y1="0.38735" x2="6.54685" y2="0.40005" layer="21"/>
<rectangle x1="6.64845" y1="0.38735" x2="6.87705" y2="0.40005" layer="21"/>
<rectangle x1="6.97865" y1="0.38735" x2="7.20725" y2="0.40005" layer="21"/>
<rectangle x1="7.30885" y1="0.38735" x2="7.53745" y2="0.40005" layer="21"/>
<rectangle x1="7.63905" y1="0.38735" x2="7.86765" y2="0.40005" layer="21"/>
<rectangle x1="7.99465" y1="0.38735" x2="8.21055" y2="0.40005" layer="21"/>
<rectangle x1="8.36295" y1="0.38735" x2="8.59155" y2="0.40005" layer="21"/>
<rectangle x1="8.82015" y1="0.38735" x2="9.03605" y2="0.40005" layer="21"/>
<rectangle x1="9.13765" y1="0.38735" x2="9.36625" y2="0.40005" layer="21"/>
<rectangle x1="9.46785" y1="0.38735" x2="9.70915" y2="0.40005" layer="21"/>
<rectangle x1="9.81075" y1="0.38735" x2="10.00125" y2="0.40005" layer="21"/>
<rectangle x1="10.11555" y1="0.38735" x2="10.33145" y2="0.40005" layer="21"/>
<rectangle x1="10.64895" y1="0.38735" x2="10.89025" y2="0.40005" layer="21"/>
<rectangle x1="11.11885" y1="0.38735" x2="11.32205" y2="0.40005" layer="21"/>
<rectangle x1="11.43635" y1="0.38735" x2="11.65225" y2="0.40005" layer="21"/>
<rectangle x1="11.76655" y1="0.38735" x2="11.99515" y2="0.40005" layer="21"/>
<rectangle x1="12.09675" y1="0.38735" x2="12.32535" y2="0.40005" layer="21"/>
<rectangle x1="0.13335" y1="0.40005" x2="0.69215" y2="0.41275" layer="21"/>
<rectangle x1="0.78105" y1="0.40005" x2="1.00965" y2="0.41275" layer="21"/>
<rectangle x1="1.12395" y1="0.40005" x2="1.33985" y2="0.41275" layer="21"/>
<rectangle x1="1.44145" y1="0.40005" x2="1.67005" y2="0.41275" layer="21"/>
<rectangle x1="1.88595" y1="0.40005" x2="2.11455" y2="0.41275" layer="21"/>
<rectangle x1="2.21615" y1="0.40005" x2="2.44475" y2="0.41275" layer="21"/>
<rectangle x1="2.53365" y1="0.40005" x2="2.73685" y2="0.41275" layer="21"/>
<rectangle x1="2.83845" y1="0.40005" x2="3.05435" y2="0.41275" layer="21"/>
<rectangle x1="3.14325" y1="0.40005" x2="3.37185" y2="0.41275" layer="21"/>
<rectangle x1="3.47345" y1="0.40005" x2="3.70205" y2="0.41275" layer="21"/>
<rectangle x1="3.80365" y1="0.40005" x2="4.00685" y2="0.41275" layer="21"/>
<rectangle x1="4.10845" y1="0.40005" x2="4.33705" y2="0.41275" layer="21"/>
<rectangle x1="4.43865" y1="0.40005" x2="4.66725" y2="0.41275" layer="21"/>
<rectangle x1="4.78155" y1="0.40005" x2="4.97205" y2="0.41275" layer="21"/>
<rectangle x1="5.07365" y1="0.40005" x2="5.30225" y2="0.41275" layer="21"/>
<rectangle x1="5.40385" y1="0.40005" x2="5.61975" y2="0.41275" layer="21"/>
<rectangle x1="5.96265" y1="0.40005" x2="6.19125" y2="0.41275" layer="21"/>
<rectangle x1="6.31825" y1="0.40005" x2="6.54685" y2="0.41275" layer="21"/>
<rectangle x1="6.64845" y1="0.40005" x2="6.87705" y2="0.41275" layer="21"/>
<rectangle x1="6.97865" y1="0.40005" x2="7.20725" y2="0.41275" layer="21"/>
<rectangle x1="7.30885" y1="0.40005" x2="7.53745" y2="0.41275" layer="21"/>
<rectangle x1="7.63905" y1="0.40005" x2="7.86765" y2="0.41275" layer="21"/>
<rectangle x1="7.99465" y1="0.40005" x2="8.21055" y2="0.41275" layer="21"/>
<rectangle x1="8.36295" y1="0.40005" x2="8.59155" y2="0.41275" layer="21"/>
<rectangle x1="8.82015" y1="0.40005" x2="9.03605" y2="0.41275" layer="21"/>
<rectangle x1="9.13765" y1="0.40005" x2="9.36625" y2="0.41275" layer="21"/>
<rectangle x1="9.46785" y1="0.40005" x2="9.70915" y2="0.41275" layer="21"/>
<rectangle x1="9.81075" y1="0.40005" x2="10.00125" y2="0.41275" layer="21"/>
<rectangle x1="10.11555" y1="0.40005" x2="10.31875" y2="0.41275" layer="21"/>
<rectangle x1="10.64895" y1="0.40005" x2="10.89025" y2="0.41275" layer="21"/>
<rectangle x1="11.11885" y1="0.40005" x2="11.32205" y2="0.41275" layer="21"/>
<rectangle x1="11.43635" y1="0.40005" x2="11.65225" y2="0.41275" layer="21"/>
<rectangle x1="11.76655" y1="0.40005" x2="11.99515" y2="0.41275" layer="21"/>
<rectangle x1="12.09675" y1="0.40005" x2="12.32535" y2="0.41275" layer="21"/>
<rectangle x1="0.13335" y1="0.41275" x2="0.69215" y2="0.42545" layer="21"/>
<rectangle x1="0.78105" y1="0.41275" x2="1.00965" y2="0.42545" layer="21"/>
<rectangle x1="1.12395" y1="0.41275" x2="1.33985" y2="0.42545" layer="21"/>
<rectangle x1="1.44145" y1="0.41275" x2="1.67005" y2="0.42545" layer="21"/>
<rectangle x1="1.88595" y1="0.41275" x2="2.11455" y2="0.42545" layer="21"/>
<rectangle x1="2.21615" y1="0.41275" x2="2.44475" y2="0.42545" layer="21"/>
<rectangle x1="2.83845" y1="0.41275" x2="3.05435" y2="0.42545" layer="21"/>
<rectangle x1="3.14325" y1="0.41275" x2="3.37185" y2="0.42545" layer="21"/>
<rectangle x1="3.47345" y1="0.41275" x2="3.70205" y2="0.42545" layer="21"/>
<rectangle x1="3.80365" y1="0.41275" x2="4.00685" y2="0.42545" layer="21"/>
<rectangle x1="4.10845" y1="0.41275" x2="4.33705" y2="0.42545" layer="21"/>
<rectangle x1="4.43865" y1="0.41275" x2="4.66725" y2="0.42545" layer="21"/>
<rectangle x1="4.78155" y1="0.41275" x2="4.97205" y2="0.42545" layer="21"/>
<rectangle x1="5.07365" y1="0.41275" x2="5.30225" y2="0.42545" layer="21"/>
<rectangle x1="5.40385" y1="0.41275" x2="5.61975" y2="0.42545" layer="21"/>
<rectangle x1="5.96265" y1="0.41275" x2="6.19125" y2="0.42545" layer="21"/>
<rectangle x1="6.31825" y1="0.41275" x2="6.54685" y2="0.42545" layer="21"/>
<rectangle x1="6.64845" y1="0.41275" x2="6.87705" y2="0.42545" layer="21"/>
<rectangle x1="6.97865" y1="0.41275" x2="7.20725" y2="0.42545" layer="21"/>
<rectangle x1="7.30885" y1="0.41275" x2="7.53745" y2="0.42545" layer="21"/>
<rectangle x1="7.63905" y1="0.41275" x2="7.86765" y2="0.42545" layer="21"/>
<rectangle x1="7.99465" y1="0.41275" x2="8.21055" y2="0.42545" layer="21"/>
<rectangle x1="8.36295" y1="0.41275" x2="8.59155" y2="0.42545" layer="21"/>
<rectangle x1="8.80745" y1="0.41275" x2="9.03605" y2="0.42545" layer="21"/>
<rectangle x1="9.13765" y1="0.41275" x2="9.36625" y2="0.42545" layer="21"/>
<rectangle x1="9.46785" y1="0.41275" x2="9.70915" y2="0.42545" layer="21"/>
<rectangle x1="10.10285" y1="0.41275" x2="10.31875" y2="0.42545" layer="21"/>
<rectangle x1="10.64895" y1="0.41275" x2="10.89025" y2="0.42545" layer="21"/>
<rectangle x1="11.11885" y1="0.41275" x2="11.33475" y2="0.42545" layer="21"/>
<rectangle x1="11.43635" y1="0.41275" x2="11.65225" y2="0.42545" layer="21"/>
<rectangle x1="11.76655" y1="0.41275" x2="11.99515" y2="0.42545" layer="21"/>
<rectangle x1="12.09675" y1="0.41275" x2="12.32535" y2="0.42545" layer="21"/>
<rectangle x1="0.13335" y1="0.42545" x2="0.67945" y2="0.43815" layer="21"/>
<rectangle x1="0.78105" y1="0.42545" x2="1.00965" y2="0.43815" layer="21"/>
<rectangle x1="1.12395" y1="0.42545" x2="1.33985" y2="0.43815" layer="21"/>
<rectangle x1="1.44145" y1="0.42545" x2="1.67005" y2="0.43815" layer="21"/>
<rectangle x1="1.88595" y1="0.42545" x2="2.11455" y2="0.43815" layer="21"/>
<rectangle x1="2.21615" y1="0.42545" x2="2.44475" y2="0.43815" layer="21"/>
<rectangle x1="2.81305" y1="0.42545" x2="3.05435" y2="0.43815" layer="21"/>
<rectangle x1="3.14325" y1="0.42545" x2="3.37185" y2="0.43815" layer="21"/>
<rectangle x1="3.47345" y1="0.42545" x2="3.70205" y2="0.43815" layer="21"/>
<rectangle x1="3.80365" y1="0.42545" x2="4.00685" y2="0.43815" layer="21"/>
<rectangle x1="4.10845" y1="0.42545" x2="4.33705" y2="0.43815" layer="21"/>
<rectangle x1="4.43865" y1="0.42545" x2="4.66725" y2="0.43815" layer="21"/>
<rectangle x1="4.78155" y1="0.42545" x2="4.97205" y2="0.43815" layer="21"/>
<rectangle x1="5.07365" y1="0.42545" x2="5.30225" y2="0.43815" layer="21"/>
<rectangle x1="5.40385" y1="0.42545" x2="5.61975" y2="0.43815" layer="21"/>
<rectangle x1="5.96265" y1="0.42545" x2="6.19125" y2="0.43815" layer="21"/>
<rectangle x1="6.31825" y1="0.42545" x2="6.54685" y2="0.43815" layer="21"/>
<rectangle x1="6.64845" y1="0.42545" x2="6.87705" y2="0.43815" layer="21"/>
<rectangle x1="6.97865" y1="0.42545" x2="7.20725" y2="0.43815" layer="21"/>
<rectangle x1="7.30885" y1="0.42545" x2="7.53745" y2="0.43815" layer="21"/>
<rectangle x1="7.63905" y1="0.42545" x2="7.86765" y2="0.43815" layer="21"/>
<rectangle x1="7.99465" y1="0.42545" x2="8.21055" y2="0.43815" layer="21"/>
<rectangle x1="8.36295" y1="0.42545" x2="8.59155" y2="0.43815" layer="21"/>
<rectangle x1="8.80745" y1="0.42545" x2="9.03605" y2="0.43815" layer="21"/>
<rectangle x1="9.13765" y1="0.42545" x2="9.36625" y2="0.43815" layer="21"/>
<rectangle x1="9.46785" y1="0.42545" x2="9.70915" y2="0.43815" layer="21"/>
<rectangle x1="10.09015" y1="0.42545" x2="10.31875" y2="0.43815" layer="21"/>
<rectangle x1="10.64895" y1="0.42545" x2="10.89025" y2="0.43815" layer="21"/>
<rectangle x1="11.11885" y1="0.42545" x2="11.33475" y2="0.43815" layer="21"/>
<rectangle x1="11.43635" y1="0.42545" x2="11.65225" y2="0.43815" layer="21"/>
<rectangle x1="11.76655" y1="0.42545" x2="11.99515" y2="0.43815" layer="21"/>
<rectangle x1="12.09675" y1="0.42545" x2="12.32535" y2="0.43815" layer="21"/>
<rectangle x1="0.13335" y1="0.43815" x2="0.67945" y2="0.45085" layer="21"/>
<rectangle x1="0.78105" y1="0.43815" x2="1.00965" y2="0.45085" layer="21"/>
<rectangle x1="1.12395" y1="0.43815" x2="1.33985" y2="0.45085" layer="21"/>
<rectangle x1="1.44145" y1="0.43815" x2="1.67005" y2="0.45085" layer="21"/>
<rectangle x1="1.88595" y1="0.43815" x2="2.11455" y2="0.45085" layer="21"/>
<rectangle x1="2.21615" y1="0.43815" x2="2.44475" y2="0.45085" layer="21"/>
<rectangle x1="2.80035" y1="0.43815" x2="3.05435" y2="0.45085" layer="21"/>
<rectangle x1="3.14325" y1="0.43815" x2="3.37185" y2="0.45085" layer="21"/>
<rectangle x1="3.47345" y1="0.43815" x2="3.70205" y2="0.45085" layer="21"/>
<rectangle x1="3.80365" y1="0.43815" x2="4.01955" y2="0.45085" layer="21"/>
<rectangle x1="4.10845" y1="0.43815" x2="4.33705" y2="0.45085" layer="21"/>
<rectangle x1="4.43865" y1="0.43815" x2="4.66725" y2="0.45085" layer="21"/>
<rectangle x1="4.78155" y1="0.43815" x2="4.97205" y2="0.45085" layer="21"/>
<rectangle x1="5.07365" y1="0.43815" x2="5.30225" y2="0.45085" layer="21"/>
<rectangle x1="5.40385" y1="0.43815" x2="5.61975" y2="0.45085" layer="21"/>
<rectangle x1="5.96265" y1="0.43815" x2="6.19125" y2="0.45085" layer="21"/>
<rectangle x1="6.31825" y1="0.43815" x2="6.54685" y2="0.45085" layer="21"/>
<rectangle x1="6.64845" y1="0.43815" x2="6.87705" y2="0.45085" layer="21"/>
<rectangle x1="6.97865" y1="0.43815" x2="7.20725" y2="0.45085" layer="21"/>
<rectangle x1="7.30885" y1="0.43815" x2="7.53745" y2="0.45085" layer="21"/>
<rectangle x1="7.63905" y1="0.43815" x2="7.86765" y2="0.45085" layer="21"/>
<rectangle x1="7.99465" y1="0.43815" x2="8.21055" y2="0.45085" layer="21"/>
<rectangle x1="8.36295" y1="0.43815" x2="8.59155" y2="0.45085" layer="21"/>
<rectangle x1="8.80745" y1="0.43815" x2="9.03605" y2="0.45085" layer="21"/>
<rectangle x1="9.13765" y1="0.43815" x2="9.36625" y2="0.45085" layer="21"/>
<rectangle x1="9.46785" y1="0.43815" x2="9.70915" y2="0.45085" layer="21"/>
<rectangle x1="10.06475" y1="0.43815" x2="10.31875" y2="0.45085" layer="21"/>
<rectangle x1="10.64895" y1="0.43815" x2="10.89025" y2="0.45085" layer="21"/>
<rectangle x1="11.11885" y1="0.43815" x2="11.33475" y2="0.45085" layer="21"/>
<rectangle x1="11.43635" y1="0.43815" x2="11.65225" y2="0.45085" layer="21"/>
<rectangle x1="11.76655" y1="0.43815" x2="11.99515" y2="0.45085" layer="21"/>
<rectangle x1="12.09675" y1="0.43815" x2="12.32535" y2="0.45085" layer="21"/>
<rectangle x1="0.13335" y1="0.45085" x2="0.67945" y2="0.46355" layer="21"/>
<rectangle x1="0.78105" y1="0.45085" x2="1.00965" y2="0.46355" layer="21"/>
<rectangle x1="1.12395" y1="0.45085" x2="1.33985" y2="0.46355" layer="21"/>
<rectangle x1="1.44145" y1="0.45085" x2="1.67005" y2="0.46355" layer="21"/>
<rectangle x1="1.88595" y1="0.45085" x2="2.11455" y2="0.46355" layer="21"/>
<rectangle x1="2.21615" y1="0.45085" x2="2.44475" y2="0.46355" layer="21"/>
<rectangle x1="2.77495" y1="0.45085" x2="3.04165" y2="0.46355" layer="21"/>
<rectangle x1="3.14325" y1="0.45085" x2="3.37185" y2="0.46355" layer="21"/>
<rectangle x1="3.47345" y1="0.45085" x2="3.70205" y2="0.46355" layer="21"/>
<rectangle x1="3.80365" y1="0.45085" x2="4.01955" y2="0.46355" layer="21"/>
<rectangle x1="4.10845" y1="0.45085" x2="4.33705" y2="0.46355" layer="21"/>
<rectangle x1="4.43865" y1="0.45085" x2="4.66725" y2="0.46355" layer="21"/>
<rectangle x1="4.78155" y1="0.45085" x2="4.98475" y2="0.46355" layer="21"/>
<rectangle x1="5.07365" y1="0.45085" x2="5.30225" y2="0.46355" layer="21"/>
<rectangle x1="5.40385" y1="0.45085" x2="5.61975" y2="0.46355" layer="21"/>
<rectangle x1="5.94995" y1="0.45085" x2="6.19125" y2="0.46355" layer="21"/>
<rectangle x1="6.31825" y1="0.45085" x2="6.54685" y2="0.46355" layer="21"/>
<rectangle x1="6.64845" y1="0.45085" x2="6.87705" y2="0.46355" layer="21"/>
<rectangle x1="6.97865" y1="0.45085" x2="7.20725" y2="0.46355" layer="21"/>
<rectangle x1="7.30885" y1="0.45085" x2="7.53745" y2="0.46355" layer="21"/>
<rectangle x1="7.63905" y1="0.45085" x2="7.86765" y2="0.46355" layer="21"/>
<rectangle x1="7.99465" y1="0.45085" x2="8.21055" y2="0.46355" layer="21"/>
<rectangle x1="8.36295" y1="0.45085" x2="8.59155" y2="0.46355" layer="21"/>
<rectangle x1="8.80745" y1="0.45085" x2="9.03605" y2="0.46355" layer="21"/>
<rectangle x1="9.13765" y1="0.45085" x2="9.36625" y2="0.46355" layer="21"/>
<rectangle x1="9.46785" y1="0.45085" x2="9.70915" y2="0.46355" layer="21"/>
<rectangle x1="10.05205" y1="0.45085" x2="10.31875" y2="0.46355" layer="21"/>
<rectangle x1="10.64895" y1="0.45085" x2="10.89025" y2="0.46355" layer="21"/>
<rectangle x1="11.11885" y1="0.45085" x2="11.33475" y2="0.46355" layer="21"/>
<rectangle x1="11.43635" y1="0.45085" x2="11.65225" y2="0.46355" layer="21"/>
<rectangle x1="11.76655" y1="0.45085" x2="11.99515" y2="0.46355" layer="21"/>
<rectangle x1="12.09675" y1="0.45085" x2="12.32535" y2="0.46355" layer="21"/>
<rectangle x1="0.13335" y1="0.46355" x2="0.67945" y2="0.47625" layer="21"/>
<rectangle x1="0.78105" y1="0.46355" x2="1.00965" y2="0.47625" layer="21"/>
<rectangle x1="1.44145" y1="0.46355" x2="1.67005" y2="0.47625" layer="21"/>
<rectangle x1="1.88595" y1="0.46355" x2="2.11455" y2="0.47625" layer="21"/>
<rectangle x1="2.21615" y1="0.46355" x2="2.44475" y2="0.47625" layer="21"/>
<rectangle x1="2.74955" y1="0.46355" x2="3.04165" y2="0.47625" layer="21"/>
<rectangle x1="3.14325" y1="0.46355" x2="3.37185" y2="0.47625" layer="21"/>
<rectangle x1="3.47345" y1="0.46355" x2="3.70205" y2="0.47625" layer="21"/>
<rectangle x1="3.80365" y1="0.46355" x2="4.01955" y2="0.47625" layer="21"/>
<rectangle x1="4.10845" y1="0.46355" x2="4.33705" y2="0.47625" layer="21"/>
<rectangle x1="4.43865" y1="0.46355" x2="4.66725" y2="0.47625" layer="21"/>
<rectangle x1="5.07365" y1="0.46355" x2="5.30225" y2="0.47625" layer="21"/>
<rectangle x1="5.94995" y1="0.46355" x2="6.19125" y2="0.47625" layer="21"/>
<rectangle x1="6.31825" y1="0.46355" x2="6.54685" y2="0.47625" layer="21"/>
<rectangle x1="6.64845" y1="0.46355" x2="6.87705" y2="0.47625" layer="21"/>
<rectangle x1="6.97865" y1="0.46355" x2="7.20725" y2="0.47625" layer="21"/>
<rectangle x1="7.30885" y1="0.46355" x2="7.53745" y2="0.47625" layer="21"/>
<rectangle x1="7.63905" y1="0.46355" x2="7.86765" y2="0.47625" layer="21"/>
<rectangle x1="7.99465" y1="0.46355" x2="8.21055" y2="0.47625" layer="21"/>
<rectangle x1="8.36295" y1="0.46355" x2="8.59155" y2="0.47625" layer="21"/>
<rectangle x1="8.80745" y1="0.46355" x2="9.03605" y2="0.47625" layer="21"/>
<rectangle x1="9.13765" y1="0.46355" x2="9.36625" y2="0.47625" layer="21"/>
<rectangle x1="9.46785" y1="0.46355" x2="9.70915" y2="0.47625" layer="21"/>
<rectangle x1="10.02665" y1="0.46355" x2="10.30605" y2="0.47625" layer="21"/>
<rectangle x1="10.64895" y1="0.46355" x2="10.89025" y2="0.47625" layer="21"/>
<rectangle x1="11.13155" y1="0.46355" x2="11.34745" y2="0.47625" layer="21"/>
<rectangle x1="11.43635" y1="0.46355" x2="11.65225" y2="0.47625" layer="21"/>
<rectangle x1="11.76655" y1="0.46355" x2="11.99515" y2="0.47625" layer="21"/>
<rectangle x1="12.09675" y1="0.46355" x2="12.32535" y2="0.47625" layer="21"/>
<rectangle x1="0.14605" y1="0.47625" x2="0.36195" y2="0.48895" layer="21"/>
<rectangle x1="0.46355" y1="0.47625" x2="0.67945" y2="0.48895" layer="21"/>
<rectangle x1="0.78105" y1="0.47625" x2="1.00965" y2="0.48895" layer="21"/>
<rectangle x1="1.44145" y1="0.47625" x2="1.67005" y2="0.48895" layer="21"/>
<rectangle x1="1.88595" y1="0.47625" x2="2.11455" y2="0.48895" layer="21"/>
<rectangle x1="2.21615" y1="0.47625" x2="2.44475" y2="0.48895" layer="21"/>
<rectangle x1="2.73685" y1="0.47625" x2="3.04165" y2="0.48895" layer="21"/>
<rectangle x1="3.14325" y1="0.47625" x2="3.37185" y2="0.48895" layer="21"/>
<rectangle x1="3.47345" y1="0.47625" x2="3.70205" y2="0.48895" layer="21"/>
<rectangle x1="3.81635" y1="0.47625" x2="4.03225" y2="0.48895" layer="21"/>
<rectangle x1="4.10845" y1="0.47625" x2="4.33705" y2="0.48895" layer="21"/>
<rectangle x1="4.43865" y1="0.47625" x2="4.66725" y2="0.48895" layer="21"/>
<rectangle x1="5.07365" y1="0.47625" x2="5.30225" y2="0.48895" layer="21"/>
<rectangle x1="5.94995" y1="0.47625" x2="6.19125" y2="0.48895" layer="21"/>
<rectangle x1="6.31825" y1="0.47625" x2="6.54685" y2="0.48895" layer="21"/>
<rectangle x1="6.64845" y1="0.47625" x2="6.87705" y2="0.48895" layer="21"/>
<rectangle x1="6.97865" y1="0.47625" x2="7.20725" y2="0.48895" layer="21"/>
<rectangle x1="7.30885" y1="0.47625" x2="7.53745" y2="0.48895" layer="21"/>
<rectangle x1="7.63905" y1="0.47625" x2="7.86765" y2="0.48895" layer="21"/>
<rectangle x1="7.99465" y1="0.47625" x2="8.21055" y2="0.48895" layer="21"/>
<rectangle x1="8.36295" y1="0.47625" x2="8.59155" y2="0.48895" layer="21"/>
<rectangle x1="8.80745" y1="0.47625" x2="9.03605" y2="0.48895" layer="21"/>
<rectangle x1="9.13765" y1="0.47625" x2="9.36625" y2="0.48895" layer="21"/>
<rectangle x1="9.46785" y1="0.47625" x2="9.70915" y2="0.48895" layer="21"/>
<rectangle x1="10.00125" y1="0.47625" x2="10.30605" y2="0.48895" layer="21"/>
<rectangle x1="10.64895" y1="0.47625" x2="10.89025" y2="0.48895" layer="21"/>
<rectangle x1="11.13155" y1="0.47625" x2="11.34745" y2="0.48895" layer="21"/>
<rectangle x1="11.43635" y1="0.47625" x2="11.65225" y2="0.48895" layer="21"/>
<rectangle x1="11.76655" y1="0.47625" x2="11.99515" y2="0.48895" layer="21"/>
<rectangle x1="12.09675" y1="0.47625" x2="12.32535" y2="0.48895" layer="21"/>
<rectangle x1="0.14605" y1="0.48895" x2="0.34925" y2="0.50165" layer="21"/>
<rectangle x1="0.46355" y1="0.48895" x2="0.67945" y2="0.50165" layer="21"/>
<rectangle x1="0.78105" y1="0.48895" x2="1.00965" y2="0.50165" layer="21"/>
<rectangle x1="1.44145" y1="0.48895" x2="1.67005" y2="0.50165" layer="21"/>
<rectangle x1="1.88595" y1="0.48895" x2="2.11455" y2="0.50165" layer="21"/>
<rectangle x1="2.21615" y1="0.48895" x2="2.44475" y2="0.50165" layer="21"/>
<rectangle x1="2.71145" y1="0.48895" x2="3.02895" y2="0.50165" layer="21"/>
<rectangle x1="3.14325" y1="0.48895" x2="3.37185" y2="0.50165" layer="21"/>
<rectangle x1="3.47345" y1="0.48895" x2="3.70205" y2="0.50165" layer="21"/>
<rectangle x1="3.81635" y1="0.48895" x2="4.04495" y2="0.50165" layer="21"/>
<rectangle x1="4.10845" y1="0.48895" x2="4.33705" y2="0.50165" layer="21"/>
<rectangle x1="4.43865" y1="0.48895" x2="4.66725" y2="0.50165" layer="21"/>
<rectangle x1="5.07365" y1="0.48895" x2="5.30225" y2="0.50165" layer="21"/>
<rectangle x1="5.94995" y1="0.48895" x2="6.19125" y2="0.50165" layer="21"/>
<rectangle x1="6.31825" y1="0.48895" x2="6.54685" y2="0.50165" layer="21"/>
<rectangle x1="6.64845" y1="0.48895" x2="6.87705" y2="0.50165" layer="21"/>
<rectangle x1="6.97865" y1="0.48895" x2="7.20725" y2="0.50165" layer="21"/>
<rectangle x1="7.30885" y1="0.48895" x2="7.53745" y2="0.50165" layer="21"/>
<rectangle x1="7.63905" y1="0.48895" x2="7.86765" y2="0.50165" layer="21"/>
<rectangle x1="7.99465" y1="0.48895" x2="8.21055" y2="0.50165" layer="21"/>
<rectangle x1="8.36295" y1="0.48895" x2="8.59155" y2="0.50165" layer="21"/>
<rectangle x1="8.80745" y1="0.48895" x2="9.03605" y2="0.50165" layer="21"/>
<rectangle x1="9.13765" y1="0.48895" x2="9.36625" y2="0.50165" layer="21"/>
<rectangle x1="9.46785" y1="0.48895" x2="9.70915" y2="0.50165" layer="21"/>
<rectangle x1="9.97585" y1="0.48895" x2="10.30605" y2="0.50165" layer="21"/>
<rectangle x1="10.64895" y1="0.48895" x2="10.89025" y2="0.50165" layer="21"/>
<rectangle x1="11.14425" y1="0.48895" x2="11.36015" y2="0.50165" layer="21"/>
<rectangle x1="11.43635" y1="0.48895" x2="11.65225" y2="0.50165" layer="21"/>
<rectangle x1="11.76655" y1="0.48895" x2="11.99515" y2="0.50165" layer="21"/>
<rectangle x1="12.09675" y1="0.48895" x2="12.32535" y2="0.50165" layer="21"/>
<rectangle x1="0.14605" y1="0.50165" x2="0.34925" y2="0.51435" layer="21"/>
<rectangle x1="0.46355" y1="0.50165" x2="0.66675" y2="0.51435" layer="21"/>
<rectangle x1="0.78105" y1="0.50165" x2="1.00965" y2="0.51435" layer="21"/>
<rectangle x1="1.44145" y1="0.50165" x2="1.67005" y2="0.51435" layer="21"/>
<rectangle x1="1.88595" y1="0.50165" x2="2.11455" y2="0.51435" layer="21"/>
<rectangle x1="2.21615" y1="0.50165" x2="2.44475" y2="0.51435" layer="21"/>
<rectangle x1="2.68605" y1="0.50165" x2="3.01625" y2="0.51435" layer="21"/>
<rectangle x1="3.14325" y1="0.50165" x2="3.37185" y2="0.51435" layer="21"/>
<rectangle x1="3.47345" y1="0.50165" x2="3.70205" y2="0.51435" layer="21"/>
<rectangle x1="3.82905" y1="0.50165" x2="4.05765" y2="0.51435" layer="21"/>
<rectangle x1="4.10845" y1="0.50165" x2="4.33705" y2="0.51435" layer="21"/>
<rectangle x1="4.43865" y1="0.50165" x2="4.66725" y2="0.51435" layer="21"/>
<rectangle x1="5.07365" y1="0.50165" x2="5.30225" y2="0.51435" layer="21"/>
<rectangle x1="5.94995" y1="0.50165" x2="6.19125" y2="0.51435" layer="21"/>
<rectangle x1="6.31825" y1="0.50165" x2="6.54685" y2="0.51435" layer="21"/>
<rectangle x1="6.64845" y1="0.50165" x2="6.87705" y2="0.51435" layer="21"/>
<rectangle x1="6.97865" y1="0.50165" x2="7.20725" y2="0.51435" layer="21"/>
<rectangle x1="7.30885" y1="0.50165" x2="7.53745" y2="0.51435" layer="21"/>
<rectangle x1="7.63905" y1="0.50165" x2="7.86765" y2="0.51435" layer="21"/>
<rectangle x1="7.99465" y1="0.50165" x2="8.21055" y2="0.51435" layer="21"/>
<rectangle x1="8.36295" y1="0.50165" x2="8.59155" y2="0.51435" layer="21"/>
<rectangle x1="8.80745" y1="0.50165" x2="9.03605" y2="0.51435" layer="21"/>
<rectangle x1="9.13765" y1="0.50165" x2="9.36625" y2="0.51435" layer="21"/>
<rectangle x1="9.46785" y1="0.50165" x2="9.70915" y2="0.51435" layer="21"/>
<rectangle x1="9.96315" y1="0.50165" x2="10.29335" y2="0.51435" layer="21"/>
<rectangle x1="10.64895" y1="0.50165" x2="10.89025" y2="0.51435" layer="21"/>
<rectangle x1="11.14425" y1="0.50165" x2="11.37285" y2="0.51435" layer="21"/>
<rectangle x1="11.43635" y1="0.50165" x2="11.65225" y2="0.51435" layer="21"/>
<rectangle x1="11.76655" y1="0.50165" x2="11.99515" y2="0.51435" layer="21"/>
<rectangle x1="12.09675" y1="0.50165" x2="12.32535" y2="0.51435" layer="21"/>
<rectangle x1="0.14605" y1="0.51435" x2="0.34925" y2="0.52705" layer="21"/>
<rectangle x1="0.46355" y1="0.51435" x2="0.66675" y2="0.52705" layer="21"/>
<rectangle x1="0.78105" y1="0.51435" x2="1.00965" y2="0.52705" layer="21"/>
<rectangle x1="1.44145" y1="0.51435" x2="1.67005" y2="0.52705" layer="21"/>
<rectangle x1="1.88595" y1="0.51435" x2="2.11455" y2="0.52705" layer="21"/>
<rectangle x1="2.21615" y1="0.51435" x2="2.44475" y2="0.52705" layer="21"/>
<rectangle x1="2.66065" y1="0.51435" x2="3.01625" y2="0.52705" layer="21"/>
<rectangle x1="3.14325" y1="0.51435" x2="3.37185" y2="0.52705" layer="21"/>
<rectangle x1="3.47345" y1="0.51435" x2="3.70205" y2="0.52705" layer="21"/>
<rectangle x1="3.84175" y1="0.51435" x2="4.07035" y2="0.52705" layer="21"/>
<rectangle x1="4.10845" y1="0.51435" x2="4.33705" y2="0.52705" layer="21"/>
<rectangle x1="4.43865" y1="0.51435" x2="4.66725" y2="0.52705" layer="21"/>
<rectangle x1="5.07365" y1="0.51435" x2="5.30225" y2="0.52705" layer="21"/>
<rectangle x1="5.94995" y1="0.51435" x2="6.19125" y2="0.52705" layer="21"/>
<rectangle x1="6.31825" y1="0.51435" x2="6.54685" y2="0.52705" layer="21"/>
<rectangle x1="6.64845" y1="0.51435" x2="6.87705" y2="0.52705" layer="21"/>
<rectangle x1="6.97865" y1="0.51435" x2="7.20725" y2="0.52705" layer="21"/>
<rectangle x1="7.30885" y1="0.51435" x2="7.53745" y2="0.52705" layer="21"/>
<rectangle x1="7.63905" y1="0.51435" x2="7.86765" y2="0.52705" layer="21"/>
<rectangle x1="7.99465" y1="0.51435" x2="8.21055" y2="0.52705" layer="21"/>
<rectangle x1="8.36295" y1="0.51435" x2="8.59155" y2="0.52705" layer="21"/>
<rectangle x1="8.80745" y1="0.51435" x2="9.03605" y2="0.52705" layer="21"/>
<rectangle x1="9.13765" y1="0.51435" x2="9.36625" y2="0.52705" layer="21"/>
<rectangle x1="9.46785" y1="0.51435" x2="9.70915" y2="0.52705" layer="21"/>
<rectangle x1="9.93775" y1="0.51435" x2="10.28065" y2="0.52705" layer="21"/>
<rectangle x1="10.64895" y1="0.51435" x2="10.89025" y2="0.52705" layer="21"/>
<rectangle x1="11.16965" y1="0.51435" x2="11.38555" y2="0.52705" layer="21"/>
<rectangle x1="11.43635" y1="0.51435" x2="11.65225" y2="0.52705" layer="21"/>
<rectangle x1="11.76655" y1="0.51435" x2="11.99515" y2="0.52705" layer="21"/>
<rectangle x1="12.09675" y1="0.51435" x2="12.32535" y2="0.52705" layer="21"/>
<rectangle x1="0.14605" y1="0.52705" x2="0.34925" y2="0.53975" layer="21"/>
<rectangle x1="0.46355" y1="0.52705" x2="0.66675" y2="0.53975" layer="21"/>
<rectangle x1="0.78105" y1="0.52705" x2="1.00965" y2="0.53975" layer="21"/>
<rectangle x1="1.44145" y1="0.52705" x2="1.67005" y2="0.53975" layer="21"/>
<rectangle x1="1.88595" y1="0.52705" x2="2.11455" y2="0.53975" layer="21"/>
<rectangle x1="2.21615" y1="0.52705" x2="2.44475" y2="0.53975" layer="21"/>
<rectangle x1="2.64795" y1="0.52705" x2="3.00355" y2="0.53975" layer="21"/>
<rectangle x1="3.14325" y1="0.52705" x2="3.37185" y2="0.53975" layer="21"/>
<rectangle x1="3.47345" y1="0.52705" x2="3.70205" y2="0.53975" layer="21"/>
<rectangle x1="3.86715" y1="0.52705" x2="4.08305" y2="0.53975" layer="21"/>
<rectangle x1="4.10845" y1="0.52705" x2="4.33705" y2="0.53975" layer="21"/>
<rectangle x1="4.43865" y1="0.52705" x2="4.66725" y2="0.53975" layer="21"/>
<rectangle x1="5.07365" y1="0.52705" x2="5.30225" y2="0.53975" layer="21"/>
<rectangle x1="5.94995" y1="0.52705" x2="6.19125" y2="0.53975" layer="21"/>
<rectangle x1="6.64845" y1="0.52705" x2="6.87705" y2="0.53975" layer="21"/>
<rectangle x1="6.97865" y1="0.52705" x2="7.20725" y2="0.53975" layer="21"/>
<rectangle x1="7.30885" y1="0.52705" x2="7.53745" y2="0.53975" layer="21"/>
<rectangle x1="7.63905" y1="0.52705" x2="7.86765" y2="0.53975" layer="21"/>
<rectangle x1="7.99465" y1="0.52705" x2="8.21055" y2="0.53975" layer="21"/>
<rectangle x1="8.36295" y1="0.52705" x2="8.59155" y2="0.53975" layer="21"/>
<rectangle x1="8.80745" y1="0.52705" x2="9.03605" y2="0.53975" layer="21"/>
<rectangle x1="9.13765" y1="0.52705" x2="9.36625" y2="0.53975" layer="21"/>
<rectangle x1="9.46785" y1="0.52705" x2="9.70915" y2="0.53975" layer="21"/>
<rectangle x1="9.91235" y1="0.52705" x2="10.26795" y2="0.53975" layer="21"/>
<rectangle x1="10.64895" y1="0.52705" x2="10.89025" y2="0.53975" layer="21"/>
<rectangle x1="11.18235" y1="0.52705" x2="11.41095" y2="0.53975" layer="21"/>
<rectangle x1="11.43635" y1="0.52705" x2="11.65225" y2="0.53975" layer="21"/>
<rectangle x1="11.76655" y1="0.52705" x2="11.99515" y2="0.53975" layer="21"/>
<rectangle x1="12.09675" y1="0.52705" x2="12.32535" y2="0.53975" layer="21"/>
<rectangle x1="0.14605" y1="0.53975" x2="0.36195" y2="0.55245" layer="21"/>
<rectangle x1="0.46355" y1="0.53975" x2="0.66675" y2="0.55245" layer="21"/>
<rectangle x1="0.78105" y1="0.53975" x2="1.32715" y2="0.55245" layer="21"/>
<rectangle x1="1.44145" y1="0.53975" x2="1.67005" y2="0.55245" layer="21"/>
<rectangle x1="1.88595" y1="0.53975" x2="2.11455" y2="0.55245" layer="21"/>
<rectangle x1="2.21615" y1="0.53975" x2="2.44475" y2="0.55245" layer="21"/>
<rectangle x1="2.62255" y1="0.53975" x2="2.97815" y2="0.55245" layer="21"/>
<rectangle x1="3.14325" y1="0.53975" x2="3.37185" y2="0.55245" layer="21"/>
<rectangle x1="3.47345" y1="0.53975" x2="3.70205" y2="0.55245" layer="21"/>
<rectangle x1="3.89255" y1="0.53975" x2="4.33705" y2="0.55245" layer="21"/>
<rectangle x1="4.43865" y1="0.53975" x2="4.66725" y2="0.55245" layer="21"/>
<rectangle x1="5.07365" y1="0.53975" x2="5.61975" y2="0.55245" layer="21"/>
<rectangle x1="5.94995" y1="0.53975" x2="6.19125" y2="0.55245" layer="21"/>
<rectangle x1="6.64845" y1="0.53975" x2="6.87705" y2="0.55245" layer="21"/>
<rectangle x1="6.97865" y1="0.53975" x2="7.20725" y2="0.55245" layer="21"/>
<rectangle x1="7.30885" y1="0.53975" x2="7.53745" y2="0.55245" layer="21"/>
<rectangle x1="7.63905" y1="0.53975" x2="7.86765" y2="0.55245" layer="21"/>
<rectangle x1="7.99465" y1="0.53975" x2="8.21055" y2="0.55245" layer="21"/>
<rectangle x1="8.36295" y1="0.53975" x2="8.59155" y2="0.55245" layer="21"/>
<rectangle x1="8.80745" y1="0.53975" x2="9.03605" y2="0.55245" layer="21"/>
<rectangle x1="9.13765" y1="0.53975" x2="9.36625" y2="0.55245" layer="21"/>
<rectangle x1="9.46785" y1="0.53975" x2="9.70915" y2="0.55245" layer="21"/>
<rectangle x1="9.89965" y1="0.53975" x2="10.25525" y2="0.55245" layer="21"/>
<rectangle x1="10.64895" y1="0.53975" x2="10.89025" y2="0.55245" layer="21"/>
<rectangle x1="11.20775" y1="0.53975" x2="11.65225" y2="0.55245" layer="21"/>
<rectangle x1="11.76655" y1="0.53975" x2="11.99515" y2="0.55245" layer="21"/>
<rectangle x1="12.09675" y1="0.53975" x2="12.32535" y2="0.55245" layer="21"/>
<rectangle x1="0.14605" y1="0.55245" x2="0.36195" y2="0.56515" layer="21"/>
<rectangle x1="0.46355" y1="0.55245" x2="0.66675" y2="0.56515" layer="21"/>
<rectangle x1="0.78105" y1="0.55245" x2="1.33985" y2="0.56515" layer="21"/>
<rectangle x1="1.44145" y1="0.55245" x2="1.67005" y2="0.56515" layer="21"/>
<rectangle x1="1.88595" y1="0.55245" x2="2.11455" y2="0.56515" layer="21"/>
<rectangle x1="2.21615" y1="0.55245" x2="2.44475" y2="0.56515" layer="21"/>
<rectangle x1="2.60985" y1="0.55245" x2="2.96545" y2="0.56515" layer="21"/>
<rectangle x1="3.14325" y1="0.55245" x2="3.37185" y2="0.56515" layer="21"/>
<rectangle x1="3.47345" y1="0.55245" x2="3.70205" y2="0.56515" layer="21"/>
<rectangle x1="3.91795" y1="0.55245" x2="4.33705" y2="0.56515" layer="21"/>
<rectangle x1="4.43865" y1="0.55245" x2="4.66725" y2="0.56515" layer="21"/>
<rectangle x1="5.07365" y1="0.55245" x2="5.61975" y2="0.56515" layer="21"/>
<rectangle x1="5.94995" y1="0.55245" x2="6.19125" y2="0.56515" layer="21"/>
<rectangle x1="6.64845" y1="0.55245" x2="6.87705" y2="0.56515" layer="21"/>
<rectangle x1="6.97865" y1="0.55245" x2="7.20725" y2="0.56515" layer="21"/>
<rectangle x1="7.30885" y1="0.55245" x2="7.53745" y2="0.56515" layer="21"/>
<rectangle x1="7.63905" y1="0.55245" x2="7.86765" y2="0.56515" layer="21"/>
<rectangle x1="7.99465" y1="0.55245" x2="8.21055" y2="0.56515" layer="21"/>
<rectangle x1="8.36295" y1="0.55245" x2="8.59155" y2="0.56515" layer="21"/>
<rectangle x1="8.80745" y1="0.55245" x2="9.03605" y2="0.56515" layer="21"/>
<rectangle x1="9.13765" y1="0.55245" x2="9.36625" y2="0.56515" layer="21"/>
<rectangle x1="9.46785" y1="0.55245" x2="9.70915" y2="0.56515" layer="21"/>
<rectangle x1="9.87425" y1="0.55245" x2="10.22985" y2="0.56515" layer="21"/>
<rectangle x1="10.64895" y1="0.55245" x2="10.89025" y2="0.56515" layer="21"/>
<rectangle x1="11.23315" y1="0.55245" x2="11.65225" y2="0.56515" layer="21"/>
<rectangle x1="11.76655" y1="0.55245" x2="11.99515" y2="0.56515" layer="21"/>
<rectangle x1="12.09675" y1="0.55245" x2="12.32535" y2="0.56515" layer="21"/>
<rectangle x1="0.14605" y1="0.56515" x2="0.36195" y2="0.57785" layer="21"/>
<rectangle x1="0.46355" y1="0.56515" x2="0.66675" y2="0.57785" layer="21"/>
<rectangle x1="0.78105" y1="0.56515" x2="1.33985" y2="0.57785" layer="21"/>
<rectangle x1="1.44145" y1="0.56515" x2="1.67005" y2="0.57785" layer="21"/>
<rectangle x1="1.88595" y1="0.56515" x2="2.11455" y2="0.57785" layer="21"/>
<rectangle x1="2.21615" y1="0.56515" x2="2.44475" y2="0.57785" layer="21"/>
<rectangle x1="2.59715" y1="0.56515" x2="2.94005" y2="0.57785" layer="21"/>
<rectangle x1="3.14325" y1="0.56515" x2="3.37185" y2="0.57785" layer="21"/>
<rectangle x1="3.47345" y1="0.56515" x2="3.70205" y2="0.57785" layer="21"/>
<rectangle x1="3.95605" y1="0.56515" x2="4.33705" y2="0.57785" layer="21"/>
<rectangle x1="4.43865" y1="0.56515" x2="4.66725" y2="0.57785" layer="21"/>
<rectangle x1="5.07365" y1="0.56515" x2="5.61975" y2="0.57785" layer="21"/>
<rectangle x1="5.94995" y1="0.56515" x2="6.19125" y2="0.57785" layer="21"/>
<rectangle x1="6.64845" y1="0.56515" x2="6.87705" y2="0.57785" layer="21"/>
<rectangle x1="6.97865" y1="0.56515" x2="7.20725" y2="0.57785" layer="21"/>
<rectangle x1="7.30885" y1="0.56515" x2="7.53745" y2="0.57785" layer="21"/>
<rectangle x1="7.63905" y1="0.56515" x2="7.86765" y2="0.57785" layer="21"/>
<rectangle x1="7.99465" y1="0.56515" x2="8.21055" y2="0.57785" layer="21"/>
<rectangle x1="8.36295" y1="0.56515" x2="8.59155" y2="0.57785" layer="21"/>
<rectangle x1="8.80745" y1="0.56515" x2="9.03605" y2="0.57785" layer="21"/>
<rectangle x1="9.13765" y1="0.56515" x2="9.36625" y2="0.57785" layer="21"/>
<rectangle x1="9.46785" y1="0.56515" x2="9.70915" y2="0.57785" layer="21"/>
<rectangle x1="9.86155" y1="0.56515" x2="10.21715" y2="0.57785" layer="21"/>
<rectangle x1="10.64895" y1="0.56515" x2="10.89025" y2="0.57785" layer="21"/>
<rectangle x1="11.27125" y1="0.56515" x2="11.65225" y2="0.57785" layer="21"/>
<rectangle x1="11.76655" y1="0.56515" x2="11.99515" y2="0.57785" layer="21"/>
<rectangle x1="12.09675" y1="0.56515" x2="12.32535" y2="0.57785" layer="21"/>
<rectangle x1="0.15875" y1="0.57785" x2="0.36195" y2="0.59055" layer="21"/>
<rectangle x1="0.45085" y1="0.57785" x2="0.66675" y2="0.59055" layer="21"/>
<rectangle x1="0.78105" y1="0.57785" x2="1.33985" y2="0.59055" layer="21"/>
<rectangle x1="1.44145" y1="0.57785" x2="1.67005" y2="0.59055" layer="21"/>
<rectangle x1="1.88595" y1="0.57785" x2="2.11455" y2="0.59055" layer="21"/>
<rectangle x1="2.21615" y1="0.57785" x2="2.44475" y2="0.59055" layer="21"/>
<rectangle x1="2.57175" y1="0.57785" x2="2.91465" y2="0.59055" layer="21"/>
<rectangle x1="3.14325" y1="0.57785" x2="3.37185" y2="0.59055" layer="21"/>
<rectangle x1="3.47345" y1="0.57785" x2="3.70205" y2="0.59055" layer="21"/>
<rectangle x1="3.98145" y1="0.57785" x2="4.33705" y2="0.59055" layer="21"/>
<rectangle x1="4.43865" y1="0.57785" x2="4.66725" y2="0.59055" layer="21"/>
<rectangle x1="5.07365" y1="0.57785" x2="5.61975" y2="0.59055" layer="21"/>
<rectangle x1="5.94995" y1="0.57785" x2="6.19125" y2="0.59055" layer="21"/>
<rectangle x1="6.64845" y1="0.57785" x2="6.87705" y2="0.59055" layer="21"/>
<rectangle x1="6.97865" y1="0.57785" x2="7.20725" y2="0.59055" layer="21"/>
<rectangle x1="7.30885" y1="0.57785" x2="7.53745" y2="0.59055" layer="21"/>
<rectangle x1="7.63905" y1="0.57785" x2="7.86765" y2="0.59055" layer="21"/>
<rectangle x1="7.99465" y1="0.57785" x2="8.21055" y2="0.59055" layer="21"/>
<rectangle x1="8.36295" y1="0.57785" x2="8.59155" y2="0.59055" layer="21"/>
<rectangle x1="8.80745" y1="0.57785" x2="9.03605" y2="0.59055" layer="21"/>
<rectangle x1="9.13765" y1="0.57785" x2="9.36625" y2="0.59055" layer="21"/>
<rectangle x1="9.46785" y1="0.57785" x2="9.70915" y2="0.59055" layer="21"/>
<rectangle x1="9.84885" y1="0.57785" x2="10.19175" y2="0.59055" layer="21"/>
<rectangle x1="10.64895" y1="0.57785" x2="10.89025" y2="0.59055" layer="21"/>
<rectangle x1="11.29665" y1="0.57785" x2="11.65225" y2="0.59055" layer="21"/>
<rectangle x1="11.76655" y1="0.57785" x2="11.99515" y2="0.59055" layer="21"/>
<rectangle x1="12.09675" y1="0.57785" x2="12.32535" y2="0.59055" layer="21"/>
<rectangle x1="0.15875" y1="0.59055" x2="0.36195" y2="0.60325" layer="21"/>
<rectangle x1="0.45085" y1="0.59055" x2="0.65405" y2="0.60325" layer="21"/>
<rectangle x1="0.78105" y1="0.59055" x2="1.33985" y2="0.60325" layer="21"/>
<rectangle x1="1.44145" y1="0.59055" x2="1.67005" y2="0.60325" layer="21"/>
<rectangle x1="1.88595" y1="0.59055" x2="2.11455" y2="0.60325" layer="21"/>
<rectangle x1="2.21615" y1="0.59055" x2="2.44475" y2="0.60325" layer="21"/>
<rectangle x1="2.57175" y1="0.59055" x2="2.90195" y2="0.60325" layer="21"/>
<rectangle x1="3.14325" y1="0.59055" x2="3.37185" y2="0.60325" layer="21"/>
<rectangle x1="3.47345" y1="0.59055" x2="3.70205" y2="0.60325" layer="21"/>
<rectangle x1="4.01955" y1="0.59055" x2="4.33705" y2="0.60325" layer="21"/>
<rectangle x1="4.43865" y1="0.59055" x2="4.66725" y2="0.60325" layer="21"/>
<rectangle x1="5.07365" y1="0.59055" x2="5.61975" y2="0.60325" layer="21"/>
<rectangle x1="5.94995" y1="0.59055" x2="6.19125" y2="0.60325" layer="21"/>
<rectangle x1="6.64845" y1="0.59055" x2="6.87705" y2="0.60325" layer="21"/>
<rectangle x1="6.97865" y1="0.59055" x2="7.20725" y2="0.60325" layer="21"/>
<rectangle x1="7.30885" y1="0.59055" x2="7.53745" y2="0.60325" layer="21"/>
<rectangle x1="7.63905" y1="0.59055" x2="7.86765" y2="0.60325" layer="21"/>
<rectangle x1="7.99465" y1="0.59055" x2="8.21055" y2="0.60325" layer="21"/>
<rectangle x1="8.36295" y1="0.59055" x2="8.59155" y2="0.60325" layer="21"/>
<rectangle x1="8.80745" y1="0.59055" x2="9.03605" y2="0.60325" layer="21"/>
<rectangle x1="9.13765" y1="0.59055" x2="9.36625" y2="0.60325" layer="21"/>
<rectangle x1="9.46785" y1="0.59055" x2="9.70915" y2="0.60325" layer="21"/>
<rectangle x1="9.83615" y1="0.59055" x2="10.16635" y2="0.60325" layer="21"/>
<rectangle x1="10.64895" y1="0.59055" x2="10.89025" y2="0.60325" layer="21"/>
<rectangle x1="11.33475" y1="0.59055" x2="11.65225" y2="0.60325" layer="21"/>
<rectangle x1="11.76655" y1="0.59055" x2="11.99515" y2="0.60325" layer="21"/>
<rectangle x1="12.09675" y1="0.59055" x2="12.32535" y2="0.60325" layer="21"/>
<rectangle x1="0.15875" y1="0.60325" x2="0.36195" y2="0.61595" layer="21"/>
<rectangle x1="0.45085" y1="0.60325" x2="0.65405" y2="0.61595" layer="21"/>
<rectangle x1="0.78105" y1="0.60325" x2="1.33985" y2="0.61595" layer="21"/>
<rectangle x1="1.44145" y1="0.60325" x2="1.67005" y2="0.61595" layer="21"/>
<rectangle x1="1.88595" y1="0.60325" x2="2.11455" y2="0.61595" layer="21"/>
<rectangle x1="2.21615" y1="0.60325" x2="2.44475" y2="0.61595" layer="21"/>
<rectangle x1="2.55905" y1="0.60325" x2="2.87655" y2="0.61595" layer="21"/>
<rectangle x1="3.14325" y1="0.60325" x2="3.37185" y2="0.61595" layer="21"/>
<rectangle x1="3.47345" y1="0.60325" x2="3.70205" y2="0.61595" layer="21"/>
<rectangle x1="4.04495" y1="0.60325" x2="4.33705" y2="0.61595" layer="21"/>
<rectangle x1="4.43865" y1="0.60325" x2="4.66725" y2="0.61595" layer="21"/>
<rectangle x1="5.07365" y1="0.60325" x2="5.61975" y2="0.61595" layer="21"/>
<rectangle x1="5.94995" y1="0.60325" x2="6.19125" y2="0.61595" layer="21"/>
<rectangle x1="6.64845" y1="0.60325" x2="6.87705" y2="0.61595" layer="21"/>
<rectangle x1="6.97865" y1="0.60325" x2="7.20725" y2="0.61595" layer="21"/>
<rectangle x1="7.30885" y1="0.60325" x2="7.53745" y2="0.61595" layer="21"/>
<rectangle x1="7.63905" y1="0.60325" x2="7.86765" y2="0.61595" layer="21"/>
<rectangle x1="7.99465" y1="0.60325" x2="8.21055" y2="0.61595" layer="21"/>
<rectangle x1="8.36295" y1="0.60325" x2="8.59155" y2="0.61595" layer="21"/>
<rectangle x1="8.80745" y1="0.60325" x2="9.03605" y2="0.61595" layer="21"/>
<rectangle x1="9.13765" y1="0.60325" x2="9.36625" y2="0.61595" layer="21"/>
<rectangle x1="9.46785" y1="0.60325" x2="9.70915" y2="0.61595" layer="21"/>
<rectangle x1="9.82345" y1="0.60325" x2="10.14095" y2="0.61595" layer="21"/>
<rectangle x1="10.64895" y1="0.60325" x2="10.89025" y2="0.61595" layer="21"/>
<rectangle x1="11.36015" y1="0.60325" x2="11.65225" y2="0.61595" layer="21"/>
<rectangle x1="11.76655" y1="0.60325" x2="11.99515" y2="0.61595" layer="21"/>
<rectangle x1="12.09675" y1="0.60325" x2="12.32535" y2="0.61595" layer="21"/>
<rectangle x1="0.15875" y1="0.61595" x2="0.36195" y2="0.62865" layer="21"/>
<rectangle x1="0.45085" y1="0.61595" x2="0.65405" y2="0.62865" layer="21"/>
<rectangle x1="0.78105" y1="0.61595" x2="1.33985" y2="0.62865" layer="21"/>
<rectangle x1="1.44145" y1="0.61595" x2="1.68275" y2="0.62865" layer="21"/>
<rectangle x1="1.88595" y1="0.61595" x2="2.11455" y2="0.62865" layer="21"/>
<rectangle x1="2.21615" y1="0.61595" x2="2.44475" y2="0.62865" layer="21"/>
<rectangle x1="2.54635" y1="0.61595" x2="2.85115" y2="0.62865" layer="21"/>
<rectangle x1="3.14325" y1="0.61595" x2="3.37185" y2="0.62865" layer="21"/>
<rectangle x1="3.47345" y1="0.61595" x2="3.70205" y2="0.62865" layer="21"/>
<rectangle x1="4.07035" y1="0.61595" x2="4.33705" y2="0.62865" layer="21"/>
<rectangle x1="4.43865" y1="0.61595" x2="4.66725" y2="0.62865" layer="21"/>
<rectangle x1="5.07365" y1="0.61595" x2="5.61975" y2="0.62865" layer="21"/>
<rectangle x1="5.94995" y1="0.61595" x2="6.19125" y2="0.62865" layer="21"/>
<rectangle x1="6.64845" y1="0.61595" x2="6.87705" y2="0.62865" layer="21"/>
<rectangle x1="6.97865" y1="0.61595" x2="7.20725" y2="0.62865" layer="21"/>
<rectangle x1="7.30885" y1="0.61595" x2="7.53745" y2="0.62865" layer="21"/>
<rectangle x1="7.63905" y1="0.61595" x2="7.86765" y2="0.62865" layer="21"/>
<rectangle x1="7.99465" y1="0.61595" x2="8.21055" y2="0.62865" layer="21"/>
<rectangle x1="8.36295" y1="0.61595" x2="8.60425" y2="0.62865" layer="21"/>
<rectangle x1="8.80745" y1="0.61595" x2="9.03605" y2="0.62865" layer="21"/>
<rectangle x1="9.13765" y1="0.61595" x2="9.36625" y2="0.62865" layer="21"/>
<rectangle x1="9.46785" y1="0.61595" x2="9.70915" y2="0.62865" layer="21"/>
<rectangle x1="9.82345" y1="0.61595" x2="10.11555" y2="0.62865" layer="21"/>
<rectangle x1="10.64895" y1="0.61595" x2="10.89025" y2="0.62865" layer="21"/>
<rectangle x1="11.38555" y1="0.61595" x2="11.65225" y2="0.62865" layer="21"/>
<rectangle x1="11.76655" y1="0.61595" x2="11.99515" y2="0.62865" layer="21"/>
<rectangle x1="12.09675" y1="0.61595" x2="12.32535" y2="0.62865" layer="21"/>
<rectangle x1="0.15875" y1="0.62865" x2="0.36195" y2="0.64135" layer="21"/>
<rectangle x1="0.45085" y1="0.62865" x2="0.65405" y2="0.64135" layer="21"/>
<rectangle x1="0.78105" y1="0.62865" x2="1.33985" y2="0.64135" layer="21"/>
<rectangle x1="1.44145" y1="0.62865" x2="1.68275" y2="0.64135" layer="21"/>
<rectangle x1="1.88595" y1="0.62865" x2="2.11455" y2="0.64135" layer="21"/>
<rectangle x1="2.21615" y1="0.62865" x2="2.44475" y2="0.64135" layer="21"/>
<rectangle x1="2.54635" y1="0.62865" x2="2.82575" y2="0.64135" layer="21"/>
<rectangle x1="3.14325" y1="0.62865" x2="3.37185" y2="0.64135" layer="21"/>
<rectangle x1="3.47345" y1="0.62865" x2="3.70205" y2="0.64135" layer="21"/>
<rectangle x1="4.09575" y1="0.62865" x2="4.33705" y2="0.64135" layer="21"/>
<rectangle x1="4.43865" y1="0.62865" x2="4.66725" y2="0.64135" layer="21"/>
<rectangle x1="5.07365" y1="0.62865" x2="5.61975" y2="0.64135" layer="21"/>
<rectangle x1="5.94995" y1="0.62865" x2="6.19125" y2="0.64135" layer="21"/>
<rectangle x1="6.64845" y1="0.62865" x2="6.87705" y2="0.64135" layer="21"/>
<rectangle x1="6.97865" y1="0.62865" x2="7.20725" y2="0.64135" layer="21"/>
<rectangle x1="7.30885" y1="0.62865" x2="7.53745" y2="0.64135" layer="21"/>
<rectangle x1="7.63905" y1="0.62865" x2="7.86765" y2="0.64135" layer="21"/>
<rectangle x1="7.99465" y1="0.62865" x2="8.21055" y2="0.64135" layer="21"/>
<rectangle x1="8.36295" y1="0.62865" x2="8.60425" y2="0.64135" layer="21"/>
<rectangle x1="8.80745" y1="0.62865" x2="9.03605" y2="0.64135" layer="21"/>
<rectangle x1="9.13765" y1="0.62865" x2="9.36625" y2="0.64135" layer="21"/>
<rectangle x1="9.46785" y1="0.62865" x2="9.70915" y2="0.64135" layer="21"/>
<rectangle x1="9.82345" y1="0.62865" x2="10.09015" y2="0.64135" layer="21"/>
<rectangle x1="10.64895" y1="0.62865" x2="10.89025" y2="0.64135" layer="21"/>
<rectangle x1="11.41095" y1="0.62865" x2="11.65225" y2="0.64135" layer="21"/>
<rectangle x1="11.76655" y1="0.62865" x2="11.99515" y2="0.64135" layer="21"/>
<rectangle x1="12.09675" y1="0.62865" x2="12.32535" y2="0.64135" layer="21"/>
<rectangle x1="0.15875" y1="0.64135" x2="0.36195" y2="0.65405" layer="21"/>
<rectangle x1="0.45085" y1="0.64135" x2="0.65405" y2="0.65405" layer="21"/>
<rectangle x1="0.78105" y1="0.64135" x2="1.33985" y2="0.65405" layer="21"/>
<rectangle x1="1.44145" y1="0.64135" x2="1.69545" y2="0.65405" layer="21"/>
<rectangle x1="1.88595" y1="0.64135" x2="2.11455" y2="0.65405" layer="21"/>
<rectangle x1="2.21615" y1="0.64135" x2="2.44475" y2="0.65405" layer="21"/>
<rectangle x1="2.54635" y1="0.64135" x2="2.80035" y2="0.65405" layer="21"/>
<rectangle x1="3.14325" y1="0.64135" x2="3.37185" y2="0.65405" layer="21"/>
<rectangle x1="3.47345" y1="0.64135" x2="3.70205" y2="0.65405" layer="21"/>
<rectangle x1="4.10845" y1="0.64135" x2="4.33705" y2="0.65405" layer="21"/>
<rectangle x1="4.43865" y1="0.64135" x2="4.66725" y2="0.65405" layer="21"/>
<rectangle x1="5.07365" y1="0.64135" x2="5.61975" y2="0.65405" layer="21"/>
<rectangle x1="5.94995" y1="0.64135" x2="6.19125" y2="0.65405" layer="21"/>
<rectangle x1="6.64845" y1="0.64135" x2="6.87705" y2="0.65405" layer="21"/>
<rectangle x1="6.97865" y1="0.64135" x2="7.20725" y2="0.65405" layer="21"/>
<rectangle x1="7.30885" y1="0.64135" x2="7.53745" y2="0.65405" layer="21"/>
<rectangle x1="7.63905" y1="0.64135" x2="7.86765" y2="0.65405" layer="21"/>
<rectangle x1="7.99465" y1="0.64135" x2="8.21055" y2="0.65405" layer="21"/>
<rectangle x1="8.36295" y1="0.64135" x2="8.61695" y2="0.65405" layer="21"/>
<rectangle x1="8.80745" y1="0.64135" x2="9.03605" y2="0.65405" layer="21"/>
<rectangle x1="9.13765" y1="0.64135" x2="9.36625" y2="0.65405" layer="21"/>
<rectangle x1="9.46785" y1="0.64135" x2="9.70915" y2="0.65405" layer="21"/>
<rectangle x1="9.81075" y1="0.64135" x2="10.06475" y2="0.65405" layer="21"/>
<rectangle x1="10.64895" y1="0.64135" x2="10.89025" y2="0.65405" layer="21"/>
<rectangle x1="11.42365" y1="0.64135" x2="11.65225" y2="0.65405" layer="21"/>
<rectangle x1="11.76655" y1="0.64135" x2="11.99515" y2="0.65405" layer="21"/>
<rectangle x1="12.09675" y1="0.64135" x2="12.32535" y2="0.65405" layer="21"/>
<rectangle x1="0.15875" y1="0.65405" x2="0.36195" y2="0.66675" layer="21"/>
<rectangle x1="0.45085" y1="0.65405" x2="0.65405" y2="0.66675" layer="21"/>
<rectangle x1="0.78105" y1="0.65405" x2="1.00965" y2="0.66675" layer="21"/>
<rectangle x1="1.09855" y1="0.65405" x2="1.33985" y2="0.66675" layer="21"/>
<rectangle x1="1.44145" y1="0.65405" x2="1.69545" y2="0.66675" layer="21"/>
<rectangle x1="1.88595" y1="0.65405" x2="2.11455" y2="0.66675" layer="21"/>
<rectangle x1="2.21615" y1="0.65405" x2="2.44475" y2="0.66675" layer="21"/>
<rectangle x1="2.53365" y1="0.65405" x2="2.77495" y2="0.66675" layer="21"/>
<rectangle x1="3.14325" y1="0.65405" x2="3.37185" y2="0.66675" layer="21"/>
<rectangle x1="3.47345" y1="0.65405" x2="3.70205" y2="0.66675" layer="21"/>
<rectangle x1="3.80365" y1="0.65405" x2="4.00685" y2="0.66675" layer="21"/>
<rectangle x1="4.10845" y1="0.65405" x2="4.33705" y2="0.66675" layer="21"/>
<rectangle x1="4.43865" y1="0.65405" x2="4.66725" y2="0.66675" layer="21"/>
<rectangle x1="5.07365" y1="0.65405" x2="5.30225" y2="0.66675" layer="21"/>
<rectangle x1="5.39115" y1="0.65405" x2="5.61975" y2="0.66675" layer="21"/>
<rectangle x1="5.94995" y1="0.65405" x2="6.19125" y2="0.66675" layer="21"/>
<rectangle x1="6.64845" y1="0.65405" x2="6.87705" y2="0.66675" layer="21"/>
<rectangle x1="6.97865" y1="0.65405" x2="7.20725" y2="0.66675" layer="21"/>
<rectangle x1="7.30885" y1="0.65405" x2="7.53745" y2="0.66675" layer="21"/>
<rectangle x1="7.63905" y1="0.65405" x2="7.86765" y2="0.66675" layer="21"/>
<rectangle x1="7.99465" y1="0.65405" x2="8.21055" y2="0.66675" layer="21"/>
<rectangle x1="8.36295" y1="0.65405" x2="8.61695" y2="0.66675" layer="21"/>
<rectangle x1="8.80745" y1="0.65405" x2="9.03605" y2="0.66675" layer="21"/>
<rectangle x1="9.13765" y1="0.65405" x2="9.36625" y2="0.66675" layer="21"/>
<rectangle x1="9.46785" y1="0.65405" x2="9.70915" y2="0.66675" layer="21"/>
<rectangle x1="9.81075" y1="0.65405" x2="10.05205" y2="0.66675" layer="21"/>
<rectangle x1="10.64895" y1="0.65405" x2="10.89025" y2="0.66675" layer="21"/>
<rectangle x1="11.11885" y1="0.65405" x2="11.32205" y2="0.66675" layer="21"/>
<rectangle x1="11.42365" y1="0.65405" x2="11.65225" y2="0.66675" layer="21"/>
<rectangle x1="11.76655" y1="0.65405" x2="11.99515" y2="0.66675" layer="21"/>
<rectangle x1="12.09675" y1="0.65405" x2="12.32535" y2="0.66675" layer="21"/>
<rectangle x1="0.17145" y1="0.66675" x2="0.37465" y2="0.67945" layer="21"/>
<rectangle x1="0.45085" y1="0.66675" x2="0.65405" y2="0.67945" layer="21"/>
<rectangle x1="0.78105" y1="0.66675" x2="1.00965" y2="0.67945" layer="21"/>
<rectangle x1="1.11125" y1="0.66675" x2="1.33985" y2="0.67945" layer="21"/>
<rectangle x1="1.44145" y1="0.66675" x2="1.72085" y2="0.67945" layer="21"/>
<rectangle x1="1.88595" y1="0.66675" x2="2.11455" y2="0.67945" layer="21"/>
<rectangle x1="2.21615" y1="0.66675" x2="2.44475" y2="0.67945" layer="21"/>
<rectangle x1="2.53365" y1="0.66675" x2="2.76225" y2="0.67945" layer="21"/>
<rectangle x1="3.14325" y1="0.66675" x2="3.37185" y2="0.67945" layer="21"/>
<rectangle x1="3.47345" y1="0.66675" x2="3.70205" y2="0.67945" layer="21"/>
<rectangle x1="3.80365" y1="0.66675" x2="4.00685" y2="0.67945" layer="21"/>
<rectangle x1="4.10845" y1="0.66675" x2="4.33705" y2="0.67945" layer="21"/>
<rectangle x1="4.43865" y1="0.66675" x2="4.66725" y2="0.67945" layer="21"/>
<rectangle x1="4.76885" y1="0.66675" x2="4.97205" y2="0.67945" layer="21"/>
<rectangle x1="5.07365" y1="0.66675" x2="5.30225" y2="0.67945" layer="21"/>
<rectangle x1="5.39115" y1="0.66675" x2="5.61975" y2="0.67945" layer="21"/>
<rectangle x1="5.94995" y1="0.66675" x2="6.19125" y2="0.67945" layer="21"/>
<rectangle x1="6.64845" y1="0.66675" x2="6.87705" y2="0.67945" layer="21"/>
<rectangle x1="6.97865" y1="0.66675" x2="7.20725" y2="0.67945" layer="21"/>
<rectangle x1="7.30885" y1="0.66675" x2="7.53745" y2="0.67945" layer="21"/>
<rectangle x1="7.63905" y1="0.66675" x2="7.86765" y2="0.67945" layer="21"/>
<rectangle x1="7.99465" y1="0.66675" x2="8.21055" y2="0.67945" layer="21"/>
<rectangle x1="8.36295" y1="0.66675" x2="8.64235" y2="0.67945" layer="21"/>
<rectangle x1="8.82015" y1="0.66675" x2="9.03605" y2="0.67945" layer="21"/>
<rectangle x1="9.13765" y1="0.66675" x2="9.36625" y2="0.67945" layer="21"/>
<rectangle x1="9.46785" y1="0.66675" x2="9.70915" y2="0.67945" layer="21"/>
<rectangle x1="9.81075" y1="0.66675" x2="10.03935" y2="0.67945" layer="21"/>
<rectangle x1="10.64895" y1="0.66675" x2="10.89025" y2="0.67945" layer="21"/>
<rectangle x1="11.11885" y1="0.66675" x2="11.32205" y2="0.67945" layer="21"/>
<rectangle x1="11.42365" y1="0.66675" x2="11.65225" y2="0.67945" layer="21"/>
<rectangle x1="11.76655" y1="0.66675" x2="11.99515" y2="0.67945" layer="21"/>
<rectangle x1="12.09675" y1="0.66675" x2="12.32535" y2="0.67945" layer="21"/>
<rectangle x1="0.17145" y1="0.67945" x2="0.37465" y2="0.69215" layer="21"/>
<rectangle x1="0.45085" y1="0.67945" x2="0.64135" y2="0.69215" layer="21"/>
<rectangle x1="0.78105" y1="0.67945" x2="1.00965" y2="0.69215" layer="21"/>
<rectangle x1="1.11125" y1="0.67945" x2="1.32715" y2="0.69215" layer="21"/>
<rectangle x1="1.44145" y1="0.67945" x2="1.74625" y2="0.69215" layer="21"/>
<rectangle x1="1.88595" y1="0.67945" x2="2.11455" y2="0.69215" layer="21"/>
<rectangle x1="2.21615" y1="0.67945" x2="2.44475" y2="0.69215" layer="21"/>
<rectangle x1="2.53365" y1="0.67945" x2="2.74955" y2="0.69215" layer="21"/>
<rectangle x1="3.14325" y1="0.67945" x2="3.37185" y2="0.69215" layer="21"/>
<rectangle x1="3.47345" y1="0.67945" x2="3.70205" y2="0.69215" layer="21"/>
<rectangle x1="3.80365" y1="0.67945" x2="4.00685" y2="0.69215" layer="21"/>
<rectangle x1="4.10845" y1="0.67945" x2="4.33705" y2="0.69215" layer="21"/>
<rectangle x1="4.43865" y1="0.67945" x2="4.66725" y2="0.69215" layer="21"/>
<rectangle x1="4.76885" y1="0.67945" x2="4.97205" y2="0.69215" layer="21"/>
<rectangle x1="5.07365" y1="0.67945" x2="5.30225" y2="0.69215" layer="21"/>
<rectangle x1="5.39115" y1="0.67945" x2="5.61975" y2="0.69215" layer="21"/>
<rectangle x1="5.94995" y1="0.67945" x2="6.19125" y2="0.69215" layer="21"/>
<rectangle x1="6.64845" y1="0.67945" x2="6.87705" y2="0.69215" layer="21"/>
<rectangle x1="6.97865" y1="0.67945" x2="7.20725" y2="0.69215" layer="21"/>
<rectangle x1="7.30885" y1="0.67945" x2="7.53745" y2="0.69215" layer="21"/>
<rectangle x1="7.63905" y1="0.67945" x2="7.86765" y2="0.69215" layer="21"/>
<rectangle x1="7.99465" y1="0.67945" x2="8.21055" y2="0.69215" layer="21"/>
<rectangle x1="8.36295" y1="0.67945" x2="8.66775" y2="0.69215" layer="21"/>
<rectangle x1="8.82015" y1="0.67945" x2="9.03605" y2="0.69215" layer="21"/>
<rectangle x1="9.13765" y1="0.67945" x2="9.36625" y2="0.69215" layer="21"/>
<rectangle x1="9.46785" y1="0.67945" x2="9.70915" y2="0.69215" layer="21"/>
<rectangle x1="9.79805" y1="0.67945" x2="10.02665" y2="0.69215" layer="21"/>
<rectangle x1="10.64895" y1="0.67945" x2="10.89025" y2="0.69215" layer="21"/>
<rectangle x1="11.11885" y1="0.67945" x2="11.32205" y2="0.69215" layer="21"/>
<rectangle x1="11.43635" y1="0.67945" x2="11.65225" y2="0.69215" layer="21"/>
<rectangle x1="11.76655" y1="0.67945" x2="11.99515" y2="0.69215" layer="21"/>
<rectangle x1="12.09675" y1="0.67945" x2="12.32535" y2="0.69215" layer="21"/>
<rectangle x1="0.17145" y1="0.69215" x2="0.37465" y2="0.70485" layer="21"/>
<rectangle x1="0.45085" y1="0.69215" x2="0.64135" y2="0.70485" layer="21"/>
<rectangle x1="0.78105" y1="0.69215" x2="1.00965" y2="0.70485" layer="21"/>
<rectangle x1="1.11125" y1="0.69215" x2="1.32715" y2="0.70485" layer="21"/>
<rectangle x1="1.44145" y1="0.69215" x2="1.82245" y2="0.70485" layer="21"/>
<rectangle x1="1.89865" y1="0.69215" x2="2.11455" y2="0.70485" layer="21"/>
<rectangle x1="2.21615" y1="0.69215" x2="2.44475" y2="0.70485" layer="21"/>
<rectangle x1="2.53365" y1="0.69215" x2="2.73685" y2="0.70485" layer="21"/>
<rectangle x1="3.14325" y1="0.69215" x2="3.37185" y2="0.70485" layer="21"/>
<rectangle x1="3.47345" y1="0.69215" x2="3.70205" y2="0.70485" layer="21"/>
<rectangle x1="3.80365" y1="0.69215" x2="4.00685" y2="0.70485" layer="21"/>
<rectangle x1="4.10845" y1="0.69215" x2="4.33705" y2="0.70485" layer="21"/>
<rectangle x1="4.43865" y1="0.69215" x2="4.66725" y2="0.70485" layer="21"/>
<rectangle x1="4.76885" y1="0.69215" x2="4.97205" y2="0.70485" layer="21"/>
<rectangle x1="5.07365" y1="0.69215" x2="5.30225" y2="0.70485" layer="21"/>
<rectangle x1="5.39115" y1="0.69215" x2="5.61975" y2="0.70485" layer="21"/>
<rectangle x1="5.94995" y1="0.69215" x2="6.19125" y2="0.70485" layer="21"/>
<rectangle x1="6.66115" y1="0.69215" x2="6.87705" y2="0.70485" layer="21"/>
<rectangle x1="6.97865" y1="0.69215" x2="7.20725" y2="0.70485" layer="21"/>
<rectangle x1="7.30885" y1="0.69215" x2="7.53745" y2="0.70485" layer="21"/>
<rectangle x1="7.63905" y1="0.69215" x2="7.86765" y2="0.70485" layer="21"/>
<rectangle x1="7.99465" y1="0.69215" x2="8.21055" y2="0.70485" layer="21"/>
<rectangle x1="8.36295" y1="0.69215" x2="8.74395" y2="0.70485" layer="21"/>
<rectangle x1="8.82015" y1="0.69215" x2="9.03605" y2="0.70485" layer="21"/>
<rectangle x1="9.13765" y1="0.69215" x2="9.36625" y2="0.70485" layer="21"/>
<rectangle x1="9.46785" y1="0.69215" x2="9.70915" y2="0.70485" layer="21"/>
<rectangle x1="9.79805" y1="0.69215" x2="10.01395" y2="0.70485" layer="21"/>
<rectangle x1="10.64895" y1="0.69215" x2="10.89025" y2="0.70485" layer="21"/>
<rectangle x1="11.11885" y1="0.69215" x2="11.32205" y2="0.70485" layer="21"/>
<rectangle x1="11.43635" y1="0.69215" x2="11.65225" y2="0.70485" layer="21"/>
<rectangle x1="11.76655" y1="0.69215" x2="11.99515" y2="0.70485" layer="21"/>
<rectangle x1="12.09675" y1="0.69215" x2="12.32535" y2="0.70485" layer="21"/>
<rectangle x1="0.17145" y1="0.70485" x2="0.37465" y2="0.71755" layer="21"/>
<rectangle x1="0.45085" y1="0.70485" x2="0.64135" y2="0.71755" layer="21"/>
<rectangle x1="0.78105" y1="0.70485" x2="1.00965" y2="0.71755" layer="21"/>
<rectangle x1="1.11125" y1="0.70485" x2="1.32715" y2="0.71755" layer="21"/>
<rectangle x1="1.44145" y1="0.70485" x2="1.82245" y2="0.71755" layer="21"/>
<rectangle x1="1.89865" y1="0.70485" x2="2.11455" y2="0.71755" layer="21"/>
<rectangle x1="2.21615" y1="0.70485" x2="2.44475" y2="0.71755" layer="21"/>
<rectangle x1="2.53365" y1="0.70485" x2="2.73685" y2="0.71755" layer="21"/>
<rectangle x1="2.83845" y1="0.70485" x2="3.04165" y2="0.71755" layer="21"/>
<rectangle x1="3.14325" y1="0.70485" x2="3.37185" y2="0.71755" layer="21"/>
<rectangle x1="3.47345" y1="0.70485" x2="3.70205" y2="0.71755" layer="21"/>
<rectangle x1="3.80365" y1="0.70485" x2="4.00685" y2="0.71755" layer="21"/>
<rectangle x1="4.10845" y1="0.70485" x2="4.33705" y2="0.71755" layer="21"/>
<rectangle x1="4.43865" y1="0.70485" x2="4.66725" y2="0.71755" layer="21"/>
<rectangle x1="4.76885" y1="0.70485" x2="4.97205" y2="0.71755" layer="21"/>
<rectangle x1="5.07365" y1="0.70485" x2="5.30225" y2="0.71755" layer="21"/>
<rectangle x1="5.39115" y1="0.70485" x2="5.61975" y2="0.71755" layer="21"/>
<rectangle x1="5.94995" y1="0.70485" x2="6.19125" y2="0.71755" layer="21"/>
<rectangle x1="6.66115" y1="0.70485" x2="6.87705" y2="0.71755" layer="21"/>
<rectangle x1="6.97865" y1="0.70485" x2="7.20725" y2="0.71755" layer="21"/>
<rectangle x1="7.30885" y1="0.70485" x2="7.53745" y2="0.71755" layer="21"/>
<rectangle x1="7.63905" y1="0.70485" x2="7.86765" y2="0.71755" layer="21"/>
<rectangle x1="7.99465" y1="0.70485" x2="8.21055" y2="0.71755" layer="21"/>
<rectangle x1="8.36295" y1="0.70485" x2="8.74395" y2="0.71755" layer="21"/>
<rectangle x1="8.82015" y1="0.70485" x2="9.03605" y2="0.71755" layer="21"/>
<rectangle x1="9.13765" y1="0.70485" x2="9.36625" y2="0.71755" layer="21"/>
<rectangle x1="9.46785" y1="0.70485" x2="9.70915" y2="0.71755" layer="21"/>
<rectangle x1="9.79805" y1="0.70485" x2="10.00125" y2="0.71755" layer="21"/>
<rectangle x1="10.11555" y1="0.70485" x2="10.30605" y2="0.71755" layer="21"/>
<rectangle x1="10.64895" y1="0.70485" x2="10.89025" y2="0.71755" layer="21"/>
<rectangle x1="11.11885" y1="0.70485" x2="11.32205" y2="0.71755" layer="21"/>
<rectangle x1="11.43635" y1="0.70485" x2="11.65225" y2="0.71755" layer="21"/>
<rectangle x1="11.76655" y1="0.70485" x2="11.99515" y2="0.71755" layer="21"/>
<rectangle x1="12.09675" y1="0.70485" x2="12.32535" y2="0.71755" layer="21"/>
<rectangle x1="0.17145" y1="0.71755" x2="0.37465" y2="0.73025" layer="21"/>
<rectangle x1="0.43815" y1="0.71755" x2="0.64135" y2="0.73025" layer="21"/>
<rectangle x1="0.78105" y1="0.71755" x2="1.00965" y2="0.73025" layer="21"/>
<rectangle x1="1.11125" y1="0.71755" x2="1.32715" y2="0.73025" layer="21"/>
<rectangle x1="1.44145" y1="0.71755" x2="1.82245" y2="0.73025" layer="21"/>
<rectangle x1="1.89865" y1="0.71755" x2="2.11455" y2="0.73025" layer="21"/>
<rectangle x1="2.21615" y1="0.71755" x2="2.44475" y2="0.73025" layer="21"/>
<rectangle x1="2.53365" y1="0.71755" x2="2.73685" y2="0.73025" layer="21"/>
<rectangle x1="2.83845" y1="0.71755" x2="3.04165" y2="0.73025" layer="21"/>
<rectangle x1="3.14325" y1="0.71755" x2="3.37185" y2="0.73025" layer="21"/>
<rectangle x1="3.47345" y1="0.71755" x2="3.70205" y2="0.73025" layer="21"/>
<rectangle x1="3.80365" y1="0.71755" x2="4.00685" y2="0.73025" layer="21"/>
<rectangle x1="4.10845" y1="0.71755" x2="4.33705" y2="0.73025" layer="21"/>
<rectangle x1="4.45135" y1="0.71755" x2="4.66725" y2="0.73025" layer="21"/>
<rectangle x1="4.76885" y1="0.71755" x2="4.97205" y2="0.73025" layer="21"/>
<rectangle x1="5.07365" y1="0.71755" x2="5.30225" y2="0.73025" layer="21"/>
<rectangle x1="5.39115" y1="0.71755" x2="5.61975" y2="0.73025" layer="21"/>
<rectangle x1="5.94995" y1="0.71755" x2="6.19125" y2="0.73025" layer="21"/>
<rectangle x1="6.31825" y1="0.71755" x2="6.54685" y2="0.73025" layer="21"/>
<rectangle x1="6.66115" y1="0.71755" x2="6.87705" y2="0.73025" layer="21"/>
<rectangle x1="6.97865" y1="0.71755" x2="7.20725" y2="0.73025" layer="21"/>
<rectangle x1="7.30885" y1="0.71755" x2="7.53745" y2="0.73025" layer="21"/>
<rectangle x1="7.63905" y1="0.71755" x2="7.86765" y2="0.73025" layer="21"/>
<rectangle x1="7.99465" y1="0.71755" x2="8.21055" y2="0.73025" layer="21"/>
<rectangle x1="8.36295" y1="0.71755" x2="8.74395" y2="0.73025" layer="21"/>
<rectangle x1="8.82015" y1="0.71755" x2="9.03605" y2="0.73025" layer="21"/>
<rectangle x1="9.13765" y1="0.71755" x2="9.36625" y2="0.73025" layer="21"/>
<rectangle x1="9.46785" y1="0.71755" x2="9.70915" y2="0.73025" layer="21"/>
<rectangle x1="9.79805" y1="0.71755" x2="10.00125" y2="0.73025" layer="21"/>
<rectangle x1="10.11555" y1="0.71755" x2="10.30605" y2="0.73025" layer="21"/>
<rectangle x1="10.64895" y1="0.71755" x2="10.89025" y2="0.73025" layer="21"/>
<rectangle x1="11.11885" y1="0.71755" x2="11.32205" y2="0.73025" layer="21"/>
<rectangle x1="11.43635" y1="0.71755" x2="11.65225" y2="0.73025" layer="21"/>
<rectangle x1="11.76655" y1="0.71755" x2="11.99515" y2="0.73025" layer="21"/>
<rectangle x1="12.09675" y1="0.71755" x2="12.32535" y2="0.73025" layer="21"/>
<rectangle x1="0.17145" y1="0.73025" x2="0.37465" y2="0.74295" layer="21"/>
<rectangle x1="0.43815" y1="0.73025" x2="0.64135" y2="0.74295" layer="21"/>
<rectangle x1="0.79375" y1="0.73025" x2="1.00965" y2="0.74295" layer="21"/>
<rectangle x1="1.11125" y1="0.73025" x2="1.32715" y2="0.74295" layer="21"/>
<rectangle x1="1.44145" y1="0.73025" x2="1.82245" y2="0.74295" layer="21"/>
<rectangle x1="1.89865" y1="0.73025" x2="2.11455" y2="0.74295" layer="21"/>
<rectangle x1="2.21615" y1="0.73025" x2="2.43205" y2="0.74295" layer="21"/>
<rectangle x1="2.53365" y1="0.73025" x2="2.73685" y2="0.74295" layer="21"/>
<rectangle x1="2.83845" y1="0.73025" x2="3.04165" y2="0.74295" layer="21"/>
<rectangle x1="3.14325" y1="0.73025" x2="3.37185" y2="0.74295" layer="21"/>
<rectangle x1="3.47345" y1="0.73025" x2="3.70205" y2="0.74295" layer="21"/>
<rectangle x1="3.80365" y1="0.73025" x2="4.00685" y2="0.74295" layer="21"/>
<rectangle x1="4.10845" y1="0.73025" x2="4.33705" y2="0.74295" layer="21"/>
<rectangle x1="4.45135" y1="0.73025" x2="4.66725" y2="0.74295" layer="21"/>
<rectangle x1="4.76885" y1="0.73025" x2="4.97205" y2="0.74295" layer="21"/>
<rectangle x1="5.07365" y1="0.73025" x2="5.30225" y2="0.74295" layer="21"/>
<rectangle x1="5.39115" y1="0.73025" x2="5.61975" y2="0.74295" layer="21"/>
<rectangle x1="5.94995" y1="0.73025" x2="6.19125" y2="0.74295" layer="21"/>
<rectangle x1="6.31825" y1="0.73025" x2="6.54685" y2="0.74295" layer="21"/>
<rectangle x1="6.66115" y1="0.73025" x2="6.87705" y2="0.74295" layer="21"/>
<rectangle x1="6.97865" y1="0.73025" x2="7.19455" y2="0.74295" layer="21"/>
<rectangle x1="7.30885" y1="0.73025" x2="7.53745" y2="0.74295" layer="21"/>
<rectangle x1="7.63905" y1="0.73025" x2="7.86765" y2="0.74295" layer="21"/>
<rectangle x1="7.99465" y1="0.73025" x2="8.21055" y2="0.74295" layer="21"/>
<rectangle x1="8.36295" y1="0.73025" x2="8.74395" y2="0.74295" layer="21"/>
<rectangle x1="8.82015" y1="0.73025" x2="9.03605" y2="0.74295" layer="21"/>
<rectangle x1="9.13765" y1="0.73025" x2="9.36625" y2="0.74295" layer="21"/>
<rectangle x1="9.46785" y1="0.73025" x2="9.70915" y2="0.74295" layer="21"/>
<rectangle x1="9.79805" y1="0.73025" x2="10.00125" y2="0.74295" layer="21"/>
<rectangle x1="10.11555" y1="0.73025" x2="10.30605" y2="0.74295" layer="21"/>
<rectangle x1="10.64895" y1="0.73025" x2="10.89025" y2="0.74295" layer="21"/>
<rectangle x1="11.11885" y1="0.73025" x2="11.32205" y2="0.74295" layer="21"/>
<rectangle x1="11.43635" y1="0.73025" x2="11.65225" y2="0.74295" layer="21"/>
<rectangle x1="11.76655" y1="0.73025" x2="11.99515" y2="0.74295" layer="21"/>
<rectangle x1="12.09675" y1="0.73025" x2="12.32535" y2="0.74295" layer="21"/>
<rectangle x1="0.17145" y1="0.74295" x2="0.37465" y2="0.75565" layer="21"/>
<rectangle x1="0.43815" y1="0.74295" x2="0.64135" y2="0.75565" layer="21"/>
<rectangle x1="0.79375" y1="0.74295" x2="1.00965" y2="0.75565" layer="21"/>
<rectangle x1="1.11125" y1="0.74295" x2="1.32715" y2="0.75565" layer="21"/>
<rectangle x1="1.44145" y1="0.74295" x2="1.82245" y2="0.75565" layer="21"/>
<rectangle x1="1.89865" y1="0.74295" x2="2.11455" y2="0.75565" layer="21"/>
<rectangle x1="2.21615" y1="0.74295" x2="2.43205" y2="0.75565" layer="21"/>
<rectangle x1="2.53365" y1="0.74295" x2="2.73685" y2="0.75565" layer="21"/>
<rectangle x1="2.83845" y1="0.74295" x2="3.04165" y2="0.75565" layer="21"/>
<rectangle x1="3.14325" y1="0.74295" x2="3.37185" y2="0.75565" layer="21"/>
<rectangle x1="3.47345" y1="0.74295" x2="3.70205" y2="0.75565" layer="21"/>
<rectangle x1="3.80365" y1="0.74295" x2="4.00685" y2="0.75565" layer="21"/>
<rectangle x1="4.10845" y1="0.74295" x2="4.33705" y2="0.75565" layer="21"/>
<rectangle x1="4.45135" y1="0.74295" x2="4.66725" y2="0.75565" layer="21"/>
<rectangle x1="4.76885" y1="0.74295" x2="4.97205" y2="0.75565" layer="21"/>
<rectangle x1="5.07365" y1="0.74295" x2="5.30225" y2="0.75565" layer="21"/>
<rectangle x1="5.39115" y1="0.74295" x2="5.61975" y2="0.75565" layer="21"/>
<rectangle x1="5.94995" y1="0.74295" x2="6.19125" y2="0.75565" layer="21"/>
<rectangle x1="6.31825" y1="0.74295" x2="6.54685" y2="0.75565" layer="21"/>
<rectangle x1="6.66115" y1="0.74295" x2="6.87705" y2="0.75565" layer="21"/>
<rectangle x1="6.97865" y1="0.74295" x2="7.19455" y2="0.75565" layer="21"/>
<rectangle x1="7.30885" y1="0.74295" x2="7.53745" y2="0.75565" layer="21"/>
<rectangle x1="7.63905" y1="0.74295" x2="7.86765" y2="0.75565" layer="21"/>
<rectangle x1="7.99465" y1="0.74295" x2="8.21055" y2="0.75565" layer="21"/>
<rectangle x1="8.36295" y1="0.74295" x2="8.74395" y2="0.75565" layer="21"/>
<rectangle x1="8.82015" y1="0.74295" x2="9.03605" y2="0.75565" layer="21"/>
<rectangle x1="9.13765" y1="0.74295" x2="9.35355" y2="0.75565" layer="21"/>
<rectangle x1="9.46785" y1="0.74295" x2="9.70915" y2="0.75565" layer="21"/>
<rectangle x1="9.79805" y1="0.74295" x2="10.00125" y2="0.75565" layer="21"/>
<rectangle x1="10.11555" y1="0.74295" x2="10.30605" y2="0.75565" layer="21"/>
<rectangle x1="10.64895" y1="0.74295" x2="10.89025" y2="0.75565" layer="21"/>
<rectangle x1="11.11885" y1="0.74295" x2="11.32205" y2="0.75565" layer="21"/>
<rectangle x1="11.43635" y1="0.74295" x2="11.65225" y2="0.75565" layer="21"/>
<rectangle x1="11.76655" y1="0.74295" x2="11.99515" y2="0.75565" layer="21"/>
<rectangle x1="12.09675" y1="0.74295" x2="12.32535" y2="0.75565" layer="21"/>
<rectangle x1="0.17145" y1="0.75565" x2="0.37465" y2="0.76835" layer="21"/>
<rectangle x1="0.43815" y1="0.75565" x2="0.62865" y2="0.76835" layer="21"/>
<rectangle x1="0.79375" y1="0.75565" x2="1.00965" y2="0.76835" layer="21"/>
<rectangle x1="1.11125" y1="0.75565" x2="1.32715" y2="0.76835" layer="21"/>
<rectangle x1="1.44145" y1="0.75565" x2="1.82245" y2="0.76835" layer="21"/>
<rectangle x1="1.89865" y1="0.75565" x2="2.11455" y2="0.76835" layer="21"/>
<rectangle x1="2.21615" y1="0.75565" x2="2.43205" y2="0.76835" layer="21"/>
<rectangle x1="2.53365" y1="0.75565" x2="2.73685" y2="0.76835" layer="21"/>
<rectangle x1="2.83845" y1="0.75565" x2="3.04165" y2="0.76835" layer="21"/>
<rectangle x1="3.14325" y1="0.75565" x2="3.37185" y2="0.76835" layer="21"/>
<rectangle x1="3.47345" y1="0.75565" x2="3.70205" y2="0.76835" layer="21"/>
<rectangle x1="3.80365" y1="0.75565" x2="4.00685" y2="0.76835" layer="21"/>
<rectangle x1="4.10845" y1="0.75565" x2="4.33705" y2="0.76835" layer="21"/>
<rectangle x1="4.45135" y1="0.75565" x2="4.66725" y2="0.76835" layer="21"/>
<rectangle x1="4.76885" y1="0.75565" x2="4.97205" y2="0.76835" layer="21"/>
<rectangle x1="5.08635" y1="0.75565" x2="5.30225" y2="0.76835" layer="21"/>
<rectangle x1="5.39115" y1="0.75565" x2="5.60705" y2="0.76835" layer="21"/>
<rectangle x1="5.94995" y1="0.75565" x2="6.19125" y2="0.76835" layer="21"/>
<rectangle x1="6.31825" y1="0.75565" x2="6.54685" y2="0.76835" layer="21"/>
<rectangle x1="6.66115" y1="0.75565" x2="6.87705" y2="0.76835" layer="21"/>
<rectangle x1="6.97865" y1="0.75565" x2="7.19455" y2="0.76835" layer="21"/>
<rectangle x1="7.30885" y1="0.75565" x2="7.53745" y2="0.76835" layer="21"/>
<rectangle x1="7.63905" y1="0.75565" x2="7.86765" y2="0.76835" layer="21"/>
<rectangle x1="7.99465" y1="0.75565" x2="8.21055" y2="0.76835" layer="21"/>
<rectangle x1="8.36295" y1="0.75565" x2="8.74395" y2="0.76835" layer="21"/>
<rectangle x1="8.82015" y1="0.75565" x2="9.03605" y2="0.76835" layer="21"/>
<rectangle x1="9.13765" y1="0.75565" x2="9.35355" y2="0.76835" layer="21"/>
<rectangle x1="9.46785" y1="0.75565" x2="9.70915" y2="0.76835" layer="21"/>
<rectangle x1="9.79805" y1="0.75565" x2="10.00125" y2="0.76835" layer="21"/>
<rectangle x1="10.11555" y1="0.75565" x2="10.30605" y2="0.76835" layer="21"/>
<rectangle x1="10.64895" y1="0.75565" x2="10.89025" y2="0.76835" layer="21"/>
<rectangle x1="11.11885" y1="0.75565" x2="11.32205" y2="0.76835" layer="21"/>
<rectangle x1="11.43635" y1="0.75565" x2="11.65225" y2="0.76835" layer="21"/>
<rectangle x1="11.76655" y1="0.75565" x2="11.99515" y2="0.76835" layer="21"/>
<rectangle x1="12.09675" y1="0.75565" x2="12.32535" y2="0.76835" layer="21"/>
<rectangle x1="0.18415" y1="0.76835" x2="0.38735" y2="0.78105" layer="21"/>
<rectangle x1="0.43815" y1="0.76835" x2="0.62865" y2="0.78105" layer="21"/>
<rectangle x1="0.79375" y1="0.76835" x2="1.00965" y2="0.78105" layer="21"/>
<rectangle x1="1.11125" y1="0.76835" x2="1.32715" y2="0.78105" layer="21"/>
<rectangle x1="1.44145" y1="0.76835" x2="1.82245" y2="0.78105" layer="21"/>
<rectangle x1="1.89865" y1="0.76835" x2="2.11455" y2="0.78105" layer="21"/>
<rectangle x1="2.21615" y1="0.76835" x2="2.43205" y2="0.78105" layer="21"/>
<rectangle x1="2.53365" y1="0.76835" x2="2.73685" y2="0.78105" layer="21"/>
<rectangle x1="2.83845" y1="0.76835" x2="3.02895" y2="0.78105" layer="21"/>
<rectangle x1="3.14325" y1="0.76835" x2="3.37185" y2="0.78105" layer="21"/>
<rectangle x1="3.47345" y1="0.76835" x2="3.70205" y2="0.78105" layer="21"/>
<rectangle x1="3.80365" y1="0.76835" x2="4.00685" y2="0.78105" layer="21"/>
<rectangle x1="4.10845" y1="0.76835" x2="4.33705" y2="0.78105" layer="21"/>
<rectangle x1="4.45135" y1="0.76835" x2="4.66725" y2="0.78105" layer="21"/>
<rectangle x1="4.76885" y1="0.76835" x2="4.97205" y2="0.78105" layer="21"/>
<rectangle x1="5.08635" y1="0.76835" x2="5.30225" y2="0.78105" layer="21"/>
<rectangle x1="5.39115" y1="0.76835" x2="5.60705" y2="0.78105" layer="21"/>
<rectangle x1="5.94995" y1="0.76835" x2="6.19125" y2="0.78105" layer="21"/>
<rectangle x1="6.31825" y1="0.76835" x2="6.54685" y2="0.78105" layer="21"/>
<rectangle x1="6.66115" y1="0.76835" x2="6.87705" y2="0.78105" layer="21"/>
<rectangle x1="6.97865" y1="0.76835" x2="7.19455" y2="0.78105" layer="21"/>
<rectangle x1="7.30885" y1="0.76835" x2="7.53745" y2="0.78105" layer="21"/>
<rectangle x1="7.63905" y1="0.76835" x2="7.86765" y2="0.78105" layer="21"/>
<rectangle x1="7.99465" y1="0.76835" x2="8.21055" y2="0.78105" layer="21"/>
<rectangle x1="8.36295" y1="0.76835" x2="8.74395" y2="0.78105" layer="21"/>
<rectangle x1="8.82015" y1="0.76835" x2="9.03605" y2="0.78105" layer="21"/>
<rectangle x1="9.13765" y1="0.76835" x2="9.35355" y2="0.78105" layer="21"/>
<rectangle x1="9.46785" y1="0.76835" x2="9.70915" y2="0.78105" layer="21"/>
<rectangle x1="9.79805" y1="0.76835" x2="10.00125" y2="0.78105" layer="21"/>
<rectangle x1="10.10285" y1="0.76835" x2="10.30605" y2="0.78105" layer="21"/>
<rectangle x1="10.64895" y1="0.76835" x2="10.89025" y2="0.78105" layer="21"/>
<rectangle x1="11.11885" y1="0.76835" x2="11.32205" y2="0.78105" layer="21"/>
<rectangle x1="11.43635" y1="0.76835" x2="11.65225" y2="0.78105" layer="21"/>
<rectangle x1="11.76655" y1="0.76835" x2="11.99515" y2="0.78105" layer="21"/>
<rectangle x1="12.09675" y1="0.76835" x2="12.32535" y2="0.78105" layer="21"/>
<rectangle x1="0.18415" y1="0.78105" x2="0.38735" y2="0.79375" layer="21"/>
<rectangle x1="0.43815" y1="0.78105" x2="0.62865" y2="0.79375" layer="21"/>
<rectangle x1="0.79375" y1="0.78105" x2="1.00965" y2="0.79375" layer="21"/>
<rectangle x1="1.11125" y1="0.78105" x2="1.31445" y2="0.79375" layer="21"/>
<rectangle x1="1.44145" y1="0.78105" x2="1.82245" y2="0.79375" layer="21"/>
<rectangle x1="1.91135" y1="0.78105" x2="2.11455" y2="0.79375" layer="21"/>
<rectangle x1="2.21615" y1="0.78105" x2="2.43205" y2="0.79375" layer="21"/>
<rectangle x1="2.53365" y1="0.78105" x2="2.73685" y2="0.79375" layer="21"/>
<rectangle x1="2.83845" y1="0.78105" x2="3.02895" y2="0.79375" layer="21"/>
<rectangle x1="3.14325" y1="0.78105" x2="3.37185" y2="0.79375" layer="21"/>
<rectangle x1="3.47345" y1="0.78105" x2="3.70205" y2="0.79375" layer="21"/>
<rectangle x1="3.80365" y1="0.78105" x2="4.00685" y2="0.79375" layer="21"/>
<rectangle x1="4.10845" y1="0.78105" x2="4.32435" y2="0.79375" layer="21"/>
<rectangle x1="4.45135" y1="0.78105" x2="4.66725" y2="0.79375" layer="21"/>
<rectangle x1="4.76885" y1="0.78105" x2="4.95935" y2="0.79375" layer="21"/>
<rectangle x1="5.08635" y1="0.78105" x2="5.30225" y2="0.79375" layer="21"/>
<rectangle x1="5.39115" y1="0.78105" x2="5.60705" y2="0.79375" layer="21"/>
<rectangle x1="5.94995" y1="0.78105" x2="6.19125" y2="0.79375" layer="21"/>
<rectangle x1="6.31825" y1="0.78105" x2="6.54685" y2="0.79375" layer="21"/>
<rectangle x1="6.66115" y1="0.78105" x2="6.87705" y2="0.79375" layer="21"/>
<rectangle x1="6.97865" y1="0.78105" x2="7.19455" y2="0.79375" layer="21"/>
<rectangle x1="7.30885" y1="0.78105" x2="7.53745" y2="0.79375" layer="21"/>
<rectangle x1="7.63905" y1="0.78105" x2="7.86765" y2="0.79375" layer="21"/>
<rectangle x1="7.99465" y1="0.78105" x2="8.21055" y2="0.79375" layer="21"/>
<rectangle x1="8.36295" y1="0.78105" x2="8.74395" y2="0.79375" layer="21"/>
<rectangle x1="8.83285" y1="0.78105" x2="9.03605" y2="0.79375" layer="21"/>
<rectangle x1="9.13765" y1="0.78105" x2="9.35355" y2="0.79375" layer="21"/>
<rectangle x1="9.46785" y1="0.78105" x2="9.70915" y2="0.79375" layer="21"/>
<rectangle x1="9.79805" y1="0.78105" x2="10.00125" y2="0.79375" layer="21"/>
<rectangle x1="10.10285" y1="0.78105" x2="10.30605" y2="0.79375" layer="21"/>
<rectangle x1="10.64895" y1="0.78105" x2="10.89025" y2="0.79375" layer="21"/>
<rectangle x1="11.13155" y1="0.78105" x2="11.32205" y2="0.79375" layer="21"/>
<rectangle x1="11.42365" y1="0.78105" x2="11.65225" y2="0.79375" layer="21"/>
<rectangle x1="11.76655" y1="0.78105" x2="11.99515" y2="0.79375" layer="21"/>
<rectangle x1="12.09675" y1="0.78105" x2="12.32535" y2="0.79375" layer="21"/>
<rectangle x1="0.18415" y1="0.79375" x2="0.38735" y2="0.80645" layer="21"/>
<rectangle x1="0.43815" y1="0.79375" x2="0.62865" y2="0.80645" layer="21"/>
<rectangle x1="0.80645" y1="0.79375" x2="1.00965" y2="0.80645" layer="21"/>
<rectangle x1="1.11125" y1="0.79375" x2="1.31445" y2="0.80645" layer="21"/>
<rectangle x1="1.44145" y1="0.79375" x2="1.82245" y2="0.80645" layer="21"/>
<rectangle x1="1.91135" y1="0.79375" x2="2.11455" y2="0.80645" layer="21"/>
<rectangle x1="2.21615" y1="0.79375" x2="2.43205" y2="0.80645" layer="21"/>
<rectangle x1="2.53365" y1="0.79375" x2="2.73685" y2="0.80645" layer="21"/>
<rectangle x1="2.83845" y1="0.79375" x2="3.02895" y2="0.80645" layer="21"/>
<rectangle x1="3.14325" y1="0.79375" x2="3.37185" y2="0.80645" layer="21"/>
<rectangle x1="3.47345" y1="0.79375" x2="3.70205" y2="0.80645" layer="21"/>
<rectangle x1="3.80365" y1="0.79375" x2="4.00685" y2="0.80645" layer="21"/>
<rectangle x1="4.10845" y1="0.79375" x2="4.32435" y2="0.80645" layer="21"/>
<rectangle x1="4.45135" y1="0.79375" x2="4.66725" y2="0.80645" layer="21"/>
<rectangle x1="4.76885" y1="0.79375" x2="4.95935" y2="0.80645" layer="21"/>
<rectangle x1="5.08635" y1="0.79375" x2="5.30225" y2="0.80645" layer="21"/>
<rectangle x1="5.39115" y1="0.79375" x2="5.60705" y2="0.80645" layer="21"/>
<rectangle x1="5.94995" y1="0.79375" x2="6.19125" y2="0.80645" layer="21"/>
<rectangle x1="6.31825" y1="0.79375" x2="6.54685" y2="0.80645" layer="21"/>
<rectangle x1="6.67385" y1="0.79375" x2="6.87705" y2="0.80645" layer="21"/>
<rectangle x1="6.97865" y1="0.79375" x2="7.19455" y2="0.80645" layer="21"/>
<rectangle x1="7.30885" y1="0.79375" x2="7.53745" y2="0.80645" layer="21"/>
<rectangle x1="7.63905" y1="0.79375" x2="7.86765" y2="0.80645" layer="21"/>
<rectangle x1="7.99465" y1="0.79375" x2="8.21055" y2="0.80645" layer="21"/>
<rectangle x1="8.36295" y1="0.79375" x2="8.74395" y2="0.80645" layer="21"/>
<rectangle x1="8.83285" y1="0.79375" x2="9.04875" y2="0.80645" layer="21"/>
<rectangle x1="9.13765" y1="0.79375" x2="9.35355" y2="0.80645" layer="21"/>
<rectangle x1="9.46785" y1="0.79375" x2="9.70915" y2="0.80645" layer="21"/>
<rectangle x1="9.81075" y1="0.79375" x2="10.00125" y2="0.80645" layer="21"/>
<rectangle x1="10.10285" y1="0.79375" x2="10.30605" y2="0.80645" layer="21"/>
<rectangle x1="10.64895" y1="0.79375" x2="10.89025" y2="0.80645" layer="21"/>
<rectangle x1="11.13155" y1="0.79375" x2="11.33475" y2="0.80645" layer="21"/>
<rectangle x1="11.42365" y1="0.79375" x2="11.65225" y2="0.80645" layer="21"/>
<rectangle x1="11.76655" y1="0.79375" x2="11.99515" y2="0.80645" layer="21"/>
<rectangle x1="12.09675" y1="0.79375" x2="12.32535" y2="0.80645" layer="21"/>
<rectangle x1="0.18415" y1="0.80645" x2="0.38735" y2="0.81915" layer="21"/>
<rectangle x1="0.43815" y1="0.80645" x2="0.62865" y2="0.81915" layer="21"/>
<rectangle x1="0.80645" y1="0.80645" x2="1.00965" y2="0.81915" layer="21"/>
<rectangle x1="1.09855" y1="0.80645" x2="1.31445" y2="0.81915" layer="21"/>
<rectangle x1="1.44145" y1="0.80645" x2="1.82245" y2="0.81915" layer="21"/>
<rectangle x1="1.91135" y1="0.80645" x2="2.12725" y2="0.81915" layer="21"/>
<rectangle x1="2.21615" y1="0.80645" x2="2.41935" y2="0.81915" layer="21"/>
<rectangle x1="2.53365" y1="0.80645" x2="2.73685" y2="0.81915" layer="21"/>
<rectangle x1="2.83845" y1="0.80645" x2="3.02895" y2="0.81915" layer="21"/>
<rectangle x1="3.14325" y1="0.80645" x2="3.38455" y2="0.81915" layer="21"/>
<rectangle x1="3.47345" y1="0.80645" x2="3.70205" y2="0.81915" layer="21"/>
<rectangle x1="3.81635" y1="0.80645" x2="4.00685" y2="0.81915" layer="21"/>
<rectangle x1="4.10845" y1="0.80645" x2="4.32435" y2="0.81915" layer="21"/>
<rectangle x1="4.46405" y1="0.80645" x2="4.67995" y2="0.81915" layer="21"/>
<rectangle x1="4.75615" y1="0.80645" x2="4.95935" y2="0.81915" layer="21"/>
<rectangle x1="5.09905" y1="0.80645" x2="5.30225" y2="0.81915" layer="21"/>
<rectangle x1="5.39115" y1="0.80645" x2="5.59435" y2="0.81915" layer="21"/>
<rectangle x1="5.94995" y1="0.80645" x2="6.19125" y2="0.81915" layer="21"/>
<rectangle x1="6.31825" y1="0.80645" x2="6.54685" y2="0.81915" layer="21"/>
<rectangle x1="6.67385" y1="0.80645" x2="6.88975" y2="0.81915" layer="21"/>
<rectangle x1="6.96595" y1="0.80645" x2="7.18185" y2="0.81915" layer="21"/>
<rectangle x1="7.30885" y1="0.80645" x2="7.55015" y2="0.81915" layer="21"/>
<rectangle x1="7.63905" y1="0.80645" x2="7.85495" y2="0.81915" layer="21"/>
<rectangle x1="7.99465" y1="0.80645" x2="8.21055" y2="0.81915" layer="21"/>
<rectangle x1="8.36295" y1="0.80645" x2="8.74395" y2="0.81915" layer="21"/>
<rectangle x1="8.83285" y1="0.80645" x2="9.04875" y2="0.81915" layer="21"/>
<rectangle x1="9.13765" y1="0.80645" x2="9.34085" y2="0.81915" layer="21"/>
<rectangle x1="9.46785" y1="0.80645" x2="9.70915" y2="0.81915" layer="21"/>
<rectangle x1="9.81075" y1="0.80645" x2="10.01395" y2="0.81915" layer="21"/>
<rectangle x1="10.10285" y1="0.80645" x2="10.30605" y2="0.81915" layer="21"/>
<rectangle x1="10.64895" y1="0.80645" x2="10.89025" y2="0.81915" layer="21"/>
<rectangle x1="11.13155" y1="0.80645" x2="11.33475" y2="0.81915" layer="21"/>
<rectangle x1="11.42365" y1="0.80645" x2="11.63955" y2="0.81915" layer="21"/>
<rectangle x1="11.76655" y1="0.80645" x2="11.99515" y2="0.81915" layer="21"/>
<rectangle x1="12.08405" y1="0.80645" x2="12.32535" y2="0.81915" layer="21"/>
<rectangle x1="0.18415" y1="0.81915" x2="0.38735" y2="0.83185" layer="21"/>
<rectangle x1="0.43815" y1="0.81915" x2="0.62865" y2="0.83185" layer="21"/>
<rectangle x1="0.80645" y1="0.81915" x2="1.02235" y2="0.83185" layer="21"/>
<rectangle x1="1.09855" y1="0.81915" x2="1.30175" y2="0.83185" layer="21"/>
<rectangle x1="1.44145" y1="0.81915" x2="1.82245" y2="0.83185" layer="21"/>
<rectangle x1="1.91135" y1="0.81915" x2="2.12725" y2="0.83185" layer="21"/>
<rectangle x1="2.20345" y1="0.81915" x2="2.41935" y2="0.83185" layer="21"/>
<rectangle x1="2.53365" y1="0.81915" x2="2.74955" y2="0.83185" layer="21"/>
<rectangle x1="2.82575" y1="0.81915" x2="3.02895" y2="0.83185" layer="21"/>
<rectangle x1="3.14325" y1="0.81915" x2="3.38455" y2="0.83185" layer="21"/>
<rectangle x1="3.46075" y1="0.81915" x2="3.70205" y2="0.83185" layer="21"/>
<rectangle x1="3.81635" y1="0.81915" x2="4.01955" y2="0.83185" layer="21"/>
<rectangle x1="4.09575" y1="0.81915" x2="4.32435" y2="0.83185" layer="21"/>
<rectangle x1="4.46405" y1="0.81915" x2="4.67995" y2="0.83185" layer="21"/>
<rectangle x1="4.75615" y1="0.81915" x2="4.95935" y2="0.83185" layer="21"/>
<rectangle x1="5.09905" y1="0.81915" x2="5.31495" y2="0.83185" layer="21"/>
<rectangle x1="5.37845" y1="0.81915" x2="5.59435" y2="0.83185" layer="21"/>
<rectangle x1="5.94995" y1="0.81915" x2="6.19125" y2="0.83185" layer="21"/>
<rectangle x1="6.31825" y1="0.81915" x2="6.54685" y2="0.83185" layer="21"/>
<rectangle x1="6.67385" y1="0.81915" x2="6.88975" y2="0.83185" layer="21"/>
<rectangle x1="6.96595" y1="0.81915" x2="7.18185" y2="0.83185" layer="21"/>
<rectangle x1="7.30885" y1="0.81915" x2="7.55015" y2="0.83185" layer="21"/>
<rectangle x1="7.62635" y1="0.81915" x2="7.85495" y2="0.83185" layer="21"/>
<rectangle x1="7.93115" y1="0.81915" x2="8.28675" y2="0.83185" layer="21"/>
<rectangle x1="8.36295" y1="0.81915" x2="8.74395" y2="0.83185" layer="21"/>
<rectangle x1="8.83285" y1="0.81915" x2="9.04875" y2="0.83185" layer="21"/>
<rectangle x1="9.12495" y1="0.81915" x2="9.34085" y2="0.83185" layer="21"/>
<rectangle x1="9.46785" y1="0.81915" x2="9.70915" y2="0.83185" layer="21"/>
<rectangle x1="9.81075" y1="0.81915" x2="10.01395" y2="0.83185" layer="21"/>
<rectangle x1="10.10285" y1="0.81915" x2="10.30605" y2="0.83185" layer="21"/>
<rectangle x1="10.64895" y1="0.81915" x2="10.89025" y2="0.83185" layer="21"/>
<rectangle x1="11.13155" y1="0.81915" x2="11.33475" y2="0.83185" layer="21"/>
<rectangle x1="11.41095" y1="0.81915" x2="11.63955" y2="0.83185" layer="21"/>
<rectangle x1="11.76655" y1="0.81915" x2="12.00785" y2="0.83185" layer="21"/>
<rectangle x1="12.08405" y1="0.81915" x2="12.32535" y2="0.83185" layer="21"/>
<rectangle x1="0.18415" y1="0.83185" x2="0.38735" y2="0.84455" layer="21"/>
<rectangle x1="0.43815" y1="0.83185" x2="0.62865" y2="0.84455" layer="21"/>
<rectangle x1="0.81915" y1="0.83185" x2="1.03505" y2="0.84455" layer="21"/>
<rectangle x1="1.07315" y1="0.83185" x2="1.30175" y2="0.84455" layer="21"/>
<rectangle x1="1.44145" y1="0.83185" x2="1.82245" y2="0.84455" layer="21"/>
<rectangle x1="1.92405" y1="0.83185" x2="2.15265" y2="0.84455" layer="21"/>
<rectangle x1="2.19075" y1="0.83185" x2="2.40665" y2="0.84455" layer="21"/>
<rectangle x1="2.54635" y1="0.83185" x2="2.76225" y2="0.84455" layer="21"/>
<rectangle x1="2.81305" y1="0.83185" x2="3.02895" y2="0.84455" layer="21"/>
<rectangle x1="3.14325" y1="0.83185" x2="3.40995" y2="0.84455" layer="21"/>
<rectangle x1="3.44805" y1="0.83185" x2="3.68935" y2="0.84455" layer="21"/>
<rectangle x1="3.81635" y1="0.83185" x2="4.03225" y2="0.84455" layer="21"/>
<rectangle x1="4.07035" y1="0.83185" x2="4.31165" y2="0.84455" layer="21"/>
<rectangle x1="4.46405" y1="0.83185" x2="4.70535" y2="0.84455" layer="21"/>
<rectangle x1="4.73075" y1="0.83185" x2="4.94665" y2="0.84455" layer="21"/>
<rectangle x1="5.09905" y1="0.83185" x2="5.32765" y2="0.84455" layer="21"/>
<rectangle x1="5.36575" y1="0.83185" x2="5.58165" y2="0.84455" layer="21"/>
<rectangle x1="5.96265" y1="0.83185" x2="6.19125" y2="0.84455" layer="21"/>
<rectangle x1="6.31825" y1="0.83185" x2="6.54685" y2="0.84455" layer="21"/>
<rectangle x1="6.68655" y1="0.83185" x2="6.91515" y2="0.84455" layer="21"/>
<rectangle x1="6.95325" y1="0.83185" x2="7.16915" y2="0.84455" layer="21"/>
<rectangle x1="7.30885" y1="0.83185" x2="7.57555" y2="0.84455" layer="21"/>
<rectangle x1="7.61365" y1="0.83185" x2="7.85495" y2="0.84455" layer="21"/>
<rectangle x1="7.93115" y1="0.83185" x2="8.28675" y2="0.84455" layer="21"/>
<rectangle x1="8.36295" y1="0.83185" x2="8.74395" y2="0.84455" layer="21"/>
<rectangle x1="8.84555" y1="0.83185" x2="9.07415" y2="0.84455" layer="21"/>
<rectangle x1="9.11225" y1="0.83185" x2="9.32815" y2="0.84455" layer="21"/>
<rectangle x1="9.46785" y1="0.83185" x2="9.70915" y2="0.84455" layer="21"/>
<rectangle x1="9.81075" y1="0.83185" x2="10.03935" y2="0.84455" layer="21"/>
<rectangle x1="10.07745" y1="0.83185" x2="10.29335" y2="0.84455" layer="21"/>
<rectangle x1="10.64895" y1="0.83185" x2="10.89025" y2="0.84455" layer="21"/>
<rectangle x1="11.14425" y1="0.83185" x2="11.36015" y2="0.84455" layer="21"/>
<rectangle x1="11.39825" y1="0.83185" x2="11.63955" y2="0.84455" layer="21"/>
<rectangle x1="11.76655" y1="0.83185" x2="12.02055" y2="0.84455" layer="21"/>
<rectangle x1="12.05865" y1="0.83185" x2="12.32535" y2="0.84455" layer="21"/>
<rectangle x1="0.18415" y1="0.84455" x2="0.38735" y2="0.85725" layer="21"/>
<rectangle x1="0.43815" y1="0.84455" x2="0.61595" y2="0.85725" layer="21"/>
<rectangle x1="0.81915" y1="0.84455" x2="1.28905" y2="0.85725" layer="21"/>
<rectangle x1="1.44145" y1="0.84455" x2="1.65735" y2="0.85725" layer="21"/>
<rectangle x1="1.68275" y1="0.84455" x2="1.82245" y2="0.85725" layer="21"/>
<rectangle x1="1.92405" y1="0.84455" x2="2.40665" y2="0.85725" layer="21"/>
<rectangle x1="2.54635" y1="0.84455" x2="3.01625" y2="0.85725" layer="21"/>
<rectangle x1="3.14325" y1="0.84455" x2="3.68935" y2="0.85725" layer="21"/>
<rectangle x1="3.82905" y1="0.84455" x2="4.31165" y2="0.85725" layer="21"/>
<rectangle x1="4.47675" y1="0.84455" x2="4.94665" y2="0.85725" layer="21"/>
<rectangle x1="5.11175" y1="0.84455" x2="5.58165" y2="0.85725" layer="21"/>
<rectangle x1="5.96265" y1="0.84455" x2="6.19125" y2="0.85725" layer="21"/>
<rectangle x1="6.31825" y1="0.84455" x2="6.54685" y2="0.85725" layer="21"/>
<rectangle x1="6.68655" y1="0.84455" x2="7.16915" y2="0.85725" layer="21"/>
<rectangle x1="7.30885" y1="0.84455" x2="7.85495" y2="0.85725" layer="21"/>
<rectangle x1="7.93115" y1="0.84455" x2="8.28675" y2="0.85725" layer="21"/>
<rectangle x1="8.36295" y1="0.84455" x2="8.57885" y2="0.85725" layer="21"/>
<rectangle x1="8.60425" y1="0.84455" x2="8.74395" y2="0.85725" layer="21"/>
<rectangle x1="8.84555" y1="0.84455" x2="9.32815" y2="0.85725" layer="21"/>
<rectangle x1="9.46785" y1="0.84455" x2="9.70915" y2="0.85725" layer="21"/>
<rectangle x1="9.82345" y1="0.84455" x2="10.29335" y2="0.85725" layer="21"/>
<rectangle x1="10.64895" y1="0.84455" x2="10.89025" y2="0.85725" layer="21"/>
<rectangle x1="11.14425" y1="0.84455" x2="11.62685" y2="0.85725" layer="21"/>
<rectangle x1="11.76655" y1="0.84455" x2="12.31265" y2="0.85725" layer="21"/>
<rectangle x1="0.19685" y1="0.85725" x2="0.38735" y2="0.86995" layer="21"/>
<rectangle x1="0.42545" y1="0.85725" x2="0.61595" y2="0.86995" layer="21"/>
<rectangle x1="0.83185" y1="0.85725" x2="1.28905" y2="0.86995" layer="21"/>
<rectangle x1="1.44145" y1="0.85725" x2="1.65735" y2="0.86995" layer="21"/>
<rectangle x1="1.69545" y1="0.85725" x2="1.82245" y2="0.86995" layer="21"/>
<rectangle x1="1.93675" y1="0.85725" x2="2.39395" y2="0.86995" layer="21"/>
<rectangle x1="2.54635" y1="0.85725" x2="3.01625" y2="0.86995" layer="21"/>
<rectangle x1="3.14325" y1="0.85725" x2="3.37185" y2="0.86995" layer="21"/>
<rectangle x1="3.38455" y1="0.85725" x2="3.68935" y2="0.86995" layer="21"/>
<rectangle x1="3.84175" y1="0.85725" x2="4.31165" y2="0.86995" layer="21"/>
<rectangle x1="4.48945" y1="0.85725" x2="4.93395" y2="0.86995" layer="21"/>
<rectangle x1="5.11175" y1="0.85725" x2="5.56895" y2="0.86995" layer="21"/>
<rectangle x1="5.96265" y1="0.85725" x2="6.19125" y2="0.86995" layer="21"/>
<rectangle x1="6.31825" y1="0.85725" x2="6.54685" y2="0.86995" layer="21"/>
<rectangle x1="6.69925" y1="0.85725" x2="7.15645" y2="0.86995" layer="21"/>
<rectangle x1="7.30885" y1="0.85725" x2="7.85495" y2="0.86995" layer="21"/>
<rectangle x1="7.93115" y1="0.85725" x2="8.28675" y2="0.86995" layer="21"/>
<rectangle x1="8.36295" y1="0.85725" x2="8.57885" y2="0.86995" layer="21"/>
<rectangle x1="8.61695" y1="0.85725" x2="8.74395" y2="0.86995" layer="21"/>
<rectangle x1="8.85825" y1="0.85725" x2="9.31545" y2="0.86995" layer="21"/>
<rectangle x1="9.46785" y1="0.85725" x2="9.70915" y2="0.86995" layer="21"/>
<rectangle x1="9.82345" y1="0.85725" x2="10.28065" y2="0.86995" layer="21"/>
<rectangle x1="10.64895" y1="0.85725" x2="10.89025" y2="0.86995" layer="21"/>
<rectangle x1="11.15695" y1="0.85725" x2="11.62685" y2="0.86995" layer="21"/>
<rectangle x1="11.76655" y1="0.85725" x2="12.31265" y2="0.86995" layer="21"/>
<rectangle x1="0.19685" y1="0.86995" x2="0.40005" y2="0.88265" layer="21"/>
<rectangle x1="0.42545" y1="0.86995" x2="0.61595" y2="0.88265" layer="21"/>
<rectangle x1="0.83185" y1="0.86995" x2="1.27635" y2="0.88265" layer="21"/>
<rectangle x1="1.44145" y1="0.86995" x2="1.65735" y2="0.88265" layer="21"/>
<rectangle x1="1.69545" y1="0.86995" x2="1.82245" y2="0.88265" layer="21"/>
<rectangle x1="1.94945" y1="0.86995" x2="2.38125" y2="0.88265" layer="21"/>
<rectangle x1="2.55905" y1="0.86995" x2="3.00355" y2="0.88265" layer="21"/>
<rectangle x1="3.14325" y1="0.86995" x2="3.37185" y2="0.88265" layer="21"/>
<rectangle x1="3.39725" y1="0.86995" x2="3.68935" y2="0.88265" layer="21"/>
<rectangle x1="3.84175" y1="0.86995" x2="4.29895" y2="0.88265" layer="21"/>
<rectangle x1="4.48945" y1="0.86995" x2="4.92125" y2="0.88265" layer="21"/>
<rectangle x1="5.12445" y1="0.86995" x2="5.55625" y2="0.88265" layer="21"/>
<rectangle x1="5.96265" y1="0.86995" x2="6.19125" y2="0.88265" layer="21"/>
<rectangle x1="6.31825" y1="0.86995" x2="6.54685" y2="0.88265" layer="21"/>
<rectangle x1="6.71195" y1="0.86995" x2="7.14375" y2="0.88265" layer="21"/>
<rectangle x1="7.30885" y1="0.86995" x2="7.55015" y2="0.88265" layer="21"/>
<rectangle x1="7.56285" y1="0.86995" x2="7.84225" y2="0.88265" layer="21"/>
<rectangle x1="7.93115" y1="0.86995" x2="8.28675" y2="0.88265" layer="21"/>
<rectangle x1="8.36295" y1="0.86995" x2="8.57885" y2="0.88265" layer="21"/>
<rectangle x1="8.62965" y1="0.86995" x2="8.74395" y2="0.88265" layer="21"/>
<rectangle x1="8.87095" y1="0.86995" x2="9.30275" y2="0.88265" layer="21"/>
<rectangle x1="9.46785" y1="0.86995" x2="9.70915" y2="0.88265" layer="21"/>
<rectangle x1="9.83615" y1="0.86995" x2="10.28065" y2="0.88265" layer="21"/>
<rectangle x1="10.64895" y1="0.86995" x2="10.89025" y2="0.88265" layer="21"/>
<rectangle x1="11.16965" y1="0.86995" x2="11.61415" y2="0.88265" layer="21"/>
<rectangle x1="11.76655" y1="0.86995" x2="11.99515" y2="0.88265" layer="21"/>
<rectangle x1="12.00785" y1="0.86995" x2="12.31265" y2="0.88265" layer="21"/>
<rectangle x1="0.19685" y1="0.88265" x2="0.40005" y2="0.89535" layer="21"/>
<rectangle x1="0.42545" y1="0.88265" x2="0.61595" y2="0.89535" layer="21"/>
<rectangle x1="0.84455" y1="0.88265" x2="1.26365" y2="0.89535" layer="21"/>
<rectangle x1="1.44145" y1="0.88265" x2="1.65735" y2="0.89535" layer="21"/>
<rectangle x1="1.70815" y1="0.88265" x2="1.82245" y2="0.89535" layer="21"/>
<rectangle x1="1.96215" y1="0.88265" x2="2.36855" y2="0.89535" layer="21"/>
<rectangle x1="2.57175" y1="0.88265" x2="2.99085" y2="0.89535" layer="21"/>
<rectangle x1="3.14325" y1="0.88265" x2="3.37185" y2="0.89535" layer="21"/>
<rectangle x1="3.40995" y1="0.88265" x2="3.67665" y2="0.89535" layer="21"/>
<rectangle x1="3.85445" y1="0.88265" x2="4.28625" y2="0.89535" layer="21"/>
<rectangle x1="4.50215" y1="0.88265" x2="4.90855" y2="0.89535" layer="21"/>
<rectangle x1="5.13715" y1="0.88265" x2="5.54355" y2="0.89535" layer="21"/>
<rectangle x1="5.96265" y1="0.88265" x2="6.19125" y2="0.89535" layer="21"/>
<rectangle x1="6.31825" y1="0.88265" x2="6.54685" y2="0.89535" layer="21"/>
<rectangle x1="6.72465" y1="0.88265" x2="7.13105" y2="0.89535" layer="21"/>
<rectangle x1="7.30885" y1="0.88265" x2="7.53745" y2="0.89535" layer="21"/>
<rectangle x1="7.57555" y1="0.88265" x2="7.84225" y2="0.89535" layer="21"/>
<rectangle x1="7.93115" y1="0.88265" x2="8.28675" y2="0.89535" layer="21"/>
<rectangle x1="8.36295" y1="0.88265" x2="8.57885" y2="0.89535" layer="21"/>
<rectangle x1="8.62965" y1="0.88265" x2="8.74395" y2="0.89535" layer="21"/>
<rectangle x1="8.88365" y1="0.88265" x2="9.29005" y2="0.89535" layer="21"/>
<rectangle x1="9.46785" y1="0.88265" x2="9.70915" y2="0.89535" layer="21"/>
<rectangle x1="9.83615" y1="0.88265" x2="10.26795" y2="0.89535" layer="21"/>
<rectangle x1="10.64895" y1="0.88265" x2="10.89025" y2="0.89535" layer="21"/>
<rectangle x1="11.18235" y1="0.88265" x2="11.60145" y2="0.89535" layer="21"/>
<rectangle x1="11.76655" y1="0.88265" x2="11.99515" y2="0.89535" layer="21"/>
<rectangle x1="12.02055" y1="0.88265" x2="12.29995" y2="0.89535" layer="21"/>
<rectangle x1="0.19685" y1="0.89535" x2="0.40005" y2="0.90805" layer="21"/>
<rectangle x1="0.42545" y1="0.89535" x2="0.61595" y2="0.90805" layer="21"/>
<rectangle x1="0.85725" y1="0.89535" x2="1.25095" y2="0.90805" layer="21"/>
<rectangle x1="1.44145" y1="0.89535" x2="1.65735" y2="0.90805" layer="21"/>
<rectangle x1="1.72085" y1="0.89535" x2="1.82245" y2="0.90805" layer="21"/>
<rectangle x1="1.97485" y1="0.89535" x2="2.35585" y2="0.90805" layer="21"/>
<rectangle x1="2.58445" y1="0.89535" x2="2.97815" y2="0.90805" layer="21"/>
<rectangle x1="3.14325" y1="0.89535" x2="3.37185" y2="0.90805" layer="21"/>
<rectangle x1="3.42265" y1="0.89535" x2="3.67665" y2="0.90805" layer="21"/>
<rectangle x1="3.87985" y1="0.89535" x2="4.27355" y2="0.90805" layer="21"/>
<rectangle x1="4.52755" y1="0.89535" x2="4.89585" y2="0.90805" layer="21"/>
<rectangle x1="5.14985" y1="0.89535" x2="5.53085" y2="0.90805" layer="21"/>
<rectangle x1="5.96265" y1="0.89535" x2="6.19125" y2="0.90805" layer="21"/>
<rectangle x1="6.30555" y1="0.89535" x2="6.54685" y2="0.90805" layer="21"/>
<rectangle x1="6.73735" y1="0.89535" x2="7.11835" y2="0.90805" layer="21"/>
<rectangle x1="7.30885" y1="0.89535" x2="7.53745" y2="0.90805" layer="21"/>
<rectangle x1="7.57555" y1="0.89535" x2="7.82955" y2="0.90805" layer="21"/>
<rectangle x1="7.93115" y1="0.89535" x2="8.28675" y2="0.90805" layer="21"/>
<rectangle x1="8.36295" y1="0.89535" x2="8.57885" y2="0.90805" layer="21"/>
<rectangle x1="8.64235" y1="0.89535" x2="8.74395" y2="0.90805" layer="21"/>
<rectangle x1="8.89635" y1="0.89535" x2="9.27735" y2="0.90805" layer="21"/>
<rectangle x1="9.46785" y1="0.89535" x2="9.70915" y2="0.90805" layer="21"/>
<rectangle x1="9.84885" y1="0.89535" x2="10.25525" y2="0.90805" layer="21"/>
<rectangle x1="10.64895" y1="0.89535" x2="10.89025" y2="0.90805" layer="21"/>
<rectangle x1="11.19505" y1="0.89535" x2="11.58875" y2="0.90805" layer="21"/>
<rectangle x1="11.76655" y1="0.89535" x2="11.99515" y2="0.90805" layer="21"/>
<rectangle x1="12.03325" y1="0.89535" x2="12.29995" y2="0.90805" layer="21"/>
<rectangle x1="0.19685" y1="0.90805" x2="0.40005" y2="0.92075" layer="21"/>
<rectangle x1="0.42545" y1="0.90805" x2="0.61595" y2="0.92075" layer="21"/>
<rectangle x1="0.88265" y1="0.90805" x2="1.22555" y2="0.92075" layer="21"/>
<rectangle x1="1.44145" y1="0.90805" x2="1.65735" y2="0.92075" layer="21"/>
<rectangle x1="1.73355" y1="0.90805" x2="1.82245" y2="0.92075" layer="21"/>
<rectangle x1="1.98755" y1="0.90805" x2="2.34315" y2="0.92075" layer="21"/>
<rectangle x1="2.59715" y1="0.90805" x2="2.96545" y2="0.92075" layer="21"/>
<rectangle x1="3.14325" y1="0.90805" x2="3.37185" y2="0.92075" layer="21"/>
<rectangle x1="3.43535" y1="0.90805" x2="3.66395" y2="0.92075" layer="21"/>
<rectangle x1="3.89255" y1="0.90805" x2="4.26085" y2="0.92075" layer="21"/>
<rectangle x1="4.54025" y1="0.90805" x2="4.88315" y2="0.92075" layer="21"/>
<rectangle x1="5.16255" y1="0.90805" x2="5.51815" y2="0.92075" layer="21"/>
<rectangle x1="5.96265" y1="0.90805" x2="6.19125" y2="0.92075" layer="21"/>
<rectangle x1="6.30555" y1="0.90805" x2="6.54685" y2="0.92075" layer="21"/>
<rectangle x1="6.75005" y1="0.90805" x2="7.10565" y2="0.92075" layer="21"/>
<rectangle x1="7.30885" y1="0.90805" x2="7.53745" y2="0.92075" layer="21"/>
<rectangle x1="7.58825" y1="0.90805" x2="7.82955" y2="0.92075" layer="21"/>
<rectangle x1="7.93115" y1="0.90805" x2="8.28675" y2="0.92075" layer="21"/>
<rectangle x1="8.36295" y1="0.90805" x2="8.57885" y2="0.92075" layer="21"/>
<rectangle x1="8.65505" y1="0.90805" x2="8.74395" y2="0.92075" layer="21"/>
<rectangle x1="8.90905" y1="0.90805" x2="9.26465" y2="0.92075" layer="21"/>
<rectangle x1="9.46785" y1="0.90805" x2="9.70915" y2="0.92075" layer="21"/>
<rectangle x1="9.87425" y1="0.90805" x2="10.22985" y2="0.92075" layer="21"/>
<rectangle x1="10.64895" y1="0.90805" x2="10.89025" y2="0.92075" layer="21"/>
<rectangle x1="11.20775" y1="0.90805" x2="11.57605" y2="0.92075" layer="21"/>
<rectangle x1="11.76655" y1="0.90805" x2="11.99515" y2="0.92075" layer="21"/>
<rectangle x1="12.04595" y1="0.90805" x2="12.28725" y2="0.92075" layer="21"/>
<rectangle x1="0.19685" y1="0.92075" x2="0.61595" y2="0.93345" layer="21"/>
<rectangle x1="0.89535" y1="0.92075" x2="1.20015" y2="0.93345" layer="21"/>
<rectangle x1="1.44145" y1="0.92075" x2="1.67005" y2="0.93345" layer="21"/>
<rectangle x1="1.74625" y1="0.92075" x2="1.82245" y2="0.93345" layer="21"/>
<rectangle x1="2.01295" y1="0.92075" x2="2.31775" y2="0.93345" layer="21"/>
<rectangle x1="2.62255" y1="0.92075" x2="2.94005" y2="0.93345" layer="21"/>
<rectangle x1="3.14325" y1="0.92075" x2="3.37185" y2="0.93345" layer="21"/>
<rectangle x1="3.44805" y1="0.92075" x2="3.65125" y2="0.93345" layer="21"/>
<rectangle x1="3.91795" y1="0.92075" x2="4.23545" y2="0.93345" layer="21"/>
<rectangle x1="4.56565" y1="0.92075" x2="4.85775" y2="0.93345" layer="21"/>
<rectangle x1="5.18795" y1="0.92075" x2="5.49275" y2="0.93345" layer="21"/>
<rectangle x1="5.96265" y1="0.92075" x2="6.19125" y2="0.93345" layer="21"/>
<rectangle x1="6.30555" y1="0.92075" x2="6.54685" y2="0.93345" layer="21"/>
<rectangle x1="6.77545" y1="0.92075" x2="7.08025" y2="0.93345" layer="21"/>
<rectangle x1="7.30885" y1="0.92075" x2="7.53745" y2="0.93345" layer="21"/>
<rectangle x1="7.60095" y1="0.92075" x2="7.81685" y2="0.93345" layer="21"/>
<rectangle x1="7.98195" y1="0.92075" x2="8.22325" y2="0.93345" layer="21"/>
<rectangle x1="8.36295" y1="0.92075" x2="8.59155" y2="0.93345" layer="21"/>
<rectangle x1="8.66775" y1="0.92075" x2="8.74395" y2="0.93345" layer="21"/>
<rectangle x1="8.93445" y1="0.92075" x2="9.23925" y2="0.93345" layer="21"/>
<rectangle x1="9.46785" y1="0.92075" x2="9.70915" y2="0.93345" layer="21"/>
<rectangle x1="9.88695" y1="0.92075" x2="10.21715" y2="0.93345" layer="21"/>
<rectangle x1="10.64895" y1="0.92075" x2="10.89025" y2="0.93345" layer="21"/>
<rectangle x1="11.23315" y1="0.92075" x2="11.55065" y2="0.93345" layer="21"/>
<rectangle x1="11.76655" y1="0.92075" x2="11.99515" y2="0.93345" layer="21"/>
<rectangle x1="12.05865" y1="0.92075" x2="12.27455" y2="0.93345" layer="21"/>
<rectangle x1="0.19685" y1="0.93345" x2="0.60325" y2="0.94615" layer="21"/>
<rectangle x1="0.93345" y1="0.93345" x2="1.17475" y2="0.94615" layer="21"/>
<rectangle x1="1.44145" y1="0.93345" x2="1.67005" y2="0.94615" layer="21"/>
<rectangle x1="1.75895" y1="0.93345" x2="1.82245" y2="0.94615" layer="21"/>
<rectangle x1="2.03835" y1="0.93345" x2="2.27965" y2="0.94615" layer="21"/>
<rectangle x1="2.64795" y1="0.93345" x2="2.91465" y2="0.94615" layer="21"/>
<rectangle x1="3.14325" y1="0.93345" x2="3.37185" y2="0.94615" layer="21"/>
<rectangle x1="3.47345" y1="0.93345" x2="3.62585" y2="0.94615" layer="21"/>
<rectangle x1="3.94335" y1="0.93345" x2="4.21005" y2="0.94615" layer="21"/>
<rectangle x1="4.59105" y1="0.93345" x2="4.83235" y2="0.94615" layer="21"/>
<rectangle x1="5.22605" y1="0.93345" x2="5.46735" y2="0.94615" layer="21"/>
<rectangle x1="5.96265" y1="0.93345" x2="6.19125" y2="0.94615" layer="21"/>
<rectangle x1="6.30555" y1="0.93345" x2="6.53415" y2="0.94615" layer="21"/>
<rectangle x1="6.80085" y1="0.93345" x2="7.04215" y2="0.94615" layer="21"/>
<rectangle x1="7.30885" y1="0.93345" x2="7.53745" y2="0.94615" layer="21"/>
<rectangle x1="7.62635" y1="0.93345" x2="7.79145" y2="0.94615" layer="21"/>
<rectangle x1="7.99465" y1="0.93345" x2="8.21055" y2="0.94615" layer="21"/>
<rectangle x1="8.36295" y1="0.93345" x2="8.59155" y2="0.94615" layer="21"/>
<rectangle x1="8.68045" y1="0.93345" x2="8.74395" y2="0.94615" layer="21"/>
<rectangle x1="8.95985" y1="0.93345" x2="9.20115" y2="0.94615" layer="21"/>
<rectangle x1="9.46785" y1="0.93345" x2="9.70915" y2="0.94615" layer="21"/>
<rectangle x1="9.92505" y1="0.93345" x2="10.17905" y2="0.94615" layer="21"/>
<rectangle x1="10.64895" y1="0.93345" x2="10.89025" y2="0.94615" layer="21"/>
<rectangle x1="11.25855" y1="0.93345" x2="11.52525" y2="0.94615" layer="21"/>
<rectangle x1="11.76655" y1="0.93345" x2="11.99515" y2="0.94615" layer="21"/>
<rectangle x1="12.07135" y1="0.93345" x2="12.24915" y2="0.94615" layer="21"/>
<rectangle x1="0.19685" y1="0.94615" x2="0.60325" y2="0.95885" layer="21"/>
<rectangle x1="0.97155" y1="0.94615" x2="1.12395" y2="0.95885" layer="21"/>
<rectangle x1="1.79705" y1="0.94615" x2="1.82245" y2="0.95885" layer="21"/>
<rectangle x1="2.07645" y1="0.94615" x2="2.24155" y2="0.95885" layer="21"/>
<rectangle x1="2.69875" y1="0.94615" x2="2.86385" y2="0.95885" layer="21"/>
<rectangle x1="3.49885" y1="0.94615" x2="3.60045" y2="0.95885" layer="21"/>
<rectangle x1="3.99415" y1="0.94615" x2="4.15925" y2="0.95885" layer="21"/>
<rectangle x1="4.62915" y1="0.94615" x2="4.78155" y2="0.95885" layer="21"/>
<rectangle x1="5.26415" y1="0.94615" x2="5.41655" y2="0.95885" layer="21"/>
<rectangle x1="5.96265" y1="0.94615" x2="6.19125" y2="0.95885" layer="21"/>
<rectangle x1="6.30555" y1="0.94615" x2="6.53415" y2="0.95885" layer="21"/>
<rectangle x1="6.83895" y1="0.94615" x2="7.00405" y2="0.95885" layer="21"/>
<rectangle x1="7.65175" y1="0.94615" x2="7.76605" y2="0.95885" layer="21"/>
<rectangle x1="7.99465" y1="0.94615" x2="8.21055" y2="0.95885" layer="21"/>
<rectangle x1="8.71855" y1="0.94615" x2="8.74395" y2="0.95885" layer="21"/>
<rectangle x1="8.99795" y1="0.94615" x2="9.16305" y2="0.95885" layer="21"/>
<rectangle x1="9.46785" y1="0.94615" x2="9.70915" y2="0.95885" layer="21"/>
<rectangle x1="9.96315" y1="0.94615" x2="10.12825" y2="0.95885" layer="21"/>
<rectangle x1="10.64895" y1="0.94615" x2="10.89025" y2="0.95885" layer="21"/>
<rectangle x1="11.30935" y1="0.94615" x2="11.47445" y2="0.95885" layer="21"/>
<rectangle x1="11.76655" y1="0.94615" x2="11.99515" y2="0.95885" layer="21"/>
<rectangle x1="12.10945" y1="0.94615" x2="12.22375" y2="0.95885" layer="21"/>
<rectangle x1="0.20955" y1="0.95885" x2="0.60325" y2="0.97155" layer="21"/>
<rectangle x1="5.96265" y1="0.95885" x2="6.20395" y2="0.97155" layer="21"/>
<rectangle x1="6.30555" y1="0.95885" x2="6.53415" y2="0.97155" layer="21"/>
<rectangle x1="7.99465" y1="0.95885" x2="8.21055" y2="0.97155" layer="21"/>
<rectangle x1="9.46785" y1="0.95885" x2="9.70915" y2="0.97155" layer="21"/>
<rectangle x1="10.64895" y1="0.95885" x2="10.89025" y2="0.97155" layer="21"/>
<rectangle x1="11.76655" y1="0.95885" x2="11.99515" y2="0.97155" layer="21"/>
<rectangle x1="0.20955" y1="0.97155" x2="0.60325" y2="0.98425" layer="21"/>
<rectangle x1="5.97535" y1="0.97155" x2="6.20395" y2="0.98425" layer="21"/>
<rectangle x1="6.30555" y1="0.97155" x2="6.53415" y2="0.98425" layer="21"/>
<rectangle x1="7.99465" y1="0.97155" x2="8.21055" y2="0.98425" layer="21"/>
<rectangle x1="9.46785" y1="0.97155" x2="9.70915" y2="0.98425" layer="21"/>
<rectangle x1="10.64895" y1="0.97155" x2="10.89025" y2="0.98425" layer="21"/>
<rectangle x1="11.76655" y1="0.97155" x2="11.99515" y2="0.98425" layer="21"/>
<rectangle x1="0.20955" y1="0.98425" x2="0.60325" y2="0.99695" layer="21"/>
<rectangle x1="5.97535" y1="0.98425" x2="6.21665" y2="0.99695" layer="21"/>
<rectangle x1="6.29285" y1="0.98425" x2="6.52145" y2="0.99695" layer="21"/>
<rectangle x1="7.99465" y1="0.98425" x2="8.21055" y2="0.99695" layer="21"/>
<rectangle x1="9.46785" y1="0.98425" x2="9.70915" y2="0.99695" layer="21"/>
<rectangle x1="10.64895" y1="0.98425" x2="10.89025" y2="0.99695" layer="21"/>
<rectangle x1="11.76655" y1="0.98425" x2="11.99515" y2="0.99695" layer="21"/>
<rectangle x1="0.20955" y1="0.99695" x2="0.60325" y2="1.00965" layer="21"/>
<rectangle x1="5.97535" y1="0.99695" x2="6.22935" y2="1.00965" layer="21"/>
<rectangle x1="6.26745" y1="0.99695" x2="6.52145" y2="1.00965" layer="21"/>
<rectangle x1="7.99465" y1="0.99695" x2="8.21055" y2="1.00965" layer="21"/>
<rectangle x1="9.46785" y1="0.99695" x2="9.70915" y2="1.00965" layer="21"/>
<rectangle x1="10.64895" y1="0.99695" x2="10.89025" y2="1.00965" layer="21"/>
<rectangle x1="11.76655" y1="0.99695" x2="11.99515" y2="1.00965" layer="21"/>
<rectangle x1="0.20955" y1="1.00965" x2="0.59055" y2="1.02235" layer="21"/>
<rectangle x1="5.98805" y1="1.00965" x2="6.52145" y2="1.02235" layer="21"/>
<rectangle x1="7.99465" y1="1.00965" x2="8.21055" y2="1.02235" layer="21"/>
<rectangle x1="9.46785" y1="1.00965" x2="9.70915" y2="1.02235" layer="21"/>
<rectangle x1="10.64895" y1="1.00965" x2="10.89025" y2="1.02235" layer="21"/>
<rectangle x1="11.76655" y1="1.00965" x2="11.99515" y2="1.02235" layer="21"/>
<rectangle x1="0.20955" y1="1.02235" x2="0.59055" y2="1.03505" layer="21"/>
<rectangle x1="6.00075" y1="1.02235" x2="6.50875" y2="1.03505" layer="21"/>
<rectangle x1="7.99465" y1="1.02235" x2="8.21055" y2="1.03505" layer="21"/>
<rectangle x1="9.46785" y1="1.02235" x2="9.70915" y2="1.03505" layer="21"/>
<rectangle x1="10.64895" y1="1.02235" x2="10.89025" y2="1.03505" layer="21"/>
<rectangle x1="11.76655" y1="1.02235" x2="11.99515" y2="1.03505" layer="21"/>
<rectangle x1="0.20955" y1="1.03505" x2="0.59055" y2="1.04775" layer="21"/>
<rectangle x1="6.00075" y1="1.03505" x2="6.50875" y2="1.04775" layer="21"/>
<rectangle x1="7.99465" y1="1.03505" x2="8.21055" y2="1.04775" layer="21"/>
<rectangle x1="9.46785" y1="1.03505" x2="9.70915" y2="1.04775" layer="21"/>
<rectangle x1="10.64895" y1="1.03505" x2="10.89025" y2="1.04775" layer="21"/>
<rectangle x1="11.76655" y1="1.03505" x2="11.99515" y2="1.04775" layer="21"/>
<rectangle x1="0.22225" y1="1.04775" x2="0.59055" y2="1.06045" layer="21"/>
<rectangle x1="6.01345" y1="1.04775" x2="6.49605" y2="1.06045" layer="21"/>
<rectangle x1="7.99465" y1="1.04775" x2="8.21055" y2="1.06045" layer="21"/>
<rectangle x1="9.46785" y1="1.04775" x2="9.70915" y2="1.06045" layer="21"/>
<rectangle x1="10.64895" y1="1.04775" x2="10.89025" y2="1.06045" layer="21"/>
<rectangle x1="11.76655" y1="1.04775" x2="11.99515" y2="1.06045" layer="21"/>
<rectangle x1="0.22225" y1="1.06045" x2="0.59055" y2="1.07315" layer="21"/>
<rectangle x1="6.02615" y1="1.06045" x2="6.48335" y2="1.07315" layer="21"/>
<rectangle x1="9.46785" y1="1.06045" x2="9.70915" y2="1.07315" layer="21"/>
<rectangle x1="10.64895" y1="1.06045" x2="10.89025" y2="1.07315" layer="21"/>
<rectangle x1="11.76655" y1="1.06045" x2="11.99515" y2="1.07315" layer="21"/>
<rectangle x1="0.22225" y1="1.07315" x2="0.59055" y2="1.08585" layer="21"/>
<rectangle x1="6.03885" y1="1.07315" x2="6.47065" y2="1.08585" layer="21"/>
<rectangle x1="9.46785" y1="1.07315" x2="9.70915" y2="1.08585" layer="21"/>
<rectangle x1="10.64895" y1="1.07315" x2="10.89025" y2="1.08585" layer="21"/>
<rectangle x1="11.76655" y1="1.07315" x2="11.99515" y2="1.08585" layer="21"/>
<rectangle x1="0.22225" y1="1.08585" x2="0.59055" y2="1.09855" layer="21"/>
<rectangle x1="6.05155" y1="1.08585" x2="6.44525" y2="1.09855" layer="21"/>
<rectangle x1="9.46785" y1="1.08585" x2="9.70915" y2="1.09855" layer="21"/>
<rectangle x1="10.64895" y1="1.08585" x2="10.89025" y2="1.09855" layer="21"/>
<rectangle x1="11.76655" y1="1.08585" x2="11.99515" y2="1.09855" layer="21"/>
<rectangle x1="0.22225" y1="1.09855" x2="0.57785" y2="1.11125" layer="21"/>
<rectangle x1="6.07695" y1="1.09855" x2="6.43255" y2="1.11125" layer="21"/>
<rectangle x1="9.46785" y1="1.09855" x2="9.70915" y2="1.11125" layer="21"/>
<rectangle x1="10.64895" y1="1.09855" x2="10.89025" y2="1.11125" layer="21"/>
<rectangle x1="11.76655" y1="1.09855" x2="11.99515" y2="1.11125" layer="21"/>
<rectangle x1="0.22225" y1="1.11125" x2="0.57785" y2="1.12395" layer="21"/>
<rectangle x1="6.10235" y1="1.11125" x2="6.40715" y2="1.12395" layer="21"/>
<rectangle x1="9.46785" y1="1.11125" x2="9.70915" y2="1.12395" layer="21"/>
<rectangle x1="10.64895" y1="1.11125" x2="10.89025" y2="1.12395" layer="21"/>
<rectangle x1="11.76655" y1="1.11125" x2="11.99515" y2="1.12395" layer="21"/>
<rectangle x1="6.12775" y1="1.12395" x2="6.36905" y2="1.13665" layer="21"/>
<rectangle x1="6.17855" y1="1.13665" x2="6.31825" y2="1.14935" layer="21"/>
<rectangle x1="0.33655" y1="1.64465" x2="3.63855" y2="1.65735" layer="21"/>
<rectangle x1="0.29845" y1="1.65735" x2="3.67665" y2="1.67005" layer="21"/>
<rectangle x1="0.27305" y1="1.67005" x2="3.71475" y2="1.68275" layer="21"/>
<rectangle x1="8.79475" y1="1.67005" x2="12.08405" y2="1.68275" layer="21"/>
<rectangle x1="0.26035" y1="1.68275" x2="3.74015" y2="1.69545" layer="21"/>
<rectangle x1="8.74395" y1="1.68275" x2="12.12215" y2="1.69545" layer="21"/>
<rectangle x1="0.24765" y1="1.69545" x2="3.75285" y2="1.70815" layer="21"/>
<rectangle x1="8.71855" y1="1.69545" x2="12.14755" y2="1.70815" layer="21"/>
<rectangle x1="0.24765" y1="1.70815" x2="3.77825" y2="1.72085" layer="21"/>
<rectangle x1="8.69315" y1="1.70815" x2="12.16025" y2="1.72085" layer="21"/>
<rectangle x1="0.23495" y1="1.72085" x2="3.79095" y2="1.73355" layer="21"/>
<rectangle x1="6.17855" y1="1.72085" x2="6.41985" y2="1.73355" layer="21"/>
<rectangle x1="8.66775" y1="1.72085" x2="12.17295" y2="1.73355" layer="21"/>
<rectangle x1="0.23495" y1="1.73355" x2="3.80365" y2="1.74625" layer="21"/>
<rectangle x1="6.05155" y1="1.73355" x2="6.53415" y2="1.74625" layer="21"/>
<rectangle x1="8.64235" y1="1.73355" x2="12.18565" y2="1.74625" layer="21"/>
<rectangle x1="0.23495" y1="1.74625" x2="3.82905" y2="1.75895" layer="21"/>
<rectangle x1="5.97535" y1="1.74625" x2="6.61035" y2="1.75895" layer="21"/>
<rectangle x1="8.62965" y1="1.74625" x2="12.18565" y2="1.75895" layer="21"/>
<rectangle x1="0.23495" y1="1.75895" x2="3.84175" y2="1.77165" layer="21"/>
<rectangle x1="5.91185" y1="1.75895" x2="6.67385" y2="1.77165" layer="21"/>
<rectangle x1="8.61695" y1="1.75895" x2="12.18565" y2="1.77165" layer="21"/>
<rectangle x1="0.23495" y1="1.77165" x2="3.84175" y2="1.78435" layer="21"/>
<rectangle x1="5.86105" y1="1.77165" x2="6.72465" y2="1.78435" layer="21"/>
<rectangle x1="8.60425" y1="1.77165" x2="12.19835" y2="1.78435" layer="21"/>
<rectangle x1="0.23495" y1="1.78435" x2="3.85445" y2="1.79705" layer="21"/>
<rectangle x1="5.81025" y1="1.78435" x2="6.77545" y2="1.79705" layer="21"/>
<rectangle x1="8.59155" y1="1.78435" x2="12.19835" y2="1.79705" layer="21"/>
<rectangle x1="0.23495" y1="1.79705" x2="3.86715" y2="1.80975" layer="21"/>
<rectangle x1="5.77215" y1="1.79705" x2="6.81355" y2="1.80975" layer="21"/>
<rectangle x1="8.57885" y1="1.79705" x2="12.19835" y2="1.80975" layer="21"/>
<rectangle x1="0.23495" y1="1.80975" x2="3.87985" y2="1.82245" layer="21"/>
<rectangle x1="5.73405" y1="1.80975" x2="6.85165" y2="1.82245" layer="21"/>
<rectangle x1="8.56615" y1="1.80975" x2="12.19835" y2="1.82245" layer="21"/>
<rectangle x1="0.24765" y1="1.82245" x2="3.89255" y2="1.83515" layer="21"/>
<rectangle x1="5.69595" y1="1.82245" x2="6.88975" y2="1.83515" layer="21"/>
<rectangle x1="8.55345" y1="1.82245" x2="12.18565" y2="1.83515" layer="21"/>
<rectangle x1="0.24765" y1="1.83515" x2="3.89255" y2="1.84785" layer="21"/>
<rectangle x1="5.65785" y1="1.83515" x2="6.92785" y2="1.84785" layer="21"/>
<rectangle x1="8.54075" y1="1.83515" x2="12.18565" y2="1.84785" layer="21"/>
<rectangle x1="0.26035" y1="1.84785" x2="3.90525" y2="1.86055" layer="21"/>
<rectangle x1="5.63245" y1="1.84785" x2="6.96595" y2="1.86055" layer="21"/>
<rectangle x1="8.54075" y1="1.84785" x2="12.18565" y2="1.86055" layer="21"/>
<rectangle x1="0.26035" y1="1.86055" x2="3.07975" y2="1.87325" layer="21"/>
<rectangle x1="3.32105" y1="1.86055" x2="3.90525" y2="1.87325" layer="21"/>
<rectangle x1="5.59435" y1="1.86055" x2="6.99135" y2="1.87325" layer="21"/>
<rectangle x1="8.52805" y1="1.86055" x2="12.17295" y2="1.87325" layer="21"/>
<rectangle x1="0.27305" y1="1.87325" x2="3.01625" y2="1.88595" layer="21"/>
<rectangle x1="3.38455" y1="1.87325" x2="3.91795" y2="1.88595" layer="21"/>
<rectangle x1="5.56895" y1="1.87325" x2="7.01675" y2="1.88595" layer="21"/>
<rectangle x1="8.52805" y1="1.87325" x2="12.17295" y2="1.88595" layer="21"/>
<rectangle x1="0.28575" y1="1.88595" x2="2.96545" y2="1.89865" layer="21"/>
<rectangle x1="3.42265" y1="1.88595" x2="3.91795" y2="1.89865" layer="21"/>
<rectangle x1="5.54355" y1="1.88595" x2="7.04215" y2="1.89865" layer="21"/>
<rectangle x1="8.51535" y1="1.88595" x2="12.16025" y2="1.89865" layer="21"/>
<rectangle x1="0.29845" y1="1.89865" x2="2.91465" y2="1.91135" layer="21"/>
<rectangle x1="3.44805" y1="1.89865" x2="3.93065" y2="1.91135" layer="21"/>
<rectangle x1="5.51815" y1="1.89865" x2="7.06755" y2="1.91135" layer="21"/>
<rectangle x1="8.51535" y1="1.89865" x2="12.14755" y2="1.91135" layer="21"/>
<rectangle x1="0.31115" y1="1.91135" x2="2.91465" y2="1.92405" layer="21"/>
<rectangle x1="3.47345" y1="1.91135" x2="3.93065" y2="1.92405" layer="21"/>
<rectangle x1="5.49275" y1="1.91135" x2="7.09295" y2="1.92405" layer="21"/>
<rectangle x1="8.50265" y1="1.91135" x2="12.13485" y2="1.92405" layer="21"/>
<rectangle x1="0.32385" y1="1.92405" x2="2.91465" y2="1.93675" layer="21"/>
<rectangle x1="3.49885" y1="1.92405" x2="3.93065" y2="1.93675" layer="21"/>
<rectangle x1="5.46735" y1="1.92405" x2="7.11835" y2="1.93675" layer="21"/>
<rectangle x1="8.50265" y1="1.92405" x2="12.13485" y2="1.93675" layer="21"/>
<rectangle x1="0.33655" y1="1.93675" x2="2.91465" y2="1.94945" layer="21"/>
<rectangle x1="3.52425" y1="1.93675" x2="3.94335" y2="1.94945" layer="21"/>
<rectangle x1="5.44195" y1="1.93675" x2="7.14375" y2="1.94945" layer="21"/>
<rectangle x1="8.48995" y1="1.93675" x2="12.12215" y2="1.94945" layer="21"/>
<rectangle x1="0.34925" y1="1.94945" x2="2.91465" y2="1.96215" layer="21"/>
<rectangle x1="3.52425" y1="1.94945" x2="3.94335" y2="1.96215" layer="21"/>
<rectangle x1="5.41655" y1="1.94945" x2="7.16915" y2="1.96215" layer="21"/>
<rectangle x1="8.48995" y1="1.94945" x2="12.10945" y2="1.96215" layer="21"/>
<rectangle x1="0.36195" y1="1.96215" x2="2.92735" y2="1.97485" layer="21"/>
<rectangle x1="3.51155" y1="1.96215" x2="3.94335" y2="1.97485" layer="21"/>
<rectangle x1="5.40385" y1="1.96215" x2="7.19455" y2="1.97485" layer="21"/>
<rectangle x1="8.48995" y1="1.96215" x2="12.09675" y2="1.97485" layer="21"/>
<rectangle x1="0.37465" y1="1.97485" x2="2.73685" y2="1.98755" layer="21"/>
<rectangle x1="2.77495" y1="1.97485" x2="2.92735" y2="1.98755" layer="21"/>
<rectangle x1="3.49885" y1="1.97485" x2="3.94335" y2="1.98755" layer="21"/>
<rectangle x1="5.37845" y1="1.97485" x2="7.20725" y2="1.98755" layer="21"/>
<rectangle x1="8.48995" y1="1.97485" x2="12.08405" y2="1.98755" layer="21"/>
<rectangle x1="0.38735" y1="1.98755" x2="2.71145" y2="2.00025" layer="21"/>
<rectangle x1="2.77495" y1="1.98755" x2="2.92735" y2="2.00025" layer="21"/>
<rectangle x1="3.48615" y1="1.98755" x2="3.94335" y2="2.00025" layer="21"/>
<rectangle x1="5.35305" y1="1.98755" x2="7.23265" y2="2.00025" layer="21"/>
<rectangle x1="8.47725" y1="1.98755" x2="12.07135" y2="2.00025" layer="21"/>
<rectangle x1="0.40005" y1="2.00025" x2="2.68605" y2="2.01295" layer="21"/>
<rectangle x1="2.77495" y1="2.00025" x2="2.94005" y2="2.01295" layer="21"/>
<rectangle x1="3.47345" y1="2.00025" x2="3.95605" y2="2.01295" layer="21"/>
<rectangle x1="5.34035" y1="2.00025" x2="7.24535" y2="2.01295" layer="21"/>
<rectangle x1="8.47725" y1="2.00025" x2="12.05865" y2="2.01295" layer="21"/>
<rectangle x1="0.41275" y1="2.01295" x2="2.66065" y2="2.02565" layer="21"/>
<rectangle x1="2.78765" y1="2.01295" x2="2.94005" y2="2.02565" layer="21"/>
<rectangle x1="3.46075" y1="2.01295" x2="3.95605" y2="2.02565" layer="21"/>
<rectangle x1="5.31495" y1="2.01295" x2="7.27075" y2="2.02565" layer="21"/>
<rectangle x1="8.47725" y1="2.01295" x2="12.04595" y2="2.02565" layer="21"/>
<rectangle x1="0.42545" y1="2.02565" x2="2.63525" y2="2.03835" layer="21"/>
<rectangle x1="2.78765" y1="2.02565" x2="2.94005" y2="2.03835" layer="21"/>
<rectangle x1="3.44805" y1="2.02565" x2="3.95605" y2="2.03835" layer="21"/>
<rectangle x1="5.30225" y1="2.02565" x2="7.28345" y2="2.03835" layer="21"/>
<rectangle x1="8.47725" y1="2.02565" x2="12.03325" y2="2.03835" layer="21"/>
<rectangle x1="0.43815" y1="2.03835" x2="2.62255" y2="2.05105" layer="21"/>
<rectangle x1="2.78765" y1="2.03835" x2="2.94005" y2="2.05105" layer="21"/>
<rectangle x1="3.43535" y1="2.03835" x2="3.95605" y2="2.05105" layer="21"/>
<rectangle x1="5.27685" y1="2.03835" x2="7.30885" y2="2.05105" layer="21"/>
<rectangle x1="8.47725" y1="2.03835" x2="12.02055" y2="2.05105" layer="21"/>
<rectangle x1="0.45085" y1="2.05105" x2="2.59715" y2="2.06375" layer="21"/>
<rectangle x1="2.78765" y1="2.05105" x2="2.95275" y2="2.06375" layer="21"/>
<rectangle x1="3.42265" y1="2.05105" x2="3.95605" y2="2.06375" layer="21"/>
<rectangle x1="5.26415" y1="2.05105" x2="7.32155" y2="2.06375" layer="21"/>
<rectangle x1="8.47725" y1="2.05105" x2="12.00785" y2="2.06375" layer="21"/>
<rectangle x1="0.46355" y1="2.06375" x2="2.57175" y2="2.07645" layer="21"/>
<rectangle x1="2.80035" y1="2.06375" x2="2.95275" y2="2.07645" layer="21"/>
<rectangle x1="3.40995" y1="2.06375" x2="3.62585" y2="2.07645" layer="21"/>
<rectangle x1="3.65125" y1="2.06375" x2="3.95605" y2="2.07645" layer="21"/>
<rectangle x1="5.25145" y1="2.06375" x2="7.34695" y2="2.07645" layer="21"/>
<rectangle x1="8.47725" y1="2.06375" x2="9.06145" y2="2.07645" layer="21"/>
<rectangle x1="9.07415" y1="2.06375" x2="11.99515" y2="2.07645" layer="21"/>
<rectangle x1="0.47625" y1="2.07645" x2="2.55905" y2="2.08915" layer="21"/>
<rectangle x1="2.80035" y1="2.07645" x2="2.95275" y2="2.08915" layer="21"/>
<rectangle x1="3.39725" y1="2.07645" x2="3.61315" y2="2.08915" layer="21"/>
<rectangle x1="3.66395" y1="2.07645" x2="3.95605" y2="2.08915" layer="21"/>
<rectangle x1="5.22605" y1="2.07645" x2="7.35965" y2="2.08915" layer="21"/>
<rectangle x1="8.47725" y1="2.07645" x2="9.04875" y2="2.08915" layer="21"/>
<rectangle x1="9.08685" y1="2.07645" x2="11.98245" y2="2.08915" layer="21"/>
<rectangle x1="0.48895" y1="2.08915" x2="2.53365" y2="2.10185" layer="21"/>
<rectangle x1="2.80035" y1="2.08915" x2="2.95275" y2="2.10185" layer="21"/>
<rectangle x1="3.38455" y1="2.08915" x2="3.60045" y2="2.10185" layer="21"/>
<rectangle x1="3.66395" y1="2.08915" x2="3.95605" y2="2.10185" layer="21"/>
<rectangle x1="5.21335" y1="2.08915" x2="7.37235" y2="2.10185" layer="21"/>
<rectangle x1="8.47725" y1="2.08915" x2="9.03605" y2="2.10185" layer="21"/>
<rectangle x1="9.09955" y1="2.08915" x2="11.96975" y2="2.10185" layer="21"/>
<rectangle x1="0.50165" y1="2.10185" x2="2.52095" y2="2.11455" layer="21"/>
<rectangle x1="2.81305" y1="2.10185" x2="2.96545" y2="2.11455" layer="21"/>
<rectangle x1="3.37185" y1="2.10185" x2="3.57505" y2="2.11455" layer="21"/>
<rectangle x1="3.67665" y1="2.10185" x2="3.95605" y2="2.11455" layer="21"/>
<rectangle x1="5.20065" y1="2.10185" x2="7.38505" y2="2.11455" layer="21"/>
<rectangle x1="8.47725" y1="2.10185" x2="9.02335" y2="2.11455" layer="21"/>
<rectangle x1="9.11225" y1="2.10185" x2="11.95705" y2="2.11455" layer="21"/>
<rectangle x1="0.51435" y1="2.11455" x2="2.50825" y2="2.12725" layer="21"/>
<rectangle x1="2.81305" y1="2.11455" x2="2.96545" y2="2.12725" layer="21"/>
<rectangle x1="3.35915" y1="2.11455" x2="3.56235" y2="2.12725" layer="21"/>
<rectangle x1="3.68935" y1="2.11455" x2="3.95605" y2="2.12725" layer="21"/>
<rectangle x1="5.17525" y1="2.11455" x2="7.41045" y2="2.12725" layer="21"/>
<rectangle x1="8.47725" y1="2.11455" x2="9.01065" y2="2.12725" layer="21"/>
<rectangle x1="9.12495" y1="2.11455" x2="11.94435" y2="2.12725" layer="21"/>
<rectangle x1="0.52705" y1="2.12725" x2="2.50825" y2="2.13995" layer="21"/>
<rectangle x1="2.81305" y1="2.12725" x2="2.96545" y2="2.13995" layer="21"/>
<rectangle x1="3.34645" y1="2.12725" x2="3.54965" y2="2.13995" layer="21"/>
<rectangle x1="3.68935" y1="2.12725" x2="3.95605" y2="2.13995" layer="21"/>
<rectangle x1="5.16255" y1="2.12725" x2="7.42315" y2="2.13995" layer="21"/>
<rectangle x1="8.47725" y1="2.12725" x2="8.99795" y2="2.13995" layer="21"/>
<rectangle x1="9.13765" y1="2.12725" x2="11.93165" y2="2.13995" layer="21"/>
<rectangle x1="0.53975" y1="2.13995" x2="2.52095" y2="2.15265" layer="21"/>
<rectangle x1="2.81305" y1="2.13995" x2="2.96545" y2="2.15265" layer="21"/>
<rectangle x1="3.33375" y1="2.13995" x2="3.53695" y2="2.15265" layer="21"/>
<rectangle x1="3.68935" y1="2.13995" x2="3.95605" y2="2.15265" layer="21"/>
<rectangle x1="5.14985" y1="2.13995" x2="7.43585" y2="2.15265" layer="21"/>
<rectangle x1="8.47725" y1="2.13995" x2="8.98525" y2="2.15265" layer="21"/>
<rectangle x1="9.15035" y1="2.13995" x2="9.31545" y2="2.15265" layer="21"/>
<rectangle x1="9.34085" y1="2.13995" x2="11.91895" y2="2.15265" layer="21"/>
<rectangle x1="0.55245" y1="2.15265" x2="2.53365" y2="2.16535" layer="21"/>
<rectangle x1="2.82575" y1="2.15265" x2="2.97815" y2="2.16535" layer="21"/>
<rectangle x1="3.32105" y1="2.15265" x2="3.52425" y2="2.16535" layer="21"/>
<rectangle x1="3.70205" y1="2.15265" x2="3.95605" y2="2.16535" layer="21"/>
<rectangle x1="5.13715" y1="2.15265" x2="7.44855" y2="2.16535" layer="21"/>
<rectangle x1="8.47725" y1="2.15265" x2="8.97255" y2="2.16535" layer="21"/>
<rectangle x1="9.16305" y1="2.15265" x2="9.23925" y2="2.16535" layer="21"/>
<rectangle x1="9.36625" y1="2.15265" x2="11.90625" y2="2.16535" layer="21"/>
<rectangle x1="0.56515" y1="2.16535" x2="2.54635" y2="2.17805" layer="21"/>
<rectangle x1="2.82575" y1="2.16535" x2="2.97815" y2="2.17805" layer="21"/>
<rectangle x1="3.30835" y1="2.16535" x2="3.51155" y2="2.17805" layer="21"/>
<rectangle x1="3.70205" y1="2.16535" x2="3.95605" y2="2.17805" layer="21"/>
<rectangle x1="5.12445" y1="2.16535" x2="7.46125" y2="2.17805" layer="21"/>
<rectangle x1="8.47725" y1="2.16535" x2="8.95985" y2="2.17805" layer="21"/>
<rectangle x1="9.17575" y1="2.16535" x2="9.20115" y2="2.17805" layer="21"/>
<rectangle x1="9.36625" y1="2.16535" x2="9.53135" y2="2.17805" layer="21"/>
<rectangle x1="9.63295" y1="2.16535" x2="11.89355" y2="2.17805" layer="21"/>
<rectangle x1="0.57785" y1="2.17805" x2="2.55905" y2="2.19075" layer="21"/>
<rectangle x1="2.82575" y1="2.17805" x2="2.97815" y2="2.19075" layer="21"/>
<rectangle x1="3.29565" y1="2.17805" x2="3.49885" y2="2.19075" layer="21"/>
<rectangle x1="3.71475" y1="2.17805" x2="3.95605" y2="2.19075" layer="21"/>
<rectangle x1="5.11175" y1="2.17805" x2="7.47395" y2="2.19075" layer="21"/>
<rectangle x1="8.47725" y1="2.17805" x2="8.94715" y2="2.19075" layer="21"/>
<rectangle x1="9.36625" y1="2.17805" x2="9.53135" y2="2.19075" layer="21"/>
<rectangle x1="9.68375" y1="2.17805" x2="11.88085" y2="2.19075" layer="21"/>
<rectangle x1="0.59055" y1="2.19075" x2="2.57175" y2="2.20345" layer="21"/>
<rectangle x1="2.82575" y1="2.19075" x2="2.99085" y2="2.20345" layer="21"/>
<rectangle x1="3.28295" y1="2.19075" x2="3.48615" y2="2.20345" layer="21"/>
<rectangle x1="3.71475" y1="2.19075" x2="3.95605" y2="2.20345" layer="21"/>
<rectangle x1="5.09905" y1="2.19075" x2="7.49935" y2="2.20345" layer="21"/>
<rectangle x1="8.47725" y1="2.19075" x2="8.93445" y2="2.20345" layer="21"/>
<rectangle x1="9.35355" y1="2.19075" x2="9.53135" y2="2.20345" layer="21"/>
<rectangle x1="9.70915" y1="2.19075" x2="11.86815" y2="2.20345" layer="21"/>
<rectangle x1="0.60325" y1="2.20345" x2="2.58445" y2="2.21615" layer="21"/>
<rectangle x1="2.83845" y1="2.20345" x2="2.99085" y2="2.21615" layer="21"/>
<rectangle x1="3.27025" y1="2.20345" x2="3.47345" y2="2.21615" layer="21"/>
<rectangle x1="3.71475" y1="2.20345" x2="3.95605" y2="2.21615" layer="21"/>
<rectangle x1="5.08635" y1="2.20345" x2="7.51205" y2="2.21615" layer="21"/>
<rectangle x1="8.47725" y1="2.20345" x2="8.92175" y2="2.21615" layer="21"/>
<rectangle x1="9.34085" y1="2.20345" x2="9.53135" y2="2.21615" layer="21"/>
<rectangle x1="9.70915" y1="2.20345" x2="11.85545" y2="2.21615" layer="21"/>
<rectangle x1="0.61595" y1="2.21615" x2="2.59715" y2="2.22885" layer="21"/>
<rectangle x1="2.83845" y1="2.21615" x2="2.99085" y2="2.22885" layer="21"/>
<rectangle x1="3.25755" y1="2.21615" x2="3.46075" y2="2.22885" layer="21"/>
<rectangle x1="3.72745" y1="2.21615" x2="3.95605" y2="2.22885" layer="21"/>
<rectangle x1="5.07365" y1="2.21615" x2="7.52475" y2="2.22885" layer="21"/>
<rectangle x1="8.47725" y1="2.21615" x2="8.90905" y2="2.22885" layer="21"/>
<rectangle x1="9.25195" y1="2.21615" x2="9.55675" y2="2.22885" layer="21"/>
<rectangle x1="9.70915" y1="2.21615" x2="11.84275" y2="2.22885" layer="21"/>
<rectangle x1="0.62865" y1="2.22885" x2="2.60985" y2="2.24155" layer="21"/>
<rectangle x1="2.83845" y1="2.22885" x2="2.99085" y2="2.24155" layer="21"/>
<rectangle x1="3.24485" y1="2.22885" x2="3.44805" y2="2.24155" layer="21"/>
<rectangle x1="3.72745" y1="2.22885" x2="3.95605" y2="2.24155" layer="21"/>
<rectangle x1="5.06095" y1="2.22885" x2="7.53745" y2="2.24155" layer="21"/>
<rectangle x1="8.47725" y1="2.22885" x2="8.89635" y2="2.24155" layer="21"/>
<rectangle x1="9.23925" y1="2.22885" x2="9.60755" y2="2.24155" layer="21"/>
<rectangle x1="9.70915" y1="2.22885" x2="11.83005" y2="2.24155" layer="21"/>
<rectangle x1="0.64135" y1="2.24155" x2="2.62255" y2="2.25425" layer="21"/>
<rectangle x1="2.83845" y1="2.24155" x2="3.00355" y2="2.25425" layer="21"/>
<rectangle x1="3.23215" y1="2.24155" x2="3.43535" y2="2.25425" layer="21"/>
<rectangle x1="3.72745" y1="2.24155" x2="3.95605" y2="2.25425" layer="21"/>
<rectangle x1="5.04825" y1="2.24155" x2="7.55015" y2="2.25425" layer="21"/>
<rectangle x1="8.47725" y1="2.24155" x2="8.88365" y2="2.25425" layer="21"/>
<rectangle x1="9.25195" y1="2.24155" x2="9.67105" y2="2.25425" layer="21"/>
<rectangle x1="9.68375" y1="2.24155" x2="11.81735" y2="2.25425" layer="21"/>
<rectangle x1="0.65405" y1="2.25425" x2="2.63525" y2="2.26695" layer="21"/>
<rectangle x1="2.85115" y1="2.25425" x2="3.00355" y2="2.26695" layer="21"/>
<rectangle x1="3.21945" y1="2.25425" x2="3.42265" y2="2.26695" layer="21"/>
<rectangle x1="3.72745" y1="2.25425" x2="3.95605" y2="2.26695" layer="21"/>
<rectangle x1="5.03555" y1="2.25425" x2="7.56285" y2="2.26695" layer="21"/>
<rectangle x1="8.47725" y1="2.25425" x2="8.87095" y2="2.26695" layer="21"/>
<rectangle x1="9.26465" y1="2.25425" x2="9.87425" y2="2.26695" layer="21"/>
<rectangle x1="9.91235" y1="2.25425" x2="11.80465" y2="2.26695" layer="21"/>
<rectangle x1="0.66675" y1="2.26695" x2="2.64795" y2="2.27965" layer="21"/>
<rectangle x1="2.85115" y1="2.26695" x2="3.10515" y2="2.27965" layer="21"/>
<rectangle x1="3.20675" y1="2.26695" x2="3.40995" y2="2.27965" layer="21"/>
<rectangle x1="3.74015" y1="2.26695" x2="3.95605" y2="2.27965" layer="21"/>
<rectangle x1="5.02285" y1="2.26695" x2="7.57555" y2="2.27965" layer="21"/>
<rectangle x1="8.47725" y1="2.26695" x2="8.85825" y2="2.27965" layer="21"/>
<rectangle x1="9.27735" y1="2.26695" x2="9.86155" y2="2.27965" layer="21"/>
<rectangle x1="9.95045" y1="2.26695" x2="11.79195" y2="2.27965" layer="21"/>
<rectangle x1="0.67945" y1="2.27965" x2="2.66065" y2="2.29235" layer="21"/>
<rectangle x1="2.83845" y1="2.27965" x2="3.14325" y2="2.29235" layer="21"/>
<rectangle x1="3.19405" y1="2.27965" x2="3.39725" y2="2.29235" layer="21"/>
<rectangle x1="3.74015" y1="2.27965" x2="3.95605" y2="2.29235" layer="21"/>
<rectangle x1="5.01015" y1="2.27965" x2="7.58825" y2="2.29235" layer="21"/>
<rectangle x1="8.47725" y1="2.27965" x2="8.84555" y2="2.29235" layer="21"/>
<rectangle x1="9.29005" y1="2.27965" x2="9.86155" y2="2.29235" layer="21"/>
<rectangle x1="9.97585" y1="2.27965" x2="11.77925" y2="2.29235" layer="21"/>
<rectangle x1="0.69215" y1="2.29235" x2="2.67335" y2="2.30505" layer="21"/>
<rectangle x1="2.80035" y1="2.29235" x2="3.38455" y2="2.30505" layer="21"/>
<rectangle x1="3.74015" y1="2.29235" x2="3.95605" y2="2.30505" layer="21"/>
<rectangle x1="4.99745" y1="2.29235" x2="7.58825" y2="2.30505" layer="21"/>
<rectangle x1="8.47725" y1="2.29235" x2="8.83285" y2="2.30505" layer="21"/>
<rectangle x1="9.30275" y1="2.29235" x2="9.86155" y2="2.30505" layer="21"/>
<rectangle x1="10.01395" y1="2.29235" x2="11.76655" y2="2.30505" layer="21"/>
<rectangle x1="0.70485" y1="2.30505" x2="2.68605" y2="2.31775" layer="21"/>
<rectangle x1="2.76225" y1="2.30505" x2="3.37185" y2="2.31775" layer="21"/>
<rectangle x1="3.74015" y1="2.30505" x2="3.95605" y2="2.31775" layer="21"/>
<rectangle x1="4.98475" y1="2.30505" x2="7.60095" y2="2.31775" layer="21"/>
<rectangle x1="8.47725" y1="2.30505" x2="8.82015" y2="2.31775" layer="21"/>
<rectangle x1="9.31545" y1="2.30505" x2="9.87425" y2="2.31775" layer="21"/>
<rectangle x1="10.02665" y1="2.30505" x2="11.75385" y2="2.31775" layer="21"/>
<rectangle x1="0.71755" y1="2.31775" x2="2.69875" y2="2.33045" layer="21"/>
<rectangle x1="2.72415" y1="2.31775" x2="3.35915" y2="2.33045" layer="21"/>
<rectangle x1="3.74015" y1="2.31775" x2="3.95605" y2="2.33045" layer="21"/>
<rectangle x1="4.97205" y1="2.31775" x2="7.61365" y2="2.33045" layer="21"/>
<rectangle x1="8.47725" y1="2.31775" x2="8.80745" y2="2.33045" layer="21"/>
<rectangle x1="9.32815" y1="2.31775" x2="9.89965" y2="2.33045" layer="21"/>
<rectangle x1="10.03935" y1="2.31775" x2="11.74115" y2="2.33045" layer="21"/>
<rectangle x1="0.73025" y1="2.33045" x2="3.34645" y2="2.34315" layer="21"/>
<rectangle x1="3.74015" y1="2.33045" x2="3.95605" y2="2.34315" layer="21"/>
<rectangle x1="4.95935" y1="2.33045" x2="7.62635" y2="2.34315" layer="21"/>
<rectangle x1="8.47725" y1="2.33045" x2="8.80745" y2="2.34315" layer="21"/>
<rectangle x1="9.32815" y1="2.33045" x2="9.92505" y2="2.34315" layer="21"/>
<rectangle x1="10.03935" y1="2.33045" x2="11.72845" y2="2.34315" layer="21"/>
<rectangle x1="0.74295" y1="2.34315" x2="3.33375" y2="2.35585" layer="21"/>
<rectangle x1="3.74015" y1="2.34315" x2="3.95605" y2="2.35585" layer="21"/>
<rectangle x1="4.94665" y1="2.34315" x2="7.63905" y2="2.35585" layer="21"/>
<rectangle x1="8.47725" y1="2.34315" x2="8.82015" y2="2.35585" layer="21"/>
<rectangle x1="9.31545" y1="2.34315" x2="9.95045" y2="2.35585" layer="21"/>
<rectangle x1="10.02665" y1="2.34315" x2="11.71575" y2="2.35585" layer="21"/>
<rectangle x1="0.75565" y1="2.35585" x2="3.32105" y2="2.36855" layer="21"/>
<rectangle x1="3.74015" y1="2.35585" x2="3.95605" y2="2.36855" layer="21"/>
<rectangle x1="4.93395" y1="2.35585" x2="7.65175" y2="2.36855" layer="21"/>
<rectangle x1="8.47725" y1="2.35585" x2="8.83285" y2="2.36855" layer="21"/>
<rectangle x1="9.30275" y1="2.35585" x2="9.98855" y2="2.36855" layer="21"/>
<rectangle x1="10.01395" y1="2.35585" x2="11.70305" y2="2.36855" layer="21"/>
<rectangle x1="0.76835" y1="2.36855" x2="3.30835" y2="2.38125" layer="21"/>
<rectangle x1="3.74015" y1="2.36855" x2="3.95605" y2="2.38125" layer="21"/>
<rectangle x1="4.93395" y1="2.36855" x2="7.66445" y2="2.38125" layer="21"/>
<rectangle x1="8.47725" y1="2.36855" x2="8.84555" y2="2.38125" layer="21"/>
<rectangle x1="9.29005" y1="2.36855" x2="11.69035" y2="2.38125" layer="21"/>
<rectangle x1="0.78105" y1="2.38125" x2="3.29565" y2="2.39395" layer="21"/>
<rectangle x1="3.74015" y1="2.38125" x2="3.95605" y2="2.39395" layer="21"/>
<rectangle x1="4.92125" y1="2.38125" x2="7.66445" y2="2.39395" layer="21"/>
<rectangle x1="8.47725" y1="2.38125" x2="8.85825" y2="2.39395" layer="21"/>
<rectangle x1="9.27735" y1="2.38125" x2="11.67765" y2="2.39395" layer="21"/>
<rectangle x1="0.79375" y1="2.39395" x2="3.29565" y2="2.40665" layer="21"/>
<rectangle x1="3.74015" y1="2.39395" x2="3.95605" y2="2.40665" layer="21"/>
<rectangle x1="4.90855" y1="2.39395" x2="6.14045" y2="2.40665" layer="21"/>
<rectangle x1="6.44525" y1="2.39395" x2="7.67715" y2="2.40665" layer="21"/>
<rectangle x1="8.47725" y1="2.39395" x2="8.87095" y2="2.40665" layer="21"/>
<rectangle x1="9.26465" y1="2.39395" x2="10.20445" y2="2.40665" layer="21"/>
<rectangle x1="10.21715" y1="2.39395" x2="11.66495" y2="2.40665" layer="21"/>
<rectangle x1="0.80645" y1="2.40665" x2="3.29565" y2="2.41935" layer="21"/>
<rectangle x1="3.74015" y1="2.40665" x2="3.95605" y2="2.41935" layer="21"/>
<rectangle x1="4.89585" y1="2.40665" x2="6.06425" y2="2.41935" layer="21"/>
<rectangle x1="6.52145" y1="2.40665" x2="7.68985" y2="2.41935" layer="21"/>
<rectangle x1="8.47725" y1="2.40665" x2="8.88365" y2="2.41935" layer="21"/>
<rectangle x1="9.25195" y1="2.40665" x2="10.17905" y2="2.41935" layer="21"/>
<rectangle x1="10.24255" y1="2.40665" x2="11.65225" y2="2.41935" layer="21"/>
<rectangle x1="0.81915" y1="2.41935" x2="3.29565" y2="2.43205" layer="21"/>
<rectangle x1="3.74015" y1="2.41935" x2="3.95605" y2="2.43205" layer="21"/>
<rectangle x1="4.88315" y1="2.41935" x2="6.01345" y2="2.43205" layer="21"/>
<rectangle x1="6.57225" y1="2.41935" x2="7.70255" y2="2.43205" layer="21"/>
<rectangle x1="8.47725" y1="2.41935" x2="8.89635" y2="2.43205" layer="21"/>
<rectangle x1="9.23925" y1="2.41935" x2="10.17905" y2="2.43205" layer="21"/>
<rectangle x1="10.26795" y1="2.41935" x2="11.63955" y2="2.43205" layer="21"/>
<rectangle x1="0.83185" y1="2.43205" x2="3.30835" y2="2.44475" layer="21"/>
<rectangle x1="3.74015" y1="2.43205" x2="3.95605" y2="2.44475" layer="21"/>
<rectangle x1="4.88315" y1="2.43205" x2="5.97535" y2="2.44475" layer="21"/>
<rectangle x1="6.62305" y1="2.43205" x2="7.70255" y2="2.44475" layer="21"/>
<rectangle x1="8.47725" y1="2.43205" x2="8.90905" y2="2.44475" layer="21"/>
<rectangle x1="9.22655" y1="2.43205" x2="10.17905" y2="2.44475" layer="21"/>
<rectangle x1="10.29335" y1="2.43205" x2="11.62685" y2="2.44475" layer="21"/>
<rectangle x1="0.84455" y1="2.44475" x2="3.30835" y2="2.45745" layer="21"/>
<rectangle x1="3.74015" y1="2.44475" x2="3.95605" y2="2.45745" layer="21"/>
<rectangle x1="4.87045" y1="2.44475" x2="5.93725" y2="2.45745" layer="21"/>
<rectangle x1="6.64845" y1="2.44475" x2="7.71525" y2="2.45745" layer="21"/>
<rectangle x1="8.47725" y1="2.44475" x2="8.92175" y2="2.45745" layer="21"/>
<rectangle x1="9.21385" y1="2.44475" x2="10.17905" y2="2.45745" layer="21"/>
<rectangle x1="10.31875" y1="2.44475" x2="11.61415" y2="2.45745" layer="21"/>
<rectangle x1="0.85725" y1="2.45745" x2="3.32105" y2="2.47015" layer="21"/>
<rectangle x1="3.74015" y1="2.45745" x2="3.95605" y2="2.47015" layer="21"/>
<rectangle x1="4.85775" y1="2.45745" x2="5.89915" y2="2.47015" layer="21"/>
<rectangle x1="6.68655" y1="2.45745" x2="7.72795" y2="2.47015" layer="21"/>
<rectangle x1="8.47725" y1="2.45745" x2="8.93445" y2="2.47015" layer="21"/>
<rectangle x1="9.20115" y1="2.45745" x2="10.19175" y2="2.47015" layer="21"/>
<rectangle x1="10.33145" y1="2.45745" x2="11.60145" y2="2.47015" layer="21"/>
<rectangle x1="0.86995" y1="2.47015" x2="3.32105" y2="2.48285" layer="21"/>
<rectangle x1="3.74015" y1="2.47015" x2="3.95605" y2="2.48285" layer="21"/>
<rectangle x1="4.85775" y1="2.47015" x2="5.87375" y2="2.48285" layer="21"/>
<rectangle x1="6.72465" y1="2.47015" x2="7.74065" y2="2.48285" layer="21"/>
<rectangle x1="8.47725" y1="2.47015" x2="8.94715" y2="2.48285" layer="21"/>
<rectangle x1="9.18845" y1="2.47015" x2="10.21715" y2="2.48285" layer="21"/>
<rectangle x1="10.34415" y1="2.47015" x2="10.75055" y2="2.48285" layer="21"/>
<rectangle x1="11.00455" y1="2.47015" x2="11.58875" y2="2.48285" layer="21"/>
<rectangle x1="0.88265" y1="2.48285" x2="3.32105" y2="2.49555" layer="21"/>
<rectangle x1="3.74015" y1="2.48285" x2="3.95605" y2="2.49555" layer="21"/>
<rectangle x1="4.84505" y1="2.48285" x2="5.83565" y2="2.49555" layer="21"/>
<rectangle x1="6.75005" y1="2.48285" x2="7.74065" y2="2.49555" layer="21"/>
<rectangle x1="8.47725" y1="2.48285" x2="8.95985" y2="2.49555" layer="21"/>
<rectangle x1="9.17575" y1="2.48285" x2="10.24255" y2="2.49555" layer="21"/>
<rectangle x1="10.34415" y1="2.48285" x2="10.64895" y2="2.49555" layer="21"/>
<rectangle x1="11.11885" y1="2.48285" x2="11.57605" y2="2.49555" layer="21"/>
<rectangle x1="0.89535" y1="2.49555" x2="3.32105" y2="2.50825" layer="21"/>
<rectangle x1="3.74015" y1="2.49555" x2="3.95605" y2="2.50825" layer="21"/>
<rectangle x1="4.83235" y1="2.49555" x2="5.81025" y2="2.50825" layer="21"/>
<rectangle x1="6.77545" y1="2.49555" x2="7.75335" y2="2.50825" layer="21"/>
<rectangle x1="8.47725" y1="2.49555" x2="8.97255" y2="2.50825" layer="21"/>
<rectangle x1="9.16305" y1="2.49555" x2="10.26795" y2="2.50825" layer="21"/>
<rectangle x1="10.34415" y1="2.49555" x2="10.57275" y2="2.50825" layer="21"/>
<rectangle x1="11.19505" y1="2.49555" x2="11.56335" y2="2.50825" layer="21"/>
<rectangle x1="0.90805" y1="2.50825" x2="3.33375" y2="2.52095" layer="21"/>
<rectangle x1="3.74015" y1="2.50825" x2="3.95605" y2="2.52095" layer="21"/>
<rectangle x1="4.83235" y1="2.50825" x2="5.78485" y2="2.52095" layer="21"/>
<rectangle x1="6.80085" y1="2.50825" x2="7.75335" y2="2.52095" layer="21"/>
<rectangle x1="8.47725" y1="2.50825" x2="8.98525" y2="2.52095" layer="21"/>
<rectangle x1="9.15035" y1="2.50825" x2="10.28065" y2="2.52095" layer="21"/>
<rectangle x1="10.33145" y1="2.50825" x2="10.50925" y2="2.52095" layer="21"/>
<rectangle x1="11.25855" y1="2.50825" x2="11.55065" y2="2.52095" layer="21"/>
<rectangle x1="0.92075" y1="2.52095" x2="3.33375" y2="2.53365" layer="21"/>
<rectangle x1="3.74015" y1="2.52095" x2="3.95605" y2="2.53365" layer="21"/>
<rectangle x1="4.81965" y1="2.52095" x2="5.75945" y2="2.53365" layer="21"/>
<rectangle x1="6.82625" y1="2.52095" x2="7.76605" y2="2.53365" layer="21"/>
<rectangle x1="8.47725" y1="2.52095" x2="8.99795" y2="2.53365" layer="21"/>
<rectangle x1="9.13765" y1="2.52095" x2="10.45845" y2="2.53365" layer="21"/>
<rectangle x1="11.30935" y1="2.52095" x2="11.53795" y2="2.53365" layer="21"/>
<rectangle x1="0.93345" y1="2.53365" x2="3.33375" y2="2.54635" layer="21"/>
<rectangle x1="3.72745" y1="2.53365" x2="3.95605" y2="2.54635" layer="21"/>
<rectangle x1="4.80695" y1="2.53365" x2="5.74675" y2="2.54635" layer="21"/>
<rectangle x1="6.83895" y1="2.53365" x2="7.77875" y2="2.54635" layer="21"/>
<rectangle x1="8.47725" y1="2.53365" x2="9.01065" y2="2.54635" layer="21"/>
<rectangle x1="9.12495" y1="2.53365" x2="10.40765" y2="2.54635" layer="21"/>
<rectangle x1="11.36015" y1="2.53365" x2="11.52525" y2="2.54635" layer="21"/>
<rectangle x1="0.94615" y1="2.54635" x2="3.33375" y2="2.55905" layer="21"/>
<rectangle x1="3.72745" y1="2.54635" x2="3.95605" y2="2.55905" layer="21"/>
<rectangle x1="4.80695" y1="2.54635" x2="5.72135" y2="2.55905" layer="21"/>
<rectangle x1="6.86435" y1="2.54635" x2="7.77875" y2="2.55905" layer="21"/>
<rectangle x1="8.47725" y1="2.54635" x2="9.02335" y2="2.55905" layer="21"/>
<rectangle x1="9.11225" y1="2.54635" x2="10.36955" y2="2.55905" layer="21"/>
<rectangle x1="11.39825" y1="2.54635" x2="11.51255" y2="2.55905" layer="21"/>
<rectangle x1="0.95885" y1="2.55905" x2="3.33375" y2="2.57175" layer="21"/>
<rectangle x1="3.72745" y1="2.55905" x2="3.95605" y2="2.57175" layer="21"/>
<rectangle x1="4.79425" y1="2.55905" x2="5.69595" y2="2.57175" layer="21"/>
<rectangle x1="6.88975" y1="2.55905" x2="7.79145" y2="2.57175" layer="21"/>
<rectangle x1="8.47725" y1="2.55905" x2="9.03605" y2="2.57175" layer="21"/>
<rectangle x1="9.09955" y1="2.55905" x2="10.33145" y2="2.57175" layer="21"/>
<rectangle x1="11.43635" y1="2.55905" x2="11.49985" y2="2.57175" layer="21"/>
<rectangle x1="0.97155" y1="2.57175" x2="3.33375" y2="2.58445" layer="21"/>
<rectangle x1="3.72745" y1="2.57175" x2="3.95605" y2="2.58445" layer="21"/>
<rectangle x1="4.78155" y1="2.57175" x2="5.68325" y2="2.58445" layer="21"/>
<rectangle x1="6.90245" y1="2.57175" x2="7.80415" y2="2.58445" layer="21"/>
<rectangle x1="8.47725" y1="2.57175" x2="9.04875" y2="2.58445" layer="21"/>
<rectangle x1="9.08685" y1="2.57175" x2="10.29335" y2="2.58445" layer="21"/>
<rectangle x1="11.47445" y1="2.57175" x2="11.48715" y2="2.58445" layer="21"/>
<rectangle x1="0.98425" y1="2.58445" x2="3.33375" y2="2.59715" layer="21"/>
<rectangle x1="3.72745" y1="2.58445" x2="3.95605" y2="2.59715" layer="21"/>
<rectangle x1="4.78155" y1="2.58445" x2="5.65785" y2="2.59715" layer="21"/>
<rectangle x1="6.92785" y1="2.58445" x2="7.80415" y2="2.59715" layer="21"/>
<rectangle x1="8.47725" y1="2.58445" x2="9.06145" y2="2.59715" layer="21"/>
<rectangle x1="9.07415" y1="2.58445" x2="10.26795" y2="2.59715" layer="21"/>
<rectangle x1="0.99695" y1="2.59715" x2="3.33375" y2="2.60985" layer="21"/>
<rectangle x1="3.71475" y1="2.59715" x2="3.95605" y2="2.60985" layer="21"/>
<rectangle x1="4.76885" y1="2.59715" x2="5.64515" y2="2.60985" layer="21"/>
<rectangle x1="6.94055" y1="2.59715" x2="7.80415" y2="2.60985" layer="21"/>
<rectangle x1="8.47725" y1="2.59715" x2="10.22985" y2="2.60985" layer="21"/>
<rectangle x1="1.00965" y1="2.60985" x2="3.38455" y2="2.62255" layer="21"/>
<rectangle x1="3.71475" y1="2.60985" x2="3.95605" y2="2.62255" layer="21"/>
<rectangle x1="4.76885" y1="2.60985" x2="5.63245" y2="2.62255" layer="21"/>
<rectangle x1="6.95325" y1="2.60985" x2="7.77875" y2="2.62255" layer="21"/>
<rectangle x1="8.47725" y1="2.60985" x2="10.20445" y2="2.62255" layer="21"/>
<rectangle x1="1.02235" y1="2.62255" x2="3.42265" y2="2.63525" layer="21"/>
<rectangle x1="3.71475" y1="2.62255" x2="3.95605" y2="2.63525" layer="21"/>
<rectangle x1="4.75615" y1="2.62255" x2="5.61975" y2="2.63525" layer="21"/>
<rectangle x1="6.97865" y1="2.62255" x2="7.76605" y2="2.63525" layer="21"/>
<rectangle x1="8.47725" y1="2.62255" x2="8.90905" y2="2.63525" layer="21"/>
<rectangle x1="8.95985" y1="2.62255" x2="10.17905" y2="2.63525" layer="21"/>
<rectangle x1="1.03505" y1="2.63525" x2="3.47345" y2="2.64795" layer="21"/>
<rectangle x1="3.71475" y1="2.63525" x2="3.95605" y2="2.64795" layer="21"/>
<rectangle x1="4.75615" y1="2.63525" x2="5.59435" y2="2.64795" layer="21"/>
<rectangle x1="6.99135" y1="2.63525" x2="7.74065" y2="2.64795" layer="21"/>
<rectangle x1="8.47725" y1="2.63525" x2="8.90905" y2="2.64795" layer="21"/>
<rectangle x1="8.97255" y1="2.63525" x2="10.15365" y2="2.64795" layer="21"/>
<rectangle x1="1.04775" y1="2.64795" x2="3.52425" y2="2.66065" layer="21"/>
<rectangle x1="3.70205" y1="2.64795" x2="3.95605" y2="2.66065" layer="21"/>
<rectangle x1="4.74345" y1="2.64795" x2="5.58165" y2="2.66065" layer="21"/>
<rectangle x1="7.00405" y1="2.64795" x2="7.71525" y2="2.66065" layer="21"/>
<rectangle x1="8.47725" y1="2.64795" x2="8.90905" y2="2.66065" layer="21"/>
<rectangle x1="8.97255" y1="2.64795" x2="10.12825" y2="2.66065" layer="21"/>
<rectangle x1="1.06045" y1="2.66065" x2="3.57505" y2="2.67335" layer="21"/>
<rectangle x1="3.70205" y1="2.66065" x2="3.95605" y2="2.67335" layer="21"/>
<rectangle x1="4.73075" y1="2.66065" x2="5.56895" y2="2.67335" layer="21"/>
<rectangle x1="7.01675" y1="2.66065" x2="7.68985" y2="2.67335" layer="21"/>
<rectangle x1="8.47725" y1="2.66065" x2="8.90905" y2="2.67335" layer="21"/>
<rectangle x1="8.97255" y1="2.66065" x2="10.10285" y2="2.67335" layer="21"/>
<rectangle x1="1.07315" y1="2.67335" x2="3.61315" y2="2.68605" layer="21"/>
<rectangle x1="3.70205" y1="2.67335" x2="3.95605" y2="2.68605" layer="21"/>
<rectangle x1="4.73075" y1="2.67335" x2="5.55625" y2="2.68605" layer="21"/>
<rectangle x1="7.02945" y1="2.67335" x2="7.67715" y2="2.68605" layer="21"/>
<rectangle x1="8.47725" y1="2.67335" x2="8.90905" y2="2.68605" layer="21"/>
<rectangle x1="8.97255" y1="2.67335" x2="10.07745" y2="2.68605" layer="21"/>
<rectangle x1="1.08585" y1="2.68605" x2="3.66395" y2="2.69875" layer="21"/>
<rectangle x1="3.68935" y1="2.68605" x2="3.95605" y2="2.69875" layer="21"/>
<rectangle x1="4.71805" y1="2.68605" x2="5.54355" y2="2.69875" layer="21"/>
<rectangle x1="7.04215" y1="2.68605" x2="7.65175" y2="2.69875" layer="21"/>
<rectangle x1="8.47725" y1="2.68605" x2="8.90905" y2="2.69875" layer="21"/>
<rectangle x1="8.97255" y1="2.68605" x2="10.05205" y2="2.69875" layer="21"/>
<rectangle x1="1.09855" y1="2.69875" x2="3.95605" y2="2.71145" layer="21"/>
<rectangle x1="4.71805" y1="2.69875" x2="5.53085" y2="2.71145" layer="21"/>
<rectangle x1="7.05485" y1="2.69875" x2="7.62635" y2="2.71145" layer="21"/>
<rectangle x1="8.47725" y1="2.69875" x2="8.90905" y2="2.71145" layer="21"/>
<rectangle x1="8.97255" y1="2.69875" x2="10.02665" y2="2.71145" layer="21"/>
<rectangle x1="1.11125" y1="2.71145" x2="3.95605" y2="2.72415" layer="21"/>
<rectangle x1="4.70535" y1="2.71145" x2="5.51815" y2="2.72415" layer="21"/>
<rectangle x1="7.06755" y1="2.71145" x2="7.61365" y2="2.72415" layer="21"/>
<rectangle x1="8.47725" y1="2.71145" x2="8.90905" y2="2.72415" layer="21"/>
<rectangle x1="8.97255" y1="2.71145" x2="10.01395" y2="2.72415" layer="21"/>
<rectangle x1="1.12395" y1="2.72415" x2="3.95605" y2="2.73685" layer="21"/>
<rectangle x1="4.70535" y1="2.72415" x2="5.50545" y2="2.73685" layer="21"/>
<rectangle x1="7.08025" y1="2.72415" x2="7.58825" y2="2.73685" layer="21"/>
<rectangle x1="8.47725" y1="2.72415" x2="8.90905" y2="2.73685" layer="21"/>
<rectangle x1="8.97255" y1="2.72415" x2="9.98855" y2="2.73685" layer="21"/>
<rectangle x1="1.13665" y1="2.73685" x2="3.95605" y2="2.74955" layer="21"/>
<rectangle x1="4.70535" y1="2.73685" x2="5.49275" y2="2.74955" layer="21"/>
<rectangle x1="7.09295" y1="2.73685" x2="7.56285" y2="2.74955" layer="21"/>
<rectangle x1="8.47725" y1="2.73685" x2="8.90905" y2="2.74955" layer="21"/>
<rectangle x1="8.97255" y1="2.73685" x2="9.97585" y2="2.74955" layer="21"/>
<rectangle x1="1.14935" y1="2.74955" x2="3.30835" y2="2.76225" layer="21"/>
<rectangle x1="3.33375" y1="2.74955" x2="3.95605" y2="2.76225" layer="21"/>
<rectangle x1="4.69265" y1="2.74955" x2="5.48005" y2="2.76225" layer="21"/>
<rectangle x1="7.10565" y1="2.74955" x2="7.53745" y2="2.76225" layer="21"/>
<rectangle x1="8.47725" y1="2.74955" x2="8.90905" y2="2.76225" layer="21"/>
<rectangle x1="8.98525" y1="2.74955" x2="9.95045" y2="2.76225" layer="21"/>
<rectangle x1="1.16205" y1="2.76225" x2="3.30835" y2="2.77495" layer="21"/>
<rectangle x1="3.37185" y1="2.76225" x2="3.95605" y2="2.77495" layer="21"/>
<rectangle x1="4.69265" y1="2.76225" x2="5.46735" y2="2.77495" layer="21"/>
<rectangle x1="7.11835" y1="2.76225" x2="7.52475" y2="2.77495" layer="21"/>
<rectangle x1="8.47725" y1="2.76225" x2="8.90905" y2="2.77495" layer="21"/>
<rectangle x1="8.98525" y1="2.76225" x2="9.93775" y2="2.77495" layer="21"/>
<rectangle x1="1.17475" y1="2.77495" x2="3.30835" y2="2.78765" layer="21"/>
<rectangle x1="3.42265" y1="2.77495" x2="3.95605" y2="2.78765" layer="21"/>
<rectangle x1="4.67995" y1="2.77495" x2="5.45465" y2="2.78765" layer="21"/>
<rectangle x1="7.13105" y1="2.77495" x2="7.49935" y2="2.78765" layer="21"/>
<rectangle x1="8.47725" y1="2.77495" x2="8.92175" y2="2.78765" layer="21"/>
<rectangle x1="8.97255" y1="2.77495" x2="9.91235" y2="2.78765" layer="21"/>
<rectangle x1="1.18745" y1="2.78765" x2="3.29565" y2="2.80035" layer="21"/>
<rectangle x1="3.47345" y1="2.78765" x2="3.95605" y2="2.80035" layer="21"/>
<rectangle x1="4.67995" y1="2.78765" x2="5.44195" y2="2.80035" layer="21"/>
<rectangle x1="7.14375" y1="2.78765" x2="7.47395" y2="2.80035" layer="21"/>
<rectangle x1="8.47725" y1="2.78765" x2="8.93445" y2="2.80035" layer="21"/>
<rectangle x1="8.95985" y1="2.78765" x2="9.89965" y2="2.80035" layer="21"/>
<rectangle x1="1.20015" y1="2.80035" x2="3.29565" y2="2.81305" layer="21"/>
<rectangle x1="3.51155" y1="2.80035" x2="3.95605" y2="2.81305" layer="21"/>
<rectangle x1="4.66725" y1="2.80035" x2="5.44195" y2="2.81305" layer="21"/>
<rectangle x1="7.15645" y1="2.80035" x2="7.46125" y2="2.81305" layer="21"/>
<rectangle x1="8.47725" y1="2.80035" x2="9.88695" y2="2.81305" layer="21"/>
<rectangle x1="1.21285" y1="2.81305" x2="3.29565" y2="2.82575" layer="21"/>
<rectangle x1="3.56235" y1="2.81305" x2="3.95605" y2="2.82575" layer="21"/>
<rectangle x1="4.66725" y1="2.81305" x2="5.42925" y2="2.82575" layer="21"/>
<rectangle x1="7.15645" y1="2.81305" x2="7.43585" y2="2.82575" layer="21"/>
<rectangle x1="8.47725" y1="2.81305" x2="9.86155" y2="2.82575" layer="21"/>
<rectangle x1="1.22555" y1="2.82575" x2="3.29565" y2="2.83845" layer="21"/>
<rectangle x1="3.61315" y1="2.82575" x2="3.95605" y2="2.83845" layer="21"/>
<rectangle x1="4.66725" y1="2.82575" x2="5.41655" y2="2.83845" layer="21"/>
<rectangle x1="7.16915" y1="2.82575" x2="7.41045" y2="2.83845" layer="21"/>
<rectangle x1="8.47725" y1="2.82575" x2="9.84885" y2="2.83845" layer="21"/>
<rectangle x1="1.23825" y1="2.83845" x2="3.28295" y2="2.85115" layer="21"/>
<rectangle x1="3.62585" y1="2.83845" x2="3.95605" y2="2.85115" layer="21"/>
<rectangle x1="4.65455" y1="2.83845" x2="5.40385" y2="2.85115" layer="21"/>
<rectangle x1="7.18185" y1="2.83845" x2="7.38505" y2="2.85115" layer="21"/>
<rectangle x1="8.47725" y1="2.83845" x2="9.83615" y2="2.85115" layer="21"/>
<rectangle x1="1.25095" y1="2.85115" x2="3.28295" y2="2.86385" layer="21"/>
<rectangle x1="3.62585" y1="2.85115" x2="3.95605" y2="2.86385" layer="21"/>
<rectangle x1="4.65455" y1="2.85115" x2="5.40385" y2="2.86385" layer="21"/>
<rectangle x1="7.18185" y1="2.85115" x2="7.37235" y2="2.86385" layer="21"/>
<rectangle x1="8.47725" y1="2.85115" x2="9.82345" y2="2.86385" layer="21"/>
<rectangle x1="1.26365" y1="2.86385" x2="3.27025" y2="2.87655" layer="21"/>
<rectangle x1="3.62585" y1="2.86385" x2="3.95605" y2="2.87655" layer="21"/>
<rectangle x1="4.64185" y1="2.86385" x2="5.39115" y2="2.87655" layer="21"/>
<rectangle x1="7.19455" y1="2.86385" x2="7.34695" y2="2.87655" layer="21"/>
<rectangle x1="8.47725" y1="2.86385" x2="9.79805" y2="2.87655" layer="21"/>
<rectangle x1="1.27635" y1="2.87655" x2="3.27025" y2="2.88925" layer="21"/>
<rectangle x1="3.61315" y1="2.87655" x2="3.95605" y2="2.88925" layer="21"/>
<rectangle x1="4.64185" y1="2.87655" x2="5.37845" y2="2.88925" layer="21"/>
<rectangle x1="7.20725" y1="2.87655" x2="7.32155" y2="2.88925" layer="21"/>
<rectangle x1="8.47725" y1="2.87655" x2="9.78535" y2="2.88925" layer="21"/>
<rectangle x1="1.28905" y1="2.88925" x2="3.27025" y2="2.90195" layer="21"/>
<rectangle x1="3.61315" y1="2.88925" x2="3.95605" y2="2.90195" layer="21"/>
<rectangle x1="4.64185" y1="2.88925" x2="5.37845" y2="2.90195" layer="21"/>
<rectangle x1="7.20725" y1="2.88925" x2="7.29615" y2="2.90195" layer="21"/>
<rectangle x1="8.47725" y1="2.88925" x2="9.77265" y2="2.90195" layer="21"/>
<rectangle x1="1.30175" y1="2.90195" x2="3.28295" y2="2.91465" layer="21"/>
<rectangle x1="3.60045" y1="2.90195" x2="3.95605" y2="2.91465" layer="21"/>
<rectangle x1="4.62915" y1="2.90195" x2="5.36575" y2="2.91465" layer="21"/>
<rectangle x1="7.21995" y1="2.90195" x2="7.28345" y2="2.91465" layer="21"/>
<rectangle x1="8.47725" y1="2.90195" x2="9.75995" y2="2.91465" layer="21"/>
<rectangle x1="1.31445" y1="2.91465" x2="3.29565" y2="2.92735" layer="21"/>
<rectangle x1="3.60045" y1="2.91465" x2="3.95605" y2="2.92735" layer="21"/>
<rectangle x1="4.62915" y1="2.91465" x2="5.35305" y2="2.92735" layer="21"/>
<rectangle x1="7.23265" y1="2.91465" x2="7.25805" y2="2.92735" layer="21"/>
<rectangle x1="8.47725" y1="2.91465" x2="9.74725" y2="2.92735" layer="21"/>
<rectangle x1="1.32715" y1="2.92735" x2="3.30835" y2="2.94005" layer="21"/>
<rectangle x1="3.58775" y1="2.92735" x2="3.95605" y2="2.94005" layer="21"/>
<rectangle x1="4.62915" y1="2.92735" x2="5.35305" y2="2.94005" layer="21"/>
<rectangle x1="8.47725" y1="2.92735" x2="9.73455" y2="2.94005" layer="21"/>
<rectangle x1="1.33985" y1="2.94005" x2="3.32105" y2="2.95275" layer="21"/>
<rectangle x1="3.57505" y1="2.94005" x2="3.95605" y2="2.95275" layer="21"/>
<rectangle x1="4.61645" y1="2.94005" x2="5.34035" y2="2.95275" layer="21"/>
<rectangle x1="8.47725" y1="2.94005" x2="9.72185" y2="2.95275" layer="21"/>
<rectangle x1="1.35255" y1="2.95275" x2="3.33375" y2="2.96545" layer="21"/>
<rectangle x1="3.57505" y1="2.95275" x2="3.95605" y2="2.96545" layer="21"/>
<rectangle x1="4.61645" y1="2.95275" x2="5.34035" y2="2.96545" layer="21"/>
<rectangle x1="8.47725" y1="2.95275" x2="8.98525" y2="2.96545" layer="21"/>
<rectangle x1="9.01065" y1="2.95275" x2="9.70915" y2="2.96545" layer="21"/>
<rectangle x1="1.36525" y1="2.96545" x2="3.34645" y2="2.97815" layer="21"/>
<rectangle x1="3.56235" y1="2.96545" x2="3.95605" y2="2.97815" layer="21"/>
<rectangle x1="4.61645" y1="2.96545" x2="5.32765" y2="2.97815" layer="21"/>
<rectangle x1="8.47725" y1="2.96545" x2="8.97255" y2="2.97815" layer="21"/>
<rectangle x1="9.02335" y1="2.96545" x2="9.69645" y2="2.97815" layer="21"/>
<rectangle x1="1.37795" y1="2.97815" x2="3.35915" y2="2.99085" layer="21"/>
<rectangle x1="3.56235" y1="2.97815" x2="3.95605" y2="2.99085" layer="21"/>
<rectangle x1="4.60375" y1="2.97815" x2="5.32765" y2="2.99085" layer="21"/>
<rectangle x1="8.47725" y1="2.97815" x2="8.97255" y2="2.99085" layer="21"/>
<rectangle x1="9.03605" y1="2.97815" x2="9.68375" y2="2.99085" layer="21"/>
<rectangle x1="1.39065" y1="2.99085" x2="3.37185" y2="3.00355" layer="21"/>
<rectangle x1="3.54965" y1="2.99085" x2="3.95605" y2="3.00355" layer="21"/>
<rectangle x1="4.60375" y1="2.99085" x2="5.31495" y2="3.00355" layer="21"/>
<rectangle x1="8.47725" y1="2.99085" x2="8.97255" y2="3.00355" layer="21"/>
<rectangle x1="9.03605" y1="2.99085" x2="9.67105" y2="3.00355" layer="21"/>
<rectangle x1="1.40335" y1="3.00355" x2="3.38455" y2="3.01625" layer="21"/>
<rectangle x1="3.53695" y1="3.00355" x2="3.95605" y2="3.01625" layer="21"/>
<rectangle x1="4.60375" y1="3.00355" x2="5.31495" y2="3.01625" layer="21"/>
<rectangle x1="8.47725" y1="3.00355" x2="8.97255" y2="3.01625" layer="21"/>
<rectangle x1="9.03605" y1="3.00355" x2="9.65835" y2="3.01625" layer="21"/>
<rectangle x1="1.41605" y1="3.01625" x2="3.39725" y2="3.02895" layer="21"/>
<rectangle x1="3.53695" y1="3.01625" x2="3.95605" y2="3.02895" layer="21"/>
<rectangle x1="4.60375" y1="3.01625" x2="5.30225" y2="3.02895" layer="21"/>
<rectangle x1="8.47725" y1="3.01625" x2="8.97255" y2="3.02895" layer="21"/>
<rectangle x1="9.04875" y1="3.01625" x2="9.64565" y2="3.02895" layer="21"/>
<rectangle x1="1.42875" y1="3.02895" x2="3.40995" y2="3.04165" layer="21"/>
<rectangle x1="3.52425" y1="3.02895" x2="3.95605" y2="3.04165" layer="21"/>
<rectangle x1="4.59105" y1="3.02895" x2="5.30225" y2="3.04165" layer="21"/>
<rectangle x1="8.47725" y1="3.02895" x2="8.97255" y2="3.04165" layer="21"/>
<rectangle x1="9.04875" y1="3.02895" x2="9.63295" y2="3.04165" layer="21"/>
<rectangle x1="1.44145" y1="3.04165" x2="3.42265" y2="3.05435" layer="21"/>
<rectangle x1="3.51155" y1="3.04165" x2="3.95605" y2="3.05435" layer="21"/>
<rectangle x1="4.59105" y1="3.04165" x2="5.28955" y2="3.05435" layer="21"/>
<rectangle x1="8.47725" y1="3.04165" x2="8.98525" y2="3.05435" layer="21"/>
<rectangle x1="9.04875" y1="3.04165" x2="9.62025" y2="3.05435" layer="21"/>
<rectangle x1="1.45415" y1="3.05435" x2="3.43535" y2="3.06705" layer="21"/>
<rectangle x1="3.51155" y1="3.05435" x2="3.95605" y2="3.06705" layer="21"/>
<rectangle x1="4.59105" y1="3.05435" x2="5.28955" y2="3.06705" layer="21"/>
<rectangle x1="8.47725" y1="3.05435" x2="8.98525" y2="3.06705" layer="21"/>
<rectangle x1="9.06145" y1="3.05435" x2="9.60755" y2="3.06705" layer="21"/>
<rectangle x1="1.46685" y1="3.06705" x2="3.44805" y2="3.07975" layer="21"/>
<rectangle x1="3.49885" y1="3.06705" x2="3.95605" y2="3.07975" layer="21"/>
<rectangle x1="4.59105" y1="3.06705" x2="5.28955" y2="3.07975" layer="21"/>
<rectangle x1="8.47725" y1="3.06705" x2="8.98525" y2="3.07975" layer="21"/>
<rectangle x1="9.06145" y1="3.06705" x2="9.60755" y2="3.07975" layer="21"/>
<rectangle x1="1.47955" y1="3.07975" x2="3.46075" y2="3.09245" layer="21"/>
<rectangle x1="3.48615" y1="3.07975" x2="3.95605" y2="3.09245" layer="21"/>
<rectangle x1="4.57835" y1="3.07975" x2="5.27685" y2="3.09245" layer="21"/>
<rectangle x1="8.47725" y1="3.07975" x2="8.99795" y2="3.09245" layer="21"/>
<rectangle x1="9.06145" y1="3.07975" x2="9.59485" y2="3.09245" layer="21"/>
<rectangle x1="1.49225" y1="3.09245" x2="3.95605" y2="3.10515" layer="21"/>
<rectangle x1="4.57835" y1="3.09245" x2="5.27685" y2="3.10515" layer="21"/>
<rectangle x1="8.47725" y1="3.09245" x2="8.99795" y2="3.10515" layer="21"/>
<rectangle x1="9.07415" y1="3.09245" x2="9.58215" y2="3.10515" layer="21"/>
<rectangle x1="1.50495" y1="3.10515" x2="3.95605" y2="3.11785" layer="21"/>
<rectangle x1="4.57835" y1="3.10515" x2="5.26415" y2="3.11785" layer="21"/>
<rectangle x1="8.47725" y1="3.10515" x2="9.01065" y2="3.11785" layer="21"/>
<rectangle x1="9.06145" y1="3.10515" x2="9.56945" y2="3.11785" layer="21"/>
<rectangle x1="1.51765" y1="3.11785" x2="3.95605" y2="3.13055" layer="21"/>
<rectangle x1="4.57835" y1="3.11785" x2="5.26415" y2="3.13055" layer="21"/>
<rectangle x1="8.47725" y1="3.11785" x2="9.01065" y2="3.13055" layer="21"/>
<rectangle x1="9.06145" y1="3.11785" x2="9.55675" y2="3.13055" layer="21"/>
<rectangle x1="1.53035" y1="3.13055" x2="3.95605" y2="3.14325" layer="21"/>
<rectangle x1="4.57835" y1="3.13055" x2="5.26415" y2="3.14325" layer="21"/>
<rectangle x1="8.47725" y1="3.13055" x2="9.55675" y2="3.14325" layer="21"/>
<rectangle x1="1.54305" y1="3.14325" x2="3.95605" y2="3.15595" layer="21"/>
<rectangle x1="4.56565" y1="3.14325" x2="5.25145" y2="3.15595" layer="21"/>
<rectangle x1="8.47725" y1="3.14325" x2="9.54405" y2="3.15595" layer="21"/>
<rectangle x1="1.55575" y1="3.15595" x2="3.95605" y2="3.16865" layer="21"/>
<rectangle x1="4.56565" y1="3.15595" x2="5.25145" y2="3.16865" layer="21"/>
<rectangle x1="8.47725" y1="3.15595" x2="9.53135" y2="3.16865" layer="21"/>
<rectangle x1="1.56845" y1="3.16865" x2="3.95605" y2="3.18135" layer="21"/>
<rectangle x1="4.56565" y1="3.16865" x2="5.25145" y2="3.18135" layer="21"/>
<rectangle x1="8.47725" y1="3.16865" x2="9.51865" y2="3.18135" layer="21"/>
<rectangle x1="1.58115" y1="3.18135" x2="3.95605" y2="3.19405" layer="21"/>
<rectangle x1="4.56565" y1="3.18135" x2="5.23875" y2="3.19405" layer="21"/>
<rectangle x1="8.47725" y1="3.18135" x2="9.51865" y2="3.19405" layer="21"/>
<rectangle x1="1.59385" y1="3.19405" x2="3.95605" y2="3.20675" layer="21"/>
<rectangle x1="4.56565" y1="3.19405" x2="5.23875" y2="3.20675" layer="21"/>
<rectangle x1="8.47725" y1="3.19405" x2="9.50595" y2="3.20675" layer="21"/>
<rectangle x1="1.60655" y1="3.20675" x2="3.95605" y2="3.21945" layer="21"/>
<rectangle x1="4.55295" y1="3.20675" x2="5.23875" y2="3.21945" layer="21"/>
<rectangle x1="8.47725" y1="3.20675" x2="9.49325" y2="3.21945" layer="21"/>
<rectangle x1="1.61925" y1="3.21945" x2="3.95605" y2="3.23215" layer="21"/>
<rectangle x1="4.55295" y1="3.21945" x2="5.23875" y2="3.23215" layer="21"/>
<rectangle x1="8.47725" y1="3.21945" x2="9.49325" y2="3.23215" layer="21"/>
<rectangle x1="1.63195" y1="3.23215" x2="3.95605" y2="3.24485" layer="21"/>
<rectangle x1="4.55295" y1="3.23215" x2="5.22605" y2="3.24485" layer="21"/>
<rectangle x1="8.47725" y1="3.23215" x2="9.48055" y2="3.24485" layer="21"/>
<rectangle x1="1.64465" y1="3.24485" x2="3.95605" y2="3.25755" layer="21"/>
<rectangle x1="4.55295" y1="3.24485" x2="5.22605" y2="3.25755" layer="21"/>
<rectangle x1="8.47725" y1="3.24485" x2="9.46785" y2="3.25755" layer="21"/>
<rectangle x1="1.65735" y1="3.25755" x2="3.95605" y2="3.27025" layer="21"/>
<rectangle x1="4.55295" y1="3.25755" x2="5.22605" y2="3.27025" layer="21"/>
<rectangle x1="8.47725" y1="3.25755" x2="9.46785" y2="3.27025" layer="21"/>
<rectangle x1="1.67005" y1="3.27025" x2="3.95605" y2="3.28295" layer="21"/>
<rectangle x1="4.55295" y1="3.27025" x2="5.22605" y2="3.28295" layer="21"/>
<rectangle x1="8.47725" y1="3.27025" x2="9.45515" y2="3.28295" layer="21"/>
<rectangle x1="1.68275" y1="3.28295" x2="3.95605" y2="3.29565" layer="21"/>
<rectangle x1="4.55295" y1="3.28295" x2="5.22605" y2="3.29565" layer="21"/>
<rectangle x1="8.47725" y1="3.28295" x2="9.09955" y2="3.29565" layer="21"/>
<rectangle x1="9.15035" y1="3.28295" x2="9.44245" y2="3.29565" layer="21"/>
<rectangle x1="1.69545" y1="3.29565" x2="3.95605" y2="3.30835" layer="21"/>
<rectangle x1="4.55295" y1="3.29565" x2="5.21335" y2="3.30835" layer="21"/>
<rectangle x1="8.47725" y1="3.29565" x2="9.09955" y2="3.30835" layer="21"/>
<rectangle x1="9.16305" y1="3.29565" x2="9.44245" y2="3.30835" layer="21"/>
<rectangle x1="1.70815" y1="3.30835" x2="3.95605" y2="3.32105" layer="21"/>
<rectangle x1="4.55295" y1="3.30835" x2="5.21335" y2="3.32105" layer="21"/>
<rectangle x1="8.47725" y1="3.30835" x2="9.08685" y2="3.32105" layer="21"/>
<rectangle x1="9.16305" y1="3.30835" x2="9.42975" y2="3.32105" layer="21"/>
<rectangle x1="1.72085" y1="3.32105" x2="3.95605" y2="3.33375" layer="21"/>
<rectangle x1="4.55295" y1="3.32105" x2="5.21335" y2="3.33375" layer="21"/>
<rectangle x1="8.47725" y1="3.32105" x2="9.09955" y2="3.33375" layer="21"/>
<rectangle x1="9.17575" y1="3.32105" x2="9.42975" y2="3.33375" layer="21"/>
<rectangle x1="1.73355" y1="3.33375" x2="3.95605" y2="3.34645" layer="21"/>
<rectangle x1="4.54025" y1="3.33375" x2="5.21335" y2="3.34645" layer="21"/>
<rectangle x1="8.47725" y1="3.33375" x2="9.09955" y2="3.34645" layer="21"/>
<rectangle x1="9.17575" y1="3.33375" x2="9.41705" y2="3.34645" layer="21"/>
<rectangle x1="1.74625" y1="3.34645" x2="3.95605" y2="3.35915" layer="21"/>
<rectangle x1="4.54025" y1="3.34645" x2="5.21335" y2="3.35915" layer="21"/>
<rectangle x1="8.47725" y1="3.34645" x2="9.11225" y2="3.35915" layer="21"/>
<rectangle x1="9.18845" y1="3.34645" x2="9.41705" y2="3.35915" layer="21"/>
<rectangle x1="1.75895" y1="3.35915" x2="3.95605" y2="3.37185" layer="21"/>
<rectangle x1="4.54025" y1="3.35915" x2="5.21335" y2="3.37185" layer="21"/>
<rectangle x1="8.47725" y1="3.35915" x2="9.11225" y2="3.37185" layer="21"/>
<rectangle x1="9.18845" y1="3.35915" x2="9.40435" y2="3.37185" layer="21"/>
<rectangle x1="1.77165" y1="3.37185" x2="3.95605" y2="3.38455" layer="21"/>
<rectangle x1="4.54025" y1="3.37185" x2="5.21335" y2="3.38455" layer="21"/>
<rectangle x1="8.47725" y1="3.37185" x2="9.12495" y2="3.38455" layer="21"/>
<rectangle x1="9.20115" y1="3.37185" x2="9.40435" y2="3.38455" layer="21"/>
<rectangle x1="1.78435" y1="3.38455" x2="3.95605" y2="3.39725" layer="21"/>
<rectangle x1="4.54025" y1="3.38455" x2="5.21335" y2="3.39725" layer="21"/>
<rectangle x1="8.47725" y1="3.38455" x2="9.12495" y2="3.39725" layer="21"/>
<rectangle x1="9.20115" y1="3.38455" x2="9.39165" y2="3.39725" layer="21"/>
<rectangle x1="1.79705" y1="3.39725" x2="3.95605" y2="3.40995" layer="21"/>
<rectangle x1="4.54025" y1="3.39725" x2="5.21335" y2="3.40995" layer="21"/>
<rectangle x1="8.47725" y1="3.39725" x2="9.13765" y2="3.40995" layer="21"/>
<rectangle x1="9.21385" y1="3.39725" x2="9.39165" y2="3.40995" layer="21"/>
<rectangle x1="1.80975" y1="3.40995" x2="3.95605" y2="3.42265" layer="21"/>
<rectangle x1="4.54025" y1="3.40995" x2="5.21335" y2="3.42265" layer="21"/>
<rectangle x1="8.47725" y1="3.40995" x2="9.13765" y2="3.42265" layer="21"/>
<rectangle x1="9.21385" y1="3.40995" x2="9.37895" y2="3.42265" layer="21"/>
<rectangle x1="1.82245" y1="3.42265" x2="3.95605" y2="3.43535" layer="21"/>
<rectangle x1="4.54025" y1="3.42265" x2="5.21335" y2="3.43535" layer="21"/>
<rectangle x1="8.47725" y1="3.42265" x2="9.15035" y2="3.43535" layer="21"/>
<rectangle x1="9.21385" y1="3.42265" x2="9.37895" y2="3.43535" layer="21"/>
<rectangle x1="1.83515" y1="3.43535" x2="3.95605" y2="3.44805" layer="21"/>
<rectangle x1="4.54025" y1="3.43535" x2="5.21335" y2="3.44805" layer="21"/>
<rectangle x1="8.47725" y1="3.43535" x2="9.15035" y2="3.44805" layer="21"/>
<rectangle x1="9.20115" y1="3.43535" x2="9.36625" y2="3.44805" layer="21"/>
<rectangle x1="1.84785" y1="3.44805" x2="3.95605" y2="3.46075" layer="21"/>
<rectangle x1="4.54025" y1="3.44805" x2="5.21335" y2="3.46075" layer="21"/>
<rectangle x1="8.47725" y1="3.44805" x2="9.36625" y2="3.46075" layer="21"/>
<rectangle x1="1.86055" y1="3.46075" x2="3.95605" y2="3.47345" layer="21"/>
<rectangle x1="4.54025" y1="3.46075" x2="5.21335" y2="3.47345" layer="21"/>
<rectangle x1="8.47725" y1="3.46075" x2="9.35355" y2="3.47345" layer="21"/>
<rectangle x1="1.87325" y1="3.47345" x2="3.95605" y2="3.48615" layer="21"/>
<rectangle x1="4.54025" y1="3.47345" x2="5.21335" y2="3.48615" layer="21"/>
<rectangle x1="8.47725" y1="3.47345" x2="9.35355" y2="3.48615" layer="21"/>
<rectangle x1="1.88595" y1="3.48615" x2="3.95605" y2="3.49885" layer="21"/>
<rectangle x1="4.54025" y1="3.48615" x2="5.21335" y2="3.49885" layer="21"/>
<rectangle x1="8.47725" y1="3.48615" x2="9.34085" y2="3.49885" layer="21"/>
<rectangle x1="1.89865" y1="3.49885" x2="3.95605" y2="3.51155" layer="21"/>
<rectangle x1="4.54025" y1="3.49885" x2="5.21335" y2="3.51155" layer="21"/>
<rectangle x1="8.47725" y1="3.49885" x2="9.34085" y2="3.51155" layer="21"/>
<rectangle x1="1.91135" y1="3.51155" x2="3.95605" y2="3.52425" layer="21"/>
<rectangle x1="4.54025" y1="3.51155" x2="5.21335" y2="3.52425" layer="21"/>
<rectangle x1="8.47725" y1="3.51155" x2="9.34085" y2="3.52425" layer="21"/>
<rectangle x1="1.92405" y1="3.52425" x2="3.95605" y2="3.53695" layer="21"/>
<rectangle x1="4.54025" y1="3.52425" x2="5.21335" y2="3.53695" layer="21"/>
<rectangle x1="8.47725" y1="3.52425" x2="9.32815" y2="3.53695" layer="21"/>
<rectangle x1="1.93675" y1="3.53695" x2="3.95605" y2="3.54965" layer="21"/>
<rectangle x1="4.54025" y1="3.53695" x2="5.21335" y2="3.54965" layer="21"/>
<rectangle x1="8.47725" y1="3.53695" x2="9.32815" y2="3.54965" layer="21"/>
<rectangle x1="1.94945" y1="3.54965" x2="3.95605" y2="3.56235" layer="21"/>
<rectangle x1="4.54025" y1="3.54965" x2="5.21335" y2="3.56235" layer="21"/>
<rectangle x1="8.47725" y1="3.54965" x2="9.31545" y2="3.56235" layer="21"/>
<rectangle x1="1.96215" y1="3.56235" x2="3.95605" y2="3.57505" layer="21"/>
<rectangle x1="4.54025" y1="3.56235" x2="5.21335" y2="3.57505" layer="21"/>
<rectangle x1="8.47725" y1="3.56235" x2="9.31545" y2="3.57505" layer="21"/>
<rectangle x1="1.97485" y1="3.57505" x2="3.95605" y2="3.58775" layer="21"/>
<rectangle x1="4.54025" y1="3.57505" x2="5.21335" y2="3.58775" layer="21"/>
<rectangle x1="8.47725" y1="3.57505" x2="9.31545" y2="3.58775" layer="21"/>
<rectangle x1="1.98755" y1="3.58775" x2="3.95605" y2="3.60045" layer="21"/>
<rectangle x1="4.54025" y1="3.58775" x2="5.21335" y2="3.60045" layer="21"/>
<rectangle x1="8.47725" y1="3.58775" x2="9.26465" y2="3.60045" layer="21"/>
<rectangle x1="2.00025" y1="3.60045" x2="3.95605" y2="3.61315" layer="21"/>
<rectangle x1="4.54025" y1="3.60045" x2="5.21335" y2="3.61315" layer="21"/>
<rectangle x1="8.47725" y1="3.60045" x2="9.26465" y2="3.61315" layer="21"/>
<rectangle x1="2.01295" y1="3.61315" x2="3.95605" y2="3.62585" layer="21"/>
<rectangle x1="4.55295" y1="3.61315" x2="5.21335" y2="3.62585" layer="21"/>
<rectangle x1="8.47725" y1="3.61315" x2="9.25195" y2="3.62585" layer="21"/>
<rectangle x1="2.02565" y1="3.62585" x2="3.95605" y2="3.63855" layer="21"/>
<rectangle x1="4.55295" y1="3.62585" x2="5.21335" y2="3.63855" layer="21"/>
<rectangle x1="8.47725" y1="3.62585" x2="9.26465" y2="3.63855" layer="21"/>
<rectangle x1="2.03835" y1="3.63855" x2="3.95605" y2="3.65125" layer="21"/>
<rectangle x1="4.55295" y1="3.63855" x2="5.22605" y2="3.65125" layer="21"/>
<rectangle x1="8.47725" y1="3.63855" x2="9.26465" y2="3.65125" layer="21"/>
<rectangle x1="2.05105" y1="3.65125" x2="3.95605" y2="3.66395" layer="21"/>
<rectangle x1="4.55295" y1="3.65125" x2="5.22605" y2="3.66395" layer="21"/>
<rectangle x1="8.47725" y1="3.65125" x2="9.27735" y2="3.66395" layer="21"/>
<rectangle x1="2.06375" y1="3.66395" x2="3.95605" y2="3.67665" layer="21"/>
<rectangle x1="4.55295" y1="3.66395" x2="5.22605" y2="3.67665" layer="21"/>
<rectangle x1="8.47725" y1="3.66395" x2="9.27735" y2="3.67665" layer="21"/>
<rectangle x1="2.07645" y1="3.67665" x2="3.95605" y2="3.68935" layer="21"/>
<rectangle x1="4.55295" y1="3.67665" x2="5.22605" y2="3.68935" layer="21"/>
<rectangle x1="8.47725" y1="3.67665" x2="9.27735" y2="3.68935" layer="21"/>
<rectangle x1="2.08915" y1="3.68935" x2="3.95605" y2="3.70205" layer="21"/>
<rectangle x1="4.55295" y1="3.68935" x2="5.22605" y2="3.70205" layer="21"/>
<rectangle x1="7.94385" y1="3.68935" x2="7.98195" y2="3.70205" layer="21"/>
<rectangle x1="8.47725" y1="3.68935" x2="9.27735" y2="3.70205" layer="21"/>
<rectangle x1="2.10185" y1="3.70205" x2="3.95605" y2="3.71475" layer="21"/>
<rectangle x1="4.55295" y1="3.70205" x2="5.22605" y2="3.71475" layer="21"/>
<rectangle x1="7.90575" y1="3.70205" x2="7.98195" y2="3.71475" layer="21"/>
<rectangle x1="8.47725" y1="3.70205" x2="9.27735" y2="3.71475" layer="21"/>
<rectangle x1="2.11455" y1="3.71475" x2="3.95605" y2="3.72745" layer="21"/>
<rectangle x1="4.55295" y1="3.71475" x2="5.23875" y2="3.72745" layer="21"/>
<rectangle x1="7.85495" y1="3.71475" x2="7.98195" y2="3.72745" layer="21"/>
<rectangle x1="8.47725" y1="3.71475" x2="9.26465" y2="3.72745" layer="21"/>
<rectangle x1="2.12725" y1="3.72745" x2="3.95605" y2="3.74015" layer="21"/>
<rectangle x1="4.56565" y1="3.72745" x2="5.23875" y2="3.74015" layer="21"/>
<rectangle x1="7.81685" y1="3.72745" x2="7.96925" y2="3.74015" layer="21"/>
<rectangle x1="8.47725" y1="3.72745" x2="9.26465" y2="3.74015" layer="21"/>
<rectangle x1="2.13995" y1="3.74015" x2="3.95605" y2="3.75285" layer="21"/>
<rectangle x1="4.56565" y1="3.74015" x2="5.23875" y2="3.75285" layer="21"/>
<rectangle x1="7.77875" y1="3.74015" x2="7.96925" y2="3.75285" layer="21"/>
<rectangle x1="8.47725" y1="3.74015" x2="9.26465" y2="3.75285" layer="21"/>
<rectangle x1="2.15265" y1="3.75285" x2="3.95605" y2="3.76555" layer="21"/>
<rectangle x1="4.56565" y1="3.75285" x2="5.23875" y2="3.76555" layer="21"/>
<rectangle x1="7.72795" y1="3.75285" x2="7.96925" y2="3.76555" layer="21"/>
<rectangle x1="8.47725" y1="3.75285" x2="9.25195" y2="3.76555" layer="21"/>
<rectangle x1="2.16535" y1="3.76555" x2="3.95605" y2="3.77825" layer="21"/>
<rectangle x1="4.56565" y1="3.76555" x2="5.25145" y2="3.77825" layer="21"/>
<rectangle x1="7.68985" y1="3.76555" x2="7.96925" y2="3.77825" layer="21"/>
<rectangle x1="8.47725" y1="3.76555" x2="9.25195" y2="3.77825" layer="21"/>
<rectangle x1="2.17805" y1="3.77825" x2="3.95605" y2="3.79095" layer="21"/>
<rectangle x1="4.56565" y1="3.77825" x2="5.25145" y2="3.79095" layer="21"/>
<rectangle x1="7.65175" y1="3.77825" x2="7.95655" y2="3.79095" layer="21"/>
<rectangle x1="8.47725" y1="3.77825" x2="9.25195" y2="3.79095" layer="21"/>
<rectangle x1="2.19075" y1="3.79095" x2="3.95605" y2="3.80365" layer="21"/>
<rectangle x1="4.56565" y1="3.79095" x2="5.25145" y2="3.80365" layer="21"/>
<rectangle x1="7.61365" y1="3.79095" x2="7.95655" y2="3.80365" layer="21"/>
<rectangle x1="8.47725" y1="3.79095" x2="9.25195" y2="3.80365" layer="21"/>
<rectangle x1="2.20345" y1="3.80365" x2="3.95605" y2="3.81635" layer="21"/>
<rectangle x1="4.57835" y1="3.80365" x2="5.26415" y2="3.81635" layer="21"/>
<rectangle x1="7.57555" y1="3.80365" x2="7.95655" y2="3.81635" layer="21"/>
<rectangle x1="8.47725" y1="3.80365" x2="9.25195" y2="3.81635" layer="21"/>
<rectangle x1="2.21615" y1="3.81635" x2="3.95605" y2="3.82905" layer="21"/>
<rectangle x1="4.57835" y1="3.81635" x2="5.26415" y2="3.82905" layer="21"/>
<rectangle x1="7.52475" y1="3.81635" x2="7.95655" y2="3.82905" layer="21"/>
<rectangle x1="8.47725" y1="3.81635" x2="9.23925" y2="3.82905" layer="21"/>
<rectangle x1="2.22885" y1="3.82905" x2="3.95605" y2="3.84175" layer="21"/>
<rectangle x1="4.57835" y1="3.82905" x2="5.26415" y2="3.84175" layer="21"/>
<rectangle x1="7.48665" y1="3.82905" x2="7.95655" y2="3.84175" layer="21"/>
<rectangle x1="8.47725" y1="3.82905" x2="9.23925" y2="3.84175" layer="21"/>
<rectangle x1="2.24155" y1="3.84175" x2="3.95605" y2="3.85445" layer="21"/>
<rectangle x1="4.57835" y1="3.84175" x2="5.27685" y2="3.85445" layer="21"/>
<rectangle x1="7.44855" y1="3.84175" x2="7.94385" y2="3.85445" layer="21"/>
<rectangle x1="8.47725" y1="3.84175" x2="9.23925" y2="3.85445" layer="21"/>
<rectangle x1="2.25425" y1="3.85445" x2="3.95605" y2="3.86715" layer="21"/>
<rectangle x1="4.57835" y1="3.85445" x2="5.27685" y2="3.86715" layer="21"/>
<rectangle x1="7.39775" y1="3.85445" x2="7.94385" y2="3.86715" layer="21"/>
<rectangle x1="8.47725" y1="3.85445" x2="9.23925" y2="3.86715" layer="21"/>
<rectangle x1="2.26695" y1="3.86715" x2="3.95605" y2="3.87985" layer="21"/>
<rectangle x1="4.59105" y1="3.86715" x2="5.27685" y2="3.87985" layer="21"/>
<rectangle x1="7.35965" y1="3.86715" x2="7.94385" y2="3.87985" layer="21"/>
<rectangle x1="8.47725" y1="3.86715" x2="9.23925" y2="3.87985" layer="21"/>
<rectangle x1="2.27965" y1="3.87985" x2="3.95605" y2="3.89255" layer="21"/>
<rectangle x1="4.59105" y1="3.87985" x2="5.28955" y2="3.89255" layer="21"/>
<rectangle x1="7.32155" y1="3.87985" x2="7.94385" y2="3.89255" layer="21"/>
<rectangle x1="8.47725" y1="3.87985" x2="9.23925" y2="3.89255" layer="21"/>
<rectangle x1="2.29235" y1="3.89255" x2="3.95605" y2="3.90525" layer="21"/>
<rectangle x1="4.59105" y1="3.89255" x2="5.28955" y2="3.90525" layer="21"/>
<rectangle x1="7.28345" y1="3.89255" x2="7.93115" y2="3.90525" layer="21"/>
<rectangle x1="8.47725" y1="3.89255" x2="9.22655" y2="3.90525" layer="21"/>
<rectangle x1="2.30505" y1="3.90525" x2="3.95605" y2="3.91795" layer="21"/>
<rectangle x1="4.59105" y1="3.90525" x2="5.30225" y2="3.91795" layer="21"/>
<rectangle x1="7.23265" y1="3.90525" x2="7.93115" y2="3.91795" layer="21"/>
<rectangle x1="8.47725" y1="3.90525" x2="9.22655" y2="3.91795" layer="21"/>
<rectangle x1="2.31775" y1="3.91795" x2="3.95605" y2="3.93065" layer="21"/>
<rectangle x1="4.60375" y1="3.91795" x2="5.30225" y2="3.93065" layer="21"/>
<rectangle x1="7.19455" y1="3.91795" x2="7.93115" y2="3.93065" layer="21"/>
<rectangle x1="8.47725" y1="3.91795" x2="9.22655" y2="3.93065" layer="21"/>
<rectangle x1="2.33045" y1="3.93065" x2="3.95605" y2="3.94335" layer="21"/>
<rectangle x1="4.60375" y1="3.93065" x2="5.31495" y2="3.94335" layer="21"/>
<rectangle x1="7.15645" y1="3.93065" x2="7.93115" y2="3.94335" layer="21"/>
<rectangle x1="8.47725" y1="3.93065" x2="9.22655" y2="3.94335" layer="21"/>
<rectangle x1="2.34315" y1="3.94335" x2="3.95605" y2="3.95605" layer="21"/>
<rectangle x1="4.60375" y1="3.94335" x2="5.31495" y2="3.95605" layer="21"/>
<rectangle x1="7.11835" y1="3.94335" x2="7.93115" y2="3.95605" layer="21"/>
<rectangle x1="8.47725" y1="3.94335" x2="9.22655" y2="3.95605" layer="21"/>
<rectangle x1="2.35585" y1="3.95605" x2="3.95605" y2="3.96875" layer="21"/>
<rectangle x1="4.60375" y1="3.95605" x2="5.32765" y2="3.96875" layer="21"/>
<rectangle x1="7.06755" y1="3.95605" x2="7.91845" y2="3.96875" layer="21"/>
<rectangle x1="8.47725" y1="3.95605" x2="9.22655" y2="3.96875" layer="21"/>
<rectangle x1="2.36855" y1="3.96875" x2="3.95605" y2="3.98145" layer="21"/>
<rectangle x1="4.61645" y1="3.96875" x2="5.32765" y2="3.98145" layer="21"/>
<rectangle x1="7.02945" y1="3.96875" x2="7.91845" y2="3.98145" layer="21"/>
<rectangle x1="8.47725" y1="3.96875" x2="9.22655" y2="3.98145" layer="21"/>
<rectangle x1="2.38125" y1="3.98145" x2="3.95605" y2="3.99415" layer="21"/>
<rectangle x1="4.61645" y1="3.98145" x2="5.34035" y2="3.99415" layer="21"/>
<rectangle x1="6.99135" y1="3.98145" x2="7.91845" y2="3.99415" layer="21"/>
<rectangle x1="8.47725" y1="3.98145" x2="9.22655" y2="3.99415" layer="21"/>
<rectangle x1="2.39395" y1="3.99415" x2="3.95605" y2="4.00685" layer="21"/>
<rectangle x1="4.61645" y1="3.99415" x2="5.34035" y2="4.00685" layer="21"/>
<rectangle x1="6.95325" y1="3.99415" x2="7.91845" y2="4.00685" layer="21"/>
<rectangle x1="8.47725" y1="3.99415" x2="9.21385" y2="4.00685" layer="21"/>
<rectangle x1="2.40665" y1="4.00685" x2="3.95605" y2="4.01955" layer="21"/>
<rectangle x1="4.62915" y1="4.00685" x2="5.35305" y2="4.01955" layer="21"/>
<rectangle x1="6.90245" y1="4.00685" x2="7.90575" y2="4.01955" layer="21"/>
<rectangle x1="8.47725" y1="4.00685" x2="9.21385" y2="4.01955" layer="21"/>
<rectangle x1="2.41935" y1="4.01955" x2="3.95605" y2="4.03225" layer="21"/>
<rectangle x1="4.62915" y1="4.01955" x2="5.35305" y2="4.03225" layer="21"/>
<rectangle x1="6.86435" y1="4.01955" x2="7.90575" y2="4.03225" layer="21"/>
<rectangle x1="8.47725" y1="4.01955" x2="9.21385" y2="4.03225" layer="21"/>
<rectangle x1="2.43205" y1="4.03225" x2="3.95605" y2="4.04495" layer="21"/>
<rectangle x1="4.62915" y1="4.03225" x2="5.36575" y2="4.04495" layer="21"/>
<rectangle x1="6.83895" y1="4.03225" x2="7.90575" y2="4.04495" layer="21"/>
<rectangle x1="8.47725" y1="4.03225" x2="9.21385" y2="4.04495" layer="21"/>
<rectangle x1="2.44475" y1="4.04495" x2="3.95605" y2="4.05765" layer="21"/>
<rectangle x1="4.64185" y1="4.04495" x2="5.36575" y2="4.05765" layer="21"/>
<rectangle x1="6.80085" y1="4.04495" x2="7.90575" y2="4.05765" layer="21"/>
<rectangle x1="8.47725" y1="4.04495" x2="9.21385" y2="4.05765" layer="21"/>
<rectangle x1="2.45745" y1="4.05765" x2="3.95605" y2="4.07035" layer="21"/>
<rectangle x1="4.64185" y1="4.05765" x2="5.37845" y2="4.07035" layer="21"/>
<rectangle x1="6.75005" y1="4.05765" x2="7.90575" y2="4.07035" layer="21"/>
<rectangle x1="8.47725" y1="4.05765" x2="9.21385" y2="4.07035" layer="21"/>
<rectangle x1="2.47015" y1="4.07035" x2="3.95605" y2="4.08305" layer="21"/>
<rectangle x1="4.64185" y1="4.07035" x2="5.39115" y2="4.08305" layer="21"/>
<rectangle x1="6.71195" y1="4.07035" x2="7.89305" y2="4.08305" layer="21"/>
<rectangle x1="8.47725" y1="4.07035" x2="9.21385" y2="4.08305" layer="21"/>
<rectangle x1="2.48285" y1="4.08305" x2="3.95605" y2="4.09575" layer="21"/>
<rectangle x1="4.65455" y1="4.08305" x2="5.39115" y2="4.09575" layer="21"/>
<rectangle x1="6.69925" y1="4.08305" x2="7.89305" y2="4.09575" layer="21"/>
<rectangle x1="8.47725" y1="4.08305" x2="9.21385" y2="4.09575" layer="21"/>
<rectangle x1="2.49555" y1="4.09575" x2="3.95605" y2="4.10845" layer="21"/>
<rectangle x1="4.65455" y1="4.09575" x2="5.40385" y2="4.10845" layer="21"/>
<rectangle x1="6.71195" y1="4.09575" x2="7.89305" y2="4.10845" layer="21"/>
<rectangle x1="8.47725" y1="4.09575" x2="9.21385" y2="4.10845" layer="21"/>
<rectangle x1="2.50825" y1="4.10845" x2="3.95605" y2="4.12115" layer="21"/>
<rectangle x1="4.65455" y1="4.10845" x2="5.41655" y2="4.12115" layer="21"/>
<rectangle x1="6.72465" y1="4.10845" x2="7.89305" y2="4.12115" layer="21"/>
<rectangle x1="8.47725" y1="4.10845" x2="9.21385" y2="4.12115" layer="21"/>
<rectangle x1="2.52095" y1="4.12115" x2="3.95605" y2="4.13385" layer="21"/>
<rectangle x1="4.66725" y1="4.12115" x2="5.42925" y2="4.13385" layer="21"/>
<rectangle x1="6.73735" y1="4.12115" x2="7.88035" y2="4.13385" layer="21"/>
<rectangle x1="8.47725" y1="4.12115" x2="9.21385" y2="4.13385" layer="21"/>
<rectangle x1="2.53365" y1="4.13385" x2="3.95605" y2="4.14655" layer="21"/>
<rectangle x1="4.66725" y1="4.13385" x2="5.42925" y2="4.14655" layer="21"/>
<rectangle x1="6.75005" y1="4.13385" x2="7.88035" y2="4.14655" layer="21"/>
<rectangle x1="8.47725" y1="4.13385" x2="9.21385" y2="4.14655" layer="21"/>
<rectangle x1="2.54635" y1="4.14655" x2="3.95605" y2="4.15925" layer="21"/>
<rectangle x1="4.67995" y1="4.14655" x2="5.44195" y2="4.15925" layer="21"/>
<rectangle x1="6.76275" y1="4.14655" x2="7.88035" y2="4.15925" layer="21"/>
<rectangle x1="8.47725" y1="4.14655" x2="9.21385" y2="4.15925" layer="21"/>
<rectangle x1="2.55905" y1="4.15925" x2="3.95605" y2="4.17195" layer="21"/>
<rectangle x1="4.67995" y1="4.15925" x2="5.45465" y2="4.17195" layer="21"/>
<rectangle x1="6.77545" y1="4.15925" x2="7.88035" y2="4.17195" layer="21"/>
<rectangle x1="8.47725" y1="4.15925" x2="9.21385" y2="4.17195" layer="21"/>
<rectangle x1="2.57175" y1="4.17195" x2="3.95605" y2="4.18465" layer="21"/>
<rectangle x1="4.67995" y1="4.17195" x2="5.46735" y2="4.18465" layer="21"/>
<rectangle x1="6.78815" y1="4.17195" x2="7.86765" y2="4.18465" layer="21"/>
<rectangle x1="8.47725" y1="4.17195" x2="9.21385" y2="4.18465" layer="21"/>
<rectangle x1="2.58445" y1="4.18465" x2="3.95605" y2="4.19735" layer="21"/>
<rectangle x1="4.69265" y1="4.18465" x2="5.48005" y2="4.19735" layer="21"/>
<rectangle x1="6.81355" y1="4.18465" x2="7.86765" y2="4.19735" layer="21"/>
<rectangle x1="8.47725" y1="4.18465" x2="9.21385" y2="4.19735" layer="21"/>
<rectangle x1="2.59715" y1="4.19735" x2="3.95605" y2="4.21005" layer="21"/>
<rectangle x1="4.69265" y1="4.19735" x2="5.49275" y2="4.21005" layer="21"/>
<rectangle x1="6.82625" y1="4.19735" x2="7.86765" y2="4.21005" layer="21"/>
<rectangle x1="8.47725" y1="4.19735" x2="9.21385" y2="4.21005" layer="21"/>
<rectangle x1="2.60985" y1="4.21005" x2="3.95605" y2="4.22275" layer="21"/>
<rectangle x1="4.70535" y1="4.21005" x2="5.49275" y2="4.22275" layer="21"/>
<rectangle x1="6.83895" y1="4.21005" x2="7.86765" y2="4.22275" layer="21"/>
<rectangle x1="8.47725" y1="4.21005" x2="9.21385" y2="4.22275" layer="21"/>
<rectangle x1="2.62255" y1="4.22275" x2="3.95605" y2="4.23545" layer="21"/>
<rectangle x1="4.70535" y1="4.22275" x2="5.50545" y2="4.23545" layer="21"/>
<rectangle x1="6.85165" y1="4.22275" x2="7.86765" y2="4.23545" layer="21"/>
<rectangle x1="8.47725" y1="4.22275" x2="9.21385" y2="4.23545" layer="21"/>
<rectangle x1="2.63525" y1="4.23545" x2="3.95605" y2="4.24815" layer="21"/>
<rectangle x1="4.71805" y1="4.23545" x2="5.51815" y2="4.24815" layer="21"/>
<rectangle x1="6.86435" y1="4.23545" x2="7.85495" y2="4.24815" layer="21"/>
<rectangle x1="8.47725" y1="4.23545" x2="9.21385" y2="4.24815" layer="21"/>
<rectangle x1="2.64795" y1="4.24815" x2="3.95605" y2="4.26085" layer="21"/>
<rectangle x1="4.71805" y1="4.24815" x2="5.53085" y2="4.26085" layer="21"/>
<rectangle x1="6.87705" y1="4.24815" x2="7.85495" y2="4.26085" layer="21"/>
<rectangle x1="8.47725" y1="4.24815" x2="9.21385" y2="4.26085" layer="21"/>
<rectangle x1="2.66065" y1="4.26085" x2="3.95605" y2="4.27355" layer="21"/>
<rectangle x1="4.73075" y1="4.26085" x2="5.54355" y2="4.27355" layer="21"/>
<rectangle x1="6.88975" y1="4.26085" x2="7.85495" y2="4.27355" layer="21"/>
<rectangle x1="8.47725" y1="4.26085" x2="9.21385" y2="4.27355" layer="21"/>
<rectangle x1="2.67335" y1="4.27355" x2="3.95605" y2="4.28625" layer="21"/>
<rectangle x1="4.73075" y1="4.27355" x2="5.55625" y2="4.28625" layer="21"/>
<rectangle x1="6.90245" y1="4.27355" x2="7.85495" y2="4.28625" layer="21"/>
<rectangle x1="8.47725" y1="4.27355" x2="9.21385" y2="4.28625" layer="21"/>
<rectangle x1="2.68605" y1="4.28625" x2="3.95605" y2="4.29895" layer="21"/>
<rectangle x1="4.74345" y1="4.28625" x2="5.58165" y2="4.29895" layer="21"/>
<rectangle x1="6.91515" y1="4.28625" x2="7.84225" y2="4.29895" layer="21"/>
<rectangle x1="8.47725" y1="4.28625" x2="9.22655" y2="4.29895" layer="21"/>
<rectangle x1="2.69875" y1="4.29895" x2="3.95605" y2="4.31165" layer="21"/>
<rectangle x1="4.74345" y1="4.29895" x2="5.59435" y2="4.31165" layer="21"/>
<rectangle x1="6.92785" y1="4.29895" x2="7.84225" y2="4.31165" layer="21"/>
<rectangle x1="8.47725" y1="4.29895" x2="9.22655" y2="4.31165" layer="21"/>
<rectangle x1="2.71145" y1="4.31165" x2="3.95605" y2="4.32435" layer="21"/>
<rectangle x1="4.75615" y1="4.31165" x2="5.60705" y2="4.32435" layer="21"/>
<rectangle x1="6.94055" y1="4.31165" x2="7.84225" y2="4.32435" layer="21"/>
<rectangle x1="8.47725" y1="4.31165" x2="9.22655" y2="4.32435" layer="21"/>
<rectangle x1="2.72415" y1="4.32435" x2="3.95605" y2="4.33705" layer="21"/>
<rectangle x1="4.75615" y1="4.32435" x2="5.61975" y2="4.33705" layer="21"/>
<rectangle x1="6.95325" y1="4.32435" x2="7.84225" y2="4.33705" layer="21"/>
<rectangle x1="8.47725" y1="4.32435" x2="9.22655" y2="4.33705" layer="21"/>
<rectangle x1="2.73685" y1="4.33705" x2="3.95605" y2="4.34975" layer="21"/>
<rectangle x1="4.76885" y1="4.33705" x2="5.63245" y2="4.34975" layer="21"/>
<rectangle x1="6.95325" y1="4.33705" x2="7.84225" y2="4.34975" layer="21"/>
<rectangle x1="8.47725" y1="4.33705" x2="9.22655" y2="4.34975" layer="21"/>
<rectangle x1="2.74955" y1="4.34975" x2="3.95605" y2="4.36245" layer="21"/>
<rectangle x1="4.78155" y1="4.34975" x2="5.65785" y2="4.36245" layer="21"/>
<rectangle x1="6.92785" y1="4.34975" x2="7.82955" y2="4.36245" layer="21"/>
<rectangle x1="8.47725" y1="4.34975" x2="9.22655" y2="4.36245" layer="21"/>
<rectangle x1="2.76225" y1="4.36245" x2="3.95605" y2="4.37515" layer="21"/>
<rectangle x1="4.78155" y1="4.36245" x2="5.67055" y2="4.37515" layer="21"/>
<rectangle x1="6.91515" y1="4.36245" x2="7.82955" y2="4.37515" layer="21"/>
<rectangle x1="8.47725" y1="4.36245" x2="9.22655" y2="4.37515" layer="21"/>
<rectangle x1="2.77495" y1="4.37515" x2="3.95605" y2="4.38785" layer="21"/>
<rectangle x1="4.79425" y1="4.37515" x2="5.69595" y2="4.38785" layer="21"/>
<rectangle x1="6.90245" y1="4.37515" x2="7.82955" y2="4.38785" layer="21"/>
<rectangle x1="8.47725" y1="4.37515" x2="9.22655" y2="4.38785" layer="21"/>
<rectangle x1="2.78765" y1="4.38785" x2="3.95605" y2="4.40055" layer="21"/>
<rectangle x1="4.79425" y1="4.38785" x2="5.70865" y2="4.40055" layer="21"/>
<rectangle x1="6.87705" y1="4.38785" x2="7.82955" y2="4.40055" layer="21"/>
<rectangle x1="8.47725" y1="4.38785" x2="9.22655" y2="4.40055" layer="21"/>
<rectangle x1="2.80035" y1="4.40055" x2="3.95605" y2="4.41325" layer="21"/>
<rectangle x1="4.80695" y1="4.40055" x2="5.73405" y2="4.41325" layer="21"/>
<rectangle x1="6.86435" y1="4.40055" x2="7.81685" y2="4.41325" layer="21"/>
<rectangle x1="8.47725" y1="4.40055" x2="9.23925" y2="4.41325" layer="21"/>
<rectangle x1="2.81305" y1="4.41325" x2="3.95605" y2="4.42595" layer="21"/>
<rectangle x1="4.81965" y1="4.41325" x2="5.74675" y2="4.42595" layer="21"/>
<rectangle x1="6.83895" y1="4.41325" x2="7.81685" y2="4.42595" layer="21"/>
<rectangle x1="8.47725" y1="4.41325" x2="9.23925" y2="4.42595" layer="21"/>
<rectangle x1="2.82575" y1="4.42595" x2="3.95605" y2="4.43865" layer="21"/>
<rectangle x1="4.81965" y1="4.42595" x2="5.77215" y2="4.43865" layer="21"/>
<rectangle x1="6.81355" y1="4.42595" x2="7.81685" y2="4.43865" layer="21"/>
<rectangle x1="8.47725" y1="4.42595" x2="9.23925" y2="4.43865" layer="21"/>
<rectangle x1="2.83845" y1="4.43865" x2="3.95605" y2="4.45135" layer="21"/>
<rectangle x1="4.83235" y1="4.43865" x2="5.79755" y2="4.45135" layer="21"/>
<rectangle x1="6.78815" y1="4.43865" x2="7.81685" y2="4.45135" layer="21"/>
<rectangle x1="8.47725" y1="4.43865" x2="9.23925" y2="4.45135" layer="21"/>
<rectangle x1="2.85115" y1="4.45135" x2="3.95605" y2="4.46405" layer="21"/>
<rectangle x1="4.84505" y1="4.45135" x2="5.82295" y2="4.46405" layer="21"/>
<rectangle x1="6.76275" y1="4.45135" x2="7.80415" y2="4.46405" layer="21"/>
<rectangle x1="8.47725" y1="4.45135" x2="9.23925" y2="4.46405" layer="21"/>
<rectangle x1="2.86385" y1="4.46405" x2="3.95605" y2="4.47675" layer="21"/>
<rectangle x1="4.84505" y1="4.46405" x2="5.84835" y2="4.47675" layer="21"/>
<rectangle x1="6.73735" y1="4.46405" x2="7.80415" y2="4.47675" layer="21"/>
<rectangle x1="8.47725" y1="4.46405" x2="9.25195" y2="4.47675" layer="21"/>
<rectangle x1="2.87655" y1="4.47675" x2="3.95605" y2="4.48945" layer="21"/>
<rectangle x1="4.85775" y1="4.47675" x2="5.88645" y2="4.48945" layer="21"/>
<rectangle x1="6.71195" y1="4.47675" x2="7.80415" y2="4.48945" layer="21"/>
<rectangle x1="8.47725" y1="4.47675" x2="9.25195" y2="4.48945" layer="21"/>
<rectangle x1="2.88925" y1="4.48945" x2="3.95605" y2="4.50215" layer="21"/>
<rectangle x1="4.87045" y1="4.48945" x2="5.91185" y2="4.50215" layer="21"/>
<rectangle x1="6.67385" y1="4.48945" x2="7.80415" y2="4.50215" layer="21"/>
<rectangle x1="8.47725" y1="4.48945" x2="9.25195" y2="4.50215" layer="21"/>
<rectangle x1="2.90195" y1="4.50215" x2="3.95605" y2="4.51485" layer="21"/>
<rectangle x1="4.87045" y1="4.50215" x2="5.94995" y2="4.51485" layer="21"/>
<rectangle x1="6.63575" y1="4.50215" x2="7.80415" y2="4.51485" layer="21"/>
<rectangle x1="8.47725" y1="4.50215" x2="9.25195" y2="4.51485" layer="21"/>
<rectangle x1="2.91465" y1="4.51485" x2="3.95605" y2="4.52755" layer="21"/>
<rectangle x1="4.88315" y1="4.51485" x2="5.98805" y2="4.52755" layer="21"/>
<rectangle x1="6.59765" y1="4.51485" x2="7.79145" y2="4.52755" layer="21"/>
<rectangle x1="8.47725" y1="4.51485" x2="9.25195" y2="4.52755" layer="21"/>
<rectangle x1="2.92735" y1="4.52755" x2="3.95605" y2="4.54025" layer="21"/>
<rectangle x1="4.89585" y1="4.52755" x2="6.03885" y2="4.54025" layer="21"/>
<rectangle x1="6.55955" y1="4.52755" x2="7.79145" y2="4.54025" layer="21"/>
<rectangle x1="8.47725" y1="4.52755" x2="9.26465" y2="4.54025" layer="21"/>
<rectangle x1="2.94005" y1="4.54025" x2="3.95605" y2="4.55295" layer="21"/>
<rectangle x1="4.90855" y1="4.54025" x2="6.08965" y2="4.55295" layer="21"/>
<rectangle x1="6.49605" y1="4.54025" x2="7.79145" y2="4.55295" layer="21"/>
<rectangle x1="8.47725" y1="4.54025" x2="9.26465" y2="4.55295" layer="21"/>
<rectangle x1="2.95275" y1="4.55295" x2="3.95605" y2="4.56565" layer="21"/>
<rectangle x1="4.90855" y1="4.55295" x2="6.17855" y2="4.56565" layer="21"/>
<rectangle x1="6.40715" y1="4.55295" x2="7.79145" y2="4.56565" layer="21"/>
<rectangle x1="8.47725" y1="4.55295" x2="9.26465" y2="4.56565" layer="21"/>
<rectangle x1="2.96545" y1="4.56565" x2="3.95605" y2="4.57835" layer="21"/>
<rectangle x1="4.92125" y1="4.56565" x2="7.77875" y2="4.57835" layer="21"/>
<rectangle x1="8.47725" y1="4.56565" x2="9.26465" y2="4.57835" layer="21"/>
<rectangle x1="2.97815" y1="4.57835" x2="3.95605" y2="4.59105" layer="21"/>
<rectangle x1="4.93395" y1="4.57835" x2="7.77875" y2="4.59105" layer="21"/>
<rectangle x1="8.47725" y1="4.57835" x2="9.27735" y2="4.59105" layer="21"/>
<rectangle x1="2.99085" y1="4.59105" x2="3.95605" y2="4.60375" layer="21"/>
<rectangle x1="4.94665" y1="4.59105" x2="7.77875" y2="4.60375" layer="21"/>
<rectangle x1="8.47725" y1="4.59105" x2="9.27735" y2="4.60375" layer="21"/>
<rectangle x1="3.00355" y1="4.60375" x2="3.95605" y2="4.61645" layer="21"/>
<rectangle x1="4.95935" y1="4.60375" x2="7.77875" y2="4.61645" layer="21"/>
<rectangle x1="8.47725" y1="4.60375" x2="9.27735" y2="4.61645" layer="21"/>
<rectangle x1="3.01625" y1="4.61645" x2="3.95605" y2="4.62915" layer="21"/>
<rectangle x1="4.95935" y1="4.61645" x2="7.77875" y2="4.62915" layer="21"/>
<rectangle x1="8.47725" y1="4.61645" x2="9.29005" y2="4.62915" layer="21"/>
<rectangle x1="3.02895" y1="4.62915" x2="3.95605" y2="4.64185" layer="21"/>
<rectangle x1="4.97205" y1="4.62915" x2="7.76605" y2="4.64185" layer="21"/>
<rectangle x1="8.47725" y1="4.62915" x2="9.29005" y2="4.64185" layer="21"/>
<rectangle x1="3.04165" y1="4.64185" x2="3.95605" y2="4.65455" layer="21"/>
<rectangle x1="4.98475" y1="4.64185" x2="7.76605" y2="4.65455" layer="21"/>
<rectangle x1="8.47725" y1="4.64185" x2="9.29005" y2="4.65455" layer="21"/>
<rectangle x1="3.05435" y1="4.65455" x2="3.95605" y2="4.66725" layer="21"/>
<rectangle x1="4.99745" y1="4.65455" x2="7.76605" y2="4.66725" layer="21"/>
<rectangle x1="8.47725" y1="4.65455" x2="9.30275" y2="4.66725" layer="21"/>
<rectangle x1="3.06705" y1="4.66725" x2="3.95605" y2="4.67995" layer="21"/>
<rectangle x1="5.01015" y1="4.66725" x2="7.76605" y2="4.67995" layer="21"/>
<rectangle x1="8.47725" y1="4.66725" x2="9.30275" y2="4.67995" layer="21"/>
<rectangle x1="3.07975" y1="4.67995" x2="3.95605" y2="4.69265" layer="21"/>
<rectangle x1="5.02285" y1="4.67995" x2="7.75335" y2="4.69265" layer="21"/>
<rectangle x1="8.47725" y1="4.67995" x2="9.30275" y2="4.69265" layer="21"/>
<rectangle x1="3.09245" y1="4.69265" x2="3.95605" y2="4.70535" layer="21"/>
<rectangle x1="5.03555" y1="4.69265" x2="7.75335" y2="4.70535" layer="21"/>
<rectangle x1="8.47725" y1="4.69265" x2="9.31545" y2="4.70535" layer="21"/>
<rectangle x1="3.10515" y1="4.70535" x2="3.95605" y2="4.71805" layer="21"/>
<rectangle x1="5.04825" y1="4.70535" x2="7.75335" y2="4.71805" layer="21"/>
<rectangle x1="8.47725" y1="4.70535" x2="9.31545" y2="4.71805" layer="21"/>
<rectangle x1="3.11785" y1="4.71805" x2="3.95605" y2="4.73075" layer="21"/>
<rectangle x1="5.06095" y1="4.71805" x2="7.75335" y2="4.73075" layer="21"/>
<rectangle x1="8.47725" y1="4.71805" x2="9.31545" y2="4.73075" layer="21"/>
<rectangle x1="3.13055" y1="4.73075" x2="3.95605" y2="4.74345" layer="21"/>
<rectangle x1="5.07365" y1="4.73075" x2="7.74065" y2="4.74345" layer="21"/>
<rectangle x1="8.47725" y1="4.73075" x2="9.31545" y2="4.74345" layer="21"/>
<rectangle x1="3.14325" y1="4.74345" x2="3.95605" y2="4.75615" layer="21"/>
<rectangle x1="5.08635" y1="4.74345" x2="7.74065" y2="4.75615" layer="21"/>
<rectangle x1="8.47725" y1="4.74345" x2="9.31545" y2="4.75615" layer="21"/>
<rectangle x1="3.15595" y1="4.75615" x2="3.95605" y2="4.76885" layer="21"/>
<rectangle x1="5.09905" y1="4.75615" x2="7.74065" y2="4.76885" layer="21"/>
<rectangle x1="8.47725" y1="4.75615" x2="9.30275" y2="4.76885" layer="21"/>
<rectangle x1="3.16865" y1="4.76885" x2="3.95605" y2="4.78155" layer="21"/>
<rectangle x1="5.11175" y1="4.76885" x2="7.74065" y2="4.78155" layer="21"/>
<rectangle x1="8.47725" y1="4.76885" x2="9.29005" y2="4.78155" layer="21"/>
<rectangle x1="3.18135" y1="4.78155" x2="3.95605" y2="4.79425" layer="21"/>
<rectangle x1="5.12445" y1="4.78155" x2="7.74065" y2="4.79425" layer="21"/>
<rectangle x1="8.47725" y1="4.78155" x2="9.27735" y2="4.79425" layer="21"/>
<rectangle x1="3.19405" y1="4.79425" x2="3.95605" y2="4.80695" layer="21"/>
<rectangle x1="5.13715" y1="4.79425" x2="7.44855" y2="4.80695" layer="21"/>
<rectangle x1="7.47395" y1="4.79425" x2="7.72795" y2="4.80695" layer="21"/>
<rectangle x1="8.47725" y1="4.79425" x2="9.26465" y2="4.80695" layer="21"/>
<rectangle x1="3.20675" y1="4.80695" x2="3.95605" y2="4.81965" layer="21"/>
<rectangle x1="5.16255" y1="4.80695" x2="7.43585" y2="4.81965" layer="21"/>
<rectangle x1="7.48665" y1="4.80695" x2="7.72795" y2="4.81965" layer="21"/>
<rectangle x1="8.47725" y1="4.80695" x2="9.25195" y2="4.81965" layer="21"/>
<rectangle x1="3.21945" y1="4.81965" x2="3.95605" y2="4.83235" layer="21"/>
<rectangle x1="5.17525" y1="4.81965" x2="7.41045" y2="4.83235" layer="21"/>
<rectangle x1="7.49935" y1="4.81965" x2="7.72795" y2="4.83235" layer="21"/>
<rectangle x1="8.47725" y1="4.81965" x2="9.23925" y2="4.83235" layer="21"/>
<rectangle x1="3.23215" y1="4.83235" x2="3.95605" y2="4.84505" layer="21"/>
<rectangle x1="5.18795" y1="4.83235" x2="7.39775" y2="4.84505" layer="21"/>
<rectangle x1="7.51205" y1="4.83235" x2="7.72795" y2="4.84505" layer="21"/>
<rectangle x1="8.47725" y1="4.83235" x2="9.22655" y2="4.84505" layer="21"/>
<rectangle x1="3.24485" y1="4.84505" x2="3.95605" y2="4.85775" layer="21"/>
<rectangle x1="5.20065" y1="4.84505" x2="7.38505" y2="4.85775" layer="21"/>
<rectangle x1="7.52475" y1="4.84505" x2="7.71525" y2="4.85775" layer="21"/>
<rectangle x1="8.47725" y1="4.84505" x2="9.21385" y2="4.85775" layer="21"/>
<rectangle x1="3.25755" y1="4.85775" x2="3.95605" y2="4.87045" layer="21"/>
<rectangle x1="5.21335" y1="4.85775" x2="7.37235" y2="4.87045" layer="21"/>
<rectangle x1="7.53745" y1="4.85775" x2="7.71525" y2="4.87045" layer="21"/>
<rectangle x1="8.47725" y1="4.85775" x2="9.20115" y2="4.87045" layer="21"/>
<rectangle x1="3.27025" y1="4.87045" x2="3.95605" y2="4.88315" layer="21"/>
<rectangle x1="5.23875" y1="4.87045" x2="7.34695" y2="4.88315" layer="21"/>
<rectangle x1="7.55015" y1="4.87045" x2="7.71525" y2="4.88315" layer="21"/>
<rectangle x1="8.47725" y1="4.87045" x2="9.18845" y2="4.88315" layer="21"/>
<rectangle x1="3.28295" y1="4.88315" x2="3.95605" y2="4.89585" layer="21"/>
<rectangle x1="5.25145" y1="4.88315" x2="7.33425" y2="4.89585" layer="21"/>
<rectangle x1="7.56285" y1="4.88315" x2="7.71525" y2="4.89585" layer="21"/>
<rectangle x1="8.47725" y1="4.88315" x2="9.17575" y2="4.89585" layer="21"/>
<rectangle x1="3.29565" y1="4.89585" x2="3.95605" y2="4.90855" layer="21"/>
<rectangle x1="5.26415" y1="4.89585" x2="7.32155" y2="4.90855" layer="21"/>
<rectangle x1="7.57555" y1="4.89585" x2="7.71525" y2="4.90855" layer="21"/>
<rectangle x1="8.47725" y1="4.89585" x2="9.16305" y2="4.90855" layer="21"/>
<rectangle x1="3.30835" y1="4.90855" x2="3.95605" y2="4.92125" layer="21"/>
<rectangle x1="5.28955" y1="4.90855" x2="7.29615" y2="4.92125" layer="21"/>
<rectangle x1="7.58825" y1="4.90855" x2="7.70255" y2="4.92125" layer="21"/>
<rectangle x1="8.47725" y1="4.90855" x2="9.15035" y2="4.92125" layer="21"/>
<rectangle x1="3.32105" y1="4.92125" x2="3.95605" y2="4.93395" layer="21"/>
<rectangle x1="5.30225" y1="4.92125" x2="7.28345" y2="4.93395" layer="21"/>
<rectangle x1="7.60095" y1="4.92125" x2="7.70255" y2="4.93395" layer="21"/>
<rectangle x1="8.47725" y1="4.92125" x2="9.13765" y2="4.93395" layer="21"/>
<rectangle x1="3.33375" y1="4.93395" x2="3.95605" y2="4.94665" layer="21"/>
<rectangle x1="5.32765" y1="4.93395" x2="7.25805" y2="4.94665" layer="21"/>
<rectangle x1="7.62635" y1="4.93395" x2="7.70255" y2="4.94665" layer="21"/>
<rectangle x1="8.47725" y1="4.93395" x2="9.12495" y2="4.94665" layer="21"/>
<rectangle x1="3.34645" y1="4.94665" x2="3.95605" y2="4.95935" layer="21"/>
<rectangle x1="5.34035" y1="4.94665" x2="7.24535" y2="4.95935" layer="21"/>
<rectangle x1="7.63905" y1="4.94665" x2="7.70255" y2="4.95935" layer="21"/>
<rectangle x1="8.47725" y1="4.94665" x2="9.11225" y2="4.95935" layer="21"/>
<rectangle x1="3.35915" y1="4.95935" x2="3.95605" y2="4.97205" layer="21"/>
<rectangle x1="5.36575" y1="4.95935" x2="7.21995" y2="4.97205" layer="21"/>
<rectangle x1="7.65175" y1="4.95935" x2="7.68985" y2="4.97205" layer="21"/>
<rectangle x1="8.47725" y1="4.95935" x2="9.09955" y2="4.97205" layer="21"/>
<rectangle x1="3.37185" y1="4.97205" x2="3.95605" y2="4.98475" layer="21"/>
<rectangle x1="5.39115" y1="4.97205" x2="7.20725" y2="4.98475" layer="21"/>
<rectangle x1="7.66445" y1="4.97205" x2="7.68985" y2="4.98475" layer="21"/>
<rectangle x1="8.47725" y1="4.97205" x2="9.08685" y2="4.98475" layer="21"/>
<rectangle x1="3.38455" y1="4.98475" x2="3.95605" y2="4.99745" layer="21"/>
<rectangle x1="5.40385" y1="4.98475" x2="7.18185" y2="4.99745" layer="21"/>
<rectangle x1="7.67715" y1="4.98475" x2="7.68985" y2="4.99745" layer="21"/>
<rectangle x1="8.47725" y1="4.98475" x2="9.07415" y2="4.99745" layer="21"/>
<rectangle x1="3.39725" y1="4.99745" x2="3.95605" y2="5.01015" layer="21"/>
<rectangle x1="5.42925" y1="4.99745" x2="7.15645" y2="5.01015" layer="21"/>
<rectangle x1="8.47725" y1="4.99745" x2="9.06145" y2="5.01015" layer="21"/>
<rectangle x1="3.40995" y1="5.01015" x2="3.95605" y2="5.02285" layer="21"/>
<rectangle x1="5.45465" y1="5.01015" x2="7.13105" y2="5.02285" layer="21"/>
<rectangle x1="8.47725" y1="5.01015" x2="9.04875" y2="5.02285" layer="21"/>
<rectangle x1="3.42265" y1="5.02285" x2="3.95605" y2="5.03555" layer="21"/>
<rectangle x1="5.48005" y1="5.02285" x2="7.11835" y2="5.03555" layer="21"/>
<rectangle x1="8.47725" y1="5.02285" x2="9.03605" y2="5.03555" layer="21"/>
<rectangle x1="3.43535" y1="5.03555" x2="3.95605" y2="5.04825" layer="21"/>
<rectangle x1="5.49275" y1="5.03555" x2="7.09295" y2="5.04825" layer="21"/>
<rectangle x1="8.47725" y1="5.03555" x2="9.02335" y2="5.04825" layer="21"/>
<rectangle x1="3.44805" y1="5.04825" x2="3.95605" y2="5.06095" layer="21"/>
<rectangle x1="5.51815" y1="5.04825" x2="7.06755" y2="5.06095" layer="21"/>
<rectangle x1="8.47725" y1="5.04825" x2="9.01065" y2="5.06095" layer="21"/>
<rectangle x1="3.46075" y1="5.06095" x2="3.95605" y2="5.07365" layer="21"/>
<rectangle x1="5.55625" y1="5.06095" x2="7.04215" y2="5.07365" layer="21"/>
<rectangle x1="8.47725" y1="5.06095" x2="8.99795" y2="5.07365" layer="21"/>
<rectangle x1="3.47345" y1="5.07365" x2="3.95605" y2="5.08635" layer="21"/>
<rectangle x1="5.58165" y1="5.07365" x2="7.00405" y2="5.08635" layer="21"/>
<rectangle x1="8.47725" y1="5.07365" x2="8.98525" y2="5.08635" layer="21"/>
<rectangle x1="3.48615" y1="5.08635" x2="3.95605" y2="5.09905" layer="21"/>
<rectangle x1="5.60705" y1="5.08635" x2="6.97865" y2="5.09905" layer="21"/>
<rectangle x1="8.47725" y1="5.08635" x2="8.97255" y2="5.09905" layer="21"/>
<rectangle x1="3.49885" y1="5.09905" x2="3.95605" y2="5.11175" layer="21"/>
<rectangle x1="5.63245" y1="5.09905" x2="6.95325" y2="5.11175" layer="21"/>
<rectangle x1="8.47725" y1="5.09905" x2="8.95985" y2="5.11175" layer="21"/>
<rectangle x1="3.51155" y1="5.11175" x2="3.95605" y2="5.12445" layer="21"/>
<rectangle x1="5.67055" y1="5.11175" x2="6.91515" y2="5.12445" layer="21"/>
<rectangle x1="8.47725" y1="5.11175" x2="8.94715" y2="5.12445" layer="21"/>
<rectangle x1="3.52425" y1="5.12445" x2="3.95605" y2="5.13715" layer="21"/>
<rectangle x1="5.70865" y1="5.12445" x2="6.87705" y2="5.13715" layer="21"/>
<rectangle x1="8.47725" y1="5.12445" x2="8.93445" y2="5.13715" layer="21"/>
<rectangle x1="3.53695" y1="5.13715" x2="3.95605" y2="5.14985" layer="21"/>
<rectangle x1="5.74675" y1="5.13715" x2="6.85165" y2="5.14985" layer="21"/>
<rectangle x1="8.47725" y1="5.13715" x2="8.92175" y2="5.14985" layer="21"/>
<rectangle x1="3.54965" y1="5.14985" x2="3.94335" y2="5.16255" layer="21"/>
<rectangle x1="5.78485" y1="5.14985" x2="6.80085" y2="5.16255" layer="21"/>
<rectangle x1="8.47725" y1="5.14985" x2="8.90905" y2="5.16255" layer="21"/>
<rectangle x1="3.56235" y1="5.16255" x2="3.94335" y2="5.17525" layer="21"/>
<rectangle x1="5.82295" y1="5.16255" x2="6.76275" y2="5.17525" layer="21"/>
<rectangle x1="8.47725" y1="5.16255" x2="8.89635" y2="5.17525" layer="21"/>
<rectangle x1="3.57505" y1="5.17525" x2="3.94335" y2="5.18795" layer="21"/>
<rectangle x1="5.87375" y1="5.17525" x2="6.71195" y2="5.18795" layer="21"/>
<rectangle x1="8.47725" y1="5.17525" x2="8.88365" y2="5.18795" layer="21"/>
<rectangle x1="3.58775" y1="5.18795" x2="3.94335" y2="5.20065" layer="21"/>
<rectangle x1="5.93725" y1="5.18795" x2="6.66115" y2="5.20065" layer="21"/>
<rectangle x1="8.47725" y1="5.18795" x2="8.87095" y2="5.20065" layer="21"/>
<rectangle x1="3.60045" y1="5.20065" x2="3.94335" y2="5.21335" layer="21"/>
<rectangle x1="6.00075" y1="5.20065" x2="6.58495" y2="5.21335" layer="21"/>
<rectangle x1="8.47725" y1="5.20065" x2="8.85825" y2="5.21335" layer="21"/>
<rectangle x1="3.61315" y1="5.21335" x2="3.94335" y2="5.22605" layer="21"/>
<rectangle x1="6.08965" y1="5.21335" x2="6.49605" y2="5.22605" layer="21"/>
<rectangle x1="8.47725" y1="5.21335" x2="8.84555" y2="5.22605" layer="21"/>
<rectangle x1="3.62585" y1="5.22605" x2="3.94335" y2="5.23875" layer="21"/>
<rectangle x1="8.47725" y1="5.22605" x2="8.83285" y2="5.23875" layer="21"/>
<rectangle x1="3.63855" y1="5.23875" x2="3.94335" y2="5.25145" layer="21"/>
<rectangle x1="8.47725" y1="5.23875" x2="8.82015" y2="5.25145" layer="21"/>
<rectangle x1="3.65125" y1="5.25145" x2="3.94335" y2="5.26415" layer="21"/>
<rectangle x1="8.47725" y1="5.25145" x2="8.80745" y2="5.26415" layer="21"/>
<rectangle x1="3.66395" y1="5.26415" x2="3.93065" y2="5.27685" layer="21"/>
<rectangle x1="8.47725" y1="5.26415" x2="8.79475" y2="5.27685" layer="21"/>
<rectangle x1="3.67665" y1="5.27685" x2="3.93065" y2="5.28955" layer="21"/>
<rectangle x1="8.48995" y1="5.27685" x2="8.78205" y2="5.28955" layer="21"/>
<rectangle x1="3.68935" y1="5.28955" x2="3.91795" y2="5.30225" layer="21"/>
<rectangle x1="8.48995" y1="5.28955" x2="8.76935" y2="5.30225" layer="21"/>
<rectangle x1="3.70205" y1="5.30225" x2="3.91795" y2="5.31495" layer="21"/>
<rectangle x1="8.48995" y1="5.30225" x2="8.75665" y2="5.31495" layer="21"/>
<rectangle x1="3.71475" y1="5.31495" x2="3.90525" y2="5.32765" layer="21"/>
<rectangle x1="8.50265" y1="5.31495" x2="8.74395" y2="5.32765" layer="21"/>
<rectangle x1="3.74015" y1="5.32765" x2="3.90525" y2="5.34035" layer="21"/>
<rectangle x1="8.50265" y1="5.32765" x2="8.71855" y2="5.34035" layer="21"/>
<rectangle x1="3.76555" y1="5.34035" x2="3.87985" y2="5.35305" layer="21"/>
<rectangle x1="8.51535" y1="5.34035" x2="8.70585" y2="5.35305" layer="21"/>
<rectangle x1="3.80365" y1="5.35305" x2="3.85445" y2="5.36575" layer="21"/>
<rectangle x1="8.52805" y1="5.35305" x2="8.69315" y2="5.36575" layer="21"/>
<rectangle x1="8.54075" y1="5.36575" x2="8.66775" y2="5.37845" layer="21"/>
<rectangle x1="8.56615" y1="5.37845" x2="8.62965" y2="5.39115" layer="21"/>
<text x="0.635" y="-1.905" size="1.4224" layer="21" font="vector" ratio="18">acl.mit.edu</text>
</package>
<package name="ACL-LOGO-BOTTOM">
<rectangle x1="-2.94005" y1="0.03175" x2="-2.71145" y2="0.04445" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.04445" x2="-2.71145" y2="0.05715" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.05715" x2="-2.71145" y2="0.06985" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.06985" x2="-2.71145" y2="0.08255" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.08255" x2="-2.71145" y2="0.09525" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.09525" x2="-2.71145" y2="0.10795" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.10795" x2="-2.71145" y2="0.12065" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.12065" x2="-2.71145" y2="0.13335" layer="21" rot="R180"/>
<rectangle x1="-5.86105" y1="0.12065" x2="-5.78485" y2="0.13335" layer="21" rot="R180"/>
<rectangle x1="-0.70485" y1="0.13335" x2="-0.56515" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-1.80975" y1="0.13335" x2="-1.64465" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-2.44475" y1="0.13335" x2="-2.29235" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.13335" x2="-2.71145" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-3.15595" y1="0.13335" x2="-3.06705" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.13335" x2="-3.48615" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-4.34975" y1="0.13335" x2="-4.19735" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-4.99745" y1="0.13335" x2="-4.84505" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-5.92455" y1="0.13335" x2="-5.70865" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-6.57225" y1="0.13335" x2="-6.40715" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-8.73125" y1="0.13335" x2="-8.56615" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-9.72185" y1="0.13335" x2="-9.56945" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.13335" x2="-10.80135" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="-11.77925" y1="0.13335" x2="-11.69035" y2="0.14605" layer="21" rot="R180"/>
<rectangle x1="0.09525" y1="0.14605" x2="0.33655" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-0.29845" y1="0.14605" x2="-0.05715" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-0.75565" y1="0.14605" x2="-0.51435" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.14605" x2="-1.00965" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-1.86055" y1="0.14605" x2="-1.59385" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-2.49555" y1="0.14605" x2="-2.24155" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.14605" x2="-2.71145" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-3.19405" y1="0.14605" x2="-3.04165" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-3.60045" y1="0.14605" x2="-3.44805" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.14605" x2="-3.68935" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-4.40055" y1="0.14605" x2="-4.15925" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-5.04825" y1="0.14605" x2="-4.79425" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-5.96265" y1="0.14605" x2="-5.67055" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-6.62305" y1="0.14605" x2="-6.35635" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.14605" x2="-6.87705" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.14605" x2="-7.20725" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.14605" x2="-7.66445" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.14605" x2="-7.93115" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="0.14605" x2="-8.51535" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.14605" x2="-9.03605" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-9.77265" y1="0.14605" x2="-9.51865" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.14605" x2="-10.21715" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-10.92835" y1="0.14605" x2="-10.76325" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.14605" x2="-11.00455" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-11.55065" y1="0.14605" x2="-11.33475" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="-11.80465" y1="0.14605" x2="-11.65225" y2="0.15875" layer="21" rot="R180"/>
<rectangle x1="0.09525" y1="0.15875" x2="0.33655" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-0.29845" y1="0.15875" x2="-0.05715" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-0.78105" y1="0.15875" x2="-0.47625" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.15875" x2="-1.00965" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-1.88595" y1="0.15875" x2="-1.56845" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="0.15875" x2="-2.21615" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.15875" x2="-2.71145" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-3.20675" y1="0.15875" x2="-3.01625" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-3.62585" y1="0.15875" x2="-3.42265" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.15875" x2="-3.68935" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-4.42595" y1="0.15875" x2="-4.12115" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-5.07365" y1="0.15875" x2="-4.76885" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-5.98805" y1="0.15875" x2="-5.64515" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-6.64845" y1="0.15875" x2="-6.33095" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.15875" x2="-6.87705" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.15875" x2="-7.20725" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.15875" x2="-7.62635" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.15875" x2="-7.93115" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="0.15875" x2="-8.48995" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.15875" x2="-9.03605" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-9.79805" y1="0.15875" x2="-9.48055" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.15875" x2="-10.21715" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-10.94105" y1="0.15875" x2="-10.73785" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.15875" x2="-11.00455" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-11.55065" y1="0.15875" x2="-11.33475" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="-11.83005" y1="0.15875" x2="-11.62685" y2="0.17145" layer="21" rot="R180"/>
<rectangle x1="0.09525" y1="0.17145" x2="0.33655" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-0.28575" y1="0.17145" x2="-0.05715" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-0.80645" y1="0.17145" x2="-0.45085" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.17145" x2="-1.00965" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-1.91135" y1="0.17145" x2="-1.54305" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-2.54635" y1="0.17145" x2="-2.19075" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.17145" x2="-2.71145" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-3.23215" y1="0.17145" x2="-3.00355" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-3.63855" y1="0.17145" x2="-3.40995" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.17145" x2="-3.68935" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-4.45135" y1="0.17145" x2="-4.10845" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-5.09905" y1="0.17145" x2="-4.74345" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-6.01345" y1="0.17145" x2="-5.61975" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-6.67385" y1="0.17145" x2="-6.30555" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.17145" x2="-6.87705" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.17145" x2="-7.20725" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.17145" x2="-7.60095" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.17145" x2="-7.93115" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="0.17145" x2="-8.46455" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.17145" x2="-9.03605" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-9.82345" y1="0.17145" x2="-9.45515" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.17145" x2="-10.21715" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-10.95375" y1="0.17145" x2="-10.72515" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.17145" x2="-11.00455" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-11.55065" y1="0.17145" x2="-11.33475" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="-11.84275" y1="0.17145" x2="-11.61415" y2="0.18415" layer="21" rot="R180"/>
<rectangle x1="0.09525" y1="0.18415" x2="0.33655" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-0.28575" y1="0.18415" x2="-0.05715" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-0.81915" y1="0.18415" x2="-0.43815" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.18415" x2="-1.00965" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-1.92405" y1="0.18415" x2="-1.53035" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-2.55905" y1="0.18415" x2="-2.16535" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.18415" x2="-2.71145" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-3.23215" y1="0.18415" x2="-2.99085" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-3.65125" y1="0.18415" x2="-3.39725" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.18415" x2="-3.68935" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-4.46405" y1="0.18415" x2="-4.08305" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-5.11175" y1="0.18415" x2="-4.73075" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-6.02615" y1="0.18415" x2="-5.60705" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-6.68655" y1="0.18415" x2="-6.29285" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.18415" x2="-6.87705" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.18415" x2="-7.20725" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.18415" x2="-7.58825" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.18415" x2="-7.93115" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="0.18415" x2="-8.45185" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.18415" x2="-9.03605" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-9.83615" y1="0.18415" x2="-9.44245" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.18415" x2="-10.21715" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-10.96645" y1="0.18415" x2="-10.71245" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.18415" x2="-11.00455" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.18415" x2="-11.33475" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="-11.85545" y1="0.18415" x2="-11.60145" y2="0.19685" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.19685" x2="0.32385" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-0.28575" y1="0.19685" x2="-0.05715" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-0.83185" y1="0.19685" x2="-0.42545" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.19685" x2="-1.00965" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-1.93675" y1="0.19685" x2="-1.51765" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-2.57175" y1="0.19685" x2="-2.15265" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.19685" x2="-2.71145" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-3.24485" y1="0.19685" x2="-2.97815" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-3.65125" y1="0.19685" x2="-3.38455" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.19685" x2="-3.68935" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-4.47675" y1="0.19685" x2="-4.07035" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-5.12445" y1="0.19685" x2="-4.70535" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-6.03885" y1="0.19685" x2="-5.59435" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-6.69925" y1="0.19685" x2="-6.28015" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.19685" x2="-6.87705" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.19685" x2="-7.20725" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.19685" x2="-7.57555" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.19685" x2="-7.93115" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-8.85825" y1="0.19685" x2="-8.43915" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.19685" x2="-9.03605" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-9.84885" y1="0.19685" x2="-9.42975" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.19685" x2="-10.21715" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-10.97915" y1="0.19685" x2="-10.71245" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.19685" x2="-11.00455" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.19685" x2="-11.33475" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="-11.86815" y1="0.19685" x2="-11.58875" y2="0.20955" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.20955" x2="0.32385" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-0.28575" y1="0.20955" x2="-0.04445" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-0.84455" y1="0.20955" x2="-0.41275" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.20955" x2="-1.00965" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-1.94945" y1="0.20955" x2="-1.50495" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-2.58445" y1="0.20955" x2="-2.13995" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.20955" x2="-2.71145" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-3.24485" y1="0.20955" x2="-2.96545" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-3.66395" y1="0.20955" x2="-3.38455" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.20955" x2="-3.67665" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-4.48945" y1="0.20955" x2="-4.07035" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-5.13715" y1="0.20955" x2="-4.69265" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-6.05155" y1="0.20955" x2="-5.58165" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-6.71195" y1="0.20955" x2="-6.26745" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.20955" x2="-6.87705" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.20955" x2="-7.20725" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.20955" x2="-7.57555" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.20955" x2="-7.93115" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-8.87095" y1="0.20955" x2="-8.42645" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.20955" x2="-9.03605" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-9.86155" y1="0.20955" x2="-9.41705" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.20955" x2="-10.21715" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.20955" x2="-10.69975" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.20955" x2="-11.33475" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="-11.86815" y1="0.20955" x2="-11.57605" y2="0.22225" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.22225" x2="0.32385" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-0.28575" y1="0.22225" x2="-0.04445" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-0.85725" y1="0.22225" x2="-0.40005" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.22225" x2="-1.00965" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-1.96215" y1="0.22225" x2="-1.49225" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.22225" x2="-2.13995" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.22225" x2="-2.71145" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.22225" x2="-2.95275" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.22225" x2="-3.38455" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-4.50215" y1="0.22225" x2="-4.05765" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-5.14985" y1="0.22225" x2="-4.69265" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-6.06425" y1="0.22225" x2="-5.56895" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-6.72465" y1="0.22225" x2="-6.25475" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.22225" x2="-6.87705" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.22225" x2="-7.20725" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.22225" x2="-7.56285" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.22225" x2="-7.93115" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="0.22225" x2="-8.41375" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.22225" x2="-9.03605" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-9.86155" y1="0.22225" x2="-9.40435" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.22225" x2="-10.21715" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.22225" x2="-10.69975" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.22225" x2="-11.33475" y2="0.23495" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.23495" x2="0.32385" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-0.28575" y1="0.23495" x2="-0.04445" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-0.86995" y1="0.23495" x2="-0.38735" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.23495" x2="-1.00965" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-1.97485" y1="0.23495" x2="-1.49225" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.23495" x2="-2.12725" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.23495" x2="-2.71145" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.23495" x2="-3.37185" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-4.50215" y1="0.23495" x2="-4.04495" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-5.14985" y1="0.23495" x2="-4.67995" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-6.07695" y1="0.23495" x2="-5.56895" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-6.73735" y1="0.23495" x2="-6.25475" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.23495" x2="-6.87705" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.23495" x2="-7.20725" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-7.86765" y1="0.23495" x2="-7.56285" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.23495" x2="-7.93115" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-8.89635" y1="0.23495" x2="-8.41375" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.23495" x2="-9.03605" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.23495" x2="-9.40435" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.23495" x2="-10.21715" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.23495" x2="-10.69975" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.23495" x2="-11.33475" y2="0.24765" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.24765" x2="0.32385" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-0.27305" y1="0.24765" x2="-0.04445" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-0.61595" y1="0.24765" x2="-0.38735" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-0.86995" y1="0.24765" x2="-0.64135" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.24765" x2="-1.00965" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-1.72085" y1="0.24765" x2="-1.47955" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-1.97485" y1="0.24765" x2="-1.74625" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-2.34315" y1="0.24765" x2="-2.12725" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.24765" x2="-2.36855" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-2.97815" y1="0.24765" x2="-2.71145" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.24765" x2="-3.00355" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-3.61315" y1="0.24765" x2="-3.37185" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.24765" x2="-3.63855" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-4.28625" y1="0.24765" x2="-4.04495" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-4.51485" y1="0.24765" x2="-4.31165" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-4.90855" y1="0.24765" x2="-4.66725" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-5.16255" y1="0.24765" x2="-4.93395" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-6.07695" y1="0.24765" x2="-5.55625" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-6.48335" y1="0.24765" x2="-6.24205" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-6.73735" y1="0.24765" x2="-6.50875" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.24765" x2="-6.87705" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.24765" x2="-7.20725" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.24765" x2="-7.56285" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.24765" x2="-7.93115" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-8.64235" y1="0.24765" x2="-8.40105" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-8.89635" y1="0.24765" x2="-8.66775" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.24765" x2="-9.03605" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-9.60755" y1="0.24765" x2="-9.39165" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.24765" x2="-9.64565" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.24765" x2="-10.21715" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-10.94105" y1="0.24765" x2="-10.68705" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.24765" x2="-10.96645" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-11.60145" y1="0.24765" x2="-11.33475" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.24765" x2="-11.62685" y2="0.26035" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.26035" x2="0.32385" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-0.27305" y1="0.26035" x2="-0.04445" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-0.59055" y1="0.26035" x2="-0.37465" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-0.88265" y1="0.26035" x2="-0.66675" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.26035" x2="-1.00965" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-1.69545" y1="0.26035" x2="-1.47955" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-1.98755" y1="0.26035" x2="-1.77165" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-2.31775" y1="0.26035" x2="-2.11455" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.26035" x2="-2.40665" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="0.26035" x2="-2.71145" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.26035" x2="-3.02895" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-3.58775" y1="0.26035" x2="-3.37185" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.26035" x2="-3.66395" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-4.26085" y1="0.26035" x2="-4.03225" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-4.51485" y1="0.26035" x2="-4.32435" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-4.88315" y1="0.26035" x2="-4.66725" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-5.16255" y1="0.26035" x2="-4.95935" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-6.08965" y1="0.26035" x2="-5.55625" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-6.45795" y1="0.26035" x2="-6.24205" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-6.75005" y1="0.26035" x2="-6.53415" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.26035" x2="-6.87705" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.26035" x2="-7.20725" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-7.79145" y1="0.26035" x2="-7.56285" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.26035" x2="-7.93115" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="0.26035" x2="-8.40105" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="0.26035" x2="-8.69315" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.26035" x2="-9.03605" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-9.59485" y1="0.26035" x2="-9.39165" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.26035" x2="-9.67105" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.26035" x2="-10.21715" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-10.91565" y1="0.26035" x2="-10.68705" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.26035" x2="-10.99185" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-11.57605" y1="0.26035" x2="-11.33475" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.26035" x2="-11.65225" y2="0.27305" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.27305" x2="0.32385" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-0.27305" y1="0.27305" x2="-0.04445" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-0.59055" y1="0.27305" x2="-0.37465" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-0.88265" y1="0.27305" x2="-0.67945" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.27305" x2="-1.00965" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-1.69545" y1="0.27305" x2="-1.47955" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-1.98755" y1="0.27305" x2="-1.77165" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.27305" x2="-2.11455" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.27305" x2="-2.40665" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="0.27305" x2="-2.71145" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.27305" x2="-3.04165" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-3.58775" y1="0.27305" x2="-3.37185" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.27305" x2="-3.67665" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-4.24815" y1="0.27305" x2="-4.03225" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-4.52755" y1="0.27305" x2="-4.33705" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.27305" x2="-4.66725" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="0.27305" x2="-4.97205" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-5.78485" y1="0.27305" x2="-5.54355" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-6.08965" y1="0.27305" x2="-5.86105" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-6.45795" y1="0.27305" x2="-6.24205" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-6.75005" y1="0.27305" x2="-6.53415" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.27305" x2="-6.87705" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.27305" x2="-7.20725" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-7.79145" y1="0.27305" x2="-7.56285" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.27305" x2="-7.93115" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="0.27305" x2="-8.40105" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="0.27305" x2="-8.70585" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.27305" x2="-9.03605" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-9.58215" y1="0.27305" x2="-9.39165" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.27305" x2="-9.68375" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.27305" x2="-10.21715" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.27305" x2="-10.68705" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.27305" x2="-10.99185" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-11.57605" y1="0.27305" x2="-11.33475" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.27305" x2="-11.65225" y2="0.28575" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.28575" x2="0.31115" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-0.27305" y1="0.28575" x2="-0.04445" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.28575" x2="-0.37465" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-0.88265" y1="0.28575" x2="-0.67945" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.28575" x2="-1.00965" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.28575" x2="-1.46685" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-1.98755" y1="0.28575" x2="-1.78435" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.28575" x2="-2.11455" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.28575" x2="-2.41935" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="0.28575" x2="-2.71145" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.28575" x2="-3.04165" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.28575" x2="-3.37185" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.28575" x2="-3.67665" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-4.24815" y1="0.28575" x2="-4.03225" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-4.52755" y1="0.28575" x2="-4.33705" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.28575" x2="-4.65455" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="0.28575" x2="-4.97205" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-5.77215" y1="0.28575" x2="-5.54355" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.28575" x2="-5.87375" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.28575" x2="-6.22935" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-6.75005" y1="0.28575" x2="-6.54685" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.28575" x2="-6.87705" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.28575" x2="-7.20725" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-7.79145" y1="0.28575" x2="-7.56285" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.28575" x2="-7.93115" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="0.28575" x2="-8.40105" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="0.28575" x2="-8.70585" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.28575" x2="-9.03605" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-9.58215" y1="0.28575" x2="-9.37895" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.28575" x2="-9.68375" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.28575" x2="-10.21715" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.28575" x2="-10.68705" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.28575" x2="-10.99185" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.28575" x2="-11.33475" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.28575" x2="-11.66495" y2="0.29845" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.29845" x2="0.31115" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-0.27305" y1="0.29845" x2="-0.04445" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.29845" x2="-0.36195" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.29845" x2="-0.69215" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.29845" x2="-1.00965" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.29845" x2="-1.46685" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.29845" x2="-1.78435" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.29845" x2="-2.11455" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.29845" x2="-2.41935" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.29845" x2="-2.71145" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.29845" x2="-3.04165" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.29845" x2="-3.37185" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.29845" x2="-3.67665" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.29845" x2="-4.01955" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-4.52755" y1="0.29845" x2="-4.34975" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.29845" x2="-4.65455" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="0.29845" x2="-4.97205" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-5.77215" y1="0.29845" x2="-5.54355" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.29845" x2="-5.87375" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.29845" x2="-6.22935" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.29845" x2="-6.54685" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.29845" x2="-6.87705" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.29845" x2="-7.20725" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-7.79145" y1="0.29845" x2="-7.56285" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.29845" x2="-7.93115" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.29845" x2="-8.38835" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.29845" x2="-8.70585" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.29845" x2="-9.03605" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.29845" x2="-9.37895" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.29845" x2="-9.68375" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.29845" x2="-10.21715" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.29845" x2="-10.68705" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.29845" x2="-11.00455" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.29845" x2="-11.33475" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.29845" x2="-11.66495" y2="0.31115" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.31115" x2="0.31115" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-0.27305" y1="0.31115" x2="-0.04445" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.31115" x2="-0.36195" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.31115" x2="-0.69215" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.31115" x2="-1.00965" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.31115" x2="-1.46685" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.31115" x2="-1.78435" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.31115" x2="-2.11455" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.31115" x2="-2.41935" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.31115" x2="-2.71145" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.31115" x2="-3.04165" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.31115" x2="-3.37185" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.31115" x2="-3.67665" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.31115" x2="-4.01955" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.31115" x2="-4.34975" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.31115" x2="-4.65455" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.31115" x2="-4.97205" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.31115" x2="-5.54355" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.31115" x2="-5.87375" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.31115" x2="-6.22935" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.31115" x2="-6.54685" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.31115" x2="-6.87705" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.31115" x2="-7.20725" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.31115" x2="-7.56285" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.31115" x2="-7.93115" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.31115" x2="-8.38835" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.31115" x2="-8.70585" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.31115" x2="-9.03605" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.31115" x2="-9.37895" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.31115" x2="-9.68375" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-10.61085" y1="0.31115" x2="-10.21715" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.31115" x2="-10.68705" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.31115" x2="-11.00455" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.31115" x2="-11.33475" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.31115" x2="-11.66495" y2="0.32385" layer="21" rot="R180"/>
<rectangle x1="-0.27305" y1="0.32385" x2="0.31115" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.32385" x2="-0.36195" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.32385" x2="-0.69215" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.32385" x2="-1.00965" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.32385" x2="-1.46685" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.32385" x2="-1.78435" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.32385" x2="-2.10185" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.32385" x2="-2.41935" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.32385" x2="-2.71145" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.32385" x2="-3.04165" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.32385" x2="-3.37185" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.32385" x2="-3.67665" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.32385" x2="-4.01955" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.32385" x2="-4.34975" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.32385" x2="-4.65455" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.32385" x2="-4.97205" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.32385" x2="-5.53085" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.32385" x2="-5.87375" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.32385" x2="-6.22935" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.32385" x2="-6.54685" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.32385" x2="-6.87705" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.32385" x2="-7.20725" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.32385" x2="-7.56285" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.32385" x2="-7.93115" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.32385" x2="-8.38835" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.32385" x2="-8.70585" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.32385" x2="-9.03605" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.32385" x2="-9.37895" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.32385" x2="-9.68375" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.32385" x2="-10.21715" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.32385" x2="-10.68705" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.32385" x2="-11.00455" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.32385" x2="-11.33475" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.32385" x2="-11.66495" y2="0.33655" layer="21" rot="R180"/>
<rectangle x1="-0.26035" y1="0.33655" x2="0.31115" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.33655" x2="-0.36195" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.33655" x2="-0.69215" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.33655" x2="-1.00965" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.33655" x2="-1.46685" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.33655" x2="-1.78435" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.33655" x2="-2.10185" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.33655" x2="-2.41935" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.33655" x2="-2.71145" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.33655" x2="-3.04165" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.33655" x2="-3.37185" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.33655" x2="-3.67665" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.33655" x2="-4.01955" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.33655" x2="-4.34975" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.33655" x2="-4.64185" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.33655" x2="-4.97205" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.33655" x2="-5.53085" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.33655" x2="-5.87375" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.33655" x2="-6.22935" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.33655" x2="-6.54685" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.33655" x2="-6.87705" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.33655" x2="-7.20725" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.33655" x2="-7.56285" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.33655" x2="-7.93115" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.33655" x2="-8.38835" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.33655" x2="-8.70585" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.33655" x2="-9.03605" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.33655" x2="-9.37895" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-9.89965" y1="0.33655" x2="-9.68375" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.33655" x2="-10.21715" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.33655" x2="-10.68705" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.33655" x2="-11.00455" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.33655" x2="-11.33475" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.33655" x2="-11.66495" y2="0.34925" layer="21" rot="R180"/>
<rectangle x1="-0.26035" y1="0.34925" x2="0.31115" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.34925" x2="-0.36195" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.34925" x2="-0.69215" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.34925" x2="-1.00965" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.34925" x2="-1.46685" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.34925" x2="-1.78435" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.34925" x2="-2.10185" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.34925" x2="-2.41935" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.34925" x2="-2.71145" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.34925" x2="-3.04165" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.34925" x2="-3.37185" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.34925" x2="-3.67665" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.34925" x2="-4.01955" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.34925" x2="-4.34975" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.34925" x2="-4.64185" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.34925" x2="-4.97205" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.34925" x2="-5.53085" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.34925" x2="-5.87375" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.34925" x2="-6.22935" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.34925" x2="-6.54685" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.34925" x2="-6.87705" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.34925" x2="-7.20725" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.34925" x2="-7.56285" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.34925" x2="-7.93115" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.34925" x2="-8.38835" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.34925" x2="-8.70585" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.34925" x2="-9.03605" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.34925" x2="-9.37895" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-9.89965" y1="0.34925" x2="-9.68375" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.34925" x2="-10.21715" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.34925" x2="-10.68705" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.34925" x2="-11.00455" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.34925" x2="-11.33475" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.34925" x2="-11.66495" y2="0.36195" layer="21" rot="R180"/>
<rectangle x1="-0.26035" y1="0.36195" x2="0.31115" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.36195" x2="-0.34925" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.36195" x2="-0.69215" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.36195" x2="-1.00965" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.36195" x2="-1.46685" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.36195" x2="-1.78435" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.36195" x2="-2.10185" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.36195" x2="-2.41935" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.36195" x2="-2.71145" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.36195" x2="-3.04165" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.36195" x2="-3.37185" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.36195" x2="-3.67665" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.36195" x2="-4.01955" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.36195" x2="-4.34975" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.36195" x2="-4.64185" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.36195" x2="-4.97205" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.36195" x2="-5.53085" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.36195" x2="-5.87375" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.36195" x2="-6.22935" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.36195" x2="-6.54685" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.36195" x2="-6.87705" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.36195" x2="-7.20725" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.36195" x2="-7.56285" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.36195" x2="-7.93115" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.36195" x2="-8.38835" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.36195" x2="-8.70585" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.36195" x2="-9.03605" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.36195" x2="-9.37895" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-9.89965" y1="0.36195" x2="-9.68375" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.36195" x2="-10.21715" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.36195" x2="-10.68705" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.36195" x2="-11.00455" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.36195" x2="-11.33475" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.36195" x2="-11.66495" y2="0.37465" layer="21" rot="R180"/>
<rectangle x1="-0.26035" y1="0.37465" x2="0.31115" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.37465" x2="-0.34925" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.37465" x2="-0.69215" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.37465" x2="-1.00965" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.37465" x2="-1.46685" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.37465" x2="-1.78435" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.37465" x2="-2.10185" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.37465" x2="-2.41935" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.37465" x2="-2.71145" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.37465" x2="-3.04165" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.37465" x2="-3.37185" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.37465" x2="-3.67665" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.37465" x2="-4.01955" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.37465" x2="-4.34975" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.37465" x2="-4.64185" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.37465" x2="-4.97205" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.37465" x2="-5.53085" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.37465" x2="-5.87375" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.37465" x2="-6.22935" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.37465" x2="-6.54685" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.37465" x2="-6.87705" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.37465" x2="-7.20725" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.37465" x2="-7.56285" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.37465" x2="-7.93115" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.37465" x2="-8.38835" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.37465" x2="-8.70585" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.37465" x2="-9.03605" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.37465" x2="-9.37895" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-9.89965" y1="0.37465" x2="-9.68375" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.37465" x2="-10.21715" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.37465" x2="-10.68705" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.37465" x2="-11.00455" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.37465" x2="-11.33475" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.37465" x2="-11.66495" y2="0.38735" layer="21" rot="R180"/>
<rectangle x1="-0.26035" y1="0.38735" x2="0.29845" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.38735" x2="-0.34925" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.38735" x2="-0.69215" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.38735" x2="-1.00965" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.38735" x2="-1.45415" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.38735" x2="-1.78435" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.38735" x2="-2.10185" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.38735" x2="-2.41935" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.38735" x2="-2.71145" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.38735" x2="-3.04165" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.38735" x2="-3.37185" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.38735" x2="-3.67665" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.38735" x2="-4.01955" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.38735" x2="-4.34975" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.38735" x2="-4.64185" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.38735" x2="-4.97205" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.38735" x2="-5.53085" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.38735" x2="-5.88645" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.38735" x2="-6.21665" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.38735" x2="-6.54685" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.38735" x2="-6.87705" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.38735" x2="-7.20725" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.38735" x2="-7.56285" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.38735" x2="-7.93115" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.38735" x2="-8.38835" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.38735" x2="-8.70585" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.38735" x2="-9.03605" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.38735" x2="-9.37895" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-9.89965" y1="0.38735" x2="-9.68375" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.38735" x2="-10.21715" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.38735" x2="-10.68705" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.38735" x2="-11.00455" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.38735" x2="-11.33475" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.38735" x2="-11.66495" y2="0.40005" layer="21" rot="R180"/>
<rectangle x1="-0.26035" y1="0.40005" x2="0.29845" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.40005" x2="-0.34925" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.40005" x2="-0.69215" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.40005" x2="-1.00965" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.40005" x2="-1.45415" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.40005" x2="-1.78435" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.40005" x2="-2.10185" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.40005" x2="-2.40665" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.40005" x2="-2.71145" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.40005" x2="-3.04165" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.40005" x2="-3.37185" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.40005" x2="-3.67665" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.40005" x2="-4.00685" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.40005" x2="-4.34975" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.40005" x2="-4.64185" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.40005" x2="-4.97205" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.40005" x2="-5.53085" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.40005" x2="-5.88645" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.40005" x2="-6.21665" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.40005" x2="-6.54685" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.40005" x2="-6.87705" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.40005" x2="-7.20725" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.40005" x2="-7.56285" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.40005" x2="-7.93115" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.40005" x2="-8.38835" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.40005" x2="-8.70585" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.40005" x2="-9.03605" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.40005" x2="-9.37895" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.40005" x2="-9.68375" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.40005" x2="-10.21715" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.40005" x2="-10.68705" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.40005" x2="-11.00455" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.40005" x2="-11.33475" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.40005" x2="-11.66495" y2="0.41275" layer="21" rot="R180"/>
<rectangle x1="-0.26035" y1="0.41275" x2="0.29845" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.41275" x2="-0.34925" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.41275" x2="-0.69215" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.41275" x2="-1.00965" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.41275" x2="-1.45415" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.41275" x2="-1.78435" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.41275" x2="-2.40665" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.41275" x2="-2.71145" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.41275" x2="-3.04165" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.41275" x2="-3.37185" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.41275" x2="-3.67665" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.41275" x2="-4.00685" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.41275" x2="-4.34975" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.41275" x2="-4.64185" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.41275" x2="-4.97205" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.41275" x2="-5.53085" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.41275" x2="-5.88645" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.41275" x2="-6.21665" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.41275" x2="-6.54685" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.41275" x2="-6.87705" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.41275" x2="-7.20725" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.41275" x2="-7.56285" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.41275" x2="-7.93115" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.41275" x2="-8.37565" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.41275" x2="-8.70585" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.41275" x2="-9.03605" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.41275" x2="-9.67105" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.41275" x2="-10.21715" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.41275" x2="-10.68705" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.41275" x2="-11.00455" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.41275" x2="-11.33475" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.41275" x2="-11.66495" y2="0.42545" layer="21" rot="R180"/>
<rectangle x1="-0.24765" y1="0.42545" x2="0.29845" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.42545" x2="-0.34925" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.42545" x2="-0.69215" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.42545" x2="-1.00965" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.42545" x2="-1.45415" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.42545" x2="-1.78435" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.42545" x2="-2.38125" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.42545" x2="-2.71145" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.42545" x2="-3.04165" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.42545" x2="-3.37185" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.42545" x2="-3.67665" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.42545" x2="-4.00685" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.42545" x2="-4.34975" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.42545" x2="-4.64185" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.42545" x2="-4.97205" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.42545" x2="-5.53085" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.42545" x2="-5.88645" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.42545" x2="-6.21665" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.42545" x2="-6.54685" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.42545" x2="-6.87705" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.42545" x2="-7.20725" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.42545" x2="-7.56285" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.42545" x2="-7.93115" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.42545" x2="-8.37565" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.42545" x2="-8.70585" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.42545" x2="-9.03605" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.42545" x2="-9.65835" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.42545" x2="-10.21715" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.42545" x2="-10.68705" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.42545" x2="-11.00455" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.42545" x2="-11.33475" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.42545" x2="-11.66495" y2="0.43815" layer="21" rot="R180"/>
<rectangle x1="-0.24765" y1="0.43815" x2="0.29845" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.43815" x2="-0.34925" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.43815" x2="-0.69215" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.43815" x2="-1.00965" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.43815" x2="-1.45415" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.43815" x2="-1.78435" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-2.62255" y1="0.43815" x2="-2.36855" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.43815" x2="-2.71145" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.43815" x2="-3.04165" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-3.58775" y1="0.43815" x2="-3.37185" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.43815" x2="-3.67665" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.43815" x2="-4.00685" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.43815" x2="-4.34975" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.43815" x2="-4.64185" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.43815" x2="-4.97205" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.43815" x2="-5.53085" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.43815" x2="-5.88645" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.43815" x2="-6.21665" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.43815" x2="-6.54685" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.43815" x2="-6.87705" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.43815" x2="-7.20725" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.43815" x2="-7.56285" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.43815" x2="-7.93115" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.43815" x2="-8.37565" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.43815" x2="-8.70585" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.43815" x2="-9.03605" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.43815" x2="-9.63295" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.43815" x2="-10.21715" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.43815" x2="-10.68705" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.43815" x2="-11.00455" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.43815" x2="-11.33475" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.43815" x2="-11.66495" y2="0.45085" layer="21" rot="R180"/>
<rectangle x1="-0.24765" y1="0.45085" x2="0.29845" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.45085" x2="-0.34925" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.45085" x2="-0.69215" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.45085" x2="-1.00965" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.45085" x2="-1.45415" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.45085" x2="-1.78435" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.45085" x2="-2.34315" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.45085" x2="-2.71145" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.45085" x2="-3.04165" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-3.58775" y1="0.45085" x2="-3.37185" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.45085" x2="-3.67665" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.45085" x2="-4.00685" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-4.55295" y1="0.45085" x2="-4.34975" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.45085" x2="-4.64185" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.45085" x2="-4.97205" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.45085" x2="-5.51815" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.45085" x2="-5.88645" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.45085" x2="-6.21665" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.45085" x2="-6.54685" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.45085" x2="-6.87705" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.45085" x2="-7.20725" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.45085" x2="-7.56285" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.45085" x2="-7.93115" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.45085" x2="-8.37565" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.45085" x2="-8.70585" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.45085" x2="-9.03605" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-9.88695" y1="0.45085" x2="-9.62025" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.45085" x2="-10.21715" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.45085" x2="-10.68705" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.45085" x2="-11.00455" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.45085" x2="-11.33475" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.45085" x2="-11.66495" y2="0.46355" layer="21" rot="R180"/>
<rectangle x1="-0.24765" y1="0.46355" x2="0.29845" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.46355" x2="-0.34925" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.46355" x2="-1.00965" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.46355" x2="-1.45415" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.46355" x2="-1.78435" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.46355" x2="-2.31775" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.46355" x2="-2.71145" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.46355" x2="-3.04165" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-3.58775" y1="0.46355" x2="-3.37185" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.46355" x2="-3.67665" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.46355" x2="-4.00685" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.46355" x2="-4.64185" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.46355" x2="-5.51815" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.46355" x2="-5.88645" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.46355" x2="-6.21665" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.46355" x2="-6.54685" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.46355" x2="-6.87705" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.46355" x2="-7.20725" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.46355" x2="-7.56285" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.46355" x2="-7.93115" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.46355" x2="-8.37565" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.46355" x2="-8.70585" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.46355" x2="-9.03605" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.46355" x2="-9.59485" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.46355" x2="-10.21715" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-10.91565" y1="0.46355" x2="-10.69975" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.46355" x2="-11.00455" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.46355" x2="-11.33475" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.46355" x2="-11.66495" y2="0.47625" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.47625" x2="0.28575" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-0.24765" y1="0.47625" x2="-0.03175" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.47625" x2="-0.34925" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.47625" x2="-1.00965" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.47625" x2="-1.45415" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.47625" x2="-1.78435" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.47625" x2="-2.30505" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.47625" x2="-2.71145" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.47625" x2="-3.04165" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-3.60045" y1="0.47625" x2="-3.38455" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.47625" x2="-3.67665" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.47625" x2="-4.00685" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.47625" x2="-4.64185" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.47625" x2="-5.51815" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.47625" x2="-5.88645" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.47625" x2="-6.21665" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.47625" x2="-6.54685" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.47625" x2="-6.87705" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.47625" x2="-7.20725" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.47625" x2="-7.56285" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.47625" x2="-7.93115" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.47625" x2="-8.37565" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.47625" x2="-8.70585" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.47625" x2="-9.03605" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.47625" x2="-9.56945" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.47625" x2="-10.21715" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-10.91565" y1="0.47625" x2="-10.69975" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.47625" x2="-11.00455" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.47625" x2="-11.33475" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.47625" x2="-11.66495" y2="0.48895" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.48895" x2="0.28575" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-0.24765" y1="0.48895" x2="-0.03175" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.48895" x2="-0.34925" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.48895" x2="-1.00965" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.48895" x2="-1.45415" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.48895" x2="-1.78435" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.48895" x2="-2.27965" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.48895" x2="-2.71145" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.48895" x2="-3.04165" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-3.61315" y1="0.48895" x2="-3.38455" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.48895" x2="-3.67665" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.48895" x2="-4.00685" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.48895" x2="-4.64185" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.48895" x2="-5.51815" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.48895" x2="-5.88645" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.48895" x2="-6.21665" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.48895" x2="-6.54685" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.48895" x2="-6.87705" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.48895" x2="-7.20725" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.48895" x2="-7.56285" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.48895" x2="-7.93115" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.48895" x2="-8.37565" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.48895" x2="-8.70585" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.48895" x2="-9.03605" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.48895" x2="-9.54405" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.48895" x2="-10.21715" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-10.92835" y1="0.48895" x2="-10.71245" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.48895" x2="-11.00455" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.48895" x2="-11.33475" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.48895" x2="-11.66495" y2="0.50165" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.50165" x2="0.28575" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-0.23495" y1="0.50165" x2="-0.03175" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.50165" x2="-0.34925" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.50165" x2="-1.00965" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.50165" x2="-1.45415" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.50165" x2="-1.78435" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-2.58445" y1="0.50165" x2="-2.25425" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.50165" x2="-2.71145" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.50165" x2="-3.04165" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-3.62585" y1="0.50165" x2="-3.39725" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.50165" x2="-3.67665" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.50165" x2="-4.00685" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.50165" x2="-4.64185" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.50165" x2="-5.51815" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.50165" x2="-5.88645" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.50165" x2="-6.21665" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.50165" x2="-6.54685" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.50165" x2="-6.87705" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.50165" x2="-7.20725" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.50165" x2="-7.56285" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.50165" x2="-7.93115" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.50165" x2="-8.37565" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.50165" x2="-8.70585" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.50165" x2="-9.03605" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-9.86155" y1="0.50165" x2="-9.53135" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.50165" x2="-10.21715" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-10.94105" y1="0.50165" x2="-10.71245" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.50165" x2="-11.00455" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.50165" x2="-11.33475" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.50165" x2="-11.66495" y2="0.51435" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.51435" x2="0.28575" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-0.23495" y1="0.51435" x2="-0.03175" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.51435" x2="-0.34925" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.51435" x2="-1.00965" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.51435" x2="-1.45415" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.51435" x2="-1.78435" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-2.58445" y1="0.51435" x2="-2.22885" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.51435" x2="-2.71145" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.51435" x2="-3.04165" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-3.63855" y1="0.51435" x2="-3.40995" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.51435" x2="-3.67665" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.51435" x2="-4.00685" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.51435" x2="-4.64185" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.51435" x2="-5.51815" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.51435" x2="-5.88645" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.51435" x2="-6.21665" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.51435" x2="-6.54685" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.51435" x2="-6.87705" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.51435" x2="-7.20725" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.51435" x2="-7.56285" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.51435" x2="-7.93115" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.51435" x2="-8.37565" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.51435" x2="-8.70585" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.51435" x2="-9.03605" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-9.84885" y1="0.51435" x2="-9.50595" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.51435" x2="-10.21715" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-10.95375" y1="0.51435" x2="-10.73785" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.51435" x2="-11.00455" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.51435" x2="-11.33475" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.51435" x2="-11.66495" y2="0.52705" layer="21" rot="R180"/>
<rectangle x1="0.08255" y1="0.52705" x2="0.28575" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-0.23495" y1="0.52705" x2="-0.03175" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.52705" x2="-0.34925" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.52705" x2="-1.00965" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.52705" x2="-1.45415" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.52705" x2="-1.78435" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-2.57175" y1="0.52705" x2="-2.21615" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.52705" x2="-2.71145" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.52705" x2="-3.04165" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-3.65125" y1="0.52705" x2="-3.43535" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.52705" x2="-3.67665" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.52705" x2="-4.00685" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.52705" x2="-4.64185" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.52705" x2="-5.51815" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.52705" x2="-6.21665" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.52705" x2="-6.54685" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.52705" x2="-6.87705" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.52705" x2="-7.20725" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.52705" x2="-7.56285" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.52705" x2="-7.93115" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.52705" x2="-8.37565" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.52705" x2="-8.70585" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.52705" x2="-9.03605" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-9.83615" y1="0.52705" x2="-9.48055" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.52705" x2="-10.21715" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-10.97915" y1="0.52705" x2="-10.75055" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.52705" x2="-11.00455" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.52705" x2="-11.33475" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.52705" x2="-11.66495" y2="0.53975" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.53975" x2="0.28575" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-0.23495" y1="0.53975" x2="-0.03175" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.53975" x2="-0.34925" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.53975" x2="-1.00965" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.53975" x2="-1.45415" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.53975" x2="-1.78435" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-2.54635" y1="0.53975" x2="-2.19075" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.53975" x2="-2.71145" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.53975" x2="-3.04165" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.53975" x2="-3.46075" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.53975" x2="-4.00685" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.53975" x2="-4.64185" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.53975" x2="-5.51815" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.53975" x2="-6.21665" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.53975" x2="-6.54685" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.53975" x2="-6.87705" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.53975" x2="-7.20725" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.53975" x2="-7.56285" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.53975" x2="-7.93115" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.53975" x2="-8.37565" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.53975" x2="-8.70585" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.53975" x2="-9.03605" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-9.82345" y1="0.53975" x2="-9.46785" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.53975" x2="-10.21715" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.53975" x2="-10.77595" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.53975" x2="-11.33475" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.53975" x2="-11.66495" y2="0.55245" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.55245" x2="0.28575" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-0.23495" y1="0.55245" x2="-0.03175" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.55245" x2="-0.34925" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.55245" x2="-1.00965" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.55245" x2="-1.45415" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.55245" x2="-1.78435" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="0.55245" x2="-2.17805" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.55245" x2="-2.71145" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.55245" x2="-3.04165" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.55245" x2="-3.48615" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.55245" x2="-4.00685" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.55245" x2="-4.64185" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.55245" x2="-5.51815" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.55245" x2="-6.21665" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.55245" x2="-6.54685" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.55245" x2="-6.87705" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.55245" x2="-7.20725" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.55245" x2="-7.56285" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.55245" x2="-7.93115" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.55245" x2="-8.37565" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.55245" x2="-8.70585" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.55245" x2="-9.03605" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-9.79805" y1="0.55245" x2="-9.44245" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.55245" x2="-10.21715" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.55245" x2="-10.80135" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.55245" x2="-11.33475" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.55245" x2="-11.66495" y2="0.56515" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.56515" x2="0.28575" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-0.23495" y1="0.56515" x2="-0.03175" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.56515" x2="-0.34925" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.56515" x2="-1.00965" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.56515" x2="-1.45415" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.56515" x2="-1.78435" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-2.50825" y1="0.56515" x2="-2.16535" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.56515" x2="-2.71145" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.56515" x2="-3.04165" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.56515" x2="-3.52425" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.56515" x2="-4.00685" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.56515" x2="-4.64185" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.56515" x2="-5.51815" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.56515" x2="-6.21665" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.56515" x2="-6.54685" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.56515" x2="-6.87705" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.56515" x2="-7.20725" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.56515" x2="-7.56285" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.56515" x2="-7.93115" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.56515" x2="-8.37565" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.56515" x2="-8.70585" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.56515" x2="-9.03605" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-9.78535" y1="0.56515" x2="-9.42975" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.56515" x2="-10.21715" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.56515" x2="-10.83945" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.56515" x2="-11.33475" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.56515" x2="-11.66495" y2="0.57785" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.57785" x2="0.27305" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-0.23495" y1="0.57785" x2="-0.01905" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.57785" x2="-0.34925" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.57785" x2="-1.00965" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.57785" x2="-1.45415" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.57785" x2="-1.78435" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-2.48285" y1="0.57785" x2="-2.13995" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.57785" x2="-2.71145" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.57785" x2="-3.04165" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.57785" x2="-3.54965" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.57785" x2="-4.00685" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.57785" x2="-4.64185" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.57785" x2="-5.51815" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.57785" x2="-6.21665" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.57785" x2="-6.54685" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.57785" x2="-6.87705" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.57785" x2="-7.20725" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.57785" x2="-7.56285" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.57785" x2="-7.93115" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.57785" x2="-8.37565" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.57785" x2="-8.70585" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.57785" x2="-9.03605" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-9.75995" y1="0.57785" x2="-9.41705" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.57785" x2="-10.21715" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.57785" x2="-10.86485" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.57785" x2="-11.33475" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.57785" x2="-11.66495" y2="0.59055" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.59055" x2="0.27305" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-0.22225" y1="0.59055" x2="-0.01905" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.59055" x2="-0.34925" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.59055" x2="-1.00965" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.59055" x2="-1.45415" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.59055" x2="-1.78435" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-2.47015" y1="0.59055" x2="-2.13995" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.59055" x2="-2.71145" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.59055" x2="-3.04165" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.59055" x2="-3.58775" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.59055" x2="-4.00685" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.59055" x2="-4.64185" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.59055" x2="-5.51815" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.59055" x2="-6.21665" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.59055" x2="-6.54685" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.59055" x2="-6.87705" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.59055" x2="-7.20725" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.59055" x2="-7.56285" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.59055" x2="-7.93115" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.59055" x2="-8.37565" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.59055" x2="-8.70585" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.59055" x2="-9.03605" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-9.73455" y1="0.59055" x2="-9.40435" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.59055" x2="-10.21715" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.59055" x2="-10.90295" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.59055" x2="-11.33475" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.59055" x2="-11.66495" y2="0.60325" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.60325" x2="0.27305" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-0.22225" y1="0.60325" x2="-0.01905" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.60325" x2="-0.34925" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.60325" x2="-1.00965" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.60325" x2="-1.45415" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.60325" x2="-1.78435" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-2.44475" y1="0.60325" x2="-2.12725" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.60325" x2="-2.71145" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.60325" x2="-3.04165" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.60325" x2="-3.61315" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.60325" x2="-4.00685" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.60325" x2="-4.64185" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.60325" x2="-5.51815" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.60325" x2="-6.21665" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.60325" x2="-6.54685" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.60325" x2="-6.87705" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.60325" x2="-7.20725" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.60325" x2="-7.56285" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.60325" x2="-7.93115" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.60325" x2="-8.37565" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.60325" x2="-8.70585" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.60325" x2="-9.03605" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-9.70915" y1="0.60325" x2="-9.39165" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.60325" x2="-10.21715" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.60325" x2="-10.92835" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.60325" x2="-11.33475" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.60325" x2="-11.66495" y2="0.61595" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.61595" x2="0.27305" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-0.22225" y1="0.61595" x2="-0.01905" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.61595" x2="-0.34925" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-1.25095" y1="0.61595" x2="-1.00965" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.61595" x2="-1.45415" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.61595" x2="-1.78435" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-2.41935" y1="0.61595" x2="-2.11455" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.61595" x2="-2.71145" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.61595" x2="-3.04165" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.61595" x2="-3.63855" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.61595" x2="-4.00685" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.61595" x2="-4.64185" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.61595" x2="-5.51815" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.61595" x2="-6.21665" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.61595" x2="-6.54685" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.61595" x2="-6.87705" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.61595" x2="-7.20725" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.61595" x2="-7.56285" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-8.17245" y1="0.61595" x2="-7.93115" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.61595" x2="-8.37565" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.61595" x2="-8.70585" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.61595" x2="-9.03605" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-9.68375" y1="0.61595" x2="-9.39165" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.61595" x2="-10.21715" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.61595" x2="-10.95375" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.61595" x2="-11.33475" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.61595" x2="-11.66495" y2="0.62865" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.62865" x2="0.27305" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-0.22225" y1="0.62865" x2="-0.01905" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.62865" x2="-0.34925" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-1.25095" y1="0.62865" x2="-1.00965" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.62865" x2="-1.45415" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.62865" x2="-1.78435" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-2.39395" y1="0.62865" x2="-2.11455" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.62865" x2="-2.71145" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.62865" x2="-3.04165" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.62865" x2="-3.66395" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.62865" x2="-4.00685" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.62865" x2="-4.64185" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.62865" x2="-5.51815" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.62865" x2="-6.21665" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.62865" x2="-6.54685" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.62865" x2="-6.87705" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.62865" x2="-7.20725" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.62865" x2="-7.56285" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-8.17245" y1="0.62865" x2="-7.93115" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.62865" x2="-8.37565" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.62865" x2="-8.70585" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.62865" x2="-9.03605" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-9.65835" y1="0.62865" x2="-9.39165" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.62865" x2="-10.21715" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.62865" x2="-10.97915" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.62865" x2="-11.33475" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.62865" x2="-11.66495" y2="0.64135" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.64135" x2="0.27305" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-0.22225" y1="0.64135" x2="-0.01905" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.64135" x2="-0.34925" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-1.26365" y1="0.64135" x2="-1.00965" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.64135" x2="-1.45415" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.64135" x2="-1.78435" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-2.36855" y1="0.64135" x2="-2.11455" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.64135" x2="-2.71145" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.64135" x2="-3.04165" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.64135" x2="-3.67665" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.64135" x2="-4.00685" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.64135" x2="-4.64185" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.64135" x2="-5.51815" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.64135" x2="-6.21665" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.64135" x2="-6.54685" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.64135" x2="-6.87705" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.64135" x2="-7.20725" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.64135" x2="-7.56285" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-8.18515" y1="0.64135" x2="-7.93115" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.64135" x2="-8.37565" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.64135" x2="-8.70585" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.64135" x2="-9.03605" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-9.63295" y1="0.64135" x2="-9.37895" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.64135" x2="-10.21715" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.64135" x2="-10.99185" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.64135" x2="-11.33475" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.64135" x2="-11.66495" y2="0.65405" layer="21" rot="R180"/>
<rectangle x1="0.06985" y1="0.65405" x2="0.27305" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-0.22225" y1="0.65405" x2="-0.01905" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.65405" x2="-0.34925" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.65405" x2="-0.66675" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-1.26365" y1="0.65405" x2="-1.00965" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.65405" x2="-1.45415" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.65405" x2="-1.78435" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-2.34315" y1="0.65405" x2="-2.10185" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.65405" x2="-2.71145" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.65405" x2="-3.04165" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.65405" x2="-3.37185" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.65405" x2="-3.67665" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.65405" x2="-4.00685" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.65405" x2="-4.64185" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.65405" x2="-4.95935" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.65405" x2="-5.51815" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.65405" x2="-6.21665" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.65405" x2="-6.54685" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.65405" x2="-6.87705" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.65405" x2="-7.20725" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.65405" x2="-7.56285" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-8.18515" y1="0.65405" x2="-7.93115" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.65405" x2="-8.37565" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.65405" x2="-8.70585" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.65405" x2="-9.03605" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-9.62025" y1="0.65405" x2="-9.37895" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.65405" x2="-10.21715" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.65405" x2="-10.68705" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.65405" x2="-10.99185" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.65405" x2="-11.33475" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.65405" x2="-11.66495" y2="0.66675" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.66675" x2="0.26035" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-0.22225" y1="0.66675" x2="-0.01905" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.66675" x2="-0.34925" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-0.90805" y1="0.66675" x2="-0.67945" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-1.28905" y1="0.66675" x2="-1.00965" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.66675" x2="-1.45415" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.66675" x2="-1.78435" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-2.33045" y1="0.66675" x2="-2.10185" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.66675" x2="-2.71145" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.66675" x2="-3.04165" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.66675" x2="-3.37185" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.66675" x2="-3.67665" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.66675" x2="-4.00685" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.66675" x2="-4.33705" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.66675" x2="-4.64185" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.66675" x2="-4.95935" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.66675" x2="-5.51815" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.66675" x2="-6.21665" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.66675" x2="-6.54685" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.66675" x2="-6.87705" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.66675" x2="-7.20725" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.66675" x2="-7.56285" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-8.21055" y1="0.66675" x2="-7.93115" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.66675" x2="-8.38835" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.66675" x2="-8.70585" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.66675" x2="-9.03605" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-9.60755" y1="0.66675" x2="-9.37895" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.66675" x2="-10.21715" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.66675" x2="-10.68705" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.66675" x2="-10.99185" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.66675" x2="-11.33475" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.66675" x2="-11.66495" y2="0.67945" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.67945" x2="0.26035" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-0.20955" y1="0.67945" x2="-0.01905" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.67945" x2="-0.34925" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.67945" x2="-0.67945" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-1.31445" y1="0.67945" x2="-1.00965" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.67945" x2="-1.45415" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.67945" x2="-1.78435" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-2.31775" y1="0.67945" x2="-2.10185" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.67945" x2="-2.71145" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.67945" x2="-3.04165" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.67945" x2="-3.37185" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.67945" x2="-3.67665" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.67945" x2="-4.00685" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.67945" x2="-4.33705" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.67945" x2="-4.64185" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.67945" x2="-4.95935" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.67945" x2="-5.51815" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.67945" x2="-6.21665" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.67945" x2="-6.54685" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.67945" x2="-6.87705" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.67945" x2="-7.20725" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.67945" x2="-7.56285" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-8.23595" y1="0.67945" x2="-7.93115" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.67945" x2="-8.38835" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.67945" x2="-8.70585" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.67945" x2="-9.03605" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-9.59485" y1="0.67945" x2="-9.36625" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.67945" x2="-10.21715" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.67945" x2="-10.68705" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.67945" x2="-11.00455" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.67945" x2="-11.33475" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.67945" x2="-11.66495" y2="0.69215" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.69215" x2="0.26035" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-0.20955" y1="0.69215" x2="-0.01905" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.69215" x2="-0.34925" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.69215" x2="-0.67945" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.69215" x2="-1.00965" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.69215" x2="-1.46685" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.69215" x2="-1.78435" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.69215" x2="-2.10185" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.69215" x2="-2.71145" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.69215" x2="-3.04165" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.69215" x2="-3.37185" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.69215" x2="-3.67665" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.69215" x2="-4.00685" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.69215" x2="-4.33705" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.69215" x2="-4.64185" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.69215" x2="-4.95935" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.69215" x2="-5.51815" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.69215" x2="-6.22935" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.69215" x2="-6.54685" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.69215" x2="-6.87705" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.69215" x2="-7.20725" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.69215" x2="-7.56285" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.69215" x2="-7.93115" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.69215" x2="-8.38835" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.69215" x2="-8.70585" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.69215" x2="-9.03605" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-9.58215" y1="0.69215" x2="-9.36625" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.69215" x2="-10.21715" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.69215" x2="-10.68705" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.69215" x2="-11.00455" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.69215" x2="-11.33475" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.69215" x2="-11.66495" y2="0.70485" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.70485" x2="0.26035" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-0.20955" y1="0.70485" x2="-0.01905" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.70485" x2="-0.34925" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.70485" x2="-0.67945" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.70485" x2="-1.00965" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.70485" x2="-1.46685" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.70485" x2="-1.78435" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.70485" x2="-2.10185" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.70485" x2="-2.40665" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.70485" x2="-2.71145" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.70485" x2="-3.04165" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.70485" x2="-3.37185" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.70485" x2="-3.67665" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.70485" x2="-4.00685" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.70485" x2="-4.33705" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.70485" x2="-4.64185" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.70485" x2="-4.95935" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.70485" x2="-5.51815" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.70485" x2="-6.22935" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.70485" x2="-6.54685" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.70485" x2="-6.87705" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.70485" x2="-7.20725" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.70485" x2="-7.56285" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.70485" x2="-7.93115" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.70485" x2="-8.38835" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.70485" x2="-8.70585" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.70485" x2="-9.03605" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.70485" x2="-9.36625" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.70485" x2="-9.68375" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.70485" x2="-10.21715" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.70485" x2="-10.68705" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.70485" x2="-11.00455" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.70485" x2="-11.33475" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.70485" x2="-11.66495" y2="0.71755" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.71755" x2="0.26035" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-0.20955" y1="0.71755" x2="-0.00635" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.71755" x2="-0.34925" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.71755" x2="-0.67945" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.71755" x2="-1.00965" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.71755" x2="-1.46685" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-2.01295" y1="0.71755" x2="-1.78435" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.71755" x2="-2.10185" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.71755" x2="-2.40665" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.71755" x2="-2.71145" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.71755" x2="-3.04165" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.71755" x2="-3.37185" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.71755" x2="-3.67665" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.71755" x2="-4.01955" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.71755" x2="-4.33705" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.71755" x2="-4.64185" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.71755" x2="-4.95935" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.71755" x2="-5.51815" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.71755" x2="-5.88645" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.71755" x2="-6.22935" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="0.71755" x2="-6.54685" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.71755" x2="-6.87705" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.71755" x2="-7.20725" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.71755" x2="-7.56285" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.71755" x2="-7.93115" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.71755" x2="-8.38835" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.71755" x2="-8.70585" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.71755" x2="-9.03605" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.71755" x2="-9.36625" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.71755" x2="-9.68375" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.71755" x2="-10.21715" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.71755" x2="-10.68705" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.71755" x2="-11.00455" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.71755" x2="-11.33475" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.71755" x2="-11.66495" y2="0.73025" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.73025" x2="0.26035" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-0.20955" y1="0.73025" x2="-0.00635" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.73025" x2="-0.36195" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.73025" x2="-0.67945" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.73025" x2="-1.00965" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.73025" x2="-1.46685" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.73025" x2="-1.78435" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.73025" x2="-2.10185" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.73025" x2="-2.40665" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.73025" x2="-2.71145" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.73025" x2="-3.04165" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.73025" x2="-3.37185" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.73025" x2="-3.67665" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.73025" x2="-4.01955" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.73025" x2="-4.33705" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.73025" x2="-4.64185" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.73025" x2="-4.95935" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.73025" x2="-5.51815" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.73025" x2="-5.88645" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.73025" x2="-6.22935" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.73025" x2="-6.54685" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.73025" x2="-6.87705" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.73025" x2="-7.20725" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.73025" x2="-7.56285" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.73025" x2="-7.93115" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.73025" x2="-8.38835" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="0.73025" x2="-8.70585" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.73025" x2="-9.03605" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.73025" x2="-9.36625" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.73025" x2="-9.68375" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.73025" x2="-10.21715" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.73025" x2="-10.68705" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.73025" x2="-11.00455" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.73025" x2="-11.33475" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.73025" x2="-11.66495" y2="0.74295" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.74295" x2="0.26035" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-0.20955" y1="0.74295" x2="-0.00635" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.74295" x2="-0.36195" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.74295" x2="-0.67945" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.74295" x2="-1.00965" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.74295" x2="-1.46685" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.74295" x2="-1.78435" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.74295" x2="-2.10185" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.74295" x2="-2.40665" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.74295" x2="-2.71145" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.74295" x2="-3.04165" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.74295" x2="-3.37185" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.74295" x2="-3.67665" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.74295" x2="-4.01955" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.74295" x2="-4.33705" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.74295" x2="-4.64185" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="0.74295" x2="-4.95935" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.74295" x2="-5.51815" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.74295" x2="-5.88645" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.74295" x2="-6.22935" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.74295" x2="-6.54685" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.74295" x2="-6.87705" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.74295" x2="-7.20725" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.74295" x2="-7.56285" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.74295" x2="-7.93115" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.74295" x2="-8.38835" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.74295" x2="-8.70585" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.74295" x2="-9.03605" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.74295" x2="-9.36625" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.74295" x2="-9.68375" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.74295" x2="-10.21715" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.74295" x2="-10.68705" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.74295" x2="-11.00455" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.74295" x2="-11.33475" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.74295" x2="-11.66495" y2="0.75565" layer="21" rot="R180"/>
<rectangle x1="0.05715" y1="0.75565" x2="0.26035" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-0.19685" y1="0.75565" x2="-0.00635" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.75565" x2="-0.36195" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.75565" x2="-0.67945" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.75565" x2="-1.00965" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.75565" x2="-1.46685" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.75565" x2="-1.78435" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.75565" x2="-2.10185" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-2.60985" y1="0.75565" x2="-2.40665" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.75565" x2="-2.71145" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.75565" x2="-3.04165" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.75565" x2="-3.37185" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.75565" x2="-3.67665" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.75565" x2="-4.01955" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.75565" x2="-4.33705" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.75565" x2="-4.65455" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="0.75565" x2="-4.95935" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.75565" x2="-5.51815" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.75565" x2="-5.88645" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.75565" x2="-6.22935" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.75565" x2="-6.54685" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.75565" x2="-6.87705" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.75565" x2="-7.20725" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.75565" x2="-7.56285" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.75565" x2="-7.93115" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.75565" x2="-8.38835" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.75565" x2="-8.70585" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.75565" x2="-9.03605" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.75565" x2="-9.36625" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.75565" x2="-9.68375" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.75565" x2="-10.21715" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.75565" x2="-10.68705" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.75565" x2="-11.00455" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.75565" x2="-11.33475" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.75565" x2="-11.66495" y2="0.76835" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.76835" x2="0.24765" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-0.19685" y1="0.76835" x2="-0.00635" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.76835" x2="-0.36195" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-0.89535" y1="0.76835" x2="-0.67945" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.76835" x2="-1.00965" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.76835" x2="-1.46685" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.76835" x2="-1.78435" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.76835" x2="-2.10185" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.76835" x2="-2.40665" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.76835" x2="-2.71145" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.76835" x2="-3.04165" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.76835" x2="-3.37185" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-3.90525" y1="0.76835" x2="-3.67665" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.76835" x2="-4.01955" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-4.54025" y1="0.76835" x2="-4.33705" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.76835" x2="-4.65455" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="0.76835" x2="-4.95935" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.76835" x2="-5.51815" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.76835" x2="-5.88645" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.76835" x2="-6.22935" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.76835" x2="-6.54685" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.76835" x2="-6.87705" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.76835" x2="-7.20725" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.76835" x2="-7.56285" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.76835" x2="-7.93115" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.76835" x2="-8.38835" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.76835" x2="-8.70585" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.76835" x2="-9.03605" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.76835" x2="-9.36625" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.76835" x2="-9.67105" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.76835" x2="-10.21715" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.76835" x2="-10.68705" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.76835" x2="-11.00455" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.76835" x2="-11.33475" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.76835" x2="-11.66495" y2="0.78105" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.78105" x2="0.24765" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-0.19685" y1="0.78105" x2="-0.00635" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.78105" x2="-0.36195" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-0.88265" y1="0.78105" x2="-0.67945" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.78105" x2="-1.00965" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.78105" x2="-1.47955" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.78105" x2="-1.78435" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.78105" x2="-2.10185" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.78105" x2="-2.40665" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.78105" x2="-2.71145" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.78105" x2="-3.04165" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.78105" x2="-3.37185" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-3.89255" y1="0.78105" x2="-3.67665" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.78105" x2="-4.01955" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-4.52755" y1="0.78105" x2="-4.33705" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.78105" x2="-4.65455" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="0.78105" x2="-4.95935" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.78105" x2="-5.51815" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.78105" x2="-5.88645" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.78105" x2="-6.22935" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.78105" x2="-6.54685" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.78105" x2="-6.87705" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.78105" x2="-7.20725" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.78105" x2="-7.56285" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.78105" x2="-7.93115" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="0.78105" x2="-8.40105" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.78105" x2="-8.70585" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.78105" x2="-9.03605" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.78105" x2="-9.36625" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.78105" x2="-9.67105" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.78105" x2="-10.21715" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-10.89025" y1="0.78105" x2="-10.69975" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.78105" x2="-10.99185" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.78105" x2="-11.33475" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.78105" x2="-11.66495" y2="0.79375" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.79375" x2="0.24765" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-0.19685" y1="0.79375" x2="-0.00635" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.79375" x2="-0.37465" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-0.88265" y1="0.79375" x2="-0.67945" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.79375" x2="-1.00965" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-1.68275" y1="0.79375" x2="-1.47955" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-2.00025" y1="0.79375" x2="-1.78435" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.79375" x2="-2.10185" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.79375" x2="-2.40665" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.79375" x2="-2.71145" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.79375" x2="-3.04165" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.79375" x2="-3.37185" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-3.89255" y1="0.79375" x2="-3.67665" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-4.23545" y1="0.79375" x2="-4.01955" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-4.52755" y1="0.79375" x2="-4.33705" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.79375" x2="-4.65455" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="0.79375" x2="-4.95935" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.79375" x2="-5.51815" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.79375" x2="-5.88645" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="0.79375" x2="-6.24205" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="0.79375" x2="-6.54685" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.79375" x2="-6.87705" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="0.79375" x2="-7.20725" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.79375" x2="-7.56285" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.79375" x2="-7.93115" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="0.79375" x2="-8.40105" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="0.79375" x2="-8.70585" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.79375" x2="-9.03605" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-9.56945" y1="0.79375" x2="-9.37895" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.79375" x2="-9.67105" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.79375" x2="-10.21715" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.79375" x2="-10.69975" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="0.79375" x2="-10.99185" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.79375" x2="-11.33475" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.79375" x2="-11.66495" y2="0.80645" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.80645" x2="0.24765" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-0.19685" y1="0.80645" x2="-0.00635" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-0.57785" y1="0.80645" x2="-0.37465" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-0.88265" y1="0.80645" x2="-0.66675" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.80645" x2="-1.00965" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-1.69545" y1="0.80645" x2="-1.47955" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-1.98755" y1="0.80645" x2="-1.78435" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="0.80645" x2="-2.10185" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.80645" x2="-2.40665" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="0.80645" x2="-2.71145" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.80645" x2="-3.04165" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-3.57505" y1="0.80645" x2="-3.38455" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-3.89255" y1="0.80645" x2="-3.67665" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-4.24815" y1="0.80645" x2="-4.03225" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-4.52755" y1="0.80645" x2="-4.32435" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="0.80645" x2="-4.66725" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-5.16255" y1="0.80645" x2="-4.95935" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.80645" x2="-5.51815" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.80645" x2="-5.88645" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-6.45795" y1="0.80645" x2="-6.24205" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-6.75005" y1="0.80645" x2="-6.53415" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-7.11835" y1="0.80645" x2="-6.87705" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="0.80645" x2="-7.20725" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.80645" x2="-7.56285" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.80645" x2="-7.93115" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="0.80645" x2="-8.40105" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="0.80645" x2="-8.70585" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.80645" x2="-9.03605" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-9.58215" y1="0.80645" x2="-9.37895" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.80645" x2="-9.67105" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.80645" x2="-10.21715" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.80645" x2="-10.69975" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-11.20775" y1="0.80645" x2="-10.99185" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.80645" x2="-11.33475" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.80645" x2="-11.65225" y2="0.81915" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.81915" x2="0.24765" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-0.19685" y1="0.81915" x2="-0.00635" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-0.59055" y1="0.81915" x2="-0.37465" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-0.86995" y1="0.81915" x2="-0.66675" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.81915" x2="-1.00965" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-1.69545" y1="0.81915" x2="-1.47955" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-1.98755" y1="0.81915" x2="-1.77165" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-2.31775" y1="0.81915" x2="-2.10185" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.81915" x2="-2.39395" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="0.81915" x2="-2.71145" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-3.27025" y1="0.81915" x2="-3.02895" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-3.58775" y1="0.81915" x2="-3.38455" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-3.89255" y1="0.81915" x2="-3.66395" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-4.24815" y1="0.81915" x2="-4.03225" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-4.52755" y1="0.81915" x2="-4.32435" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-4.88315" y1="0.81915" x2="-4.66725" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-5.16255" y1="0.81915" x2="-4.94665" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.81915" x2="-5.51815" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.81915" x2="-5.88645" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-6.45795" y1="0.81915" x2="-6.24205" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-6.75005" y1="0.81915" x2="-6.53415" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-7.11835" y1="0.81915" x2="-6.87705" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="0.81915" x2="-7.19455" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.81915" x2="-7.49935" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.81915" x2="-7.93115" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="0.81915" x2="-8.40105" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="0.81915" x2="-8.69315" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.81915" x2="-9.03605" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-9.58215" y1="0.81915" x2="-9.37895" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-9.87425" y1="0.81915" x2="-9.67105" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.81915" x2="-10.21715" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-10.90295" y1="0.81915" x2="-10.69975" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-11.20775" y1="0.81915" x2="-10.97915" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-11.57605" y1="0.81915" x2="-11.33475" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.81915" x2="-11.65225" y2="0.83185" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.83185" x2="0.24765" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-0.19685" y1="0.83185" x2="-0.00635" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-0.60325" y1="0.83185" x2="-0.38735" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-0.86995" y1="0.83185" x2="-0.64135" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.83185" x2="-1.00965" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-1.72085" y1="0.83185" x2="-1.49225" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-1.97485" y1="0.83185" x2="-1.75895" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-2.33045" y1="0.83185" x2="-2.11455" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-2.59715" y1="0.83185" x2="-2.38125" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-2.97815" y1="0.83185" x2="-2.71145" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.83185" x2="-3.01625" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-3.60045" y1="0.83185" x2="-3.38455" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-3.87985" y1="0.83185" x2="-3.63855" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-4.27355" y1="0.83185" x2="-4.03225" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-4.51485" y1="0.83185" x2="-4.29895" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-4.89585" y1="0.83185" x2="-4.66725" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-5.14985" y1="0.83185" x2="-4.93395" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.83185" x2="-5.53085" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.83185" x2="-5.88645" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-6.48335" y1="0.83185" x2="-6.25475" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-6.73735" y1="0.83185" x2="-6.52145" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-7.14375" y1="0.83185" x2="-6.87705" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="0.83185" x2="-7.18185" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.83185" x2="-7.49935" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.83185" x2="-7.93115" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-8.64235" y1="0.83185" x2="-8.41375" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-8.89635" y1="0.83185" x2="-8.68045" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.83185" x2="-9.03605" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-9.60755" y1="0.83185" x2="-9.37895" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-9.86155" y1="0.83185" x2="-9.64565" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.83185" x2="-10.21715" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-10.92835" y1="0.83185" x2="-10.71245" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-11.20775" y1="0.83185" x2="-10.96645" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-11.58875" y1="0.83185" x2="-11.33475" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="-11.89355" y1="0.83185" x2="-11.62685" y2="0.84455" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.84455" x2="0.24765" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-0.18415" y1="0.84455" x2="-0.00635" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-0.85725" y1="0.84455" x2="-0.38735" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-1.22555" y1="0.84455" x2="-1.00965" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.84455" x2="-1.25095" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-1.97485" y1="0.84455" x2="-1.49225" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-2.58445" y1="0.84455" x2="-2.11455" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.84455" x2="-2.71145" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-3.87985" y1="0.84455" x2="-3.39725" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-4.51485" y1="0.84455" x2="-4.04495" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-5.14985" y1="0.84455" x2="-4.67995" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.84455" x2="-5.53085" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.84455" x2="-5.88645" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-6.73735" y1="0.84455" x2="-6.25475" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="0.84455" x2="-6.87705" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.84455" x2="-7.49935" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-8.14705" y1="0.84455" x2="-7.93115" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.84455" x2="-8.17245" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-8.89635" y1="0.84455" x2="-8.41375" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.84455" x2="-9.03605" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-9.86155" y1="0.84455" x2="-9.39165" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.84455" x2="-10.21715" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-11.19505" y1="0.84455" x2="-10.71245" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.84455" x2="-11.33475" y2="0.85725" layer="21" rot="R180"/>
<rectangle x1="0.04445" y1="0.85725" x2="0.23495" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-0.18415" y1="0.85725" x2="0.00635" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-0.85725" y1="0.85725" x2="-0.40005" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-1.22555" y1="0.85725" x2="-1.00965" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.85725" x2="-1.26365" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-1.96215" y1="0.85725" x2="-1.50495" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-2.58445" y1="0.85725" x2="-2.11455" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.85725" x2="-2.71145" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.85725" x2="-2.95275" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-3.87985" y1="0.85725" x2="-3.40995" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-4.50215" y1="0.85725" x2="-4.05765" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-5.13715" y1="0.85725" x2="-4.67995" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.85725" x2="-5.53085" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.85725" x2="-5.88645" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-6.72465" y1="0.85725" x2="-6.26745" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="0.85725" x2="-6.87705" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.85725" x2="-7.49935" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-8.14705" y1="0.85725" x2="-7.93115" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.85725" x2="-8.18515" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="0.85725" x2="-8.42645" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.85725" x2="-9.03605" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-9.84885" y1="0.85725" x2="-9.39165" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.85725" x2="-10.21715" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-11.19505" y1="0.85725" x2="-10.72515" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.85725" x2="-11.33475" y2="0.86995" layer="21" rot="R180"/>
<rectangle x1="0.03175" y1="0.86995" x2="0.23495" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-0.18415" y1="0.86995" x2="0.00635" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-0.84455" y1="0.86995" x2="-0.40005" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-1.22555" y1="0.86995" x2="-1.00965" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.86995" x2="-1.26365" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-1.94945" y1="0.86995" x2="-1.51765" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-2.57175" y1="0.86995" x2="-2.12725" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.86995" x2="-2.71145" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-3.25755" y1="0.86995" x2="-2.96545" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-3.86715" y1="0.86995" x2="-3.40995" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-4.48945" y1="0.86995" x2="-4.05765" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-5.12445" y1="0.86995" x2="-4.69265" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.86995" x2="-5.53085" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.86995" x2="-5.88645" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-6.71195" y1="0.86995" x2="-6.28015" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-7.11835" y1="0.86995" x2="-6.87705" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-7.41045" y1="0.86995" x2="-7.13105" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.86995" x2="-7.49935" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-8.14705" y1="0.86995" x2="-7.93115" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.86995" x2="-8.19785" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-8.87095" y1="0.86995" x2="-8.43915" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.86995" x2="-9.03605" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-9.84885" y1="0.86995" x2="-9.40435" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.86995" x2="-10.21715" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-11.18235" y1="0.86995" x2="-10.73785" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.86995" x2="-11.33475" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="-11.88085" y1="0.86995" x2="-11.57605" y2="0.88265" layer="21" rot="R180"/>
<rectangle x1="0.03175" y1="0.88265" x2="0.23495" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-0.18415" y1="0.88265" x2="0.00635" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-0.83185" y1="0.88265" x2="-0.41275" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-1.22555" y1="0.88265" x2="-1.00965" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.88265" x2="-1.27635" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-1.93675" y1="0.88265" x2="-1.53035" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-2.55905" y1="0.88265" x2="-2.13995" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.88265" x2="-2.71145" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-3.24485" y1="0.88265" x2="-2.97815" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-3.85445" y1="0.88265" x2="-3.42265" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-4.47675" y1="0.88265" x2="-4.07035" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-5.11175" y1="0.88265" x2="-4.70535" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.88265" x2="-5.53085" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.88265" x2="-5.88645" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-6.69925" y1="0.88265" x2="-6.29285" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.88265" x2="-6.87705" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-7.41045" y1="0.88265" x2="-7.14375" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.88265" x2="-7.49935" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-8.14705" y1="0.88265" x2="-7.93115" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.88265" x2="-8.19785" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-8.85825" y1="0.88265" x2="-8.45185" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.88265" x2="-9.03605" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-9.83615" y1="0.88265" x2="-9.40435" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.88265" x2="-10.21715" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-11.16965" y1="0.88265" x2="-10.75055" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.88265" x2="-11.33475" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="-11.86815" y1="0.88265" x2="-11.58875" y2="0.89535" layer="21" rot="R180"/>
<rectangle x1="0.03175" y1="0.89535" x2="0.23495" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-0.18415" y1="0.89535" x2="0.00635" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-0.81915" y1="0.89535" x2="-0.42545" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-1.22555" y1="0.89535" x2="-1.00965" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.89535" x2="-1.28905" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-1.92405" y1="0.89535" x2="-1.54305" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-2.54635" y1="0.89535" x2="-2.15265" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.89535" x2="-2.71145" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-3.24485" y1="0.89535" x2="-2.99085" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-3.84175" y1="0.89535" x2="-3.44805" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-4.46405" y1="0.89535" x2="-4.09575" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-5.09905" y1="0.89535" x2="-4.71805" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.89535" x2="-5.53085" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.89535" x2="-5.87375" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-6.68655" y1="0.89535" x2="-6.30555" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.89535" x2="-6.87705" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-7.39775" y1="0.89535" x2="-7.14375" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.89535" x2="-7.49935" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-8.14705" y1="0.89535" x2="-7.93115" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.89535" x2="-8.21055" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="0.89535" x2="-8.46455" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.89535" x2="-9.03605" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-9.82345" y1="0.89535" x2="-9.41705" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.89535" x2="-10.21715" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-11.15695" y1="0.89535" x2="-10.76325" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.89535" x2="-11.33475" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="-11.86815" y1="0.89535" x2="-11.60145" y2="0.90805" layer="21" rot="R180"/>
<rectangle x1="0.03175" y1="0.90805" x2="0.23495" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-0.18415" y1="0.90805" x2="0.00635" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-0.79375" y1="0.90805" x2="-0.45085" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-1.22555" y1="0.90805" x2="-1.00965" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.90805" x2="-1.30175" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-1.91135" y1="0.90805" x2="-1.55575" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="0.90805" x2="-2.16535" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.90805" x2="-2.71145" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-3.23215" y1="0.90805" x2="-3.00355" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-3.82905" y1="0.90805" x2="-3.46075" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-4.45135" y1="0.90805" x2="-4.10845" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-5.08635" y1="0.90805" x2="-4.73075" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.90805" x2="-5.53085" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.90805" x2="-5.87375" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-6.67385" y1="0.90805" x2="-6.31825" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.90805" x2="-6.87705" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-7.39775" y1="0.90805" x2="-7.15645" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-7.85495" y1="0.90805" x2="-7.49935" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-8.14705" y1="0.90805" x2="-7.93115" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.90805" x2="-8.22325" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="0.90805" x2="-8.47725" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.90805" x2="-9.03605" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-9.79805" y1="0.90805" x2="-9.44245" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.90805" x2="-10.21715" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-11.14425" y1="0.90805" x2="-10.77595" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.90805" x2="-11.33475" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-11.85545" y1="0.90805" x2="-11.61415" y2="0.92075" layer="21" rot="R180"/>
<rectangle x1="-0.18415" y1="0.92075" x2="0.23495" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-0.76835" y1="0.92075" x2="-0.46355" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.92075" x2="-1.00965" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.92075" x2="-1.31445" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-1.88595" y1="0.92075" x2="-1.58115" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-2.50825" y1="0.92075" x2="-2.19075" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.92075" x2="-2.71145" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-3.21945" y1="0.92075" x2="-3.01625" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-3.80365" y1="0.92075" x2="-3.48615" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-4.42595" y1="0.92075" x2="-4.13385" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-5.06095" y1="0.92075" x2="-4.75615" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.92075" x2="-5.53085" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-6.11505" y1="0.92075" x2="-5.87375" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-6.64845" y1="0.92075" x2="-6.34365" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.92075" x2="-6.87705" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-7.38505" y1="0.92075" x2="-7.16915" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-7.79145" y1="0.92075" x2="-7.55015" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.92075" x2="-7.93115" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.92075" x2="-8.23595" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="0.92075" x2="-8.50265" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.92075" x2="-9.03605" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-9.78535" y1="0.92075" x2="-9.45515" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.92075" x2="-10.21715" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-11.11885" y1="0.92075" x2="-10.80135" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.92075" x2="-11.33475" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-11.84275" y1="0.92075" x2="-11.62685" y2="0.93345" layer="21" rot="R180"/>
<rectangle x1="-0.17145" y1="0.93345" x2="0.23495" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-0.74295" y1="0.93345" x2="-0.50165" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-1.23825" y1="0.93345" x2="-1.00965" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.93345" x2="-1.32715" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-1.84785" y1="0.93345" x2="-1.60655" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-2.48285" y1="0.93345" x2="-2.21615" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="0.93345" x2="-2.71145" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-3.19405" y1="0.93345" x2="-3.04165" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-3.77825" y1="0.93345" x2="-3.51155" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-4.40055" y1="0.93345" x2="-4.15925" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-5.03555" y1="0.93345" x2="-4.79425" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.93345" x2="-5.53085" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.93345" x2="-5.87375" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-6.61035" y1="0.93345" x2="-6.36905" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="0.93345" x2="-6.87705" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-7.35965" y1="0.93345" x2="-7.19455" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.93345" x2="-7.56285" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-8.15975" y1="0.93345" x2="-7.93115" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.93345" x2="-8.24865" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-8.76935" y1="0.93345" x2="-8.52805" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.93345" x2="-9.03605" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-9.74725" y1="0.93345" x2="-9.49325" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.93345" x2="-10.21715" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-11.09345" y1="0.93345" x2="-10.82675" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.93345" x2="-11.33475" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-11.81735" y1="0.93345" x2="-11.63955" y2="0.94615" layer="21" rot="R180"/>
<rectangle x1="-0.17145" y1="0.94615" x2="0.23495" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-0.69215" y1="0.94615" x2="-0.53975" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-1.39065" y1="0.94615" x2="-1.36525" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-1.80975" y1="0.94615" x2="-1.64465" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-2.43205" y1="0.94615" x2="-2.26695" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-3.16865" y1="0.94615" x2="-3.06705" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-3.72745" y1="0.94615" x2="-3.56235" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-4.34975" y1="0.94615" x2="-4.19735" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-4.98475" y1="0.94615" x2="-4.83235" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-5.75945" y1="0.94615" x2="-5.53085" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.94615" x2="-5.87375" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-6.57225" y1="0.94615" x2="-6.40715" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-7.33425" y1="0.94615" x2="-7.21995" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.94615" x2="-7.56285" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="0.94615" x2="-8.28675" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-8.73125" y1="0.94615" x2="-8.56615" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.94615" x2="-9.03605" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-9.69645" y1="0.94615" x2="-9.53135" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.94615" x2="-10.21715" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-11.04265" y1="0.94615" x2="-10.87755" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.94615" x2="-11.33475" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-11.79195" y1="0.94615" x2="-11.67765" y2="0.95885" layer="21" rot="R180"/>
<rectangle x1="-0.17145" y1="0.95885" x2="0.22225" y2="0.97155" layer="21" rot="R180"/>
<rectangle x1="-5.77215" y1="0.95885" x2="-5.53085" y2="0.97155" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.95885" x2="-5.87375" y2="0.97155" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.95885" x2="-7.56285" y2="0.97155" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.95885" x2="-9.03605" y2="0.97155" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.95885" x2="-10.21715" y2="0.97155" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.95885" x2="-11.33475" y2="0.97155" layer="21" rot="R180"/>
<rectangle x1="-0.17145" y1="0.97155" x2="0.22225" y2="0.98425" layer="21" rot="R180"/>
<rectangle x1="-5.77215" y1="0.97155" x2="-5.54355" y2="0.98425" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="0.97155" x2="-5.87375" y2="0.98425" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.97155" x2="-7.56285" y2="0.98425" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.97155" x2="-9.03605" y2="0.98425" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.97155" x2="-10.21715" y2="0.98425" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.97155" x2="-11.33475" y2="0.98425" layer="21" rot="R180"/>
<rectangle x1="-0.17145" y1="0.98425" x2="0.22225" y2="0.99695" layer="21" rot="R180"/>
<rectangle x1="-5.78485" y1="0.98425" x2="-5.54355" y2="0.99695" layer="21" rot="R180"/>
<rectangle x1="-6.08965" y1="0.98425" x2="-5.86105" y2="0.99695" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.98425" x2="-7.56285" y2="0.99695" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.98425" x2="-9.03605" y2="0.99695" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.98425" x2="-10.21715" y2="0.99695" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.98425" x2="-11.33475" y2="0.99695" layer="21" rot="R180"/>
<rectangle x1="-0.17145" y1="0.99695" x2="0.22225" y2="1.00965" layer="21" rot="R180"/>
<rectangle x1="-5.79755" y1="0.99695" x2="-5.54355" y2="1.00965" layer="21" rot="R180"/>
<rectangle x1="-6.08965" y1="0.99695" x2="-5.83565" y2="1.00965" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="0.99695" x2="-7.56285" y2="1.00965" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="0.99695" x2="-9.03605" y2="1.00965" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="0.99695" x2="-10.21715" y2="1.00965" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="0.99695" x2="-11.33475" y2="1.00965" layer="21" rot="R180"/>
<rectangle x1="-0.15875" y1="1.00965" x2="0.22225" y2="1.02235" layer="21" rot="R180"/>
<rectangle x1="-6.08965" y1="1.00965" x2="-5.55625" y2="1.02235" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="1.00965" x2="-7.56285" y2="1.02235" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.00965" x2="-9.03605" y2="1.02235" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.00965" x2="-10.21715" y2="1.02235" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.00965" x2="-11.33475" y2="1.02235" layer="21" rot="R180"/>
<rectangle x1="-0.15875" y1="1.02235" x2="0.22225" y2="1.03505" layer="21" rot="R180"/>
<rectangle x1="-6.07695" y1="1.02235" x2="-5.56895" y2="1.03505" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="1.02235" x2="-7.56285" y2="1.03505" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.02235" x2="-9.03605" y2="1.03505" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.02235" x2="-10.21715" y2="1.03505" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.02235" x2="-11.33475" y2="1.03505" layer="21" rot="R180"/>
<rectangle x1="-0.15875" y1="1.03505" x2="0.22225" y2="1.04775" layer="21" rot="R180"/>
<rectangle x1="-6.07695" y1="1.03505" x2="-5.56895" y2="1.04775" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="1.03505" x2="-7.56285" y2="1.04775" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.03505" x2="-9.03605" y2="1.04775" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.03505" x2="-10.21715" y2="1.04775" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.03505" x2="-11.33475" y2="1.04775" layer="21" rot="R180"/>
<rectangle x1="-0.15875" y1="1.04775" x2="0.20955" y2="1.06045" layer="21" rot="R180"/>
<rectangle x1="-6.06425" y1="1.04775" x2="-5.58165" y2="1.06045" layer="21" rot="R180"/>
<rectangle x1="-7.77875" y1="1.04775" x2="-7.56285" y2="1.06045" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.04775" x2="-9.03605" y2="1.06045" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.04775" x2="-10.21715" y2="1.06045" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.04775" x2="-11.33475" y2="1.06045" layer="21" rot="R180"/>
<rectangle x1="-0.15875" y1="1.06045" x2="0.20955" y2="1.07315" layer="21" rot="R180"/>
<rectangle x1="-6.05155" y1="1.06045" x2="-5.59435" y2="1.07315" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.06045" x2="-9.03605" y2="1.07315" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.06045" x2="-10.21715" y2="1.07315" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.06045" x2="-11.33475" y2="1.07315" layer="21" rot="R180"/>
<rectangle x1="-0.15875" y1="1.07315" x2="0.20955" y2="1.08585" layer="21" rot="R180"/>
<rectangle x1="-6.03885" y1="1.07315" x2="-5.60705" y2="1.08585" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.07315" x2="-9.03605" y2="1.08585" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.07315" x2="-10.21715" y2="1.08585" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.07315" x2="-11.33475" y2="1.08585" layer="21" rot="R180"/>
<rectangle x1="-0.15875" y1="1.08585" x2="0.20955" y2="1.09855" layer="21" rot="R180"/>
<rectangle x1="-6.01345" y1="1.08585" x2="-5.61975" y2="1.09855" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.08585" x2="-9.03605" y2="1.09855" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.08585" x2="-10.21715" y2="1.09855" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.08585" x2="-11.33475" y2="1.09855" layer="21" rot="R180"/>
<rectangle x1="-0.14605" y1="1.09855" x2="0.20955" y2="1.11125" layer="21" rot="R180"/>
<rectangle x1="-6.00075" y1="1.09855" x2="-5.64515" y2="1.11125" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.09855" x2="-9.03605" y2="1.11125" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.09855" x2="-10.21715" y2="1.11125" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.09855" x2="-11.33475" y2="1.11125" layer="21" rot="R180"/>
<rectangle x1="-0.14605" y1="1.11125" x2="0.20955" y2="1.12395" layer="21" rot="R180"/>
<rectangle x1="-5.97535" y1="1.11125" x2="-5.67055" y2="1.12395" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="1.11125" x2="-9.03605" y2="1.12395" layer="21" rot="R180"/>
<rectangle x1="-10.45845" y1="1.11125" x2="-10.21715" y2="1.12395" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="1.11125" x2="-11.33475" y2="1.12395" layer="21" rot="R180"/>
<rectangle x1="-5.93725" y1="1.12395" x2="-5.69595" y2="1.13665" layer="21" rot="R180"/>
<rectangle x1="-5.88645" y1="1.13665" x2="-5.74675" y2="1.14935" layer="21" rot="R180"/>
<rectangle x1="-3.20675" y1="1.64465" x2="0.09525" y2="1.65735" layer="21" rot="R180"/>
<rectangle x1="-3.24485" y1="1.65735" x2="0.13335" y2="1.67005" layer="21" rot="R180"/>
<rectangle x1="-3.28295" y1="1.67005" x2="0.15875" y2="1.68275" layer="21" rot="R180"/>
<rectangle x1="-11.65225" y1="1.67005" x2="-8.36295" y2="1.68275" layer="21" rot="R180"/>
<rectangle x1="-3.30835" y1="1.68275" x2="0.17145" y2="1.69545" layer="21" rot="R180"/>
<rectangle x1="-11.69035" y1="1.68275" x2="-8.31215" y2="1.69545" layer="21" rot="R180"/>
<rectangle x1="-3.32105" y1="1.69545" x2="0.18415" y2="1.70815" layer="21" rot="R180"/>
<rectangle x1="-11.71575" y1="1.69545" x2="-8.28675" y2="1.70815" layer="21" rot="R180"/>
<rectangle x1="-3.34645" y1="1.70815" x2="0.18415" y2="1.72085" layer="21" rot="R180"/>
<rectangle x1="-11.72845" y1="1.70815" x2="-8.26135" y2="1.72085" layer="21" rot="R180"/>
<rectangle x1="-3.35915" y1="1.72085" x2="0.19685" y2="1.73355" layer="21" rot="R180"/>
<rectangle x1="-5.98805" y1="1.72085" x2="-5.74675" y2="1.73355" layer="21" rot="R180"/>
<rectangle x1="-11.74115" y1="1.72085" x2="-8.23595" y2="1.73355" layer="21" rot="R180"/>
<rectangle x1="-3.37185" y1="1.73355" x2="0.19685" y2="1.74625" layer="21" rot="R180"/>
<rectangle x1="-6.10235" y1="1.73355" x2="-5.61975" y2="1.74625" layer="21" rot="R180"/>
<rectangle x1="-11.75385" y1="1.73355" x2="-8.21055" y2="1.74625" layer="21" rot="R180"/>
<rectangle x1="-3.39725" y1="1.74625" x2="0.19685" y2="1.75895" layer="21" rot="R180"/>
<rectangle x1="-6.17855" y1="1.74625" x2="-5.54355" y2="1.75895" layer="21" rot="R180"/>
<rectangle x1="-11.75385" y1="1.74625" x2="-8.19785" y2="1.75895" layer="21" rot="R180"/>
<rectangle x1="-3.40995" y1="1.75895" x2="0.19685" y2="1.77165" layer="21" rot="R180"/>
<rectangle x1="-6.24205" y1="1.75895" x2="-5.48005" y2="1.77165" layer="21" rot="R180"/>
<rectangle x1="-11.75385" y1="1.75895" x2="-8.18515" y2="1.77165" layer="21" rot="R180"/>
<rectangle x1="-3.40995" y1="1.77165" x2="0.19685" y2="1.78435" layer="21" rot="R180"/>
<rectangle x1="-6.29285" y1="1.77165" x2="-5.42925" y2="1.78435" layer="21" rot="R180"/>
<rectangle x1="-11.76655" y1="1.77165" x2="-8.17245" y2="1.78435" layer="21" rot="R180"/>
<rectangle x1="-3.42265" y1="1.78435" x2="0.19685" y2="1.79705" layer="21" rot="R180"/>
<rectangle x1="-6.34365" y1="1.78435" x2="-5.37845" y2="1.79705" layer="21" rot="R180"/>
<rectangle x1="-11.76655" y1="1.78435" x2="-8.15975" y2="1.79705" layer="21" rot="R180"/>
<rectangle x1="-3.43535" y1="1.79705" x2="0.19685" y2="1.80975" layer="21" rot="R180"/>
<rectangle x1="-6.38175" y1="1.79705" x2="-5.34035" y2="1.80975" layer="21" rot="R180"/>
<rectangle x1="-11.76655" y1="1.79705" x2="-8.14705" y2="1.80975" layer="21" rot="R180"/>
<rectangle x1="-3.44805" y1="1.80975" x2="0.19685" y2="1.82245" layer="21" rot="R180"/>
<rectangle x1="-6.41985" y1="1.80975" x2="-5.30225" y2="1.82245" layer="21" rot="R180"/>
<rectangle x1="-11.76655" y1="1.80975" x2="-8.13435" y2="1.82245" layer="21" rot="R180"/>
<rectangle x1="-3.46075" y1="1.82245" x2="0.18415" y2="1.83515" layer="21" rot="R180"/>
<rectangle x1="-6.45795" y1="1.82245" x2="-5.26415" y2="1.83515" layer="21" rot="R180"/>
<rectangle x1="-11.75385" y1="1.82245" x2="-8.12165" y2="1.83515" layer="21" rot="R180"/>
<rectangle x1="-3.46075" y1="1.83515" x2="0.18415" y2="1.84785" layer="21" rot="R180"/>
<rectangle x1="-6.49605" y1="1.83515" x2="-5.22605" y2="1.84785" layer="21" rot="R180"/>
<rectangle x1="-11.75385" y1="1.83515" x2="-8.10895" y2="1.84785" layer="21" rot="R180"/>
<rectangle x1="-3.47345" y1="1.84785" x2="0.17145" y2="1.86055" layer="21" rot="R180"/>
<rectangle x1="-6.53415" y1="1.84785" x2="-5.20065" y2="1.86055" layer="21" rot="R180"/>
<rectangle x1="-11.75385" y1="1.84785" x2="-8.10895" y2="1.86055" layer="21" rot="R180"/>
<rectangle x1="-2.64795" y1="1.86055" x2="0.17145" y2="1.87325" layer="21" rot="R180"/>
<rectangle x1="-3.47345" y1="1.86055" x2="-2.88925" y2="1.87325" layer="21" rot="R180"/>
<rectangle x1="-6.55955" y1="1.86055" x2="-5.16255" y2="1.87325" layer="21" rot="R180"/>
<rectangle x1="-11.74115" y1="1.86055" x2="-8.09625" y2="1.87325" layer="21" rot="R180"/>
<rectangle x1="-2.58445" y1="1.87325" x2="0.15875" y2="1.88595" layer="21" rot="R180"/>
<rectangle x1="-3.48615" y1="1.87325" x2="-2.95275" y2="1.88595" layer="21" rot="R180"/>
<rectangle x1="-6.58495" y1="1.87325" x2="-5.13715" y2="1.88595" layer="21" rot="R180"/>
<rectangle x1="-11.74115" y1="1.87325" x2="-8.09625" y2="1.88595" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="1.88595" x2="0.14605" y2="1.89865" layer="21" rot="R180"/>
<rectangle x1="-3.48615" y1="1.88595" x2="-2.99085" y2="1.89865" layer="21" rot="R180"/>
<rectangle x1="-6.61035" y1="1.88595" x2="-5.11175" y2="1.89865" layer="21" rot="R180"/>
<rectangle x1="-11.72845" y1="1.88595" x2="-8.08355" y2="1.89865" layer="21" rot="R180"/>
<rectangle x1="-2.48285" y1="1.89865" x2="0.13335" y2="1.91135" layer="21" rot="R180"/>
<rectangle x1="-3.49885" y1="1.89865" x2="-3.01625" y2="1.91135" layer="21" rot="R180"/>
<rectangle x1="-6.63575" y1="1.89865" x2="-5.08635" y2="1.91135" layer="21" rot="R180"/>
<rectangle x1="-11.71575" y1="1.89865" x2="-8.08355" y2="1.91135" layer="21" rot="R180"/>
<rectangle x1="-2.48285" y1="1.91135" x2="0.12065" y2="1.92405" layer="21" rot="R180"/>
<rectangle x1="-3.49885" y1="1.91135" x2="-3.04165" y2="1.92405" layer="21" rot="R180"/>
<rectangle x1="-6.66115" y1="1.91135" x2="-5.06095" y2="1.92405" layer="21" rot="R180"/>
<rectangle x1="-11.70305" y1="1.91135" x2="-8.07085" y2="1.92405" layer="21" rot="R180"/>
<rectangle x1="-2.48285" y1="1.92405" x2="0.10795" y2="1.93675" layer="21" rot="R180"/>
<rectangle x1="-3.49885" y1="1.92405" x2="-3.06705" y2="1.93675" layer="21" rot="R180"/>
<rectangle x1="-6.68655" y1="1.92405" x2="-5.03555" y2="1.93675" layer="21" rot="R180"/>
<rectangle x1="-11.70305" y1="1.92405" x2="-8.07085" y2="1.93675" layer="21" rot="R180"/>
<rectangle x1="-2.48285" y1="1.93675" x2="0.09525" y2="1.94945" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="1.93675" x2="-3.09245" y2="1.94945" layer="21" rot="R180"/>
<rectangle x1="-6.71195" y1="1.93675" x2="-5.01015" y2="1.94945" layer="21" rot="R180"/>
<rectangle x1="-11.69035" y1="1.93675" x2="-8.05815" y2="1.94945" layer="21" rot="R180"/>
<rectangle x1="-2.48285" y1="1.94945" x2="0.08255" y2="1.96215" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="1.94945" x2="-3.09245" y2="1.96215" layer="21" rot="R180"/>
<rectangle x1="-6.73735" y1="1.94945" x2="-4.98475" y2="1.96215" layer="21" rot="R180"/>
<rectangle x1="-11.67765" y1="1.94945" x2="-8.05815" y2="1.96215" layer="21" rot="R180"/>
<rectangle x1="-2.49555" y1="1.96215" x2="0.06985" y2="1.97485" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="1.96215" x2="-3.07975" y2="1.97485" layer="21" rot="R180"/>
<rectangle x1="-6.76275" y1="1.96215" x2="-4.97205" y2="1.97485" layer="21" rot="R180"/>
<rectangle x1="-11.66495" y1="1.96215" x2="-8.05815" y2="1.97485" layer="21" rot="R180"/>
<rectangle x1="-2.30505" y1="1.97485" x2="0.05715" y2="1.98755" layer="21" rot="R180"/>
<rectangle x1="-2.49555" y1="1.97485" x2="-2.34315" y2="1.98755" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="1.97485" x2="-3.06705" y2="1.98755" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="1.97485" x2="-4.94665" y2="1.98755" layer="21" rot="R180"/>
<rectangle x1="-11.65225" y1="1.97485" x2="-8.05815" y2="1.98755" layer="21" rot="R180"/>
<rectangle x1="-2.27965" y1="1.98755" x2="0.04445" y2="2.00025" layer="21" rot="R180"/>
<rectangle x1="-2.49555" y1="1.98755" x2="-2.34315" y2="2.00025" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="1.98755" x2="-3.05435" y2="2.00025" layer="21" rot="R180"/>
<rectangle x1="-6.80085" y1="1.98755" x2="-4.92125" y2="2.00025" layer="21" rot="R180"/>
<rectangle x1="-11.63955" y1="1.98755" x2="-8.04545" y2="2.00025" layer="21" rot="R180"/>
<rectangle x1="-2.25425" y1="2.00025" x2="0.03175" y2="2.01295" layer="21" rot="R180"/>
<rectangle x1="-2.50825" y1="2.00025" x2="-2.34315" y2="2.01295" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.00025" x2="-3.04165" y2="2.01295" layer="21" rot="R180"/>
<rectangle x1="-6.81355" y1="2.00025" x2="-4.90855" y2="2.01295" layer="21" rot="R180"/>
<rectangle x1="-11.62685" y1="2.00025" x2="-8.04545" y2="2.01295" layer="21" rot="R180"/>
<rectangle x1="-2.22885" y1="2.01295" x2="0.01905" y2="2.02565" layer="21" rot="R180"/>
<rectangle x1="-2.50825" y1="2.01295" x2="-2.35585" y2="2.02565" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.01295" x2="-3.02895" y2="2.02565" layer="21" rot="R180"/>
<rectangle x1="-6.83895" y1="2.01295" x2="-4.88315" y2="2.02565" layer="21" rot="R180"/>
<rectangle x1="-11.61415" y1="2.01295" x2="-8.04545" y2="2.02565" layer="21" rot="R180"/>
<rectangle x1="-2.20345" y1="2.02565" x2="0.00635" y2="2.03835" layer="21" rot="R180"/>
<rectangle x1="-2.50825" y1="2.02565" x2="-2.35585" y2="2.03835" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.02565" x2="-3.01625" y2="2.03835" layer="21" rot="R180"/>
<rectangle x1="-6.85165" y1="2.02565" x2="-4.87045" y2="2.03835" layer="21" rot="R180"/>
<rectangle x1="-11.60145" y1="2.02565" x2="-8.04545" y2="2.03835" layer="21" rot="R180"/>
<rectangle x1="-2.19075" y1="2.03835" x2="-0.00635" y2="2.05105" layer="21" rot="R180"/>
<rectangle x1="-2.50825" y1="2.03835" x2="-2.35585" y2="2.05105" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.03835" x2="-3.00355" y2="2.05105" layer="21" rot="R180"/>
<rectangle x1="-6.87705" y1="2.03835" x2="-4.84505" y2="2.05105" layer="21" rot="R180"/>
<rectangle x1="-11.58875" y1="2.03835" x2="-8.04545" y2="2.05105" layer="21" rot="R180"/>
<rectangle x1="-2.16535" y1="2.05105" x2="-0.01905" y2="2.06375" layer="21" rot="R180"/>
<rectangle x1="-2.52095" y1="2.05105" x2="-2.35585" y2="2.06375" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.05105" x2="-2.99085" y2="2.06375" layer="21" rot="R180"/>
<rectangle x1="-6.88975" y1="2.05105" x2="-4.83235" y2="2.06375" layer="21" rot="R180"/>
<rectangle x1="-11.57605" y1="2.05105" x2="-8.04545" y2="2.06375" layer="21" rot="R180"/>
<rectangle x1="-2.13995" y1="2.06375" x2="-0.03175" y2="2.07645" layer="21" rot="R180"/>
<rectangle x1="-2.52095" y1="2.06375" x2="-2.36855" y2="2.07645" layer="21" rot="R180"/>
<rectangle x1="-3.19405" y1="2.06375" x2="-2.97815" y2="2.07645" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.06375" x2="-3.21945" y2="2.07645" layer="21" rot="R180"/>
<rectangle x1="-6.91515" y1="2.06375" x2="-4.81965" y2="2.07645" layer="21" rot="R180"/>
<rectangle x1="-8.62965" y1="2.06375" x2="-8.04545" y2="2.07645" layer="21" rot="R180"/>
<rectangle x1="-11.56335" y1="2.06375" x2="-8.64235" y2="2.07645" layer="21" rot="R180"/>
<rectangle x1="-2.12725" y1="2.07645" x2="-0.04445" y2="2.08915" layer="21" rot="R180"/>
<rectangle x1="-2.52095" y1="2.07645" x2="-2.36855" y2="2.08915" layer="21" rot="R180"/>
<rectangle x1="-3.18135" y1="2.07645" x2="-2.96545" y2="2.08915" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.07645" x2="-3.23215" y2="2.08915" layer="21" rot="R180"/>
<rectangle x1="-6.92785" y1="2.07645" x2="-4.79425" y2="2.08915" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="2.07645" x2="-8.04545" y2="2.08915" layer="21" rot="R180"/>
<rectangle x1="-11.55065" y1="2.07645" x2="-8.65505" y2="2.08915" layer="21" rot="R180"/>
<rectangle x1="-2.10185" y1="2.08915" x2="-0.05715" y2="2.10185" layer="21" rot="R180"/>
<rectangle x1="-2.52095" y1="2.08915" x2="-2.36855" y2="2.10185" layer="21" rot="R180"/>
<rectangle x1="-3.16865" y1="2.08915" x2="-2.95275" y2="2.10185" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.08915" x2="-3.23215" y2="2.10185" layer="21" rot="R180"/>
<rectangle x1="-6.94055" y1="2.08915" x2="-4.78155" y2="2.10185" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="2.08915" x2="-8.04545" y2="2.10185" layer="21" rot="R180"/>
<rectangle x1="-11.53795" y1="2.08915" x2="-8.66775" y2="2.10185" layer="21" rot="R180"/>
<rectangle x1="-2.08915" y1="2.10185" x2="-0.06985" y2="2.11455" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="2.10185" x2="-2.38125" y2="2.11455" layer="21" rot="R180"/>
<rectangle x1="-3.14325" y1="2.10185" x2="-2.94005" y2="2.11455" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.10185" x2="-3.24485" y2="2.11455" layer="21" rot="R180"/>
<rectangle x1="-6.95325" y1="2.10185" x2="-4.76885" y2="2.11455" layer="21" rot="R180"/>
<rectangle x1="-8.59155" y1="2.10185" x2="-8.04545" y2="2.11455" layer="21" rot="R180"/>
<rectangle x1="-11.52525" y1="2.10185" x2="-8.68045" y2="2.11455" layer="21" rot="R180"/>
<rectangle x1="-2.07645" y1="2.11455" x2="-0.08255" y2="2.12725" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="2.11455" x2="-2.38125" y2="2.12725" layer="21" rot="R180"/>
<rectangle x1="-3.13055" y1="2.11455" x2="-2.92735" y2="2.12725" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.11455" x2="-3.25755" y2="2.12725" layer="21" rot="R180"/>
<rectangle x1="-6.97865" y1="2.11455" x2="-4.74345" y2="2.12725" layer="21" rot="R180"/>
<rectangle x1="-8.57885" y1="2.11455" x2="-8.04545" y2="2.12725" layer="21" rot="R180"/>
<rectangle x1="-11.51255" y1="2.11455" x2="-8.69315" y2="2.12725" layer="21" rot="R180"/>
<rectangle x1="-2.07645" y1="2.12725" x2="-0.09525" y2="2.13995" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="2.12725" x2="-2.38125" y2="2.13995" layer="21" rot="R180"/>
<rectangle x1="-3.11785" y1="2.12725" x2="-2.91465" y2="2.13995" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.12725" x2="-3.25755" y2="2.13995" layer="21" rot="R180"/>
<rectangle x1="-6.99135" y1="2.12725" x2="-4.73075" y2="2.13995" layer="21" rot="R180"/>
<rectangle x1="-8.56615" y1="2.12725" x2="-8.04545" y2="2.13995" layer="21" rot="R180"/>
<rectangle x1="-11.49985" y1="2.12725" x2="-8.70585" y2="2.13995" layer="21" rot="R180"/>
<rectangle x1="-2.08915" y1="2.13995" x2="-0.10795" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-2.53365" y1="2.13995" x2="-2.38125" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-3.10515" y1="2.13995" x2="-2.90195" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.13995" x2="-3.25755" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-7.00405" y1="2.13995" x2="-4.71805" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-8.55345" y1="2.13995" x2="-8.04545" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="2.13995" x2="-8.71855" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-11.48715" y1="2.13995" x2="-8.90905" y2="2.15265" layer="21" rot="R180"/>
<rectangle x1="-2.10185" y1="2.15265" x2="-0.12065" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-2.54635" y1="2.15265" x2="-2.39395" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-3.09245" y1="2.15265" x2="-2.88925" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.15265" x2="-3.27025" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-7.01675" y1="2.15265" x2="-4.70535" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="2.15265" x2="-8.04545" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="2.15265" x2="-8.73125" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-11.47445" y1="2.15265" x2="-8.93445" y2="2.16535" layer="21" rot="R180"/>
<rectangle x1="-2.11455" y1="2.16535" x2="-0.13335" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-2.54635" y1="2.16535" x2="-2.39395" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-3.07975" y1="2.16535" x2="-2.87655" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.16535" x2="-3.27025" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-7.02945" y1="2.16535" x2="-4.69265" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-8.52805" y1="2.16535" x2="-8.04545" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-8.76935" y1="2.16535" x2="-8.74395" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-9.09955" y1="2.16535" x2="-8.93445" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-11.46175" y1="2.16535" x2="-9.20115" y2="2.17805" layer="21" rot="R180"/>
<rectangle x1="-2.12725" y1="2.17805" x2="-0.14605" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-2.54635" y1="2.17805" x2="-2.39395" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-3.06705" y1="2.17805" x2="-2.86385" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.17805" x2="-3.28295" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-7.04215" y1="2.17805" x2="-4.67995" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-8.51535" y1="2.17805" x2="-8.04545" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-9.09955" y1="2.17805" x2="-8.93445" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-11.44905" y1="2.17805" x2="-9.25195" y2="2.19075" layer="21" rot="R180"/>
<rectangle x1="-2.13995" y1="2.19075" x2="-0.15875" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-2.55905" y1="2.19075" x2="-2.39395" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-3.05435" y1="2.19075" x2="-2.85115" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.19075" x2="-3.28295" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-7.06755" y1="2.19075" x2="-4.66725" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-8.50265" y1="2.19075" x2="-8.04545" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-9.09955" y1="2.19075" x2="-8.92175" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-11.43635" y1="2.19075" x2="-9.27735" y2="2.20345" layer="21" rot="R180"/>
<rectangle x1="-2.15265" y1="2.20345" x2="-0.17145" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-2.55905" y1="2.20345" x2="-2.40665" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-3.04165" y1="2.20345" x2="-2.83845" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.20345" x2="-3.28295" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-7.08025" y1="2.20345" x2="-4.65455" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-8.48995" y1="2.20345" x2="-8.04545" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-9.09955" y1="2.20345" x2="-8.90905" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-11.42365" y1="2.20345" x2="-9.27735" y2="2.21615" layer="21" rot="R180"/>
<rectangle x1="-2.16535" y1="2.21615" x2="-0.18415" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-2.55905" y1="2.21615" x2="-2.40665" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-3.02895" y1="2.21615" x2="-2.82575" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.21615" x2="-3.29565" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-7.09295" y1="2.21615" x2="-4.64185" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.21615" x2="-8.04545" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-9.12495" y1="2.21615" x2="-8.82015" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-11.41095" y1="2.21615" x2="-9.27735" y2="2.22885" layer="21" rot="R180"/>
<rectangle x1="-2.17805" y1="2.22885" x2="-0.19685" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-2.55905" y1="2.22885" x2="-2.40665" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-3.01625" y1="2.22885" x2="-2.81305" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.22885" x2="-3.29565" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="2.22885" x2="-4.62915" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-8.46455" y1="2.22885" x2="-8.04545" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-9.17575" y1="2.22885" x2="-8.80745" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-11.39825" y1="2.22885" x2="-9.27735" y2="2.24155" layer="21" rot="R180"/>
<rectangle x1="-2.19075" y1="2.24155" x2="-0.20955" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-2.57175" y1="2.24155" x2="-2.40665" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-3.00355" y1="2.24155" x2="-2.80035" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.24155" x2="-3.29565" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-7.11835" y1="2.24155" x2="-4.61645" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-8.45185" y1="2.24155" x2="-8.04545" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-9.23925" y1="2.24155" x2="-8.82015" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-11.38555" y1="2.24155" x2="-9.25195" y2="2.25425" layer="21" rot="R180"/>
<rectangle x1="-2.20345" y1="2.25425" x2="-0.22225" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-2.57175" y1="2.25425" x2="-2.41935" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-2.99085" y1="2.25425" x2="-2.78765" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.25425" x2="-3.29565" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-7.13105" y1="2.25425" x2="-4.60375" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-8.43915" y1="2.25425" x2="-8.04545" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-9.44245" y1="2.25425" x2="-8.83285" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-11.37285" y1="2.25425" x2="-9.48055" y2="2.26695" layer="21" rot="R180"/>
<rectangle x1="-2.21615" y1="2.26695" x2="-0.23495" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-2.67335" y1="2.26695" x2="-2.41935" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-2.97815" y1="2.26695" x2="-2.77495" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.26695" x2="-3.30835" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-7.14375" y1="2.26695" x2="-4.59105" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-8.42645" y1="2.26695" x2="-8.04545" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-9.42975" y1="2.26695" x2="-8.84555" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-11.36015" y1="2.26695" x2="-9.51865" y2="2.27965" layer="21" rot="R180"/>
<rectangle x1="-2.22885" y1="2.27965" x2="-0.24765" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-2.71145" y1="2.27965" x2="-2.40665" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-2.96545" y1="2.27965" x2="-2.76225" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.27965" x2="-3.30835" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-7.15645" y1="2.27965" x2="-4.57835" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-8.41375" y1="2.27965" x2="-8.04545" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-9.42975" y1="2.27965" x2="-8.85825" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-11.34745" y1="2.27965" x2="-9.54405" y2="2.29235" layer="21" rot="R180"/>
<rectangle x1="-2.24155" y1="2.29235" x2="-0.26035" y2="2.30505" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="2.29235" x2="-2.36855" y2="2.30505" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.29235" x2="-3.30835" y2="2.30505" layer="21" rot="R180"/>
<rectangle x1="-7.15645" y1="2.29235" x2="-4.56565" y2="2.30505" layer="21" rot="R180"/>
<rectangle x1="-8.40105" y1="2.29235" x2="-8.04545" y2="2.30505" layer="21" rot="R180"/>
<rectangle x1="-9.42975" y1="2.29235" x2="-8.87095" y2="2.30505" layer="21" rot="R180"/>
<rectangle x1="-11.33475" y1="2.29235" x2="-9.58215" y2="2.30505" layer="21" rot="R180"/>
<rectangle x1="-2.25425" y1="2.30505" x2="-0.27305" y2="2.31775" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="2.30505" x2="-2.33045" y2="2.31775" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.30505" x2="-3.30835" y2="2.31775" layer="21" rot="R180"/>
<rectangle x1="-7.16915" y1="2.30505" x2="-4.55295" y2="2.31775" layer="21" rot="R180"/>
<rectangle x1="-8.38835" y1="2.30505" x2="-8.04545" y2="2.31775" layer="21" rot="R180"/>
<rectangle x1="-9.44245" y1="2.30505" x2="-8.88365" y2="2.31775" layer="21" rot="R180"/>
<rectangle x1="-11.32205" y1="2.30505" x2="-9.59485" y2="2.31775" layer="21" rot="R180"/>
<rectangle x1="-2.26695" y1="2.31775" x2="-0.28575" y2="2.33045" layer="21" rot="R180"/>
<rectangle x1="-2.92735" y1="2.31775" x2="-2.29235" y2="2.33045" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.31775" x2="-3.30835" y2="2.33045" layer="21" rot="R180"/>
<rectangle x1="-7.18185" y1="2.31775" x2="-4.54025" y2="2.33045" layer="21" rot="R180"/>
<rectangle x1="-8.37565" y1="2.31775" x2="-8.04545" y2="2.33045" layer="21" rot="R180"/>
<rectangle x1="-9.46785" y1="2.31775" x2="-8.89635" y2="2.33045" layer="21" rot="R180"/>
<rectangle x1="-11.30935" y1="2.31775" x2="-9.60755" y2="2.33045" layer="21" rot="R180"/>
<rectangle x1="-2.91465" y1="2.33045" x2="-0.29845" y2="2.34315" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.33045" x2="-3.30835" y2="2.34315" layer="21" rot="R180"/>
<rectangle x1="-7.19455" y1="2.33045" x2="-4.52755" y2="2.34315" layer="21" rot="R180"/>
<rectangle x1="-8.37565" y1="2.33045" x2="-8.04545" y2="2.34315" layer="21" rot="R180"/>
<rectangle x1="-9.49325" y1="2.33045" x2="-8.89635" y2="2.34315" layer="21" rot="R180"/>
<rectangle x1="-11.29665" y1="2.33045" x2="-9.60755" y2="2.34315" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.34315" x2="-0.31115" y2="2.35585" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.34315" x2="-3.30835" y2="2.35585" layer="21" rot="R180"/>
<rectangle x1="-7.20725" y1="2.34315" x2="-4.51485" y2="2.35585" layer="21" rot="R180"/>
<rectangle x1="-8.38835" y1="2.34315" x2="-8.04545" y2="2.35585" layer="21" rot="R180"/>
<rectangle x1="-9.51865" y1="2.34315" x2="-8.88365" y2="2.35585" layer="21" rot="R180"/>
<rectangle x1="-11.28395" y1="2.34315" x2="-9.59485" y2="2.35585" layer="21" rot="R180"/>
<rectangle x1="-2.88925" y1="2.35585" x2="-0.32385" y2="2.36855" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.35585" x2="-3.30835" y2="2.36855" layer="21" rot="R180"/>
<rectangle x1="-7.21995" y1="2.35585" x2="-4.50215" y2="2.36855" layer="21" rot="R180"/>
<rectangle x1="-8.40105" y1="2.35585" x2="-8.04545" y2="2.36855" layer="21" rot="R180"/>
<rectangle x1="-9.55675" y1="2.35585" x2="-8.87095" y2="2.36855" layer="21" rot="R180"/>
<rectangle x1="-11.27125" y1="2.35585" x2="-9.58215" y2="2.36855" layer="21" rot="R180"/>
<rectangle x1="-2.87655" y1="2.36855" x2="-0.33655" y2="2.38125" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.36855" x2="-3.30835" y2="2.38125" layer="21" rot="R180"/>
<rectangle x1="-7.23265" y1="2.36855" x2="-4.50215" y2="2.38125" layer="21" rot="R180"/>
<rectangle x1="-8.41375" y1="2.36855" x2="-8.04545" y2="2.38125" layer="21" rot="R180"/>
<rectangle x1="-11.25855" y1="2.36855" x2="-8.85825" y2="2.38125" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.38125" x2="-0.34925" y2="2.39395" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.38125" x2="-3.30835" y2="2.39395" layer="21" rot="R180"/>
<rectangle x1="-7.23265" y1="2.38125" x2="-4.48945" y2="2.39395" layer="21" rot="R180"/>
<rectangle x1="-8.42645" y1="2.38125" x2="-8.04545" y2="2.39395" layer="21" rot="R180"/>
<rectangle x1="-11.24585" y1="2.38125" x2="-8.84555" y2="2.39395" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.39395" x2="-0.36195" y2="2.40665" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.39395" x2="-3.30835" y2="2.40665" layer="21" rot="R180"/>
<rectangle x1="-5.70865" y1="2.39395" x2="-4.47675" y2="2.40665" layer="21" rot="R180"/>
<rectangle x1="-7.24535" y1="2.39395" x2="-6.01345" y2="2.40665" layer="21" rot="R180"/>
<rectangle x1="-8.43915" y1="2.39395" x2="-8.04545" y2="2.40665" layer="21" rot="R180"/>
<rectangle x1="-9.77265" y1="2.39395" x2="-8.83285" y2="2.40665" layer="21" rot="R180"/>
<rectangle x1="-11.23315" y1="2.39395" x2="-9.78535" y2="2.40665" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.40665" x2="-0.37465" y2="2.41935" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.40665" x2="-3.30835" y2="2.41935" layer="21" rot="R180"/>
<rectangle x1="-5.63245" y1="2.40665" x2="-4.46405" y2="2.41935" layer="21" rot="R180"/>
<rectangle x1="-7.25805" y1="2.40665" x2="-6.08965" y2="2.41935" layer="21" rot="R180"/>
<rectangle x1="-8.45185" y1="2.40665" x2="-8.04545" y2="2.41935" layer="21" rot="R180"/>
<rectangle x1="-9.74725" y1="2.40665" x2="-8.82015" y2="2.41935" layer="21" rot="R180"/>
<rectangle x1="-11.22045" y1="2.40665" x2="-9.81075" y2="2.41935" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.41935" x2="-0.38735" y2="2.43205" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.41935" x2="-3.30835" y2="2.43205" layer="21" rot="R180"/>
<rectangle x1="-5.58165" y1="2.41935" x2="-4.45135" y2="2.43205" layer="21" rot="R180"/>
<rectangle x1="-7.27075" y1="2.41935" x2="-6.14045" y2="2.43205" layer="21" rot="R180"/>
<rectangle x1="-8.46455" y1="2.41935" x2="-8.04545" y2="2.43205" layer="21" rot="R180"/>
<rectangle x1="-9.74725" y1="2.41935" x2="-8.80745" y2="2.43205" layer="21" rot="R180"/>
<rectangle x1="-11.20775" y1="2.41935" x2="-9.83615" y2="2.43205" layer="21" rot="R180"/>
<rectangle x1="-2.87655" y1="2.43205" x2="-0.40005" y2="2.44475" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.43205" x2="-3.30835" y2="2.44475" layer="21" rot="R180"/>
<rectangle x1="-5.54355" y1="2.43205" x2="-4.45135" y2="2.44475" layer="21" rot="R180"/>
<rectangle x1="-7.27075" y1="2.43205" x2="-6.19125" y2="2.44475" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.43205" x2="-8.04545" y2="2.44475" layer="21" rot="R180"/>
<rectangle x1="-9.74725" y1="2.43205" x2="-8.79475" y2="2.44475" layer="21" rot="R180"/>
<rectangle x1="-11.19505" y1="2.43205" x2="-9.86155" y2="2.44475" layer="21" rot="R180"/>
<rectangle x1="-2.87655" y1="2.44475" x2="-0.41275" y2="2.45745" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.44475" x2="-3.30835" y2="2.45745" layer="21" rot="R180"/>
<rectangle x1="-5.50545" y1="2.44475" x2="-4.43865" y2="2.45745" layer="21" rot="R180"/>
<rectangle x1="-7.28345" y1="2.44475" x2="-6.21665" y2="2.45745" layer="21" rot="R180"/>
<rectangle x1="-8.48995" y1="2.44475" x2="-8.04545" y2="2.45745" layer="21" rot="R180"/>
<rectangle x1="-9.74725" y1="2.44475" x2="-8.78205" y2="2.45745" layer="21" rot="R180"/>
<rectangle x1="-11.18235" y1="2.44475" x2="-9.88695" y2="2.45745" layer="21" rot="R180"/>
<rectangle x1="-2.88925" y1="2.45745" x2="-0.42545" y2="2.47015" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.45745" x2="-3.30835" y2="2.47015" layer="21" rot="R180"/>
<rectangle x1="-5.46735" y1="2.45745" x2="-4.42595" y2="2.47015" layer="21" rot="R180"/>
<rectangle x1="-7.29615" y1="2.45745" x2="-6.25475" y2="2.47015" layer="21" rot="R180"/>
<rectangle x1="-8.50265" y1="2.45745" x2="-8.04545" y2="2.47015" layer="21" rot="R180"/>
<rectangle x1="-9.75995" y1="2.45745" x2="-8.76935" y2="2.47015" layer="21" rot="R180"/>
<rectangle x1="-11.16965" y1="2.45745" x2="-9.89965" y2="2.47015" layer="21" rot="R180"/>
<rectangle x1="-2.88925" y1="2.47015" x2="-0.43815" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.47015" x2="-3.30835" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-5.44195" y1="2.47015" x2="-4.42595" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="2.47015" x2="-6.29285" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-8.51535" y1="2.47015" x2="-8.04545" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-9.78535" y1="2.47015" x2="-8.75665" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-10.31875" y1="2.47015" x2="-9.91235" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-11.15695" y1="2.47015" x2="-10.57275" y2="2.48285" layer="21" rot="R180"/>
<rectangle x1="-2.88925" y1="2.48285" x2="-0.45085" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.48285" x2="-3.30835" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-5.40385" y1="2.48285" x2="-4.41325" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="2.48285" x2="-6.31825" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-8.52805" y1="2.48285" x2="-8.04545" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-9.81075" y1="2.48285" x2="-8.74395" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-10.21715" y1="2.48285" x2="-9.91235" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-11.14425" y1="2.48285" x2="-10.68705" y2="2.49555" layer="21" rot="R180"/>
<rectangle x1="-2.88925" y1="2.49555" x2="-0.46355" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.49555" x2="-3.30835" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-5.37845" y1="2.49555" x2="-4.40055" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-7.32155" y1="2.49555" x2="-6.34365" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="2.49555" x2="-8.04545" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-9.83615" y1="2.49555" x2="-8.73125" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-10.14095" y1="2.49555" x2="-9.91235" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-11.13155" y1="2.49555" x2="-10.76325" y2="2.50825" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.50825" x2="-0.47625" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.50825" x2="-3.30835" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-5.35305" y1="2.50825" x2="-4.40055" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-7.32155" y1="2.50825" x2="-6.36905" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-8.55345" y1="2.50825" x2="-8.04545" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-9.84885" y1="2.50825" x2="-8.71855" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-10.07745" y1="2.50825" x2="-9.89965" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-11.11885" y1="2.50825" x2="-10.82675" y2="2.52095" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.52095" x2="-0.48895" y2="2.53365" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.52095" x2="-3.30835" y2="2.53365" layer="21" rot="R180"/>
<rectangle x1="-5.32765" y1="2.52095" x2="-4.38785" y2="2.53365" layer="21" rot="R180"/>
<rectangle x1="-7.33425" y1="2.52095" x2="-6.39445" y2="2.53365" layer="21" rot="R180"/>
<rectangle x1="-8.56615" y1="2.52095" x2="-8.04545" y2="2.53365" layer="21" rot="R180"/>
<rectangle x1="-10.02665" y1="2.52095" x2="-8.70585" y2="2.53365" layer="21" rot="R180"/>
<rectangle x1="-11.10615" y1="2.52095" x2="-10.87755" y2="2.53365" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.53365" x2="-0.50165" y2="2.54635" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.53365" x2="-3.29565" y2="2.54635" layer="21" rot="R180"/>
<rectangle x1="-5.31495" y1="2.53365" x2="-4.37515" y2="2.54635" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="2.53365" x2="-6.40715" y2="2.54635" layer="21" rot="R180"/>
<rectangle x1="-8.57885" y1="2.53365" x2="-8.04545" y2="2.54635" layer="21" rot="R180"/>
<rectangle x1="-9.97585" y1="2.53365" x2="-8.69315" y2="2.54635" layer="21" rot="R180"/>
<rectangle x1="-11.09345" y1="2.53365" x2="-10.92835" y2="2.54635" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.54635" x2="-0.51435" y2="2.55905" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.54635" x2="-3.29565" y2="2.55905" layer="21" rot="R180"/>
<rectangle x1="-5.28955" y1="2.54635" x2="-4.37515" y2="2.55905" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="2.54635" x2="-6.43255" y2="2.55905" layer="21" rot="R180"/>
<rectangle x1="-8.59155" y1="2.54635" x2="-8.04545" y2="2.55905" layer="21" rot="R180"/>
<rectangle x1="-9.93775" y1="2.54635" x2="-8.68045" y2="2.55905" layer="21" rot="R180"/>
<rectangle x1="-11.08075" y1="2.54635" x2="-10.96645" y2="2.55905" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.55905" x2="-0.52705" y2="2.57175" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.55905" x2="-3.29565" y2="2.57175" layer="21" rot="R180"/>
<rectangle x1="-5.26415" y1="2.55905" x2="-4.36245" y2="2.57175" layer="21" rot="R180"/>
<rectangle x1="-7.35965" y1="2.55905" x2="-6.45795" y2="2.57175" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="2.55905" x2="-8.04545" y2="2.57175" layer="21" rot="R180"/>
<rectangle x1="-9.89965" y1="2.55905" x2="-8.66775" y2="2.57175" layer="21" rot="R180"/>
<rectangle x1="-11.06805" y1="2.55905" x2="-11.00455" y2="2.57175" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.57175" x2="-0.53975" y2="2.58445" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.57175" x2="-3.29565" y2="2.58445" layer="21" rot="R180"/>
<rectangle x1="-5.25145" y1="2.57175" x2="-4.34975" y2="2.58445" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="2.57175" x2="-6.47065" y2="2.58445" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="2.57175" x2="-8.04545" y2="2.58445" layer="21" rot="R180"/>
<rectangle x1="-9.86155" y1="2.57175" x2="-8.65505" y2="2.58445" layer="21" rot="R180"/>
<rectangle x1="-11.05535" y1="2.57175" x2="-11.04265" y2="2.58445" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.58445" x2="-0.55245" y2="2.59715" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.58445" x2="-3.29565" y2="2.59715" layer="21" rot="R180"/>
<rectangle x1="-5.22605" y1="2.58445" x2="-4.34975" y2="2.59715" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="2.58445" x2="-6.49605" y2="2.59715" layer="21" rot="R180"/>
<rectangle x1="-8.62965" y1="2.58445" x2="-8.04545" y2="2.59715" layer="21" rot="R180"/>
<rectangle x1="-9.83615" y1="2.58445" x2="-8.64235" y2="2.59715" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.59715" x2="-0.56515" y2="2.60985" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.59715" x2="-3.28295" y2="2.60985" layer="21" rot="R180"/>
<rectangle x1="-5.21335" y1="2.59715" x2="-4.33705" y2="2.60985" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="2.59715" x2="-6.50875" y2="2.60985" layer="21" rot="R180"/>
<rectangle x1="-9.79805" y1="2.59715" x2="-8.04545" y2="2.60985" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="2.60985" x2="-0.57785" y2="2.62255" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.60985" x2="-3.28295" y2="2.62255" layer="21" rot="R180"/>
<rectangle x1="-5.20065" y1="2.60985" x2="-4.33705" y2="2.62255" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="2.60985" x2="-6.52145" y2="2.62255" layer="21" rot="R180"/>
<rectangle x1="-9.77265" y1="2.60985" x2="-8.04545" y2="2.62255" layer="21" rot="R180"/>
<rectangle x1="-2.99085" y1="2.62255" x2="-0.59055" y2="2.63525" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.62255" x2="-3.28295" y2="2.63525" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="2.62255" x2="-4.32435" y2="2.63525" layer="21" rot="R180"/>
<rectangle x1="-7.33425" y1="2.62255" x2="-6.54685" y2="2.63525" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.62255" x2="-8.04545" y2="2.63525" layer="21" rot="R180"/>
<rectangle x1="-9.74725" y1="2.62255" x2="-8.52805" y2="2.63525" layer="21" rot="R180"/>
<rectangle x1="-3.04165" y1="2.63525" x2="-0.60325" y2="2.64795" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.63525" x2="-3.28295" y2="2.64795" layer="21" rot="R180"/>
<rectangle x1="-5.16255" y1="2.63525" x2="-4.32435" y2="2.64795" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="2.63525" x2="-6.55955" y2="2.64795" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.63525" x2="-8.04545" y2="2.64795" layer="21" rot="R180"/>
<rectangle x1="-9.72185" y1="2.63525" x2="-8.54075" y2="2.64795" layer="21" rot="R180"/>
<rectangle x1="-3.09245" y1="2.64795" x2="-0.61595" y2="2.66065" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.64795" x2="-3.27025" y2="2.66065" layer="21" rot="R180"/>
<rectangle x1="-5.14985" y1="2.64795" x2="-4.31165" y2="2.66065" layer="21" rot="R180"/>
<rectangle x1="-7.28345" y1="2.64795" x2="-6.57225" y2="2.66065" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.64795" x2="-8.04545" y2="2.66065" layer="21" rot="R180"/>
<rectangle x1="-9.69645" y1="2.64795" x2="-8.54075" y2="2.66065" layer="21" rot="R180"/>
<rectangle x1="-3.14325" y1="2.66065" x2="-0.62865" y2="2.67335" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.66065" x2="-3.27025" y2="2.67335" layer="21" rot="R180"/>
<rectangle x1="-5.13715" y1="2.66065" x2="-4.29895" y2="2.67335" layer="21" rot="R180"/>
<rectangle x1="-7.25805" y1="2.66065" x2="-6.58495" y2="2.67335" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.66065" x2="-8.04545" y2="2.67335" layer="21" rot="R180"/>
<rectangle x1="-9.67105" y1="2.66065" x2="-8.54075" y2="2.67335" layer="21" rot="R180"/>
<rectangle x1="-3.18135" y1="2.67335" x2="-0.64135" y2="2.68605" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.67335" x2="-3.27025" y2="2.68605" layer="21" rot="R180"/>
<rectangle x1="-5.12445" y1="2.67335" x2="-4.29895" y2="2.68605" layer="21" rot="R180"/>
<rectangle x1="-7.24535" y1="2.67335" x2="-6.59765" y2="2.68605" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.67335" x2="-8.04545" y2="2.68605" layer="21" rot="R180"/>
<rectangle x1="-9.64565" y1="2.67335" x2="-8.54075" y2="2.68605" layer="21" rot="R180"/>
<rectangle x1="-3.23215" y1="2.68605" x2="-0.65405" y2="2.69875" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.68605" x2="-3.25755" y2="2.69875" layer="21" rot="R180"/>
<rectangle x1="-5.11175" y1="2.68605" x2="-4.28625" y2="2.69875" layer="21" rot="R180"/>
<rectangle x1="-7.21995" y1="2.68605" x2="-6.61035" y2="2.69875" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.68605" x2="-8.04545" y2="2.69875" layer="21" rot="R180"/>
<rectangle x1="-9.62025" y1="2.68605" x2="-8.54075" y2="2.69875" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.69875" x2="-0.66675" y2="2.71145" layer="21" rot="R180"/>
<rectangle x1="-5.09905" y1="2.69875" x2="-4.28625" y2="2.71145" layer="21" rot="R180"/>
<rectangle x1="-7.19455" y1="2.69875" x2="-6.62305" y2="2.71145" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.69875" x2="-8.04545" y2="2.71145" layer="21" rot="R180"/>
<rectangle x1="-9.59485" y1="2.69875" x2="-8.54075" y2="2.71145" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.71145" x2="-0.67945" y2="2.72415" layer="21" rot="R180"/>
<rectangle x1="-5.08635" y1="2.71145" x2="-4.27355" y2="2.72415" layer="21" rot="R180"/>
<rectangle x1="-7.18185" y1="2.71145" x2="-6.63575" y2="2.72415" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.71145" x2="-8.04545" y2="2.72415" layer="21" rot="R180"/>
<rectangle x1="-9.58215" y1="2.71145" x2="-8.54075" y2="2.72415" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.72415" x2="-0.69215" y2="2.73685" layer="21" rot="R180"/>
<rectangle x1="-5.07365" y1="2.72415" x2="-4.27355" y2="2.73685" layer="21" rot="R180"/>
<rectangle x1="-7.15645" y1="2.72415" x2="-6.64845" y2="2.73685" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.72415" x2="-8.04545" y2="2.73685" layer="21" rot="R180"/>
<rectangle x1="-9.55675" y1="2.72415" x2="-8.54075" y2="2.73685" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.73685" x2="-0.70485" y2="2.74955" layer="21" rot="R180"/>
<rectangle x1="-5.06095" y1="2.73685" x2="-4.27355" y2="2.74955" layer="21" rot="R180"/>
<rectangle x1="-7.13105" y1="2.73685" x2="-6.66115" y2="2.74955" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.73685" x2="-8.04545" y2="2.74955" layer="21" rot="R180"/>
<rectangle x1="-9.54405" y1="2.73685" x2="-8.54075" y2="2.74955" layer="21" rot="R180"/>
<rectangle x1="-2.87655" y1="2.74955" x2="-0.71755" y2="2.76225" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.74955" x2="-2.90195" y2="2.76225" layer="21" rot="R180"/>
<rectangle x1="-5.04825" y1="2.74955" x2="-4.26085" y2="2.76225" layer="21" rot="R180"/>
<rectangle x1="-7.10565" y1="2.74955" x2="-6.67385" y2="2.76225" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.74955" x2="-8.04545" y2="2.76225" layer="21" rot="R180"/>
<rectangle x1="-9.51865" y1="2.74955" x2="-8.55345" y2="2.76225" layer="21" rot="R180"/>
<rectangle x1="-2.87655" y1="2.76225" x2="-0.73025" y2="2.77495" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.76225" x2="-2.94005" y2="2.77495" layer="21" rot="R180"/>
<rectangle x1="-5.03555" y1="2.76225" x2="-4.26085" y2="2.77495" layer="21" rot="R180"/>
<rectangle x1="-7.09295" y1="2.76225" x2="-6.68655" y2="2.77495" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="2.76225" x2="-8.04545" y2="2.77495" layer="21" rot="R180"/>
<rectangle x1="-9.50595" y1="2.76225" x2="-8.55345" y2="2.77495" layer="21" rot="R180"/>
<rectangle x1="-2.87655" y1="2.77495" x2="-0.74295" y2="2.78765" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.77495" x2="-2.99085" y2="2.78765" layer="21" rot="R180"/>
<rectangle x1="-5.02285" y1="2.77495" x2="-4.24815" y2="2.78765" layer="21" rot="R180"/>
<rectangle x1="-7.06755" y1="2.77495" x2="-6.69925" y2="2.78765" layer="21" rot="R180"/>
<rectangle x1="-8.48995" y1="2.77495" x2="-8.04545" y2="2.78765" layer="21" rot="R180"/>
<rectangle x1="-9.48055" y1="2.77495" x2="-8.54075" y2="2.78765" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.78765" x2="-0.75565" y2="2.80035" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.78765" x2="-3.04165" y2="2.80035" layer="21" rot="R180"/>
<rectangle x1="-5.01015" y1="2.78765" x2="-4.24815" y2="2.80035" layer="21" rot="R180"/>
<rectangle x1="-7.04215" y1="2.78765" x2="-6.71195" y2="2.80035" layer="21" rot="R180"/>
<rectangle x1="-8.50265" y1="2.78765" x2="-8.04545" y2="2.80035" layer="21" rot="R180"/>
<rectangle x1="-9.46785" y1="2.78765" x2="-8.52805" y2="2.80035" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.80035" x2="-0.76835" y2="2.81305" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.80035" x2="-3.07975" y2="2.81305" layer="21" rot="R180"/>
<rectangle x1="-5.01015" y1="2.80035" x2="-4.23545" y2="2.81305" layer="21" rot="R180"/>
<rectangle x1="-7.02945" y1="2.80035" x2="-6.72465" y2="2.81305" layer="21" rot="R180"/>
<rectangle x1="-9.45515" y1="2.80035" x2="-8.04545" y2="2.81305" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.81305" x2="-0.78105" y2="2.82575" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.81305" x2="-3.13055" y2="2.82575" layer="21" rot="R180"/>
<rectangle x1="-4.99745" y1="2.81305" x2="-4.23545" y2="2.82575" layer="21" rot="R180"/>
<rectangle x1="-7.00405" y1="2.81305" x2="-6.72465" y2="2.82575" layer="21" rot="R180"/>
<rectangle x1="-9.42975" y1="2.81305" x2="-8.04545" y2="2.82575" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.82575" x2="-0.79375" y2="2.83845" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.82575" x2="-3.18135" y2="2.83845" layer="21" rot="R180"/>
<rectangle x1="-4.98475" y1="2.82575" x2="-4.23545" y2="2.83845" layer="21" rot="R180"/>
<rectangle x1="-6.97865" y1="2.82575" x2="-6.73735" y2="2.83845" layer="21" rot="R180"/>
<rectangle x1="-9.41705" y1="2.82575" x2="-8.04545" y2="2.83845" layer="21" rot="R180"/>
<rectangle x1="-2.85115" y1="2.83845" x2="-0.80645" y2="2.85115" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.83845" x2="-3.19405" y2="2.85115" layer="21" rot="R180"/>
<rectangle x1="-4.97205" y1="2.83845" x2="-4.22275" y2="2.85115" layer="21" rot="R180"/>
<rectangle x1="-6.95325" y1="2.83845" x2="-6.75005" y2="2.85115" layer="21" rot="R180"/>
<rectangle x1="-9.40435" y1="2.83845" x2="-8.04545" y2="2.85115" layer="21" rot="R180"/>
<rectangle x1="-2.85115" y1="2.85115" x2="-0.81915" y2="2.86385" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.85115" x2="-3.19405" y2="2.86385" layer="21" rot="R180"/>
<rectangle x1="-4.97205" y1="2.85115" x2="-4.22275" y2="2.86385" layer="21" rot="R180"/>
<rectangle x1="-6.94055" y1="2.85115" x2="-6.75005" y2="2.86385" layer="21" rot="R180"/>
<rectangle x1="-9.39165" y1="2.85115" x2="-8.04545" y2="2.86385" layer="21" rot="R180"/>
<rectangle x1="-2.83845" y1="2.86385" x2="-0.83185" y2="2.87655" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.86385" x2="-3.19405" y2="2.87655" layer="21" rot="R180"/>
<rectangle x1="-4.95935" y1="2.86385" x2="-4.21005" y2="2.87655" layer="21" rot="R180"/>
<rectangle x1="-6.91515" y1="2.86385" x2="-6.76275" y2="2.87655" layer="21" rot="R180"/>
<rectangle x1="-9.36625" y1="2.86385" x2="-8.04545" y2="2.87655" layer="21" rot="R180"/>
<rectangle x1="-2.83845" y1="2.87655" x2="-0.84455" y2="2.88925" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.87655" x2="-3.18135" y2="2.88925" layer="21" rot="R180"/>
<rectangle x1="-4.94665" y1="2.87655" x2="-4.21005" y2="2.88925" layer="21" rot="R180"/>
<rectangle x1="-6.88975" y1="2.87655" x2="-6.77545" y2="2.88925" layer="21" rot="R180"/>
<rectangle x1="-9.35355" y1="2.87655" x2="-8.04545" y2="2.88925" layer="21" rot="R180"/>
<rectangle x1="-2.83845" y1="2.88925" x2="-0.85725" y2="2.90195" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.88925" x2="-3.18135" y2="2.90195" layer="21" rot="R180"/>
<rectangle x1="-4.94665" y1="2.88925" x2="-4.21005" y2="2.90195" layer="21" rot="R180"/>
<rectangle x1="-6.86435" y1="2.88925" x2="-6.77545" y2="2.90195" layer="21" rot="R180"/>
<rectangle x1="-9.34085" y1="2.88925" x2="-8.04545" y2="2.90195" layer="21" rot="R180"/>
<rectangle x1="-2.85115" y1="2.90195" x2="-0.86995" y2="2.91465" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.90195" x2="-3.16865" y2="2.91465" layer="21" rot="R180"/>
<rectangle x1="-4.93395" y1="2.90195" x2="-4.19735" y2="2.91465" layer="21" rot="R180"/>
<rectangle x1="-6.85165" y1="2.90195" x2="-6.78815" y2="2.91465" layer="21" rot="R180"/>
<rectangle x1="-9.32815" y1="2.90195" x2="-8.04545" y2="2.91465" layer="21" rot="R180"/>
<rectangle x1="-2.86385" y1="2.91465" x2="-0.88265" y2="2.92735" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.91465" x2="-3.16865" y2="2.92735" layer="21" rot="R180"/>
<rectangle x1="-4.92125" y1="2.91465" x2="-4.19735" y2="2.92735" layer="21" rot="R180"/>
<rectangle x1="-6.82625" y1="2.91465" x2="-6.80085" y2="2.92735" layer="21" rot="R180"/>
<rectangle x1="-9.31545" y1="2.91465" x2="-8.04545" y2="2.92735" layer="21" rot="R180"/>
<rectangle x1="-2.87655" y1="2.92735" x2="-0.89535" y2="2.94005" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.92735" x2="-3.15595" y2="2.94005" layer="21" rot="R180"/>
<rectangle x1="-4.92125" y1="2.92735" x2="-4.19735" y2="2.94005" layer="21" rot="R180"/>
<rectangle x1="-9.30275" y1="2.92735" x2="-8.04545" y2="2.94005" layer="21" rot="R180"/>
<rectangle x1="-2.88925" y1="2.94005" x2="-0.90805" y2="2.95275" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.94005" x2="-3.14325" y2="2.95275" layer="21" rot="R180"/>
<rectangle x1="-4.90855" y1="2.94005" x2="-4.18465" y2="2.95275" layer="21" rot="R180"/>
<rectangle x1="-9.29005" y1="2.94005" x2="-8.04545" y2="2.95275" layer="21" rot="R180"/>
<rectangle x1="-2.90195" y1="2.95275" x2="-0.92075" y2="2.96545" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.95275" x2="-3.14325" y2="2.96545" layer="21" rot="R180"/>
<rectangle x1="-4.90855" y1="2.95275" x2="-4.18465" y2="2.96545" layer="21" rot="R180"/>
<rectangle x1="-8.55345" y1="2.95275" x2="-8.04545" y2="2.96545" layer="21" rot="R180"/>
<rectangle x1="-9.27735" y1="2.95275" x2="-8.57885" y2="2.96545" layer="21" rot="R180"/>
<rectangle x1="-2.91465" y1="2.96545" x2="-0.93345" y2="2.97815" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.96545" x2="-3.13055" y2="2.97815" layer="21" rot="R180"/>
<rectangle x1="-4.89585" y1="2.96545" x2="-4.18465" y2="2.97815" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="2.96545" x2="-8.04545" y2="2.97815" layer="21" rot="R180"/>
<rectangle x1="-9.26465" y1="2.96545" x2="-8.59155" y2="2.97815" layer="21" rot="R180"/>
<rectangle x1="-2.92735" y1="2.97815" x2="-0.94615" y2="2.99085" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.97815" x2="-3.13055" y2="2.99085" layer="21" rot="R180"/>
<rectangle x1="-4.89585" y1="2.97815" x2="-4.17195" y2="2.99085" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="2.97815" x2="-8.04545" y2="2.99085" layer="21" rot="R180"/>
<rectangle x1="-9.25195" y1="2.97815" x2="-8.60425" y2="2.99085" layer="21" rot="R180"/>
<rectangle x1="-2.94005" y1="2.99085" x2="-0.95885" y2="3.00355" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="2.99085" x2="-3.11785" y2="3.00355" layer="21" rot="R180"/>
<rectangle x1="-4.88315" y1="2.99085" x2="-4.17195" y2="3.00355" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="2.99085" x2="-8.04545" y2="3.00355" layer="21" rot="R180"/>
<rectangle x1="-9.23925" y1="2.99085" x2="-8.60425" y2="3.00355" layer="21" rot="R180"/>
<rectangle x1="-2.95275" y1="3.00355" x2="-0.97155" y2="3.01625" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.00355" x2="-3.10515" y2="3.01625" layer="21" rot="R180"/>
<rectangle x1="-4.88315" y1="3.00355" x2="-4.17195" y2="3.01625" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="3.00355" x2="-8.04545" y2="3.01625" layer="21" rot="R180"/>
<rectangle x1="-9.22655" y1="3.00355" x2="-8.60425" y2="3.01625" layer="21" rot="R180"/>
<rectangle x1="-2.96545" y1="3.01625" x2="-0.98425" y2="3.02895" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.01625" x2="-3.10515" y2="3.02895" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="3.01625" x2="-4.17195" y2="3.02895" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="3.01625" x2="-8.04545" y2="3.02895" layer="21" rot="R180"/>
<rectangle x1="-9.21385" y1="3.01625" x2="-8.61695" y2="3.02895" layer="21" rot="R180"/>
<rectangle x1="-2.97815" y1="3.02895" x2="-0.99695" y2="3.04165" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.02895" x2="-3.09245" y2="3.04165" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="3.02895" x2="-4.15925" y2="3.04165" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="3.02895" x2="-8.04545" y2="3.04165" layer="21" rot="R180"/>
<rectangle x1="-9.20115" y1="3.02895" x2="-8.61695" y2="3.04165" layer="21" rot="R180"/>
<rectangle x1="-2.99085" y1="3.04165" x2="-1.00965" y2="3.05435" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.04165" x2="-3.07975" y2="3.05435" layer="21" rot="R180"/>
<rectangle x1="-4.85775" y1="3.04165" x2="-4.15925" y2="3.05435" layer="21" rot="R180"/>
<rectangle x1="-8.55345" y1="3.04165" x2="-8.04545" y2="3.05435" layer="21" rot="R180"/>
<rectangle x1="-9.18845" y1="3.04165" x2="-8.61695" y2="3.05435" layer="21" rot="R180"/>
<rectangle x1="-3.00355" y1="3.05435" x2="-1.02235" y2="3.06705" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.05435" x2="-3.07975" y2="3.06705" layer="21" rot="R180"/>
<rectangle x1="-4.85775" y1="3.05435" x2="-4.15925" y2="3.06705" layer="21" rot="R180"/>
<rectangle x1="-8.55345" y1="3.05435" x2="-8.04545" y2="3.06705" layer="21" rot="R180"/>
<rectangle x1="-9.17575" y1="3.05435" x2="-8.62965" y2="3.06705" layer="21" rot="R180"/>
<rectangle x1="-3.01625" y1="3.06705" x2="-1.03505" y2="3.07975" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.06705" x2="-3.06705" y2="3.07975" layer="21" rot="R180"/>
<rectangle x1="-4.85775" y1="3.06705" x2="-4.15925" y2="3.07975" layer="21" rot="R180"/>
<rectangle x1="-8.55345" y1="3.06705" x2="-8.04545" y2="3.07975" layer="21" rot="R180"/>
<rectangle x1="-9.17575" y1="3.06705" x2="-8.62965" y2="3.07975" layer="21" rot="R180"/>
<rectangle x1="-3.02895" y1="3.07975" x2="-1.04775" y2="3.09245" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.07975" x2="-3.05435" y2="3.09245" layer="21" rot="R180"/>
<rectangle x1="-4.84505" y1="3.07975" x2="-4.14655" y2="3.09245" layer="21" rot="R180"/>
<rectangle x1="-8.56615" y1="3.07975" x2="-8.04545" y2="3.09245" layer="21" rot="R180"/>
<rectangle x1="-9.16305" y1="3.07975" x2="-8.62965" y2="3.09245" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.09245" x2="-1.06045" y2="3.10515" layer="21" rot="R180"/>
<rectangle x1="-4.84505" y1="3.09245" x2="-4.14655" y2="3.10515" layer="21" rot="R180"/>
<rectangle x1="-8.56615" y1="3.09245" x2="-8.04545" y2="3.10515" layer="21" rot="R180"/>
<rectangle x1="-9.15035" y1="3.09245" x2="-8.64235" y2="3.10515" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.10515" x2="-1.07315" y2="3.11785" layer="21" rot="R180"/>
<rectangle x1="-4.83235" y1="3.10515" x2="-4.14655" y2="3.11785" layer="21" rot="R180"/>
<rectangle x1="-8.57885" y1="3.10515" x2="-8.04545" y2="3.11785" layer="21" rot="R180"/>
<rectangle x1="-9.13765" y1="3.10515" x2="-8.62965" y2="3.11785" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.11785" x2="-1.08585" y2="3.13055" layer="21" rot="R180"/>
<rectangle x1="-4.83235" y1="3.11785" x2="-4.14655" y2="3.13055" layer="21" rot="R180"/>
<rectangle x1="-8.57885" y1="3.11785" x2="-8.04545" y2="3.13055" layer="21" rot="R180"/>
<rectangle x1="-9.12495" y1="3.11785" x2="-8.62965" y2="3.13055" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.13055" x2="-1.09855" y2="3.14325" layer="21" rot="R180"/>
<rectangle x1="-4.83235" y1="3.13055" x2="-4.14655" y2="3.14325" layer="21" rot="R180"/>
<rectangle x1="-9.12495" y1="3.13055" x2="-8.04545" y2="3.14325" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.14325" x2="-1.11125" y2="3.15595" layer="21" rot="R180"/>
<rectangle x1="-4.81965" y1="3.14325" x2="-4.13385" y2="3.15595" layer="21" rot="R180"/>
<rectangle x1="-9.11225" y1="3.14325" x2="-8.04545" y2="3.15595" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.15595" x2="-1.12395" y2="3.16865" layer="21" rot="R180"/>
<rectangle x1="-4.81965" y1="3.15595" x2="-4.13385" y2="3.16865" layer="21" rot="R180"/>
<rectangle x1="-9.09955" y1="3.15595" x2="-8.04545" y2="3.16865" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.16865" x2="-1.13665" y2="3.18135" layer="21" rot="R180"/>
<rectangle x1="-4.81965" y1="3.16865" x2="-4.13385" y2="3.18135" layer="21" rot="R180"/>
<rectangle x1="-9.08685" y1="3.16865" x2="-8.04545" y2="3.18135" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.18135" x2="-1.14935" y2="3.19405" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.18135" x2="-4.13385" y2="3.19405" layer="21" rot="R180"/>
<rectangle x1="-9.08685" y1="3.18135" x2="-8.04545" y2="3.19405" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.19405" x2="-1.16205" y2="3.20675" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.19405" x2="-4.13385" y2="3.20675" layer="21" rot="R180"/>
<rectangle x1="-9.07415" y1="3.19405" x2="-8.04545" y2="3.20675" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.20675" x2="-1.17475" y2="3.21945" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.20675" x2="-4.12115" y2="3.21945" layer="21" rot="R180"/>
<rectangle x1="-9.06145" y1="3.20675" x2="-8.04545" y2="3.21945" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.21945" x2="-1.18745" y2="3.23215" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.21945" x2="-4.12115" y2="3.23215" layer="21" rot="R180"/>
<rectangle x1="-9.06145" y1="3.21945" x2="-8.04545" y2="3.23215" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.23215" x2="-1.20015" y2="3.24485" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.23215" x2="-4.12115" y2="3.24485" layer="21" rot="R180"/>
<rectangle x1="-9.04875" y1="3.23215" x2="-8.04545" y2="3.24485" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.24485" x2="-1.21285" y2="3.25755" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.24485" x2="-4.12115" y2="3.25755" layer="21" rot="R180"/>
<rectangle x1="-9.03605" y1="3.24485" x2="-8.04545" y2="3.25755" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.25755" x2="-1.22555" y2="3.27025" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.25755" x2="-4.12115" y2="3.27025" layer="21" rot="R180"/>
<rectangle x1="-9.03605" y1="3.25755" x2="-8.04545" y2="3.27025" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.27025" x2="-1.23825" y2="3.28295" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.27025" x2="-4.12115" y2="3.28295" layer="21" rot="R180"/>
<rectangle x1="-9.02335" y1="3.27025" x2="-8.04545" y2="3.28295" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.28295" x2="-1.25095" y2="3.29565" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.28295" x2="-4.12115" y2="3.29565" layer="21" rot="R180"/>
<rectangle x1="-8.66775" y1="3.28295" x2="-8.04545" y2="3.29565" layer="21" rot="R180"/>
<rectangle x1="-9.01065" y1="3.28295" x2="-8.71855" y2="3.29565" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.29565" x2="-1.26365" y2="3.30835" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.29565" x2="-4.12115" y2="3.30835" layer="21" rot="R180"/>
<rectangle x1="-8.66775" y1="3.29565" x2="-8.04545" y2="3.30835" layer="21" rot="R180"/>
<rectangle x1="-9.01065" y1="3.29565" x2="-8.73125" y2="3.30835" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.30835" x2="-1.27635" y2="3.32105" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.30835" x2="-4.12115" y2="3.32105" layer="21" rot="R180"/>
<rectangle x1="-8.65505" y1="3.30835" x2="-8.04545" y2="3.32105" layer="21" rot="R180"/>
<rectangle x1="-8.99795" y1="3.30835" x2="-8.73125" y2="3.32105" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.32105" x2="-1.28905" y2="3.33375" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.32105" x2="-4.12115" y2="3.33375" layer="21" rot="R180"/>
<rectangle x1="-8.66775" y1="3.32105" x2="-8.04545" y2="3.33375" layer="21" rot="R180"/>
<rectangle x1="-8.99795" y1="3.32105" x2="-8.74395" y2="3.33375" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.33375" x2="-1.30175" y2="3.34645" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.33375" x2="-4.10845" y2="3.34645" layer="21" rot="R180"/>
<rectangle x1="-8.66775" y1="3.33375" x2="-8.04545" y2="3.34645" layer="21" rot="R180"/>
<rectangle x1="-8.98525" y1="3.33375" x2="-8.74395" y2="3.34645" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.34645" x2="-1.31445" y2="3.35915" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.34645" x2="-4.10845" y2="3.35915" layer="21" rot="R180"/>
<rectangle x1="-8.68045" y1="3.34645" x2="-8.04545" y2="3.35915" layer="21" rot="R180"/>
<rectangle x1="-8.98525" y1="3.34645" x2="-8.75665" y2="3.35915" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.35915" x2="-1.32715" y2="3.37185" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.35915" x2="-4.10845" y2="3.37185" layer="21" rot="R180"/>
<rectangle x1="-8.68045" y1="3.35915" x2="-8.04545" y2="3.37185" layer="21" rot="R180"/>
<rectangle x1="-8.97255" y1="3.35915" x2="-8.75665" y2="3.37185" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.37185" x2="-1.33985" y2="3.38455" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.37185" x2="-4.10845" y2="3.38455" layer="21" rot="R180"/>
<rectangle x1="-8.69315" y1="3.37185" x2="-8.04545" y2="3.38455" layer="21" rot="R180"/>
<rectangle x1="-8.97255" y1="3.37185" x2="-8.76935" y2="3.38455" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.38455" x2="-1.35255" y2="3.39725" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.38455" x2="-4.10845" y2="3.39725" layer="21" rot="R180"/>
<rectangle x1="-8.69315" y1="3.38455" x2="-8.04545" y2="3.39725" layer="21" rot="R180"/>
<rectangle x1="-8.95985" y1="3.38455" x2="-8.76935" y2="3.39725" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.39725" x2="-1.36525" y2="3.40995" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.39725" x2="-4.10845" y2="3.40995" layer="21" rot="R180"/>
<rectangle x1="-8.70585" y1="3.39725" x2="-8.04545" y2="3.40995" layer="21" rot="R180"/>
<rectangle x1="-8.95985" y1="3.39725" x2="-8.78205" y2="3.40995" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.40995" x2="-1.37795" y2="3.42265" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.40995" x2="-4.10845" y2="3.42265" layer="21" rot="R180"/>
<rectangle x1="-8.70585" y1="3.40995" x2="-8.04545" y2="3.42265" layer="21" rot="R180"/>
<rectangle x1="-8.94715" y1="3.40995" x2="-8.78205" y2="3.42265" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.42265" x2="-1.39065" y2="3.43535" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.42265" x2="-4.10845" y2="3.43535" layer="21" rot="R180"/>
<rectangle x1="-8.71855" y1="3.42265" x2="-8.04545" y2="3.43535" layer="21" rot="R180"/>
<rectangle x1="-8.94715" y1="3.42265" x2="-8.78205" y2="3.43535" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.43535" x2="-1.40335" y2="3.44805" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.43535" x2="-4.10845" y2="3.44805" layer="21" rot="R180"/>
<rectangle x1="-8.71855" y1="3.43535" x2="-8.04545" y2="3.44805" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="3.43535" x2="-8.76935" y2="3.44805" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.44805" x2="-1.41605" y2="3.46075" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.44805" x2="-4.10845" y2="3.46075" layer="21" rot="R180"/>
<rectangle x1="-8.93445" y1="3.44805" x2="-8.04545" y2="3.46075" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.46075" x2="-1.42875" y2="3.47345" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.46075" x2="-4.10845" y2="3.47345" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="3.46075" x2="-8.04545" y2="3.47345" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.47345" x2="-1.44145" y2="3.48615" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.47345" x2="-4.10845" y2="3.48615" layer="21" rot="R180"/>
<rectangle x1="-8.92175" y1="3.47345" x2="-8.04545" y2="3.48615" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.48615" x2="-1.45415" y2="3.49885" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.48615" x2="-4.10845" y2="3.49885" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="3.48615" x2="-8.04545" y2="3.49885" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.49885" x2="-1.46685" y2="3.51155" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.49885" x2="-4.10845" y2="3.51155" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="3.49885" x2="-8.04545" y2="3.51155" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.51155" x2="-1.47955" y2="3.52425" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.51155" x2="-4.10845" y2="3.52425" layer="21" rot="R180"/>
<rectangle x1="-8.90905" y1="3.51155" x2="-8.04545" y2="3.52425" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.52425" x2="-1.49225" y2="3.53695" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.52425" x2="-4.10845" y2="3.53695" layer="21" rot="R180"/>
<rectangle x1="-8.89635" y1="3.52425" x2="-8.04545" y2="3.53695" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.53695" x2="-1.50495" y2="3.54965" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.53695" x2="-4.10845" y2="3.54965" layer="21" rot="R180"/>
<rectangle x1="-8.89635" y1="3.53695" x2="-8.04545" y2="3.54965" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.54965" x2="-1.51765" y2="3.56235" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.54965" x2="-4.10845" y2="3.56235" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="3.54965" x2="-8.04545" y2="3.56235" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.56235" x2="-1.53035" y2="3.57505" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.56235" x2="-4.10845" y2="3.57505" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="3.56235" x2="-8.04545" y2="3.57505" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.57505" x2="-1.54305" y2="3.58775" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.57505" x2="-4.10845" y2="3.58775" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="3.57505" x2="-8.04545" y2="3.58775" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.58775" x2="-1.55575" y2="3.60045" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.58775" x2="-4.10845" y2="3.60045" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="3.58775" x2="-8.04545" y2="3.60045" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.60045" x2="-1.56845" y2="3.61315" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.60045" x2="-4.10845" y2="3.61315" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="3.60045" x2="-8.04545" y2="3.61315" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.61315" x2="-1.58115" y2="3.62585" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.61315" x2="-4.12115" y2="3.62585" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="3.61315" x2="-8.04545" y2="3.62585" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.62585" x2="-1.59385" y2="3.63855" layer="21" rot="R180"/>
<rectangle x1="-4.78155" y1="3.62585" x2="-4.12115" y2="3.63855" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="3.62585" x2="-8.04545" y2="3.63855" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.63855" x2="-1.60655" y2="3.65125" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.63855" x2="-4.12115" y2="3.65125" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="3.63855" x2="-8.04545" y2="3.65125" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.65125" x2="-1.61925" y2="3.66395" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.65125" x2="-4.12115" y2="3.66395" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="3.65125" x2="-8.04545" y2="3.66395" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.66395" x2="-1.63195" y2="3.67665" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.66395" x2="-4.12115" y2="3.67665" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="3.66395" x2="-8.04545" y2="3.67665" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.67665" x2="-1.64465" y2="3.68935" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.67665" x2="-4.12115" y2="3.68935" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="3.67665" x2="-8.04545" y2="3.68935" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.68935" x2="-1.65735" y2="3.70205" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.68935" x2="-4.12115" y2="3.70205" layer="21" rot="R180"/>
<rectangle x1="-7.55015" y1="3.68935" x2="-7.51205" y2="3.70205" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="3.68935" x2="-8.04545" y2="3.70205" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.70205" x2="-1.67005" y2="3.71475" layer="21" rot="R180"/>
<rectangle x1="-4.79425" y1="3.70205" x2="-4.12115" y2="3.71475" layer="21" rot="R180"/>
<rectangle x1="-7.55015" y1="3.70205" x2="-7.47395" y2="3.71475" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="3.70205" x2="-8.04545" y2="3.71475" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.71475" x2="-1.68275" y2="3.72745" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.71475" x2="-4.12115" y2="3.72745" layer="21" rot="R180"/>
<rectangle x1="-7.55015" y1="3.71475" x2="-7.42315" y2="3.72745" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="3.71475" x2="-8.04545" y2="3.72745" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.72745" x2="-1.69545" y2="3.74015" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.72745" x2="-4.13385" y2="3.74015" layer="21" rot="R180"/>
<rectangle x1="-7.53745" y1="3.72745" x2="-7.38505" y2="3.74015" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="3.72745" x2="-8.04545" y2="3.74015" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.74015" x2="-1.70815" y2="3.75285" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.74015" x2="-4.13385" y2="3.75285" layer="21" rot="R180"/>
<rectangle x1="-7.53745" y1="3.74015" x2="-7.34695" y2="3.75285" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="3.74015" x2="-8.04545" y2="3.75285" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.75285" x2="-1.72085" y2="3.76555" layer="21" rot="R180"/>
<rectangle x1="-4.80695" y1="3.75285" x2="-4.13385" y2="3.76555" layer="21" rot="R180"/>
<rectangle x1="-7.53745" y1="3.75285" x2="-7.29615" y2="3.76555" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="3.75285" x2="-8.04545" y2="3.76555" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.76555" x2="-1.73355" y2="3.77825" layer="21" rot="R180"/>
<rectangle x1="-4.81965" y1="3.76555" x2="-4.13385" y2="3.77825" layer="21" rot="R180"/>
<rectangle x1="-7.53745" y1="3.76555" x2="-7.25805" y2="3.77825" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="3.76555" x2="-8.04545" y2="3.77825" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.77825" x2="-1.74625" y2="3.79095" layer="21" rot="R180"/>
<rectangle x1="-4.81965" y1="3.77825" x2="-4.13385" y2="3.79095" layer="21" rot="R180"/>
<rectangle x1="-7.52475" y1="3.77825" x2="-7.21995" y2="3.79095" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="3.77825" x2="-8.04545" y2="3.79095" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.79095" x2="-1.75895" y2="3.80365" layer="21" rot="R180"/>
<rectangle x1="-4.81965" y1="3.79095" x2="-4.13385" y2="3.80365" layer="21" rot="R180"/>
<rectangle x1="-7.52475" y1="3.79095" x2="-7.18185" y2="3.80365" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="3.79095" x2="-8.04545" y2="3.80365" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.80365" x2="-1.77165" y2="3.81635" layer="21" rot="R180"/>
<rectangle x1="-4.83235" y1="3.80365" x2="-4.14655" y2="3.81635" layer="21" rot="R180"/>
<rectangle x1="-7.52475" y1="3.80365" x2="-7.14375" y2="3.81635" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="3.80365" x2="-8.04545" y2="3.81635" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.81635" x2="-1.78435" y2="3.82905" layer="21" rot="R180"/>
<rectangle x1="-4.83235" y1="3.81635" x2="-4.14655" y2="3.82905" layer="21" rot="R180"/>
<rectangle x1="-7.52475" y1="3.81635" x2="-7.09295" y2="3.82905" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="3.81635" x2="-8.04545" y2="3.82905" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.82905" x2="-1.79705" y2="3.84175" layer="21" rot="R180"/>
<rectangle x1="-4.83235" y1="3.82905" x2="-4.14655" y2="3.84175" layer="21" rot="R180"/>
<rectangle x1="-7.52475" y1="3.82905" x2="-7.05485" y2="3.84175" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="3.82905" x2="-8.04545" y2="3.84175" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.84175" x2="-1.80975" y2="3.85445" layer="21" rot="R180"/>
<rectangle x1="-4.84505" y1="3.84175" x2="-4.14655" y2="3.85445" layer="21" rot="R180"/>
<rectangle x1="-7.51205" y1="3.84175" x2="-7.01675" y2="3.85445" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="3.84175" x2="-8.04545" y2="3.85445" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.85445" x2="-1.82245" y2="3.86715" layer="21" rot="R180"/>
<rectangle x1="-4.84505" y1="3.85445" x2="-4.14655" y2="3.86715" layer="21" rot="R180"/>
<rectangle x1="-7.51205" y1="3.85445" x2="-6.96595" y2="3.86715" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="3.85445" x2="-8.04545" y2="3.86715" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.86715" x2="-1.83515" y2="3.87985" layer="21" rot="R180"/>
<rectangle x1="-4.84505" y1="3.86715" x2="-4.15925" y2="3.87985" layer="21" rot="R180"/>
<rectangle x1="-7.51205" y1="3.86715" x2="-6.92785" y2="3.87985" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="3.86715" x2="-8.04545" y2="3.87985" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.87985" x2="-1.84785" y2="3.89255" layer="21" rot="R180"/>
<rectangle x1="-4.85775" y1="3.87985" x2="-4.15925" y2="3.89255" layer="21" rot="R180"/>
<rectangle x1="-7.51205" y1="3.87985" x2="-6.88975" y2="3.89255" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="3.87985" x2="-8.04545" y2="3.89255" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.89255" x2="-1.86055" y2="3.90525" layer="21" rot="R180"/>
<rectangle x1="-4.85775" y1="3.89255" x2="-4.15925" y2="3.90525" layer="21" rot="R180"/>
<rectangle x1="-7.49935" y1="3.89255" x2="-6.85165" y2="3.90525" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.89255" x2="-8.04545" y2="3.90525" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.90525" x2="-1.87325" y2="3.91795" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="3.90525" x2="-4.15925" y2="3.91795" layer="21" rot="R180"/>
<rectangle x1="-7.49935" y1="3.90525" x2="-6.80085" y2="3.91795" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.90525" x2="-8.04545" y2="3.91795" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.91795" x2="-1.88595" y2="3.93065" layer="21" rot="R180"/>
<rectangle x1="-4.87045" y1="3.91795" x2="-4.17195" y2="3.93065" layer="21" rot="R180"/>
<rectangle x1="-7.49935" y1="3.91795" x2="-6.76275" y2="3.93065" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.91795" x2="-8.04545" y2="3.93065" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.93065" x2="-1.89865" y2="3.94335" layer="21" rot="R180"/>
<rectangle x1="-4.88315" y1="3.93065" x2="-4.17195" y2="3.94335" layer="21" rot="R180"/>
<rectangle x1="-7.49935" y1="3.93065" x2="-6.72465" y2="3.94335" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.93065" x2="-8.04545" y2="3.94335" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.94335" x2="-1.91135" y2="3.95605" layer="21" rot="R180"/>
<rectangle x1="-4.88315" y1="3.94335" x2="-4.17195" y2="3.95605" layer="21" rot="R180"/>
<rectangle x1="-7.49935" y1="3.94335" x2="-6.68655" y2="3.95605" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.94335" x2="-8.04545" y2="3.95605" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.95605" x2="-1.92405" y2="3.96875" layer="21" rot="R180"/>
<rectangle x1="-4.89585" y1="3.95605" x2="-4.17195" y2="3.96875" layer="21" rot="R180"/>
<rectangle x1="-7.48665" y1="3.95605" x2="-6.63575" y2="3.96875" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.95605" x2="-8.04545" y2="3.96875" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.96875" x2="-1.93675" y2="3.98145" layer="21" rot="R180"/>
<rectangle x1="-4.89585" y1="3.96875" x2="-4.18465" y2="3.98145" layer="21" rot="R180"/>
<rectangle x1="-7.48665" y1="3.96875" x2="-6.59765" y2="3.98145" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.96875" x2="-8.04545" y2="3.98145" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.98145" x2="-1.94945" y2="3.99415" layer="21" rot="R180"/>
<rectangle x1="-4.90855" y1="3.98145" x2="-4.18465" y2="3.99415" layer="21" rot="R180"/>
<rectangle x1="-7.48665" y1="3.98145" x2="-6.55955" y2="3.99415" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="3.98145" x2="-8.04545" y2="3.99415" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="3.99415" x2="-1.96215" y2="4.00685" layer="21" rot="R180"/>
<rectangle x1="-4.90855" y1="3.99415" x2="-4.18465" y2="4.00685" layer="21" rot="R180"/>
<rectangle x1="-7.48665" y1="3.99415" x2="-6.52145" y2="4.00685" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="3.99415" x2="-8.04545" y2="4.00685" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.00685" x2="-1.97485" y2="4.01955" layer="21" rot="R180"/>
<rectangle x1="-4.92125" y1="4.00685" x2="-4.19735" y2="4.01955" layer="21" rot="R180"/>
<rectangle x1="-7.47395" y1="4.00685" x2="-6.47065" y2="4.01955" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.00685" x2="-8.04545" y2="4.01955" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.01955" x2="-1.98755" y2="4.03225" layer="21" rot="R180"/>
<rectangle x1="-4.92125" y1="4.01955" x2="-4.19735" y2="4.03225" layer="21" rot="R180"/>
<rectangle x1="-7.47395" y1="4.01955" x2="-6.43255" y2="4.03225" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.01955" x2="-8.04545" y2="4.03225" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.03225" x2="-2.00025" y2="4.04495" layer="21" rot="R180"/>
<rectangle x1="-4.93395" y1="4.03225" x2="-4.19735" y2="4.04495" layer="21" rot="R180"/>
<rectangle x1="-7.47395" y1="4.03225" x2="-6.40715" y2="4.04495" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.03225" x2="-8.04545" y2="4.04495" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.04495" x2="-2.01295" y2="4.05765" layer="21" rot="R180"/>
<rectangle x1="-4.93395" y1="4.04495" x2="-4.21005" y2="4.05765" layer="21" rot="R180"/>
<rectangle x1="-7.47395" y1="4.04495" x2="-6.36905" y2="4.05765" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.04495" x2="-8.04545" y2="4.05765" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.05765" x2="-2.02565" y2="4.07035" layer="21" rot="R180"/>
<rectangle x1="-4.94665" y1="4.05765" x2="-4.21005" y2="4.07035" layer="21" rot="R180"/>
<rectangle x1="-7.47395" y1="4.05765" x2="-6.31825" y2="4.07035" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.05765" x2="-8.04545" y2="4.07035" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.07035" x2="-2.03835" y2="4.08305" layer="21" rot="R180"/>
<rectangle x1="-4.95935" y1="4.07035" x2="-4.21005" y2="4.08305" layer="21" rot="R180"/>
<rectangle x1="-7.46125" y1="4.07035" x2="-6.28015" y2="4.08305" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.07035" x2="-8.04545" y2="4.08305" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.08305" x2="-2.05105" y2="4.09575" layer="21" rot="R180"/>
<rectangle x1="-4.95935" y1="4.08305" x2="-4.22275" y2="4.09575" layer="21" rot="R180"/>
<rectangle x1="-7.46125" y1="4.08305" x2="-6.26745" y2="4.09575" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.08305" x2="-8.04545" y2="4.09575" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.09575" x2="-2.06375" y2="4.10845" layer="21" rot="R180"/>
<rectangle x1="-4.97205" y1="4.09575" x2="-4.22275" y2="4.10845" layer="21" rot="R180"/>
<rectangle x1="-7.46125" y1="4.09575" x2="-6.28015" y2="4.10845" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.09575" x2="-8.04545" y2="4.10845" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.10845" x2="-2.07645" y2="4.12115" layer="21" rot="R180"/>
<rectangle x1="-4.98475" y1="4.10845" x2="-4.22275" y2="4.12115" layer="21" rot="R180"/>
<rectangle x1="-7.46125" y1="4.10845" x2="-6.29285" y2="4.12115" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.10845" x2="-8.04545" y2="4.12115" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.12115" x2="-2.08915" y2="4.13385" layer="21" rot="R180"/>
<rectangle x1="-4.99745" y1="4.12115" x2="-4.23545" y2="4.13385" layer="21" rot="R180"/>
<rectangle x1="-7.44855" y1="4.12115" x2="-6.30555" y2="4.13385" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.12115" x2="-8.04545" y2="4.13385" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.13385" x2="-2.10185" y2="4.14655" layer="21" rot="R180"/>
<rectangle x1="-4.99745" y1="4.13385" x2="-4.23545" y2="4.14655" layer="21" rot="R180"/>
<rectangle x1="-7.44855" y1="4.13385" x2="-6.31825" y2="4.14655" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.13385" x2="-8.04545" y2="4.14655" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.14655" x2="-2.11455" y2="4.15925" layer="21" rot="R180"/>
<rectangle x1="-5.01015" y1="4.14655" x2="-4.24815" y2="4.15925" layer="21" rot="R180"/>
<rectangle x1="-7.44855" y1="4.14655" x2="-6.33095" y2="4.15925" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.14655" x2="-8.04545" y2="4.15925" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.15925" x2="-2.12725" y2="4.17195" layer="21" rot="R180"/>
<rectangle x1="-5.02285" y1="4.15925" x2="-4.24815" y2="4.17195" layer="21" rot="R180"/>
<rectangle x1="-7.44855" y1="4.15925" x2="-6.34365" y2="4.17195" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.15925" x2="-8.04545" y2="4.17195" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.17195" x2="-2.13995" y2="4.18465" layer="21" rot="R180"/>
<rectangle x1="-5.03555" y1="4.17195" x2="-4.24815" y2="4.18465" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="4.17195" x2="-6.35635" y2="4.18465" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.17195" x2="-8.04545" y2="4.18465" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.18465" x2="-2.15265" y2="4.19735" layer="21" rot="R180"/>
<rectangle x1="-5.04825" y1="4.18465" x2="-4.26085" y2="4.19735" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="4.18465" x2="-6.38175" y2="4.19735" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.18465" x2="-8.04545" y2="4.19735" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.19735" x2="-2.16535" y2="4.21005" layer="21" rot="R180"/>
<rectangle x1="-5.06095" y1="4.19735" x2="-4.26085" y2="4.21005" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="4.19735" x2="-6.39445" y2="4.21005" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.19735" x2="-8.04545" y2="4.21005" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.21005" x2="-2.17805" y2="4.22275" layer="21" rot="R180"/>
<rectangle x1="-5.06095" y1="4.21005" x2="-4.27355" y2="4.22275" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="4.21005" x2="-6.40715" y2="4.22275" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.21005" x2="-8.04545" y2="4.22275" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.22275" x2="-2.19075" y2="4.23545" layer="21" rot="R180"/>
<rectangle x1="-5.07365" y1="4.22275" x2="-4.27355" y2="4.23545" layer="21" rot="R180"/>
<rectangle x1="-7.43585" y1="4.22275" x2="-6.41985" y2="4.23545" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.22275" x2="-8.04545" y2="4.23545" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.23545" x2="-2.20345" y2="4.24815" layer="21" rot="R180"/>
<rectangle x1="-5.08635" y1="4.23545" x2="-4.28625" y2="4.24815" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="4.23545" x2="-6.43255" y2="4.24815" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.23545" x2="-8.04545" y2="4.24815" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.24815" x2="-2.21615" y2="4.26085" layer="21" rot="R180"/>
<rectangle x1="-5.09905" y1="4.24815" x2="-4.28625" y2="4.26085" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="4.24815" x2="-6.44525" y2="4.26085" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.24815" x2="-8.04545" y2="4.26085" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.26085" x2="-2.22885" y2="4.27355" layer="21" rot="R180"/>
<rectangle x1="-5.11175" y1="4.26085" x2="-4.29895" y2="4.27355" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="4.26085" x2="-6.45795" y2="4.27355" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.26085" x2="-8.04545" y2="4.27355" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.27355" x2="-2.24155" y2="4.28625" layer="21" rot="R180"/>
<rectangle x1="-5.12445" y1="4.27355" x2="-4.29895" y2="4.28625" layer="21" rot="R180"/>
<rectangle x1="-7.42315" y1="4.27355" x2="-6.47065" y2="4.28625" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.27355" x2="-8.04545" y2="4.28625" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.28625" x2="-2.25425" y2="4.29895" layer="21" rot="R180"/>
<rectangle x1="-5.14985" y1="4.28625" x2="-4.31165" y2="4.29895" layer="21" rot="R180"/>
<rectangle x1="-7.41045" y1="4.28625" x2="-6.48335" y2="4.29895" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.28625" x2="-8.04545" y2="4.29895" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.29895" x2="-2.26695" y2="4.31165" layer="21" rot="R180"/>
<rectangle x1="-5.16255" y1="4.29895" x2="-4.31165" y2="4.31165" layer="21" rot="R180"/>
<rectangle x1="-7.41045" y1="4.29895" x2="-6.49605" y2="4.31165" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.29895" x2="-8.04545" y2="4.31165" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.31165" x2="-2.27965" y2="4.32435" layer="21" rot="R180"/>
<rectangle x1="-5.17525" y1="4.31165" x2="-4.32435" y2="4.32435" layer="21" rot="R180"/>
<rectangle x1="-7.41045" y1="4.31165" x2="-6.50875" y2="4.32435" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.31165" x2="-8.04545" y2="4.32435" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.32435" x2="-2.29235" y2="4.33705" layer="21" rot="R180"/>
<rectangle x1="-5.18795" y1="4.32435" x2="-4.32435" y2="4.33705" layer="21" rot="R180"/>
<rectangle x1="-7.41045" y1="4.32435" x2="-6.52145" y2="4.33705" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.32435" x2="-8.04545" y2="4.33705" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.33705" x2="-2.30505" y2="4.34975" layer="21" rot="R180"/>
<rectangle x1="-5.20065" y1="4.33705" x2="-4.33705" y2="4.34975" layer="21" rot="R180"/>
<rectangle x1="-7.41045" y1="4.33705" x2="-6.52145" y2="4.34975" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.33705" x2="-8.04545" y2="4.34975" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.34975" x2="-2.31775" y2="4.36245" layer="21" rot="R180"/>
<rectangle x1="-5.22605" y1="4.34975" x2="-4.34975" y2="4.36245" layer="21" rot="R180"/>
<rectangle x1="-7.39775" y1="4.34975" x2="-6.49605" y2="4.36245" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.34975" x2="-8.04545" y2="4.36245" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.36245" x2="-2.33045" y2="4.37515" layer="21" rot="R180"/>
<rectangle x1="-5.23875" y1="4.36245" x2="-4.34975" y2="4.37515" layer="21" rot="R180"/>
<rectangle x1="-7.39775" y1="4.36245" x2="-6.48335" y2="4.37515" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.36245" x2="-8.04545" y2="4.37515" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.37515" x2="-2.34315" y2="4.38785" layer="21" rot="R180"/>
<rectangle x1="-5.26415" y1="4.37515" x2="-4.36245" y2="4.38785" layer="21" rot="R180"/>
<rectangle x1="-7.39775" y1="4.37515" x2="-6.47065" y2="4.38785" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.37515" x2="-8.04545" y2="4.38785" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.38785" x2="-2.35585" y2="4.40055" layer="21" rot="R180"/>
<rectangle x1="-5.27685" y1="4.38785" x2="-4.36245" y2="4.40055" layer="21" rot="R180"/>
<rectangle x1="-7.39775" y1="4.38785" x2="-6.44525" y2="4.40055" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.38785" x2="-8.04545" y2="4.40055" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.40055" x2="-2.36855" y2="4.41325" layer="21" rot="R180"/>
<rectangle x1="-5.30225" y1="4.40055" x2="-4.37515" y2="4.41325" layer="21" rot="R180"/>
<rectangle x1="-7.38505" y1="4.40055" x2="-6.43255" y2="4.41325" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="4.40055" x2="-8.04545" y2="4.41325" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.41325" x2="-2.38125" y2="4.42595" layer="21" rot="R180"/>
<rectangle x1="-5.31495" y1="4.41325" x2="-4.38785" y2="4.42595" layer="21" rot="R180"/>
<rectangle x1="-7.38505" y1="4.41325" x2="-6.40715" y2="4.42595" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="4.41325" x2="-8.04545" y2="4.42595" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.42595" x2="-2.39395" y2="4.43865" layer="21" rot="R180"/>
<rectangle x1="-5.34035" y1="4.42595" x2="-4.38785" y2="4.43865" layer="21" rot="R180"/>
<rectangle x1="-7.38505" y1="4.42595" x2="-6.38175" y2="4.43865" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="4.42595" x2="-8.04545" y2="4.43865" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.43865" x2="-2.40665" y2="4.45135" layer="21" rot="R180"/>
<rectangle x1="-5.36575" y1="4.43865" x2="-4.40055" y2="4.45135" layer="21" rot="R180"/>
<rectangle x1="-7.38505" y1="4.43865" x2="-6.35635" y2="4.45135" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="4.43865" x2="-8.04545" y2="4.45135" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.45135" x2="-2.41935" y2="4.46405" layer="21" rot="R180"/>
<rectangle x1="-5.39115" y1="4.45135" x2="-4.41325" y2="4.46405" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="4.45135" x2="-6.33095" y2="4.46405" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="4.45135" x2="-8.04545" y2="4.46405" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.46405" x2="-2.43205" y2="4.47675" layer="21" rot="R180"/>
<rectangle x1="-5.41655" y1="4.46405" x2="-4.41325" y2="4.47675" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="4.46405" x2="-6.30555" y2="4.47675" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="4.46405" x2="-8.04545" y2="4.47675" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.47675" x2="-2.44475" y2="4.48945" layer="21" rot="R180"/>
<rectangle x1="-5.45465" y1="4.47675" x2="-4.42595" y2="4.48945" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="4.47675" x2="-6.28015" y2="4.48945" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="4.47675" x2="-8.04545" y2="4.48945" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.48945" x2="-2.45745" y2="4.50215" layer="21" rot="R180"/>
<rectangle x1="-5.48005" y1="4.48945" x2="-4.43865" y2="4.50215" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="4.48945" x2="-6.24205" y2="4.50215" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="4.48945" x2="-8.04545" y2="4.50215" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.50215" x2="-2.47015" y2="4.51485" layer="21" rot="R180"/>
<rectangle x1="-5.51815" y1="4.50215" x2="-4.43865" y2="4.51485" layer="21" rot="R180"/>
<rectangle x1="-7.37235" y1="4.50215" x2="-6.20395" y2="4.51485" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="4.50215" x2="-8.04545" y2="4.51485" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.51485" x2="-2.48285" y2="4.52755" layer="21" rot="R180"/>
<rectangle x1="-5.55625" y1="4.51485" x2="-4.45135" y2="4.52755" layer="21" rot="R180"/>
<rectangle x1="-7.35965" y1="4.51485" x2="-6.16585" y2="4.52755" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="4.51485" x2="-8.04545" y2="4.52755" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.52755" x2="-2.49555" y2="4.54025" layer="21" rot="R180"/>
<rectangle x1="-5.60705" y1="4.52755" x2="-4.46405" y2="4.54025" layer="21" rot="R180"/>
<rectangle x1="-7.35965" y1="4.52755" x2="-6.12775" y2="4.54025" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="4.52755" x2="-8.04545" y2="4.54025" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.54025" x2="-2.50825" y2="4.55295" layer="21" rot="R180"/>
<rectangle x1="-5.65785" y1="4.54025" x2="-4.47675" y2="4.55295" layer="21" rot="R180"/>
<rectangle x1="-7.35965" y1="4.54025" x2="-6.06425" y2="4.55295" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="4.54025" x2="-8.04545" y2="4.55295" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.55295" x2="-2.52095" y2="4.56565" layer="21" rot="R180"/>
<rectangle x1="-5.74675" y1="4.55295" x2="-4.47675" y2="4.56565" layer="21" rot="R180"/>
<rectangle x1="-7.35965" y1="4.55295" x2="-5.97535" y2="4.56565" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="4.55295" x2="-8.04545" y2="4.56565" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.56565" x2="-2.53365" y2="4.57835" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="4.56565" x2="-4.48945" y2="4.57835" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="4.56565" x2="-8.04545" y2="4.57835" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.57835" x2="-2.54635" y2="4.59105" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="4.57835" x2="-4.50215" y2="4.59105" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="4.57835" x2="-8.04545" y2="4.59105" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.59105" x2="-2.55905" y2="4.60375" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="4.59105" x2="-4.51485" y2="4.60375" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="4.59105" x2="-8.04545" y2="4.60375" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.60375" x2="-2.57175" y2="4.61645" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="4.60375" x2="-4.52755" y2="4.61645" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="4.60375" x2="-8.04545" y2="4.61645" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.61645" x2="-2.58445" y2="4.62915" layer="21" rot="R180"/>
<rectangle x1="-7.34695" y1="4.61645" x2="-4.52755" y2="4.62915" layer="21" rot="R180"/>
<rectangle x1="-8.85825" y1="4.61645" x2="-8.04545" y2="4.62915" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.62915" x2="-2.59715" y2="4.64185" layer="21" rot="R180"/>
<rectangle x1="-7.33425" y1="4.62915" x2="-4.54025" y2="4.64185" layer="21" rot="R180"/>
<rectangle x1="-8.85825" y1="4.62915" x2="-8.04545" y2="4.64185" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.64185" x2="-2.60985" y2="4.65455" layer="21" rot="R180"/>
<rectangle x1="-7.33425" y1="4.64185" x2="-4.55295" y2="4.65455" layer="21" rot="R180"/>
<rectangle x1="-8.85825" y1="4.64185" x2="-8.04545" y2="4.65455" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.65455" x2="-2.62255" y2="4.66725" layer="21" rot="R180"/>
<rectangle x1="-7.33425" y1="4.65455" x2="-4.56565" y2="4.66725" layer="21" rot="R180"/>
<rectangle x1="-8.87095" y1="4.65455" x2="-8.04545" y2="4.66725" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.66725" x2="-2.63525" y2="4.67995" layer="21" rot="R180"/>
<rectangle x1="-7.33425" y1="4.66725" x2="-4.57835" y2="4.67995" layer="21" rot="R180"/>
<rectangle x1="-8.87095" y1="4.66725" x2="-8.04545" y2="4.67995" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.67995" x2="-2.64795" y2="4.69265" layer="21" rot="R180"/>
<rectangle x1="-7.32155" y1="4.67995" x2="-4.59105" y2="4.69265" layer="21" rot="R180"/>
<rectangle x1="-8.87095" y1="4.67995" x2="-8.04545" y2="4.69265" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.69265" x2="-2.66065" y2="4.70535" layer="21" rot="R180"/>
<rectangle x1="-7.32155" y1="4.69265" x2="-4.60375" y2="4.70535" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="4.69265" x2="-8.04545" y2="4.70535" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.70535" x2="-2.67335" y2="4.71805" layer="21" rot="R180"/>
<rectangle x1="-7.32155" y1="4.70535" x2="-4.61645" y2="4.71805" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="4.70535" x2="-8.04545" y2="4.71805" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.71805" x2="-2.68605" y2="4.73075" layer="21" rot="R180"/>
<rectangle x1="-7.32155" y1="4.71805" x2="-4.62915" y2="4.73075" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="4.71805" x2="-8.04545" y2="4.73075" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.73075" x2="-2.69875" y2="4.74345" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="4.73075" x2="-4.64185" y2="4.74345" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="4.73075" x2="-8.04545" y2="4.74345" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.74345" x2="-2.71145" y2="4.75615" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="4.74345" x2="-4.65455" y2="4.75615" layer="21" rot="R180"/>
<rectangle x1="-8.88365" y1="4.74345" x2="-8.04545" y2="4.75615" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.75615" x2="-2.72415" y2="4.76885" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="4.75615" x2="-4.66725" y2="4.76885" layer="21" rot="R180"/>
<rectangle x1="-8.87095" y1="4.75615" x2="-8.04545" y2="4.76885" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.76885" x2="-2.73685" y2="4.78155" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="4.76885" x2="-4.67995" y2="4.78155" layer="21" rot="R180"/>
<rectangle x1="-8.85825" y1="4.76885" x2="-8.04545" y2="4.78155" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.78155" x2="-2.74955" y2="4.79425" layer="21" rot="R180"/>
<rectangle x1="-7.30885" y1="4.78155" x2="-4.69265" y2="4.79425" layer="21" rot="R180"/>
<rectangle x1="-8.84555" y1="4.78155" x2="-8.04545" y2="4.79425" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.79425" x2="-2.76225" y2="4.80695" layer="21" rot="R180"/>
<rectangle x1="-7.01675" y1="4.79425" x2="-4.70535" y2="4.80695" layer="21" rot="R180"/>
<rectangle x1="-7.29615" y1="4.79425" x2="-7.04215" y2="4.80695" layer="21" rot="R180"/>
<rectangle x1="-8.83285" y1="4.79425" x2="-8.04545" y2="4.80695" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.80695" x2="-2.77495" y2="4.81965" layer="21" rot="R180"/>
<rectangle x1="-7.00405" y1="4.80695" x2="-4.73075" y2="4.81965" layer="21" rot="R180"/>
<rectangle x1="-7.29615" y1="4.80695" x2="-7.05485" y2="4.81965" layer="21" rot="R180"/>
<rectangle x1="-8.82015" y1="4.80695" x2="-8.04545" y2="4.81965" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.81965" x2="-2.78765" y2="4.83235" layer="21" rot="R180"/>
<rectangle x1="-6.97865" y1="4.81965" x2="-4.74345" y2="4.83235" layer="21" rot="R180"/>
<rectangle x1="-7.29615" y1="4.81965" x2="-7.06755" y2="4.83235" layer="21" rot="R180"/>
<rectangle x1="-8.80745" y1="4.81965" x2="-8.04545" y2="4.83235" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.83235" x2="-2.80035" y2="4.84505" layer="21" rot="R180"/>
<rectangle x1="-6.96595" y1="4.83235" x2="-4.75615" y2="4.84505" layer="21" rot="R180"/>
<rectangle x1="-7.29615" y1="4.83235" x2="-7.08025" y2="4.84505" layer="21" rot="R180"/>
<rectangle x1="-8.79475" y1="4.83235" x2="-8.04545" y2="4.84505" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.84505" x2="-2.81305" y2="4.85775" layer="21" rot="R180"/>
<rectangle x1="-6.95325" y1="4.84505" x2="-4.76885" y2="4.85775" layer="21" rot="R180"/>
<rectangle x1="-7.28345" y1="4.84505" x2="-7.09295" y2="4.85775" layer="21" rot="R180"/>
<rectangle x1="-8.78205" y1="4.84505" x2="-8.04545" y2="4.85775" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.85775" x2="-2.82575" y2="4.87045" layer="21" rot="R180"/>
<rectangle x1="-6.94055" y1="4.85775" x2="-4.78155" y2="4.87045" layer="21" rot="R180"/>
<rectangle x1="-7.28345" y1="4.85775" x2="-7.10565" y2="4.87045" layer="21" rot="R180"/>
<rectangle x1="-8.76935" y1="4.85775" x2="-8.04545" y2="4.87045" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.87045" x2="-2.83845" y2="4.88315" layer="21" rot="R180"/>
<rectangle x1="-6.91515" y1="4.87045" x2="-4.80695" y2="4.88315" layer="21" rot="R180"/>
<rectangle x1="-7.28345" y1="4.87045" x2="-7.11835" y2="4.88315" layer="21" rot="R180"/>
<rectangle x1="-8.75665" y1="4.87045" x2="-8.04545" y2="4.88315" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.88315" x2="-2.85115" y2="4.89585" layer="21" rot="R180"/>
<rectangle x1="-6.90245" y1="4.88315" x2="-4.81965" y2="4.89585" layer="21" rot="R180"/>
<rectangle x1="-7.28345" y1="4.88315" x2="-7.13105" y2="4.89585" layer="21" rot="R180"/>
<rectangle x1="-8.74395" y1="4.88315" x2="-8.04545" y2="4.89585" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.89585" x2="-2.86385" y2="4.90855" layer="21" rot="R180"/>
<rectangle x1="-6.88975" y1="4.89585" x2="-4.83235" y2="4.90855" layer="21" rot="R180"/>
<rectangle x1="-7.28345" y1="4.89585" x2="-7.14375" y2="4.90855" layer="21" rot="R180"/>
<rectangle x1="-8.73125" y1="4.89585" x2="-8.04545" y2="4.90855" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.90855" x2="-2.87655" y2="4.92125" layer="21" rot="R180"/>
<rectangle x1="-6.86435" y1="4.90855" x2="-4.85775" y2="4.92125" layer="21" rot="R180"/>
<rectangle x1="-7.27075" y1="4.90855" x2="-7.15645" y2="4.92125" layer="21" rot="R180"/>
<rectangle x1="-8.71855" y1="4.90855" x2="-8.04545" y2="4.92125" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.92125" x2="-2.88925" y2="4.93395" layer="21" rot="R180"/>
<rectangle x1="-6.85165" y1="4.92125" x2="-4.87045" y2="4.93395" layer="21" rot="R180"/>
<rectangle x1="-7.27075" y1="4.92125" x2="-7.16915" y2="4.93395" layer="21" rot="R180"/>
<rectangle x1="-8.70585" y1="4.92125" x2="-8.04545" y2="4.93395" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.93395" x2="-2.90195" y2="4.94665" layer="21" rot="R180"/>
<rectangle x1="-6.82625" y1="4.93395" x2="-4.89585" y2="4.94665" layer="21" rot="R180"/>
<rectangle x1="-7.27075" y1="4.93395" x2="-7.19455" y2="4.94665" layer="21" rot="R180"/>
<rectangle x1="-8.69315" y1="4.93395" x2="-8.04545" y2="4.94665" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.94665" x2="-2.91465" y2="4.95935" layer="21" rot="R180"/>
<rectangle x1="-6.81355" y1="4.94665" x2="-4.90855" y2="4.95935" layer="21" rot="R180"/>
<rectangle x1="-7.27075" y1="4.94665" x2="-7.20725" y2="4.95935" layer="21" rot="R180"/>
<rectangle x1="-8.68045" y1="4.94665" x2="-8.04545" y2="4.95935" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.95935" x2="-2.92735" y2="4.97205" layer="21" rot="R180"/>
<rectangle x1="-6.78815" y1="4.95935" x2="-4.93395" y2="4.97205" layer="21" rot="R180"/>
<rectangle x1="-7.25805" y1="4.95935" x2="-7.21995" y2="4.97205" layer="21" rot="R180"/>
<rectangle x1="-8.66775" y1="4.95935" x2="-8.04545" y2="4.97205" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.97205" x2="-2.94005" y2="4.98475" layer="21" rot="R180"/>
<rectangle x1="-6.77545" y1="4.97205" x2="-4.95935" y2="4.98475" layer="21" rot="R180"/>
<rectangle x1="-7.25805" y1="4.97205" x2="-7.23265" y2="4.98475" layer="21" rot="R180"/>
<rectangle x1="-8.65505" y1="4.97205" x2="-8.04545" y2="4.98475" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.98475" x2="-2.95275" y2="4.99745" layer="21" rot="R180"/>
<rectangle x1="-6.75005" y1="4.98475" x2="-4.97205" y2="4.99745" layer="21" rot="R180"/>
<rectangle x1="-7.25805" y1="4.98475" x2="-7.24535" y2="4.99745" layer="21" rot="R180"/>
<rectangle x1="-8.64235" y1="4.98475" x2="-8.04545" y2="4.99745" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="4.99745" x2="-2.96545" y2="5.01015" layer="21" rot="R180"/>
<rectangle x1="-6.72465" y1="4.99745" x2="-4.99745" y2="5.01015" layer="21" rot="R180"/>
<rectangle x1="-8.62965" y1="4.99745" x2="-8.04545" y2="5.01015" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.01015" x2="-2.97815" y2="5.02285" layer="21" rot="R180"/>
<rectangle x1="-6.69925" y1="5.01015" x2="-5.02285" y2="5.02285" layer="21" rot="R180"/>
<rectangle x1="-8.61695" y1="5.01015" x2="-8.04545" y2="5.02285" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.02285" x2="-2.99085" y2="5.03555" layer="21" rot="R180"/>
<rectangle x1="-6.68655" y1="5.02285" x2="-5.04825" y2="5.03555" layer="21" rot="R180"/>
<rectangle x1="-8.60425" y1="5.02285" x2="-8.04545" y2="5.03555" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.03555" x2="-3.00355" y2="5.04825" layer="21" rot="R180"/>
<rectangle x1="-6.66115" y1="5.03555" x2="-5.06095" y2="5.04825" layer="21" rot="R180"/>
<rectangle x1="-8.59155" y1="5.03555" x2="-8.04545" y2="5.04825" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.04825" x2="-3.01625" y2="5.06095" layer="21" rot="R180"/>
<rectangle x1="-6.63575" y1="5.04825" x2="-5.08635" y2="5.06095" layer="21" rot="R180"/>
<rectangle x1="-8.57885" y1="5.04825" x2="-8.04545" y2="5.06095" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.06095" x2="-3.02895" y2="5.07365" layer="21" rot="R180"/>
<rectangle x1="-6.61035" y1="5.06095" x2="-5.12445" y2="5.07365" layer="21" rot="R180"/>
<rectangle x1="-8.56615" y1="5.06095" x2="-8.04545" y2="5.07365" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.07365" x2="-3.04165" y2="5.08635" layer="21" rot="R180"/>
<rectangle x1="-6.57225" y1="5.07365" x2="-5.14985" y2="5.08635" layer="21" rot="R180"/>
<rectangle x1="-8.55345" y1="5.07365" x2="-8.04545" y2="5.08635" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.08635" x2="-3.05435" y2="5.09905" layer="21" rot="R180"/>
<rectangle x1="-6.54685" y1="5.08635" x2="-5.17525" y2="5.09905" layer="21" rot="R180"/>
<rectangle x1="-8.54075" y1="5.08635" x2="-8.04545" y2="5.09905" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.09905" x2="-3.06705" y2="5.11175" layer="21" rot="R180"/>
<rectangle x1="-6.52145" y1="5.09905" x2="-5.20065" y2="5.11175" layer="21" rot="R180"/>
<rectangle x1="-8.52805" y1="5.09905" x2="-8.04545" y2="5.11175" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.11175" x2="-3.07975" y2="5.12445" layer="21" rot="R180"/>
<rectangle x1="-6.48335" y1="5.11175" x2="-5.23875" y2="5.12445" layer="21" rot="R180"/>
<rectangle x1="-8.51535" y1="5.11175" x2="-8.04545" y2="5.12445" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.12445" x2="-3.09245" y2="5.13715" layer="21" rot="R180"/>
<rectangle x1="-6.44525" y1="5.12445" x2="-5.27685" y2="5.13715" layer="21" rot="R180"/>
<rectangle x1="-8.50265" y1="5.12445" x2="-8.04545" y2="5.13715" layer="21" rot="R180"/>
<rectangle x1="-3.52425" y1="5.13715" x2="-3.10515" y2="5.14985" layer="21" rot="R180"/>
<rectangle x1="-6.41985" y1="5.13715" x2="-5.31495" y2="5.14985" layer="21" rot="R180"/>
<rectangle x1="-8.48995" y1="5.13715" x2="-8.04545" y2="5.14985" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.14985" x2="-3.11785" y2="5.16255" layer="21" rot="R180"/>
<rectangle x1="-6.36905" y1="5.14985" x2="-5.35305" y2="5.16255" layer="21" rot="R180"/>
<rectangle x1="-8.47725" y1="5.14985" x2="-8.04545" y2="5.16255" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.16255" x2="-3.13055" y2="5.17525" layer="21" rot="R180"/>
<rectangle x1="-6.33095" y1="5.16255" x2="-5.39115" y2="5.17525" layer="21" rot="R180"/>
<rectangle x1="-8.46455" y1="5.16255" x2="-8.04545" y2="5.17525" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.17525" x2="-3.14325" y2="5.18795" layer="21" rot="R180"/>
<rectangle x1="-6.28015" y1="5.17525" x2="-5.44195" y2="5.18795" layer="21" rot="R180"/>
<rectangle x1="-8.45185" y1="5.17525" x2="-8.04545" y2="5.18795" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.18795" x2="-3.15595" y2="5.20065" layer="21" rot="R180"/>
<rectangle x1="-6.22935" y1="5.18795" x2="-5.50545" y2="5.20065" layer="21" rot="R180"/>
<rectangle x1="-8.43915" y1="5.18795" x2="-8.04545" y2="5.20065" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.20065" x2="-3.16865" y2="5.21335" layer="21" rot="R180"/>
<rectangle x1="-6.15315" y1="5.20065" x2="-5.56895" y2="5.21335" layer="21" rot="R180"/>
<rectangle x1="-8.42645" y1="5.20065" x2="-8.04545" y2="5.21335" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.21335" x2="-3.18135" y2="5.22605" layer="21" rot="R180"/>
<rectangle x1="-6.06425" y1="5.21335" x2="-5.65785" y2="5.22605" layer="21" rot="R180"/>
<rectangle x1="-8.41375" y1="5.21335" x2="-8.04545" y2="5.22605" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.22605" x2="-3.19405" y2="5.23875" layer="21" rot="R180"/>
<rectangle x1="-8.40105" y1="5.22605" x2="-8.04545" y2="5.23875" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.23875" x2="-3.20675" y2="5.25145" layer="21" rot="R180"/>
<rectangle x1="-8.38835" y1="5.23875" x2="-8.04545" y2="5.25145" layer="21" rot="R180"/>
<rectangle x1="-3.51155" y1="5.25145" x2="-3.21945" y2="5.26415" layer="21" rot="R180"/>
<rectangle x1="-8.37565" y1="5.25145" x2="-8.04545" y2="5.26415" layer="21" rot="R180"/>
<rectangle x1="-3.49885" y1="5.26415" x2="-3.23215" y2="5.27685" layer="21" rot="R180"/>
<rectangle x1="-8.36295" y1="5.26415" x2="-8.04545" y2="5.27685" layer="21" rot="R180"/>
<rectangle x1="-3.49885" y1="5.27685" x2="-3.24485" y2="5.28955" layer="21" rot="R180"/>
<rectangle x1="-8.35025" y1="5.27685" x2="-8.05815" y2="5.28955" layer="21" rot="R180"/>
<rectangle x1="-3.48615" y1="5.28955" x2="-3.25755" y2="5.30225" layer="21" rot="R180"/>
<rectangle x1="-8.33755" y1="5.28955" x2="-8.05815" y2="5.30225" layer="21" rot="R180"/>
<rectangle x1="-3.48615" y1="5.30225" x2="-3.27025" y2="5.31495" layer="21" rot="R180"/>
<rectangle x1="-8.32485" y1="5.30225" x2="-8.05815" y2="5.31495" layer="21" rot="R180"/>
<rectangle x1="-3.47345" y1="5.31495" x2="-3.28295" y2="5.32765" layer="21" rot="R180"/>
<rectangle x1="-8.31215" y1="5.31495" x2="-8.07085" y2="5.32765" layer="21" rot="R180"/>
<rectangle x1="-3.47345" y1="5.32765" x2="-3.30835" y2="5.34035" layer="21" rot="R180"/>
<rectangle x1="-8.28675" y1="5.32765" x2="-8.07085" y2="5.34035" layer="21" rot="R180"/>
<rectangle x1="-3.44805" y1="5.34035" x2="-3.33375" y2="5.35305" layer="21" rot="R180"/>
<rectangle x1="-8.27405" y1="5.34035" x2="-8.08355" y2="5.35305" layer="21" rot="R180"/>
<rectangle x1="-3.42265" y1="5.35305" x2="-3.37185" y2="5.36575" layer="21" rot="R180"/>
<rectangle x1="-8.26135" y1="5.35305" x2="-8.09625" y2="5.36575" layer="21" rot="R180"/>
<rectangle x1="-8.23595" y1="5.36575" x2="-8.10895" y2="5.37845" layer="21" rot="R180"/>
<rectangle x1="-8.19785" y1="5.37845" x2="-8.13435" y2="5.39115" layer="21" rot="R180"/>
<text x="-2.8956" y="-0.9652" size="0.8128" layer="22" font="vector" rot="MR0">acl.mit.edu</text>
</package>
</packages>
<symbols>
<symbol name="LETTER_L">
<wire x1="0" y1="185.42" x2="248.92" y2="185.42" width="0.4064" layer="94"/>
<wire x1="248.92" y1="185.42" x2="248.92" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="185.42" x2="0" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="0" x2="248.92" y2="0" width="0.4064" layer="94"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.254" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94" font="vector">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94" font="vector">REV:</text>
<text x="1.524" y="17.78" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="15.494" y="17.78" size="2.7432" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="2.54" y="31.75" size="1.9304" layer="94">Developed by the Aerospace Controls Lab</text>
<text x="2.54" y="27.94" size="1.9304" layer="94">Massachusetts Institute of Technology</text>
<text x="2.54" y="24.13" size="1.9304" layer="94">http://acl.mit.edu</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Design by:</text>
</symbol>
<symbol name="M10">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="1" x="-12.7" y="5.08" length="middle"/>
<pin name="2" x="-12.7" y="2.54" length="middle"/>
<pin name="3" x="-12.7" y="0" length="middle"/>
<pin name="4" x="-12.7" y="-2.54" length="middle"/>
<pin name="5" x="-12.7" y="-5.08" length="middle"/>
<pin name="10" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="9" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="8" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="7" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="6" x="12.7" y="-5.08" length="middle" rot="R180"/>
<text x="-7.62" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.778" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="ACL-LOGO">
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME-LETTER" prefix="FRAME">
<description>&lt;b&gt;Schematic Frame&lt;/b&gt;&lt;p&gt;
Standard 8.5x11 US Letter frame</description>
<gates>
<gate name="G$1" symbol="LETTER_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="147.32" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M10">
<gates>
<gate name="G$1" symbol="M10" x="0" y="0"/>
</gates>
<devices>
<device name="RECEPTACLE" package="M10-0.4MM-SMD-RECEPTACLE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="HEADER" package="M10-0.4MM-SMD-HEADER">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ACL-LOGO">
<gates>
<gate name="G$1" symbol="ACL-LOGO" x="0" y="0"/>
</gates>
<devices>
<device name="LARGE" package="ACL-LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="ACL-LOGO-BOTTOM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find connectors and sockets- basically anything that can be plugged into or onto.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; CC v3.0 Share-Alike You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X07">
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0.635" x2="16.51" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X07_LOCK">
<wire x1="14.605" y1="1.27" x2="15.875" y2="1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="1.27" x2="16.51" y2="0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.635" x2="15.875" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="11.43" y1="0.635" x2="12.065" y2="1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="1.27" x2="13.335" y2="1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="13.335" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="13.335" y1="-1.27" x2="12.065" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="11.43" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="14.605" y1="1.27" x2="13.97" y2="0.635" width="0.2032" layer="21"/>
<wire x1="13.97" y1="-0.635" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="15.875" y1="-1.27" x2="14.605" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.2032" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.89" y1="0.635" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.795" y2="1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="1.27" x2="11.43" y2="0.635" width="0.2032" layer="21"/>
<wire x1="11.43" y1="-0.635" x2="10.795" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.89" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0.635" x2="16.51" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" diameter="1.8796" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X07_LOCK_LONGPADS">
<wire x1="1.524" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="4.064" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="6.604" y1="0" x2="6.096" y2="0" width="0.2032" layer="21"/>
<wire x1="9.144" y1="0" x2="8.636" y2="0" width="0.2032" layer="21"/>
<wire x1="11.684" y1="0" x2="11.176" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0" x2="16.256" y2="0" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0" x2="16.51" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="16.51" y1="-0.9906" x2="16.2306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0" x2="16.51" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0.9906" x2="16.2306" y2="1.27" width="0.2032" layer="21"/>
<wire x1="14.224" y1="0" x2="13.716" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="-0.127" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0.127" drill="1.016" shape="long" rot="R90"/>
<text x="-1.27" y="1.905" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<rectangle x1="4.7879" y1="-0.2921" x2="5.3721" y2="0.2921" layer="51"/>
<rectangle x1="7.3279" y1="-0.2921" x2="7.9121" y2="0.2921" layer="51" rot="R90"/>
<rectangle x1="9.8679" y1="-0.2921" x2="10.4521" y2="0.2921" layer="51"/>
<rectangle x1="12.4079" y1="-0.2921" x2="12.9921" y2="0.2921" layer="51"/>
<rectangle x1="14.9479" y1="-0.2921" x2="15.5321" y2="0.2921" layer="51"/>
</package>
<package name="1X07_LONGPADS">
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="16.51" y1="0.635" x2="16.51" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" shape="long" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="1.016" diameter="1.8796" shape="long" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="1.016" diameter="1.8796" shape="long" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="1.016" diameter="1.8796" shape="long" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="1.016" diameter="1.8796" shape="long" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="1.016" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.3462" y="2.4638" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="14.986" y1="-0.254" x2="15.494" y2="0.254" layer="51"/>
<rectangle x1="12.446" y1="-0.254" x2="12.954" y2="0.254" layer="51"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<hole x="0" y="0" drill="1.016"/>
<hole x="2.54" y="0" drill="1.016"/>
<hole x="5.08" y="0" drill="1.016"/>
<hole x="7.62" y="0" drill="1.016"/>
<hole x="10.16" y="0" drill="1.016"/>
<hole x="12.7" y="0" drill="1.016"/>
<hole x="15.24" y="0" drill="1.016"/>
</package>
<package name="1X07_HOLES_ONLY">
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="5.08" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="7.62" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="10.16" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="12.7" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="15.24" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="3" x="5.08" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="4" x="7.62" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="5" x="10.16" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="6" x="12.7" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="7" x="15.24" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<hole x="5.08" y="0" drill="1.4732"/>
<hole x="7.62" y="0" drill="1.4732"/>
<hole x="10.16" y="0" drill="1.4732"/>
<hole x="12.7" y="0" drill="1.4732"/>
<hole x="15.24" y="0" drill="1.4732"/>
</package>
</packages>
<symbols>
<symbol name="M07">
<wire x1="1.27" y1="-7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="12.7" x2="1.27" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="10.16" x2="0" y2="10.16" width="0.6096" layer="94"/>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="13.462" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="7" x="5.08" y="10.16" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M07" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Header 7&lt;/b&gt;
Standard 7-pin 0.1" header. Use with straight break away headers (SKU : PRT-00116), right angle break away headers (PRT-00553), swiss pins (PRT-00743), machine pins (PRT-00117), and female headers (PRT-00115).  NOTES ON THE VARIANTS LOCK and LOCK_LONGPADS... This footprint was designed to help hold the alignment of a through-hole component (i.e. 7-pin header) while soldering it into place. You may notice that each hole has been shifted either up or down by 0.005 of an inch from it's more standard position (which is usually a perfectly straight line). This slight alteration caused the pins (the squares in the middle) to touch the edges of the holes. Because they are alternating, it causes a "brace" to hold the header in place. 0.005 has proven to be the perfect amount of "off-center" position when using our standard breakaway headers. Although looks a little odd when you look at the bare footprint, once you have a header in there, the alteration is very hard to notice. Also, if you push a header all the way into place, it is covered up entirely on the bottom side. This idea of altering the position of holes to aid alignment will be further integrated into the Sparkfun Library for other footprints. It can help hold any component with 3 or more connection pins.</description>
<gates>
<gate name="G$1" symbol="M07" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X07">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK" package="1X07_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X07_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LONGPADS" package="1X07_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGOPINS_HOLES_ONLY" package="1X07_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="ACL" deviceset="FRAME-LETTER" device=""/>
<part name="U$1" library="ACL" deviceset="M10" device="HEADER"/>
<part name="JP1" library="SparkFun-Connectors" deviceset="M07" device=""/>
<part name="JP2" library="SparkFun-Connectors" deviceset="M07" device=""/>
<part name="U$2" library="ACL" deviceset="ACL-LOGO" device="LARGE"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="FRAME1" gate="G$2" x="147.32" y="0"/>
<instance part="U$1" gate="G$1" x="86.36" y="114.3"/>
<instance part="JP1" gate="G$1" x="137.16" y="124.46"/>
<instance part="JP2" gate="G$1" x="167.64" y="124.46"/>
<instance part="U$2" gate="G$1" x="71.12" y="139.7"/>
</instances>
<busses>
</busses>
<nets>
<net name="TMS" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="7"/>
<wire x1="149.86" y1="134.62" x2="142.24" y2="134.62" width="0.1524" layer="91"/>
<label x="142.24" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="4"/>
<wire x1="66.04" y1="111.76" x2="73.66" y2="111.76" width="0.1524" layer="91"/>
<label x="66.04" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="TDI" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="6"/>
<wire x1="149.86" y1="132.08" x2="142.24" y2="132.08" width="0.1524" layer="91"/>
<label x="142.24" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="5"/>
<wire x1="66.04" y1="109.22" x2="73.66" y2="109.22" width="0.1524" layer="91"/>
<label x="66.04" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="5"/>
<wire x1="149.86" y1="129.54" x2="142.24" y2="129.54" width="0.1524" layer="91"/>
<label x="142.24" y="129.54" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="1"/>
<wire x1="66.04" y1="119.38" x2="73.66" y2="119.38" width="0.1524" layer="91"/>
<label x="66.04" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="TDO" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="4"/>
<wire x1="149.86" y1="127" x2="142.24" y2="127" width="0.1524" layer="91"/>
<label x="142.24" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="8"/>
<wire x1="106.68" y1="114.3" x2="99.06" y2="114.3" width="0.1524" layer="91"/>
<label x="99.06" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="TCK" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="3"/>
<wire x1="149.86" y1="124.46" x2="142.24" y2="124.46" width="0.1524" layer="91"/>
<label x="142.24" y="124.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="2"/>
<wire x1="149.86" y1="121.92" x2="142.24" y2="121.92" width="0.1524" layer="91"/>
<label x="142.24" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="10"/>
<wire x1="106.68" y1="119.38" x2="99.06" y2="119.38" width="0.1524" layer="91"/>
<label x="99.06" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="EMU0" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="1"/>
<wire x1="149.86" y1="119.38" x2="142.24" y2="119.38" width="0.1524" layer="91"/>
<label x="142.24" y="119.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="9"/>
<wire x1="106.68" y1="116.84" x2="99.06" y2="116.84" width="0.1524" layer="91"/>
<label x="99.06" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="TRSTN" class="0">
<segment>
<pinref part="JP2" gate="G$1" pin="7"/>
<wire x1="180.34" y1="134.62" x2="172.72" y2="134.62" width="0.1524" layer="91"/>
<label x="172.72" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="7"/>
<wire x1="106.68" y1="111.76" x2="99.06" y2="111.76" width="0.1524" layer="91"/>
<label x="99.06" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="JP2" gate="G$1" pin="6"/>
<wire x1="180.34" y1="132.08" x2="172.72" y2="132.08" width="0.1524" layer="91"/>
<label x="172.72" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="4"/>
<wire x1="180.34" y1="127" x2="172.72" y2="127" width="0.1524" layer="91"/>
<label x="172.72" y="127" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="3"/>
<wire x1="180.34" y1="124.46" x2="172.72" y2="124.46" width="0.1524" layer="91"/>
<label x="172.72" y="124.46" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="2"/>
<wire x1="180.34" y1="121.92" x2="172.72" y2="121.92" width="0.1524" layer="91"/>
<label x="172.72" y="121.92" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="2"/>
<wire x1="66.04" y1="116.84" x2="73.66" y2="116.84" width="0.1524" layer="91"/>
<label x="66.04" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="EMU1" class="0">
<segment>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="180.34" y1="119.38" x2="172.72" y2="119.38" width="0.1524" layer="91"/>
<label x="172.72" y="119.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="3"/>
<wire x1="66.04" y1="114.3" x2="73.66" y2="114.3" width="0.1524" layer="91"/>
<label x="66.04" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
