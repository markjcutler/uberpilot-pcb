For v1.1:

1) Move the big power cap as close to the PIC as possible - DONE
2) Replace xbee connector with smaller one from Sparkfun - DONE
3) Make all small silkscreens bigger - DONE

5) Add 10k resistor between 3.3v and MCLR - DONE
6) Add extra 5v / gnd pair of pins for battery - DONE
7) 1uF capictor is the wrong size - DONE
8) Move the oscillator closer to the PIC and surround it with ground plane - DONE
9) Add silkscreen to denote pic3 orientation - DONE
10) Check date on back of board - DONE
11) Redo power supply - new caps and don't need 5V reg on bottom board for UP - DONE

For v1.2:
1) Bring out a couple debugging pins - DONE
2) Bring out other i2c bus - POSTPONE TO v1.3
3) Put place for accelerometer and magnetometer - POSTPONE TO v1.3
4) Update BOM - DONE
5) Place linear power regulator in the airstream if possible (determine if this is necessary or not) - POSTPONE TO v1.3
6) Ensure ground plane exists below osc. and gyro. - DONE
7) Change 1.0uF cap to 0603 package - DONE
8) Test if you can cut power to pic while powering esc's -- should be fine because of reset switch on UDB4 -- if works, add small switch to u.p. - DONE
9) Replace power switch on bottom with momentary reset switch on top? - NO, JUST POWER SWITCH
10) Switch power switch direction - N/A
11) move voltage divider to top board - DONE
12) maybe make a microQuad-specific version of this board. - DONE
	12)a) Avoid ground loops in the quad by only connecting the signal wires of the escs to the board - DONE
13) increase thickness of xbee silkscreen for directional purposes - DONE


For v1.3
1) Bring out other i2c bus - DONE
2) Put place for accelerometer and magnetometer - DONE
3) Place linear power regulator somewhere not next to xbee - DONE
4) get smaller resistors for leds (180?) - DONE
5) Fix silkscreen - DONE
6) Low ESR (< 200 milli-ohms) on 4.7uF capacitor on magnetometer - DONE
7) Update BOM - DONE
8) Move MAG to better locaion - DONE
9) Put switch after 3.3V power regulator - DONE
10) bigger battery terminal for microquads - DONE
11) check 5-pin connector and pressure sensor pads for size - DONE
12) maximize width of traces - DONE
13) verify date, "airwires", silkscreen - DONE


For v1.4
1) fix x-y silkscreen to aggree with new orientation - DONE
2) add pwm silkscreen to top board - DONE
3) pin out i2c1 for debugging - DONE

For v1.5
1) Add more decoupling caps near accel (10uF, maby 10 and 100uF electrolitic) - DONE
2) Add jumper for choosing battery voltage input - DONE
3) Pick resistor value properly for LEDs driven directly by GPIOs - DONE, 330
4) Consder using transistors to drive LEDs instead - Not now, too complicated
5) Add reset switch - DONE
6) Add power switch on battery if room - DONE, not room
7) Add 0.1" jumper on MCLR cap - Not now -- add if needed
8) Clean up silkscreen - DONE
9) Removable power connections on power dist board - DONE
10) Minimize via's if using OSHPark - DONE, not needed
11) power on input compare - DONE
12) update silkscreen date
13) add cap and voltage divider, decrease resistances - DONE
14) add eeprom - DONE
15) add rpm sensor headers - DONE
16) check BOM
17) check silkscreen on pads - DONE

For v1.5.1
1) Fix silkscreen showing UART2 as UART1 - DONE

For v1.6
1) Replace accel and gyro with MPU6000

For v2.1
1) find new programming connector that doesn't interfer with xbee - DONE
2) ensure programming connections are consistent - DONE
3) change silkscreen to reflect move from 2.2uF to 0.1uF caps around the chip, and other changes (EMU resistors) - DONE
4) check all the pin definitions in the manual - DONE

For v2.2
1) new pin to EEPROM_WP, current pin needed for adc
2) Replace programming connector
3) 1 uF cap and 8.2 pF cap near VCC to xbee
4) make room for bolt/nuts at mounting locations
5) More decoupling caps near accel (10uF, maybe 10 and 100uF electrolitic)



For v3.0
1) add http://www.digikey.com/product-detail/en/LTC3405AES6%23TR/LTC3405AES6TR-ND/480039 for using a single cell lipo
