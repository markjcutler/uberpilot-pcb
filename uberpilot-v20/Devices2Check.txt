

todo:
1 - DONE - check cpu heat sink pad dimensions
2 - DONE - check for silkscreen on pads
3 - try removing r1,r3,r4 on dev board and see if things still work
4 - DONE - add pins for i2c from c28
5 - test c28 i2c
6 - test c28 adc
7 - DONE - add resistor and cap for adc, tie adcinHI and LOW together
8 - make traces as large as possible
9 - DONE - make all the 4 pin plugs connsistent with ground and power